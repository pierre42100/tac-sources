package com.ingroupe.verify.anticovid.ui.result

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseExpandableListAdapter
import android.widget.ImageView
import android.widget.TextView
import com.ingroupe.verify.anticovid.BuildConfig
import com.ingroupe.verify.anticovid.R
import com.ingroupe.verify.anticovid.service.document.model.DocumentEngineResult
import com.ingroupe.verify.anticovid.service.document.model.DocumentRuleResult
import java.util.*

class ResultExpandableListAdapter internal constructor(
    private val context: Context,
    private val itemsList: List<DocumentEngineResult>
) : BaseExpandableListAdapter() {

    override fun getChild(groupPosition: Int, childPosition: Int): DocumentRuleResult? {
        return this.itemsList[groupPosition].listRules?.filter { !it.isValid }?.get(childPosition)
    }

    override fun getChildId(groupPosition: Int, childPosition: Int): Long {
        return childPosition.toLong()
    }

    override fun getChildView(
        groupPosition: Int,
        childPosition: Int,
        isLastChild: Boolean,
        view: View?,
        parent: ViewGroup
    ): View {
        var convertView = view
        val child = getChild(groupPosition, childPosition)
        if (convertView == null) {
            val layoutInflater =
                this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            convertView = layoutInflater.inflate(R.layout.result_expandable_item_cell, null)
        }

        val tvRuleDetail = convertView!!.findViewById<TextView>(R.id.textview_rule_detail)

        val group = getGroup(groupPosition)
        if(group.infoMessage != null) {
            val tvRule = convertView.findViewById<TextView>(R.id.textview_rule)
            tvRule.visibility = View.GONE
            tvRuleDetail.text = group.infoMessage
        } else if (child?.isValid == false) {
            if(BuildConfig.WITH_CHECK_HELP) {
                (child.rule.identifier + " / " + child.rule.getDescriptionFor(Locale.getDefault().language)).also {
                    tvRuleDetail.text = it
                }
            } else {
                tvRuleDetail.text = child.rule.getDescriptionFor(Locale.getDefault().language)
            }
        }

        return convertView
    }

    override fun getChildrenCount(listPosition: Int): Int {
        return if(this.itemsList[listPosition].infoMessage != null) 1
        else this.itemsList[listPosition].listRules?.filter { !it.isValid }?.size ?: 0
    }

    override fun getGroup(groupPosition: Int): DocumentEngineResult {
        return this.itemsList[groupPosition]
    }

    override fun getGroupCount(): Int {
        return this.itemsList.size
    }

    override fun getGroupId(listPosition: Int): Long {
        return listPosition.toLong()
    }

    override fun getGroupView(
        groupPosition: Int,
        isExpanded: Boolean,
        view: View?,
        parent: ViewGroup
    ): View {
        var convertView = view
        val group = getGroup(groupPosition)
        if (convertView == null) {
            val layoutInflater =
                this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            convertView = layoutInflater.inflate(R.layout.result_expandable_group_cell, null)
        }

        convertView?.let {
            val listTitleTextView = it.findViewById<TextView>(R.id.expandedList_group_title)
            val ivValidity = it.findViewById<ImageView>(R.id.imageView_validity)
            val ivIndicator = it.findViewById<ImageView>(R.id.imageView_group_indicator)
            ivIndicator.visibility = View.GONE

            when {
                group.isCheckedByEngine() -> {
                    if (group.isValidatedByEngine()) {
                        ivValidity.setImageResource(R.drawable.ic_checkmark)
                        ivValidity.visibility = View.VISIBLE
                    } else {
                        if (isExpanded) {
                            ivIndicator.setImageResource(R.drawable.ic_group_up)
                        } else {
                            ivIndicator.setImageResource(R.drawable.ic_group_down)
                        }
                        ivIndicator.visibility = View.VISIBLE
                        ivValidity.setImageResource(R.drawable.ic_invalid)
                        ivValidity.visibility = View.VISIBLE
                    }
                    listTitleTextView.text = "${group.zoneLabel}"
                }
                group.checkColorCountries -> {
                    ivValidity.visibility = View.INVISIBLE
                    listTitleTextView.text = context.getString(R.string.result_country_check_color, group.zoneLabel)
                }
                group.noIcon -> {
                    ivValidity.visibility = View.GONE
                    listTitleTextView.text = group.zoneLabel
                }
                else -> {
                    ivValidity.setImageResource(R.drawable.rectangle)
                    ivValidity.visibility = View.VISIBLE
                    listTitleTextView.text = group.zoneLabel
                    if (isExpanded) {
                        ivIndicator.setImageResource(R.drawable.ic_group_up)
                    } else {
                        ivIndicator.setImageResource(R.drawable.ic_group_down)
                    }
                    ivIndicator.visibility = View.VISIBLE
                }
            }
        }

        return convertView!!
    }

    override fun hasStableIds(): Boolean {
        return false
    }

    override fun isChildSelectable(listPosition: Int, expandedListPosition: Int): Boolean {
        return true
    }
}