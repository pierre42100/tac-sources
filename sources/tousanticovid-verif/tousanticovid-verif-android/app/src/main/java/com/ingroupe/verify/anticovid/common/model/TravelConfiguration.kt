package com.ingroupe.verify.anticovid.common.model

import com.ingroupe.verify.anticovid.common.Constants
import java.time.ZoneId
import java.time.ZoneOffset
import java.time.ZonedDateTime

data class TravelConfiguration(
    var travelType: Constants.TravelType,
    var controlZone: String, //Namecode
    var favoritesList: Set<String>? = null
) {
    var isCustomDate = false
    var timeUTC: ZonedDateTime? = null
        get() {
            return if (field == null) {
                val utcZoneId: ZoneId = ZoneId.ofOffset("", ZoneOffset.UTC).normalized()
                ZonedDateTime.now().withZoneSameInstant(utcZoneId)
            } else {
                field
            }
        }
        set(newDate) {
            isCustomDate = newDate != null
            field = newDate
        }
}