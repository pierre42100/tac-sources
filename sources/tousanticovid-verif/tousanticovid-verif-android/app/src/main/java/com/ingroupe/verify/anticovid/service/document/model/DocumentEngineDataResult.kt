package com.ingroupe.verify.anticovid.service.document.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class DocumentEngineDataResult(
    @SerializedName("engineResults")
    val engineResults: MutableList<DocumentEngineResult>,
    @SerializedName("alertMessage")
    var alertMessage: String? = null
) : Serializable