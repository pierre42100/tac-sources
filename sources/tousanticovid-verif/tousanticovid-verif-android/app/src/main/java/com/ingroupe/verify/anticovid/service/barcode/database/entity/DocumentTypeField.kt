package com.ingroupe.verify.anticovid.service.barcode.database.entity

import com.google.gson.annotations.SerializedName
import com.ingroupe.verify.anticovid.service.barcode.enums.OperationEnum

class DocumentTypeField (
    name: String,
    label: String?,
    regex: String?,
    operations: List<OperationEnum>?,
    @SerializedName("min")
    val min: Int? = null,
    @SerializedName("max")
    val max: Int? = null

    ): Field(name, label, regex, operations) {

    companion object {
        fun isValidDocumentField(
            documentTypeField: DocumentTypeField
        ): Boolean {
            if (isValidField(documentTypeField) && documentTypeField.min != null && documentTypeField.max != null) {
                return true
            }
            return false
        }
    }

}