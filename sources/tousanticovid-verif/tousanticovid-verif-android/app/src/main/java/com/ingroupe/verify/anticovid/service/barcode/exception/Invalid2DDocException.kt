package com.ingroupe.verify.anticovid.service.barcode.exception

class Invalid2DDocException(message: String, encoded2DDoc: String?) :
    Exception(message.replace("{}", encoded2DDoc ?: "")) {
    companion object {
        private const val serialVersionUID = 5536155474756459241L
    }
}