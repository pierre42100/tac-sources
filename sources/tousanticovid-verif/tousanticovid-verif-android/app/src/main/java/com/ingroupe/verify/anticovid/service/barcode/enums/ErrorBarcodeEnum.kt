package com.ingroupe.verify.anticovid.service.barcode.enums

enum class ErrorBarcodeEnum(val message: String) {
    ERR01("2D-Doc '{}' is not valid"),
    ERR02("Header version '{}' not found"),
    ERR03("Document type '{}' not found"),
    ERR04("Cannot convert date '{}'"),
    ERR05("Cannot get country '{}'"),
    ERR06("Certificate '{}' not found"),
    ERR07("Invalid format signature '{}'"),
    ERR08("Cannot verify signature '{}'"),
    ERR09("Check signature '{}' KO"),
    ERR10("Cannot get medical result '{}'"),
    INF01("Verify signature '{}'");

    companion object {
        const val CANNOT_READ_FULL_MESSAGE = "Une partie du message n'a pas pu être lue"
    }
}