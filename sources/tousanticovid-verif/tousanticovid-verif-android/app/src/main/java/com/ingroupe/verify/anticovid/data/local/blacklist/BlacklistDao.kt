package com.ingroupe.verify.anticovid.data.local.blacklist

import androidx.room.*

@Dao
abstract class BlacklistDao {

    @Transaction
    @Query("SELECT * FROM blacklist WHERE :type = type")
    abstract fun getAllByType(type: String): List<BlacklistLocal>

    @Transaction
    @Query("SELECT * FROM blacklist WHERE :type = type AND :hash = hash")
    abstract fun getHash(type: String, hash: String): List<BlacklistLocal>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    abstract fun insertBlacklist(vararg blacklists: BlacklistLocal)

    @Transaction
    @Query("SELECT * FROM blacklist WHERE :type = type ORDER BY id DESC LIMIT 1")
    abstract fun getLastBlacklist(type: String): List<BlacklistLocal>

    @Query("DELETE FROM blacklist WHERE :type = type AND id >= :id")
    abstract fun deleteFromId(type: String, id: Int)
}