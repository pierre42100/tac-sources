package com.ingroupe.verify.anticovid

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.SpannableString
import android.text.style.UnderlineSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import com.ingroupe.verify.anticovid.adapter.ContactAdapter
import com.ingroupe.verify.anticovid.common.FeatureChildFragment
import com.ingroupe.verify.anticovid.common.SharedViewModel
import com.ingroupe.verify.anticovid.common.Utils
import com.ingroupe.verify.anticovid.databinding.InformationMainBinding


class InformationChildFragment : FeatureChildFragment() {
    override fun getTitle(): String = "Informations et confidentialités"
    override fun getTitleId(): Int = R.string.title_information

    companion object {
        const val TAG = "information"
        fun newInstance() = InformationChildFragment()
    }
    private var _binding: InformationMainBinding? = null
    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    private lateinit var recyclerView: androidx.recyclerview.widget.RecyclerView
    private lateinit var viewAdapter: androidx.recyclerview.widget.RecyclerView.Adapter<*>
    private lateinit var viewManager: androidx.recyclerview.widget.RecyclerView.LayoutManager

    private lateinit var model: SharedViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        super.onCreateView(inflater, container, savedInstanceState)
        _binding = InformationMainBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        model = activity?.run {
            ViewModelProvider(this).get(SharedViewModel::class.java)
        } ?: throw Exception("Invalid Activity")

        model.configuration?.contacts?.let { contacts ->

            if(contacts.isEmpty()) {
                binding.textViewContactTitle.visibility = View.GONE
            }

            viewManager = androidx.recyclerview.widget.LinearLayoutManager(activity)

            viewAdapter = ContactAdapter(contacts)


            recyclerView = binding.recyclerViewContact.apply {
                // use this setting to improve performance if you know that changes
                // in content do not change the tutorial_skip_view size of the RecyclerView
                setHasFixedSize(true)

                // use a linear tutorial_skip_view manager
                layoutManager = viewManager

                // specify an viewAdapter (see also next example)
                adapter = viewAdapter
            }
        }

        model.configuration?.confAbout?.let { conf ->
            binding.textViewLinkPolConf.setOnClickListener {
                val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(conf.privacyPolicy))
                startActivity(browserIntent)
            }
            binding.textViewLinkCgu.setOnClickListener {
                val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(conf.cgu))
                startActivity(browserIntent)
            }
        }

        val contentPolConf = SpannableString(binding.textViewLinkPolConf.text)
        contentPolConf.setSpan(UnderlineSpan(), 0, contentPolConf.length, 0)
        binding.textViewLinkPolConf.text = contentPolConf

        val contentCgu = SpannableString(binding.textViewLinkCgu.text)
        contentCgu.setSpan(UnderlineSpan(), 0, contentCgu.length, 0)
        binding.textViewLinkCgu.text = contentCgu

        context?.let { c ->
            Utils.addLinktoTextView(binding.textViewLegal2, R.string.info_legal_2, R.string.info_legal_2_link, c)
            Utils.addLinktoTextView(binding.textViewLegal4, R.string.info_legal_4, R.string.info_legal_4_link, c)
        }

    }

    override fun showNavigation(): MainActivity.NavigationIcon {
        return MainActivity.NavigationIcon.BACK
    }
}