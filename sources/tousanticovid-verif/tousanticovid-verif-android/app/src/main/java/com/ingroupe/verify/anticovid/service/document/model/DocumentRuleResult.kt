package com.ingroupe.verify.anticovid.service.document.model

import dgca.verifier.app.engine.data.Rule

class DocumentRuleResult (
    var rule: Rule,
    var isValid: Boolean)