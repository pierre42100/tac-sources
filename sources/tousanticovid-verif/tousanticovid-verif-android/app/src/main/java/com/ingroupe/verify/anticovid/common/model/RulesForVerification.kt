package com.ingroupe.verify.anticovid.common.model

import dgca.verifier.app.engine.data.Rule

class RulesForVerification {

    val listRulesFavorite : MutableList<RulesFavorite> = mutableListOf()

    var greenRules: List<Rule> = listOf()
    var orangeRules: List<Rule> = listOf()
    var redRules: List<Rule> = listOf()
    var arrivalCountryCode: String = ""

    var showAlert = false
}