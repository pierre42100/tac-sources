package com.ingroupe.verify.anticovid.ui.tutorialscan2ddoc

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.ingroupe.verify.anticovid.R
import com.ingroupe.verify.anticovid.databinding.TutorialGenericLayoutBinding

class GenericTutorialFragment : Fragment() {

    companion object {
        const val TAG = "GenericTutorialFragment"

        private const val KEY_TITLE = "KEY_TITLE"
        private const val KEY_DESCRIPTION = "KEY_DESCRIPTION"
        private const val KEY_DRAWABLE = "KEY_DRAWABLE"

        fun newInstance(tutoTitle: String, tutoDescription: String, tutoDrawable: Int): GenericTutorialFragment {
            val data = Bundle()
            data.putString(KEY_TITLE, tutoTitle)
            data.putString(KEY_DESCRIPTION, tutoDescription)
            data.putInt(KEY_DRAWABLE, tutoDrawable)
            return GenericTutorialFragment().apply{
                arguments = data
            }
        }
    }

    private var _binding: TutorialGenericLayoutBinding? = null

    private val binding get() = _binding!!

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        super.onCreateView(inflater, container, savedInstanceState)
        _binding = TutorialGenericLayoutBinding.inflate(inflater, container, false)
        val view = binding.root

        binding.imageviewTuto.setImageResource(arguments?.getInt(KEY_DRAWABLE) ?: R.drawable.ic_icon_2ddoc)
        binding.textViewTutoTitle.text = arguments?.getString(KEY_TITLE) ?: ""
        binding.textViewTutoDescription.text = arguments?.getString(KEY_DESCRIPTION) ?: ""

        return view
    }
}