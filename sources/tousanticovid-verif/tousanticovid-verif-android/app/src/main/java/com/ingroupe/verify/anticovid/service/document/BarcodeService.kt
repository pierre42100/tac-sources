package com.ingroupe.verify.anticovid.service.document

import com.ingroupe.verify.anticovid.common.Constants
import com.ingroupe.verify.anticovid.synchronization.SynchronisationUtils

abstract class BarcodeService {

    companion object {

        fun isValuePresent(field: String, value: String, slEnum: Constants.SpecificList, mappedDynamicData: MutableMap<String, String>): Boolean {
            val label = SynchronisationUtils.getLabelSpecificList(slEnum, value)
            return when {
                label != null -> {
                    mappedDynamicData[field] = label
                    true
                }
                mappedDynamicData[field].isNullOrEmpty() -> {
                    mappedDynamicData[field] = value
                    false
                }
                else -> {
                    false
                }
            }
        }

        fun getVV(svEnum : Constants.ValidityValues, mode: Constants.ControlMode): Int {
            return SynchronisationUtils.getValidityValue(svEnum, mode)
        }

        fun getVVS(svEnum : Constants.ValidityValuesString, mode: Constants.ControlMode): String {
            return SynchronisationUtils.getValidityStringValue(svEnum, mode)
        }
    }
}