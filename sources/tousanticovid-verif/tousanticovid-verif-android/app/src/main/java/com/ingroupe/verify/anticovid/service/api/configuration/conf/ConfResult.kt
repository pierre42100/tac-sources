package com.ingroupe.verify.anticovid.service.api.configuration.conf

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class ConfResult(
    @SerializedName("types")
    var types: ArrayList<ConfType>? = null,
    @SerializedName("contacts")
    var contacts: ArrayList<ConfContact>? = null,
    @SerializedName("statics")
    var confStatic: ArrayList<ConfStatic>? = null,
    @SerializedName("about")
    var confAbout: ConfAbout? = null
) : Serializable