package com.ingroupe.verify.anticovid.data.local.certificates

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Transaction

@Dao
abstract class CertificatesDao {
    @Query("SELECT * from certificates")
    abstract fun getAll(): List<CertificateDcc>?

    @Query("SELECT * from certificates WHERE :key = `key` ORDER BY id DESC LIMIT 1")
    abstract fun getCertificateWithKey(key: String): CertificateDcc?

    @Insert
    abstract fun insertAll(vararg certificates: CertificateDcc)

    @Transaction
    open fun deleteAllAndInsert(vararg certificates: CertificateDcc) {
        deleteAll()
        insertAll(*certificates)
    }

    @Query("DELETE FROM certificates")
    abstract fun deleteAll()
}