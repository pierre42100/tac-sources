package com.ingroupe.verify.anticovid.service.api.configuration.sync

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class SyncResult (
    @SerializedName("period")
    var period: SyncPeriod? = null,
    @SerializedName("specificValues")
    var specificValues: SyncSpecificValues? = null,
    @SerializedName("certificates2DDoc")
    var certificates2DDoc: Map<String, String>? = null,
    @SerializedName("certificatesDCC")
    var certificatesDCC: Map<String, String>? = null,
    @SerializedName("currentDate")
    var currentDate: String? = null
): Serializable
