/*
 *  ---license-start
 *  eu-digital-green-certificates / dgca-verifier-app-android
 *  ---
 *  Copyright (C) 2021 T-Systems International GmbH and all other contributors
 *  ---
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  ---license-end
 *
 *  Created by osarapulov on 6/25/21 9:21 AM
 */

package com.ingroupe.verify.anticovid.service.api.configuration.engine

import com.fasterxml.jackson.databind.ObjectMapper
import com.ingroupe.verify.anticovid.service.api.configuration.engine.valuesets.ValueSetResult
import dgca.verifier.app.engine.data.ValueSet
import java.time.LocalDate

fun ValueSetResult.toValueSet(): ValueSet = ValueSet(
    valueSetId = this.valueSetId,
    valueSetDate = LocalDate.parse(this.valueSetDate),
    valueSetValues = ObjectMapper().readTree(this.valueSetValues.toString())
)

fun List<ValueSetResult>.toValueSets(): List<ValueSet> {
    val valueSets = mutableListOf<ValueSet>()
    for (i in this.indices) {
        val valueSetResult = this[i]
        valueSets.add(valueSetResult.toValueSet())
    }
    return valueSets
}