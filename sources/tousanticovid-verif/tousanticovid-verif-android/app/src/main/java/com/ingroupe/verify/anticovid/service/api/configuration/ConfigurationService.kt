package com.ingroupe.verify.anticovid.service.api.configuration


import com.ingroupe.verify.anticovid.service.api.configuration.blacklist.BlacklistResult
import com.ingroupe.verify.anticovid.service.api.configuration.conf.ConfResult
import com.ingroupe.verify.anticovid.service.api.configuration.engine.rules.RuleIdentifierResult
import com.ingroupe.verify.anticovid.service.api.configuration.engine.rules.RuleResult
import com.ingroupe.verify.anticovid.service.api.configuration.engine.valuesets.ValueSetIdentifierResult
import com.ingroupe.verify.anticovid.service.api.configuration.engine.valuesets.ValueSetResult
import com.ingroupe.verify.anticovid.service.api.configuration.sync.SyncResult
import retrofit2.Call
import retrofit2.http.*

interface ConfigurationService {

    @GET
    fun callSync(@Url url: String): Call<SyncResult>

    @POST
    fun sendAnalytics(@Url url: String, @Body analytics: List<String>): Call<Unit>

    @GET
    fun callConfiguration(@Url url: String): Call<ConfResult>

    // RULES ENGINE
    @GET
    fun getRulesIdentifiers(@Url url: String): Call<List<RuleIdentifierResult>>

    @POST
    fun getRules(@Url url: String, @Body ruleHashes: List<String>): Call<List<RuleResult>>

    @GET
    fun getValueSetsIdentifiers(@Url url: String): Call<List<ValueSetIdentifierResult>>

    @POST
    fun getValueSets(@Url url: String, @Body valuesetHashes: List<String>): Call<List<ValueSetResult>>

    @GET
    fun getCountries(@Url url: String): Call<List<String>>

    // BLACKLIST
    @GET
    fun getBlacklist(@Url url: String): Call<BlacklistResult>
}
