package com.ingroupe.verify.anticovid.ui.actionchoice

import android.content.Context
import android.content.SharedPreferences
import com.ingroupe.verify.anticovid.App
import com.ingroupe.verify.anticovid.common.Constants

class ActionChoicePresenterImpl : ActionChoicePresenter {

    companion object {
        const val TAG = "ActionChoiceP"
    }

    private var sharedPrefs: SharedPreferences? = null
        get() {
            if (field == null) {
                field = App.getContext().getSharedPreferences(Constants.COUNTRIES_KEY, Context.MODE_PRIVATE)
            }
            return field
        }

    override fun getSavedTravelType(): Constants.TravelType {
        return when (sharedPrefs?.getString(Constants.CountriesConfiguration.TRAVEL_TYPE.text, Constants.TravelType.DEPARTURE.text)) {
            Constants.TravelType.ARRIVAL.text -> Constants.TravelType.ARRIVAL
            else -> Constants.TravelType.DEPARTURE
        }
    }
}