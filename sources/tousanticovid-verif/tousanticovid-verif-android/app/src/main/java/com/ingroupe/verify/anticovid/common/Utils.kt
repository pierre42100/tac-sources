package com.ingroupe.verify.anticovid.common

import android.Manifest
import android.app.Activity
import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.content.res.Resources
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.net.Uri
import android.text.*
import android.text.Annotation
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.text.style.ForegroundColorSpan
import android.text.style.UnderlineSpan
import android.view.View
import android.view.ViewGroup
import android.widget.ExpandableListAdapter
import android.widget.ExpandableListView
import android.widget.TextView
import android.widget.Toast
import androidx.activity.result.ActivityResultLauncher
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.content.ContextCompat.startActivity
import androidx.fragment.app.FragmentManager
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.ingroupe.verify.anticovid.App
import com.ingroupe.verify.anticovid.R
import com.ingroupe.verify.anticovid.common.model.IncompleteDate
import com.ingroupe.verify.anticovid.dialog.CameraDeniedDialog
import com.ingroupe.verify.anticovid.dialog.CameraRequestDialog
import com.ingroupe.verify.anticovid.service.api.configuration.conf.ConfResult
import java.io.IOException
import java.io.InputStreamReader
import java.util.*


abstract class Utils {
    companion object {

        private var UserAskedCameraPermissionBefore = false

        private var currentTextToast: Toast? = null
        private var currentViewToast: Toast? = null


        fun showToast(activity: Activity, text: String, length : Int = Toast.LENGTH_SHORT) {
            activity.runOnUiThread {
                currentTextToast?.cancel()
                currentTextToast = Toast.makeText(activity, text, length)
                currentTextToast?.show()
            }
        }

        @Suppress("DEPRECATION")
        fun showViewToast(activity: Activity, idView: Int, idViewGroup: Int) {
            activity.runOnUiThread {
                val view = activity.layoutInflater.inflate(
                    idView,
                    activity.findViewById(idViewGroup)
                )
                currentViewToast?.cancel()
                currentViewToast = Toast(activity)
                currentViewToast?.view = view
                currentViewToast?.show()
            }
        }

        fun <T> notNull(vararg elements: T): Boolean {
            elements.forEach {
                if (it == null) {
                    return false
                }
            }
            return true
        }

        fun sortConf(resultConf: ConfResult?) {
            resultConf?.types?.forEach { confType ->
                confType.groups?.sortBy { it.orderGroup }
                confType.groups?.forEach { group ->
                    group.fields?.sortBy { it.order }
                }
            }
            resultConf?.confStatic?.sortBy { it.order }
        }

        fun requestCameraPermission(
            activity: Activity,
            childFragmentManager: FragmentManager,
            fragmentDialog: String,
            requestPermissionLauncher: ActivityResultLauncher<String>) {
            when {
                activity.shouldShowRequestPermissionRationale(Manifest.permission.CAMERA) -> {
                    CameraRequestDialog(requestPermissionLauncher).show(
                        childFragmentManager,
                        fragmentDialog
                    )
                }
                else -> {
                    if(UserAskedCameraPermissionBefore) {
                        CameraDeniedDialog().show(
                            childFragmentManager,
                            fragmentDialog
                        )
                    } else {
                        UserAskedCameraPermissionBefore = true
                        requestPermissionLauncher.launch(Manifest.permission.CAMERA)
                    }
                }
            }
        }

        fun isInternetAvailable(context: Context): Boolean {
            val connectivityManager =
                context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val networkCapabilities = connectivityManager.activeNetwork ?: return false
            val actNw =
                connectivityManager.getNetworkCapabilities(networkCapabilities) ?: return false
            //Will only check if Wifi or Network is turned on
            return actNw.hasCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET)
        }

        inline fun <reified T : Any> loadFromAsset(fileName: String): T? {
            val result: T
            try {
                val inputStream = App.getContext().assets.open(fileName)
                val isr = InputStreamReader(inputStream)

                val itemType = object : TypeToken<T>() {}.type
                result = Gson().fromJson(isr, itemType)

                inputStream.close()
                isr.close()
            } catch (ex: IOException) {
                ex.printStackTrace()
                return null
            }

            return result
        }

        fun addLinktoTextView(textView: TextView, fullTextId: Int, linkId: Int, context: Context) {
            val fullText = context.getText(fullTextId) as SpannedString
            val spannableString = SpannableString(fullText)

            val annotations = fullText.getSpans(0, fullText.length, Annotation::class.java)

            val clickableSpan = object : ClickableSpan() {
                override fun onClick(widget: View) {
                    val browserIntent =
                        Intent(Intent.ACTION_VIEW, Uri.parse(context.getString(linkId)))
                    startActivity(context, browserIntent, null)
                }

                override fun updateDrawState(ds: TextPaint) {
                    ds.isUnderlineText = false
                }
            }
            annotations?.find { it.value == "link" }?.let {
                spannableString.apply {
                    setSpan(
                        clickableSpan,
                        fullText.getSpanStart(it),
                        fullText.getSpanEnd(it),
                        Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
                    )
                    setSpan(
                        ForegroundColorSpan(
                            ContextCompat.getColor(context, R.color.in_blue)
                        ),
                        fullText.getSpanStart(it),
                        fullText.getSpanEnd(it),
                        0
                    )
                    setSpan(
                        UnderlineSpan(),
                        fullText.getSpanStart(it),
                        fullText.getSpanEnd(it),
                        0
                    )
                }
            }


            textView.apply {
                text = spannableString
                movementMethod = LinkMovementMethod.getInstance()
            }
        }

        fun getLocalizedResources(desiredLocale: Locale?): Resources {
            var conf: Configuration = App.getContext().resources.configuration
            conf = Configuration(conf)
            conf.setLocale(desiredLocale)
            val localizedContext = App.getContext().createConfigurationContext(conf)
            return localizedContext.resources
        }

        fun splitDateFromString(inputDate: String) : IncompleteDate? {
            val dateAsInteger = inputDate.toIntOrNull()
            if (dateAsInteger != null) {
                if (dateAsInteger in 1000..9999) {
                    return IncompleteDate(year = dateAsInteger.toString())
                }
            } else if (inputDate.matches(Regex("\\d{4}-\\d{2}"))) {
                val dateSplit = inputDate.split("-")
                return IncompleteDate(month = dateSplit[1], year = dateSplit[0])
            }

            return null
        }

        //In order to use ExpandableListView in ScrollViews
        fun setExpandableInitialHeight(listView: ExpandableListView, context: Context) {
            val listAdapter = listView.expandableListAdapter
            var totalHeight = 0
            val desiredWidth = View.MeasureSpec.makeMeasureSpec(
                context.resources.displayMetrics.widthPixels,
                View.MeasureSpec.EXACTLY
            )
            for (i in 0 until listAdapter.groupCount) {
                val groupView: View = listAdapter.getGroupView(i, true, null, listView)
                groupView.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED)
                totalHeight += groupView.measuredHeight
            }

            val params: ViewGroup.LayoutParams = listView.layoutParams
            params.height = (totalHeight + listView.dividerHeight * (listAdapter.groupCount - 1))
            listView.layoutParams = params
            listView.requestLayout()
        }

        //In order to use ExpandableListView in ScrollViews
        fun setExpandableViewHeight(
            listView: ExpandableListView,
            group: Int
        ) {
            val listAdapter = listView.expandableListAdapter as ExpandableListAdapter
            var totalHeight = 0
            val desiredWidth = View.MeasureSpec.makeMeasureSpec(
                listView.width,
                View.MeasureSpec.EXACTLY
            )
            for (i in 0 until listAdapter.groupCount) {
                val groupItem = listAdapter.getGroupView(i, false, null, listView)
                groupItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED)
                totalHeight += groupItem.measuredHeight
                if (listView.isGroupExpanded(i) && i != group
                    || !listView.isGroupExpanded(i) && i == group
                ) {
                    for (j in 0 until listAdapter.getChildrenCount(i)) {
                        val listItem = listAdapter.getChildView(
                            i, j, false, null,
                            listView
                        )
                        listItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED)
                        totalHeight += listItem.measuredHeight
                    }
                }
            }
            val params = listView.layoutParams
            var height = (totalHeight
                    + listView.dividerHeight * (listAdapter.groupCount - 1))
            if (height < 10) height = 200
            params.height = height
            listView.layoutParams = params
            listView.requestLayout()
        }

        fun dpsToPxs(dps: Int, context: Context) : Int {
            val scale: Float = context.resources.displayMetrics.density
            return (dps * scale + 0.5f).toInt()
        }

    }
}
