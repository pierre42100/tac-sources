package com.ingroupe.verify.anticovid.service.document.datamatrix

import android.content.Context
import com.ingroupe.verify.anticovid.service.document.model.DocumentFieldResult
import com.ingroupe.verify.anticovid.service.document.model.DocumentStatic2ddocResult
import java.time.ZonedDateTime

interface DatamatrixModeInterface {

    fun get2ddocTest(
        static2ddoc: DocumentStatic2ddocResult,
        mappedDynamicData: MutableMap<String, String>,
        context: Context,
        controlTimeOT: ZonedDateTime?
    )

    fun get2ddocVaccin(
        static2ddoc: DocumentStatic2ddocResult,
        mappedDynamicData: MutableMap<String, String>,
        context: Context,
        controlTimeOT: ZonedDateTime?
    )

    companion object {
        fun getField(
            fields: List<DocumentFieldResult>?,
            criteriaCode: String
        ): String? {
            val field = fields?.find { criteriaCode.isNotEmpty() && criteriaCode == it.name }
            return field?.value
        }
    }
}