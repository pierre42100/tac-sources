package com.ingroupe.verify.anticovid.service.document.model

import dgca.verifier.app.decoder.cbor.GreenCertificateData
import dgca.verifier.app.decoder.model.GreenCertificate
import java.time.ZonedDateTime

class DocumentStaticDccResult(
    var dccData : GreenCertificateData? = null,
    signature: DocumentSignatureResult? = null,
    var base64EncodedKid: String? = null,
    val issuedAt: ZonedDateTime,
    val expirationTime: ZonedDateTime
) : DocumentStaticResult(signature) {

    enum class DccType(val text: String) {
        DCC_TEST("DCC_TEST"),
        DCC_RECOVERY("DCC_RECOVERY"),
        DCC_VACCINATION("DCC_VACCINATION"),
        DCC_EXEMPTION("DCC_EXEMPTION"),
        DCC_ACTIVITY("DCC_ACTIVITY")
    }

    companion object {
        fun getResourceType(dcc : GreenCertificate?) : DccType? {
            return when {
                dcc?.tests != null -> DccType.DCC_TEST
                dcc?.recoveryStatements != null -> DccType.DCC_RECOVERY
                dcc?.vaccinations != null -> DccType.DCC_VACCINATION
                dcc?.exemption != null -> DccType.DCC_EXEMPTION
                dcc?.activity != null -> DccType.DCC_ACTIVITY
                // à laisser en dernier: s'il n'y a pas d'autres blocs que Person -> c'est un pass activité
                dcc?.person != null -> DccType.DCC_ACTIVITY
                else -> null

            }
        }
    }
}