package com.ingroupe.verify.anticovid.service.api.configuration.sync

import com.google.gson.annotations.SerializedName

class SyncBlacklist (
    @SerializedName("duplicateActivate")
    var duplicateActivate: Boolean? = null,
    @SerializedName("duplicateRetentionPeriod")
    var duplicateRetentionPeriod: Long? = null,
    @SerializedName("messages")
    var messages: SyncBlacklistMessage? = null,
    @SerializedName("lastIndexBlacklist")
    var lastIndexBlacklist: Int? = null,
    @SerializedName("lastIndexBlacklist2ddoc")
    var lastIndexBlacklist2ddoc: Int? = null,
    @SerializedName("exclusionTime")
    var exclusionTime: List<SyncExclusionTime>? = null,
    @SerializedName("exclusionQuantityLimit")
    var exclusionQuantityLimit: Int? = null,
    @SerializedName("refreshIndex")
    var refreshIndex: Int? = null,
    @SerializedName("refreshIndex2ddoc")
    var refreshIndex2ddoc: Int? = null,
    @SerializedName("refreshVersion")
    var refreshVersion: Int? = null

)