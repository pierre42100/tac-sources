package com.ingroupe.verify.anticovid.service.api.configuration.sync

import com.google.gson.annotations.SerializedName

class SyncLabel (
    @SerializedName("fr")
    var fr: String? = null,
    @SerializedName("en")
    var en: String? = null
)