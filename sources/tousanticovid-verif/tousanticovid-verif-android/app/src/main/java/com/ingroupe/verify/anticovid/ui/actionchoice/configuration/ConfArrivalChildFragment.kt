package com.ingroupe.verify.anticovid.ui.actionchoice.configuration

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.graphics.Typeface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.ingroupe.verify.anticovid.R
import com.ingroupe.verify.anticovid.common.Constants
import com.ingroupe.verify.anticovid.common.Utils
import com.ingroupe.verify.anticovid.databinding.ConfigArrivalBinding
import com.ingroupe.verify.anticovid.ui.actionchoice.countrypicker.CountryPickerActivity
import java.time.ZoneId
import java.time.format.DateTimeFormatter

class ConfArrivalChildFragment : ConfParentFragment() {

    companion object {
        const val TAG = "ConfArrivalChildFragment"
        fun newInstance() = ConfArrivalChildFragment()
    }

    private var _binding: ConfigArrivalBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        super.onCreateView(inflater, container, savedInstanceState)
        _binding = ConfigArrivalBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.layoutDepartures.setOnClickListener {
            val intentFlag = Intent(activity, CountryPickerActivity::class.java)
            intentFlag.putExtra(
                CountryPickerActivity.KEY_CODE,
                CountryPickerActivity.CODE_FAVORITES_ARRIVAL
            )
            startActivity(intentFlag)
        }

        binding.layoutControlZone.rootElement.setOnClickListener {
            val intentFlag = Intent(activity, CountryPickerActivity::class.java)
            intentFlag.putExtra(
                CountryPickerActivity.KEY_CODE,
                CountryPickerActivity.CODE_CONTROL_ZONE
            )
            startActivity(intentFlag)
        }

        binding.layoutControlDate.rootElement.setOnClickListener {
            if (model.travelConfiguration?.isCustomDate == true) {
                showDialogResetTime()
            } else {
                showDatePickerDialog()
            }
        }

        binding.layoutControlZone.textViewCellTitle.text = getString(R.string.conf_controle_destination_title)
        binding.textviewDepartureTitle.text = getString(R.string.conf_departure_title)
        binding.layoutControlDate.textViewCellTitle.text = getString(R.string.conf_controle_date_title)
    }

    override fun onResume() {
        super.onResume()
        presenter.initTravelType(Constants.TravelType.ARRIVAL)
        displayControlZone()
        displayDate()
        displayFavorites()
    }

    override fun displayControlZone() {
        val country = getPickedControlZone()
        //Default value can only occur if a previously Code was set and removed after an update of the app
        binding.layoutControlZone.textViewCellValue.text = country?.name ?: getString(R.string.conf_unknown)
    }

    override fun displayDate() {
        val datePicked = model.travelConfiguration?.timeUTC?.withZoneSameInstant(ZoneId.systemDefault())
        val date = datePicked?.format(DateTimeFormatter.ofPattern(Constants.DATE_FORMAT))
        val hours = datePicked?.format(DateTimeFormatter.ofPattern(Constants.TIME_FORMAT))

        if (model.travelConfiguration?.isCustomDate == true) {
            binding.layoutControlDate.imageViewTextPrefix.visibility = View.GONE
            binding.layoutControlDate.textViewCellValue.setTextColor(Color.BLACK)
            binding.layoutControlDate.textViewCellValue.setTypeface(null, Typeface.BOLD)
            binding.layoutControlDate.textViewCellValue.text = getString(R.string.conf_date_picked, date, hours)
        } else {
            binding.layoutControlDate.imageViewTextPrefix.visibility = View.VISIBLE
            binding.layoutControlDate.textViewCellValue.setTextColor(requireContext().getColor(R.color.in_gold))
            binding.layoutControlDate.textViewCellValue.setTypeface(null, Typeface.NORMAL)
            binding.layoutControlDate.textViewCellValue.text = getString(R.string.conf_current_datetime)
        }
    }

    @SuppressLint("InflateParams")
    override fun displayFavorites() {
        val favCountries = getPickedFavorites(Constants.CountriesConfiguration.ARRIVAL_FAVORITES.text)

        val param: LinearLayout.LayoutParams = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.WRAP_CONTENT,
            1f
        )

        binding.llDeparturesCells.removeAllViews()
        favCountries.map {
            val viewFav = layoutInflater.inflate(R.layout.config_favorite_cell, null)
            viewFav.findViewById<ImageView>(R.id.imageview_favorite).setImageResource(it.flagId)
            viewFav.findViewById<TextView>(R.id.textview_favorite).text = it.shortName?: it.nameCode
            viewFav.layoutParams = param
            binding.llDeparturesCells.addView(viewFav)
        }

        if (favCountries.size < Constants.MAX_FAVORITES) {
            val viewDef = layoutInflater.inflate(R.layout.config_favorite_cell_default, null)
            viewDef.layoutParams = param
            binding.llDeparturesCells.addView(viewDef)
        }
    }

    override fun onListFiltered() {
        Utils.showToast(activity as Activity, getString(R.string.conf_departure_removed))
    }
}