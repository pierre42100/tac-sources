package com.ingroupe.verify.anticovid.service.barcode.database.entity

import com.google.gson.annotations.SerializedName
import com.ingroupe.verify.anticovid.service.barcode.enums.OperationEnum

abstract class Field(
    @SerializedName("name")
    val name: String,
    @SerializedName("label")
    val label: String?,
    @SerializedName("regex")
    val regex: String?,
    @SerializedName("operations")
    val operations: List<OperationEnum>? = null
    ) {
    companion object {
        fun isValidField(field: Field?): Boolean {
            return field != null &&
                    field.name.isNotBlank() &&
                    !field.label.isNullOrBlank() &&
                    !field.regex.isNullOrBlank()
        }
    }
}