package com.ingroupe.verify.anticovid.common

import com.ingroupe.verify.anticovid.common.model.Country
import com.ingroupe.verify.anticovid.data.EngineManager
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.firstOrNull
import kotlinx.coroutines.runBlocking
import java.util.*

class CountryUtils {

    companion object {

        fun getCountries(): List<Country> {
            val countries = mutableListOf<Country>()
            lazyCountries.forEach { countries.add(it.clone()) }
            return countries
        }

        fun reloadCountries() {
            lazyMgr.reset()
        }

        private val lazyMgr = resettableManager()

        private val lazyCountries: List<Country> by resettableLazy(lazyMgr) { loadCountries() }

        private fun loadCountries(): List<Country> {

            val tempCountries : MutableList<Country> =
                Utils.loadFromAsset(Constants.COUNTRIES)?: mutableListOf()

            val listCountry = EngineManager.countriesRepository.getCountries()
            runBlocking(Dispatchers.IO) {
                listCountry.firstOrNull()?.let { list ->
                    list.forEach { countryCode ->

                        if (tempCountries.find { c -> c.nameCode.lowercase() == countryCode.lowercase() } == null) {
                            addCountry(countryCode, false, tempCountries)
                        }
                    }
                }

                val setDepartureCountryCode = EngineManager.rulesRepository.getDistinctDepartureCountry()
                setDepartureCountryCode.forEach { depCode ->
                    val depCountry = tempCountries.find { c -> c.nameCode.lowercase() == depCode.lowercase() }

                    if (depCountry == null) {
                        addCountry(depCode, true, tempCountries)
                    } else {
                        depCountry.isArrival = true
                    }
                }
            }

            return tempCountries.sortedBy { item -> item.nameForSort }
        }

        private fun addCountry(countryCode: String, isArrival: Boolean, tempCountries : MutableList<Country>) {
            val locale = Locale("", countryCode)
            var countryLabel = locale.displayCountry
            val sortName = locale.displayCountry.lowercase().replace("(", "").replace(")", "")
            var countryLabelForSort = if(isLocaleValid(locale)) sortName else "|$sortName"
            if(countryCode == "_c") {
                countryLabel = "Corse"
                countryLabelForSort = "corse"
            }
            if(countryLabel.lowercase() != countryCode.lowercase()) {
                val country = Country(
                    countryLabel,
                    countryCode,
                    countryLabelForSort,
                    isNational = false,
                    isArrival = isArrival,
                    isDeparture = !isArrival,
                    isFavorite = false
                )
                tempCountries.add(country)
            }
        }

        private fun isLocaleValid(locale: Locale): Boolean {
            return try {
                @Suppress("SENSELESS_COMPARISON")
                locale.isO3Language != null && locale.isO3Country != null
            } catch (e: MissingResourceException) {
                false
            }
        }
    }
}