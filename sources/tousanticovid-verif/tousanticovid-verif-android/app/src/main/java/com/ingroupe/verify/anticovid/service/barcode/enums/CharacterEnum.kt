package com.ingroupe.verify.anticovid.service.barcode.enums

enum class CharacterEnum(val unicode: String) {
    GS("\u001D"), RS("\u001E"), US("\u001F");

    companion object {
        fun isNotSpecialCharacter(character: Char): Boolean {
            return character != GS.unicode[0] && character != RS.unicode[0]
        }
    }
}