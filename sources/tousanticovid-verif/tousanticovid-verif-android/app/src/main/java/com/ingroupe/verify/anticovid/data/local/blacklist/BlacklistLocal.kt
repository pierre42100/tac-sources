package com.ingroupe.verify.anticovid.data.local.blacklist

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "blacklist", primaryKeys= [ "id", "type" ])
data class BlacklistLocal(
    val id: Int,
    val type: String,
    val hash: String,
    val active: Boolean
)