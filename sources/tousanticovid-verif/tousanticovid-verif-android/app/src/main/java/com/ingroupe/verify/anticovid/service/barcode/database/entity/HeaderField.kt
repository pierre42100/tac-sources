package com.ingroupe.verify.anticovid.service.barcode.database.entity

import com.google.gson.annotations.SerializedName
import com.ingroupe.verify.anticovid.service.barcode.enums.OperationEnum

class HeaderField(
    name: String,
    label: String?,
    regex: String?,
    operations: List<OperationEnum>?,
    @SerializedName("begin")
    val begin: Int?,
    @SerializedName("end")
    val end: Int?
) : Field(name, label, regex, operations) {

    companion object {
        fun isValidHeaderField(headerField: HeaderField?): Boolean {
            if (isValidField(headerField) && headerField?.begin != null && headerField.end != null) {
                return true
            }
            return false
        }
    }

}