package com.ingroupe.verify.anticovid.service.document.datamatrix.preparation

import com.ingroupe.verify.anticovid.App
import com.ingroupe.verify.anticovid.R
import com.ingroupe.verify.anticovid.common.Constants
import com.ingroupe.verify.anticovid.service.document.BarcodeService
import com.ingroupe.verify.anticovid.service.document.datamatrix.DatamatrixModeInterface
import com.ingroupe.verify.anticovid.service.document.model.DocumentStatic2ddocResult

data class TestPreparation(
    val valueF4: String,
    val valueF5: String,
    val valueF6: String,
    val isPCR: Boolean,
    val isAntigenic: Boolean
) {
    companion object {

        const val PERIOD_HOUR: Long = 1000L * 60L * 60L
        const val PERIOD_DAY: Long = PERIOD_HOUR * 24

        const val NEGATIVE_TEST = "N"
        const val POSITIVE_TEST = "P"

        fun getPreparation(
            static2ddoc: DocumentStatic2ddocResult,
            mappedDynamicData: MutableMap<String, String>
        ): TestPreparation? {

            // code LOINC
            val valueF4 = DatamatrixModeInterface.getField(static2ddoc.message?.fields, "F4")
            // résultat analyse
            val valueF5 = DatamatrixModeInterface.getField(static2ddoc.message?.fields, "F5")
            // date et heure prélevement
            val valueF6 = DatamatrixModeInterface.getField(static2ddoc.message?.fields, "F6")

            val isPCR =
                valueF4 != null
                        && BarcodeService.isValuePresent(
                    "F4",
                    valueF4,
                    Constants.SpecificList.TEST_PCR,
                    mappedDynamicData
                )

            val isAntigenic =
                valueF4 != null
                        && BarcodeService.isValuePresent(
                    "F4",
                    valueF4,
                    Constants.SpecificList.TEST_ANTIGENIC,
                    mappedDynamicData
                )

            val property = "@string/dynamic_B2F5.${valueF5}"
            val packageName: String = App.getContext().packageName
            val resId: Int = App.getContext().resources.getIdentifier(property, "string", packageName)
            if(resId != 0) {
                mappedDynamicData["F5"] = App.getContext().getString(resId)
            }


            if (!static2ddoc.hasValidSignature) {
                mappedDynamicData[Constants.VALIDITY_GLOBAL_FIELD] = Constants.GlobalValidity.KO.text
                mappedDynamicData[Constants.VALIDITY_STATUS_FIELD] = App.getContext().getString(R.string.invalid_2ddoc_signature)

                return null
            }

            return if(valueF4 == null || valueF5 == null || valueF6 == null) {
                null
            } else {
                TestPreparation(
                    valueF4,
                    valueF5,
                    valueF6,
                    isPCR,
                    isAntigenic
                )
            }
        }
    }
}
