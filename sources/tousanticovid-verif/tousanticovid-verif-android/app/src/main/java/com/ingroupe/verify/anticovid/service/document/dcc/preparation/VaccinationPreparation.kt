package com.ingroupe.verify.anticovid.service.document.dcc.preparation

import com.ingroupe.verify.anticovid.common.Constants
import com.ingroupe.verify.anticovid.service.document.BarcodeService
import com.ingroupe.verify.anticovid.service.document.dcc.DccModeInterface
import dgca.verifier.app.decoder.model.Vaccination
import java.time.LocalDate

data class VaccinationPreparation (
    val vacDate: LocalDate,
    val currentDate: LocalDate,
    val medicinalProductFound: Boolean,
    val manufacturerFound: Boolean,
    val vaccineFound: Boolean
) {
    companion object {

        const val VACCINE_JANSSEN = "EU/1/20/1525"
        const val VACCINE_NOVA = "EU/1/21/1618"

        fun getPreparation(
            vac: Vaccination,
            mappedDynamicData: MutableMap<String, String>
        ): VaccinationPreparation? {

            val date = DccModeInterface.getLocalDateFromDcc(vac.dateOfVaccination)
            val currentDate = LocalDate.now()

            val medicinalProductFound = BarcodeService.isValuePresent(
                "medicinalProduct",
                vac.medicinalProduct,
                Constants.SpecificList.VACCINE_MEDICAL_PRODUCT,
                mappedDynamicData
            )
            val manufacturerFound = BarcodeService.isValuePresent(
                "manufacturer",
                vac.manufacturer,
                Constants.SpecificList.VACCINE_MANUFACTURER,
                mappedDynamicData
            )
            val vaccineFound = BarcodeService.isValuePresent(
                "vaccine",
                vac.vaccine,
                Constants.SpecificList.VACCINE_PROPHYLAXIS,
                mappedDynamicData
            )

            return if(date == null) {
                null
            } else {
                VaccinationPreparation(
                    date,
                    currentDate,
                    medicinalProductFound,
                    manufacturerFound,
                    vaccineFound
                )
            }
        }
    }
}
