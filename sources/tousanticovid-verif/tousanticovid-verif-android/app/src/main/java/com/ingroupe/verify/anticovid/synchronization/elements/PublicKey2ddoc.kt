package com.ingroupe.verify.anticovid.synchronization.elements

import android.content.Context
import android.util.Base64
import android.util.Log
import com.ingroupe.verify.anticovid.App
import com.ingroupe.verify.anticovid.common.Constants
import java.security.KeyFactory
import java.security.PublicKey
import java.security.spec.X509EncodedKeySpec

object PublicKey2ddoc {

    private const val TAG = "PubKey2ddoc"

    private val publicKey2DDocMap: MutableMap<String, PublicKey> = HashMap()

    fun saveCertificates2DDoc(
        certificates2DDoc: Map<String, String>?
    ) {
        val pubKeyPref =
            App.getContext().getSharedPreferences(Constants.PUBLIC_KEY_FILE_KEY, Context.MODE_PRIVATE)

        with(pubKeyPref.edit()) {
            clear()
            certificates2DDoc?.let { pubKeys ->
                pubKeys.keys.forEach { key ->
                    putString(key, pubKeys[key])
                }
            }
            apply()
        }

        publicKey2DDocMap.clear()
    }

    fun get2DDocPublicKey(certificateKey: String): PublicKey? {

        val publicKey = publicKey2DDocMap[certificateKey]
        if(publicKey != null) {
            return publicKey
        }

        val certPref = App.getContext().getSharedPreferences(Constants.PUBLIC_KEY_FILE_KEY, Context.MODE_PRIVATE)
        val cert = certPref.getString(certificateKey, null)
        cert?.let {
            try {
                val asn = Base64.decode(it, Base64.NO_WRAP)
                val pub: PublicKey =
                    KeyFactory.getInstance("EC").generatePublic(X509EncodedKeySpec(asn))
                publicKey2DDocMap[certificateKey] = pub
                return pub
            } catch (e: Exception) {
                Log.e(TAG, "bad public key format")
            }
        }
        return null
    }
}