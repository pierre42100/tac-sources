package com.ingroupe.verify.anticovid.observer

import android.app.Application
import android.content.Context
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkCapabilities
import android.net.NetworkRequest
import androidx.lifecycle.LiveData

object NetworkLiveData : LiveData<Boolean>() {

    private lateinit var application: Application
    private lateinit var networkRequest: NetworkRequest
    private lateinit var networkCallBack: ConnectivityManager.NetworkCallback

    private var numConnected = 0

    override fun onActive() {
        super.onActive()
        getDetails()
    }

    override fun onInactive() {
        super.onInactive()
        unRegister()
    }

    fun init(application: Application) {
        NetworkLiveData.application = application
        networkRequest = NetworkRequest.Builder()
            .addTransportType(NetworkCapabilities.TRANSPORT_CELLULAR)
            .addTransportType(NetworkCapabilities.TRANSPORT_WIFI)
            .build()
    }

    private fun getDetails() {
        val cm = application.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        networkCallBack = NetworkCallbackImpl()
        cm.registerNetworkCallback(networkRequest, networkCallBack)
    }

    private fun unRegister() {
        val cm = application.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        cm.unregisterNetworkCallback(networkCallBack)
    }


    class NetworkCallbackImpl : ConnectivityManager.NetworkCallback() {
        override fun onAvailable(network: Network) {
            super.onAvailable(network)
            numConnected+=1
            if (numConnected == 1) {
                postValue(true)
            }
        }

        override fun onUnavailable() {
            super.onUnavailable()
            numConnected = (numConnected - 1).coerceAtLeast(0)
            if(numConnected == 0) {
                postValue(false)
            }
        }

        override fun onLost(network: Network) {
            super.onLost(network)
            numConnected = (numConnected - 1).coerceAtLeast(0)
            if(numConnected == 0) {
                postValue(false)
            }
        }

    }
}