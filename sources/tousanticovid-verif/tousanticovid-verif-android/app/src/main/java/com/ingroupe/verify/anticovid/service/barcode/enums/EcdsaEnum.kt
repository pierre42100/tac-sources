package com.ingroupe.verify.anticovid.service.barcode.enums

import java.util.*

enum class EcdsaEnum(
    val length: Int = 0,
    val padding: Int = 0,
    val algorithm: String? = null
) {
    P_256(103, 1, "SHA256withECDSA"),
    P_384(154, 6, "SHA384withECDSA"),
    P_521(212, 4, "SHA512withECDSA");

    companion object {
        fun getByLength(length: Int): EcdsaEnum? {
            return EnumSet.allOf(EcdsaEnum::class.java).find {
                    storeFormatEnum -> storeFormatEnum.length == length || storeFormatEnum.length + storeFormatEnum.padding == length
            }
        }
    }
}