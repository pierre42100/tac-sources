package com.ingroupe.verify.anticovid.service.api.configuration.sync

import com.google.gson.annotations.SerializedName

class SyncLabels (
    @SerializedName("health")
    var health: SyncLabelsMode? = null,
    @SerializedName("vaccine")
    var vaccine: SyncLabelsMode? = null
)