package com.ingroupe.verify.anticovid.ui.tutorialscan2ddoc

import android.graphics.Color
import android.graphics.Typeface
import android.os.Bundle
import android.text.SpannableStringBuilder
import android.text.style.CharacterStyle
import android.text.style.ForegroundColorSpan
import android.text.style.StyleSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.ingroupe.verify.anticovid.R
import com.ingroupe.verify.anticovid.databinding.TutorialGestureViewBinding
import java.util.regex.Matcher
import java.util.regex.Pattern

class GestureTutorialFragment : Fragment() {

    companion object {
        const val TAG = "GestureTutorialFragment"
        fun newInstance() = GestureTutorialFragment()
    }

    private var _binding: TutorialGestureViewBinding? = null

    private val binding get() = _binding!!

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        super.onCreateView(inflater, container, savedInstanceState)
        _binding = TutorialGestureViewBinding.inflate(inflater, container, false)
        val view = binding.root

        binding.textViewSwipeUp.text = colorizeText(R.string.tuto_scan_zoom)
        binding.textViewSwipeDown.text = colorizeText(R.string.tuto_scan_dezoom)
        binding.textViewFocus.text = colorizeText(R.string.tuto_scan_focus)

        return view
    }

    private fun colorizeText(stringResId: Int): SpannableStringBuilder {
        val pattern: Pattern = Pattern.compile("#(.*?)#")
        val stringValue = getText(stringResId)
        val ssb = SpannableStringBuilder(stringValue)

        val matcher: Matcher = pattern.matcher(stringValue)
        var matchesSoFar = 0

        while (matcher.find()) {
            //Récupération de l'index du match
            val start: Int = matcher.start() - matchesSoFar * 2
            val end: Int = matcher.end() - matchesSoFar * 2
            //Style
            val spanColor: CharacterStyle = ForegroundColorSpan(Color.parseColor("#003088"))
            val spanBold: CharacterStyle = StyleSpan(Typeface.BOLD)
            ssb.setSpan(spanColor, start + 1, end - 1, 0)
            ssb.setSpan(spanBold, start + 1, end - 1, 0)
            //Suppression des délimiteurs
            ssb.delete(start, start + 1)
            ssb.delete(end - 2, end - 1)
            matchesSoFar++
        }

        return ssb
    }
}