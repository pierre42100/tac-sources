package com.ingroupe.verify.anticovid.service.api.configuration.conf

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class ConfField(
    @SerializedName("name")
    var name: String? = null,
    @SerializedName("label")
    var label: String? = null,
    @SerializedName("order")
    var order: Int? = null,
    @SerializedName("defaultFormat")
    var defaultFormat: String? = null,
    @SerializedName("valuesFormat")
    var valuesFormat: Map<String, String>? = null
) : Serializable