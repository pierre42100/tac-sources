/*
 *  ---license-start
 *  eu-digital-green-certificates / dgca-verifier-app-android
 *  ---
 *  Copyright (C) 2021 T-Systems International GmbH and all other contributors
 *  ---
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  ---license-end
 *
 *  Created by osarapulov on 6/25/21 9:21 AM
 */

package com.ingroupe.verify.anticovid.service.api.configuration.engine.rules

import com.google.gson.JsonObject
import com.google.gson.annotations.SerializedName

/*-
 * ---license-start
 * eu-digital-green-certificates / dgc-certlogic-android
 * ---
 * Copyright (C) 2021 T-Systems International GmbH and all other contributors
 * ---
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ---license-end
 *
 * Created by osarapulov on 11.06.21 11:03
 */
data class RuleResult(
    @SerializedName("Identifier")
    val identifier: String,
    @SerializedName("Type")
    val type: String,
    @SerializedName("Version")
    val version: String,
    @SerializedName("SchemaVersion")
    val schemaVersion: String,
    @SerializedName("Engine")
    val engine: String,
    @SerializedName("EngineVersion")
    val engineVersion: String,
    @SerializedName("CertificateType")
    val certificateType: String,
    @SerializedName("Description")
    val descriptions: List<DescriptionResult>,
    @SerializedName("ValidFrom")
    val validFrom: String,
    @SerializedName("ValidTo")
    val validTo: String,
    @SerializedName("AffectedFields")
    val affectedString: List<String>,
    @SerializedName("Logic")
    val logic: JsonObject,
    @SerializedName("Country")
    val countryCode: String,
    @SerializedName("Region")
    val region: String?,
    @SerializedName("Modifier")
    val modifier: String?
)