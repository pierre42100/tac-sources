package com.ingroupe.verify.anticovid.service.barcode.database.entity

import com.google.gson.annotations.SerializedName

class DocumentType(
    @SerializedName("type")
    val type: String? = null,
    @SerializedName("label")
    val label: String? = null,
    @SerializedName("fields")
    val fields: List<String>? = null
)