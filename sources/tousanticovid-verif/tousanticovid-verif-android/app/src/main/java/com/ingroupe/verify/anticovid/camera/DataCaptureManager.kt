/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.ingroupe.verify.anticovid.camera

import com.ingroupe.verify.anticovid.BuildConfig
import com.scandit.datacapture.barcode.capture.BarcodeCapture
import com.scandit.datacapture.barcode.capture.BarcodeCaptureSettings
import com.scandit.datacapture.core.capture.DataCaptureContext
import com.scandit.datacapture.core.source.Camera


class DataCaptureManager private constructor() {
    val barcodeCapture: BarcodeCapture
    val dataCaptureContext: DataCaptureContext = DataCaptureContext.forLicenseKey(BuildConfig.SCANDIT_LICENSE_KEY)
    var camera: Camera? = getDefaultCamera()

    companion object {
        val CURRENT = DataCaptureManager()
    }

    init {
        // Create data capture context using your license key and set the camera as the frame source.

        // Use the default camera with the recommended camera settings for the BarcodeCapture mode
        // and set it as the frame source of the context. The camera is off by default and must be
        // turned on to start streaming frames to the data capture context for recognition.
        if (camera != null) {
            dataCaptureContext.setFrameSource(camera)
        } else {
            throw IllegalStateException(
                "Sample depends on a camera, which failed to initialize."
            )
        }

        // Create new barcode capture mode with default settings. Each mode of the sample will
        // apply its own settings specific to its use case.
        barcodeCapture =
            BarcodeCapture.forDataCaptureContext(dataCaptureContext, BarcodeCaptureSettings())
    }

    private fun getDefaultCamera() : Camera? {
        val cameraSettings = BarcodeCapture.createRecommendedCameraSettings()
        return Camera.getDefaultCamera(cameraSettings)

    }
}