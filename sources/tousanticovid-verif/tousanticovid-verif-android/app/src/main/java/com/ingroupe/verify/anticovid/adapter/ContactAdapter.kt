package com.ingroupe.verify.anticovid.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ingroupe.verify.anticovid.databinding.ContactViewBinding
import com.ingroupe.verify.anticovid.service.api.configuration.conf.ConfContact


class ContactAdapter(private val contacts: ArrayList<ConfContact>) :
    RecyclerView.Adapter<ContactAdapter.MyViewHolder>() {

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder.
    // Each data item is just a string in this case that is shown in a TextView.
    class MyViewHolder(val binding: ContactViewBinding) : RecyclerView.ViewHolder(binding.root)


    // Create new views (invoked by the tutorial_skip_view manager)
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): MyViewHolder {
        // create a new view
        val binding = ContactViewBinding.inflate(LayoutInflater.from(parent.context), parent, false)

        // set the view's size, margins, paddings and tutorial_skip_view parameters
        //...
        return MyViewHolder(binding)
    }

    // Replace the contents of a view (invoked by the tutorial_skip_view manager)
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element


        holder.binding.textViewContactName.text = contacts[position].name

        if (contacts[position].tel.isNullOrEmpty()) {
            holder.binding.textViewContactTel.visibility = View.GONE
        } else {
            holder.binding.textViewContactTel.visibility = View.VISIBLE
            holder.binding.textViewContactTel.text = contacts[position].tel
        }

        if (contacts[position].mail.isNullOrEmpty()) {
            holder.binding.textViewContactMail.visibility = View.GONE
        } else {
            holder.binding.textViewContactMail.visibility = View.VISIBLE
            holder.binding.textViewContactMail.text = contacts[position].mail
        }

        if (contacts[position].details.isNullOrEmpty()) {
            holder.binding.textViewContactDetails.visibility = View.GONE
        } else {
            holder.binding.textViewContactDetails.visibility = View.VISIBLE
            holder.binding.textViewContactDetails.text = contacts[position].details
        }
    }

    // Return the size of your dataset (invoked by the tutorial_skip_view manager)
    override fun getItemCount() = contacts.size
}