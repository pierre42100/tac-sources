package com.ingroupe.verify.anticovid.ui.init

import com.ingroupe.verify.anticovid.common.SharedViewModel

interface InitPresenter {

    fun loadAssets(
        model: SharedViewModel
    )

    fun initEngineIfNeeded()

    fun tearDown()
}