package com.ingroupe.verify.anticovid.common

import java.util.*


fun <T> List<T>.toSublistsOf(size: Int) : List<List<T>> {
    val initialList = this.toMutableList()
    val listResult = ArrayList<List<T>>()

    if (initialList.isEmpty() || size <= 0) {
        return listResult
    }

    if (initialList.size <= size) {
        listResult.add(initialList)
        return listResult
    }

    while (initialList.size > 0) {
        val maxIndex = if (initialList.size >= size) {
            size - 1
        } else {
            initialList.size - 1
        }

        listResult.add(initialList.slice(0..maxIndex))
        initialList.removeSlice(0, maxIndex)
    }

    return listResult
}

fun <T> MutableList<T>.removeSlice(from: Int, end: Int) {
    this.subList(from, end + 1).clear()
}
