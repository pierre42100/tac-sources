package com.ingroupe.verify.anticovid.common.model

import com.blongho.country_data.World
import com.ingroupe.verify.anticovid.R

data class Country(
    val name: String,
    val nameCode: String,
    val sortingName: String?,
    val isNational: Boolean,
    var isArrival: Boolean? = false,
    var isDeparture: Boolean? = false,
    var isFavorite: Boolean,
    var shortName: String? = null
) {
    val flagId: Int
        get() {
            //Overriding the missing flags from the Lib
            //A cool alternative would be to submit a PR to
            //the country_data library
            return when (nameCode) {
                "_c" -> R.drawable.fr
                "mq" -> R.drawable.fr
                "pf" -> R.drawable.fr
                "bl" -> R.drawable.fr
                "mf" -> R.drawable.fr
                "wf" -> R.drawable.fr
                "nc" -> R.drawable.fr
                "Autre" -> R.drawable.flag_other
                else -> World.getFlagOf(this.nameCode)
            }
        }

    val nameForSort: String
        get() {
            return sortingName?: name.lowercase().replace("(", "").replace(")", "")
        }

    fun clone(): Country {
        return Country(
            this.name,
            this.nameCode,
            this.sortingName,
            this.isNational,
            this.isArrival,
            this.isDeparture,
            this.isFavorite,
            this.shortName
        )
    }
}