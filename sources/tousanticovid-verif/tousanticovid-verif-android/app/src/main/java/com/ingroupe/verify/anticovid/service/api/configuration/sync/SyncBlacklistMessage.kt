package com.ingroupe.verify.anticovid.service.api.configuration.sync

import com.google.gson.annotations.SerializedName

class SyncBlacklistMessage (
    @SerializedName("blacklistLite")
    var blacklistLite: SyncMessage? = null,
    @SerializedName("blacklistOT")
    var blacklistOT: SyncMessage? = null,
    @SerializedName("duplicate")
    var duplicate: SyncMessage? = null
)