package com.ingroupe.verify.anticovid.auth

import com.google.gson.GsonBuilder
import com.ingroupe.verify.anticovid.BuildConfig
import com.ingroupe.verify.anticovid.common.FileLogger
import com.ingroupe.verify.anticovid.service.api.configuration.ConfigurationService
import com.scandit.datacapture.core.internal.sdk.AppAndroidEnvironment
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.security.cert.CertificateException
import java.security.cert.X509Certificate
import java.util.*
import javax.net.ssl.*

object WSConf: IWSConf {
    const val TAG = "WSConf"

    override fun getConfigurationBaseUrl(): String {
        return "/api/client/configuration"
    }

    override fun getConfigurationService(): ConfigurationService {

        val gsonBuilder = GsonBuilder().registerTypeAdapter(Date::class.java, DateDeserializer())

        return Retrofit.Builder().baseUrl(BuildConfig.ENDPOINT_URL)
            .client(getOkHttpClient())
            .addConverterFactory(GsonConverterFactory.create(gsonBuilder.create())).build().create(
                ConfigurationService::class.java
            )
    }

    @Suppress("ConstantConditionIf")
    override fun getOkHttpClient(): OkHttpClient {
        val builder: OkHttpClient.Builder = getUnsafeOkHttpClientBuilder()

        // cas de base : non authentifié sans proxy
        builder
            .addInterceptor(AcceptLanguageHeaderInterceptor())
            .addInterceptor(AddAuthorizationInterceptor())
            .addInterceptor(AddVersionInterceptor())

        if(BuildConfig.LOG_ENABLED) {
            val logging = HttpLoggingInterceptor {
                FileLogger.write(it, AppAndroidEnvironment.applicationContext)
            }
            logging.setLevel(HttpLoggingInterceptor.Level.BASIC)
            builder.addInterceptor(logging)
        }

        return builder.build()
    }

    private fun getUnsafeOkHttpClientBuilder(): OkHttpClient.Builder {
        try {
            // Create a trust manager that does not validate certificate chains
            val trustAllCerts = arrayOf<TrustManager>(object : X509TrustManager {

                override fun getAcceptedIssuers(): Array<X509Certificate> {
                    return arrayOf()
                }

                @Throws(CertificateException::class)
                override fun checkClientTrusted(chain: Array<X509Certificate>, authType: String) {
                }

                @Throws(CertificateException::class)
                override fun checkServerTrusted(chain: Array<X509Certificate>, authType: String) {
                }
            })

            // Install the all-trusting trust manager
            val sslContext = SSLContext.getInstance("SSL")
            sslContext.init(null, trustAllCerts, java.security.SecureRandom())

            // Create an ssl socket factory with our all-trusting manager
            val sslSocketFactory = sslContext.getSocketFactory()

            val builder = OkHttpClient.Builder()
            builder.sslSocketFactory(sslSocketFactory, trustAllCerts[0] as X509TrustManager)
            builder.hostnameVerifier(object : HostnameVerifier {
                override fun verify(hostname: String, session: SSLSession): Boolean {
                    return true
                }
            })
            return builder
        } catch (e: Exception) {
            throw RuntimeException(e)
        }
    }
}