package com.ingroupe.healthdocconvertor.connector.deuxddoc.dto;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ingroupe.healthdocconvertor.convertor.impl.GreenCertificateRequestAdapter;
import com.ingroupe.healthdocconvertor.exception.CannotParse2DDocException;
import com.ingroupe.healthdocconvertor.exception.Invalid2DDocInputValueException;
import com.ingroupe.healthdocconvertor.exception.Invalid2DDocPayloadException;
import com.ingroupe.healthdocconvertor.exception.Invalid2DDocSignatureException;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TwoDDocHealthCertResponseFacadeTest {

    @Nested
    public class GivenInvalidAttestation {

        @Test
        void givenTwoDDocIsNull_shouldThrowException() {
            Invalid2DDocPayloadException exception =
                assertThrows(Invalid2DDocPayloadException.class, () -> new TwoDDocHealthCertResponseFacade(null));
            assertEquals("The value of the payload is not supported", exception.getMessage());
        }

        @Test
        void givenTwoDDocIsNotAHealthCertificate_shouldThrowException() throws JsonProcessingException {
            String json = "{\n" +
                "  \"data\": {\n" +
                "    \"static\": {\n" +
                "      \"message\": {\n" +
                "        \"type\": \"foo\"\n" +
                "      }\n" +
                "    }\n" +
                "  }\n" +
                "}";
            TwoDDocHealthCertResponseFacade facade = getFacadeFromJsonString(json);

            CannotParse2DDocException exception = assertThrows(CannotParse2DDocException.class, facade::getAttestationType);
            assertEquals("The type of 2DDoc is not supported", exception.getMessage());
        }

        @Test
        void givenTwoDDocIsTestWithoutAnalysisResult_shouldThrowException() throws JsonProcessingException {
            String json = "{\n" +
                "  \"data\": {\n" +
                "    \"static\": {\n" +
                "      \"message\": {\n" +
                "        \"type\": \"B2\"\n" +
                "      }\n" +
                "    }\n" +
                "  }\n" +
                "}";
            TwoDDocHealthCertResponseFacade facade = getFacadeFromJsonString(json);

            CannotParse2DDocException exception = assertThrows(CannotParse2DDocException.class, facade::getAttestationType);
            assertEquals("A mandatory variable can not be found in the 2DDoc", exception.getMessage());
        }

        @Test
        void givenTwoDDocIsTestWithMultipleAnalysisResults_shouldThrowException() throws JsonProcessingException {
            String json = "{\n" +
                "  \"data\": {\n" +
                "    \"static\": {\n" +
                "      \"message\": {\n" +
                "        \"type\": \"B2\",\n" +
                "        \"fields\": [\n" +
                "          {\n" +
                "            \"label\": \"Résultat de l'analyse\",\n" +
                "            \"name\": \"F5\",\n" +
                "            \"value\": \"Positif\"\n" +
                "          },\n" +
                "          {\n" +
                "            \"label\": \"Résultat de l'analyse\",\n" +
                "            \"name\": \"F5\",\n" +
                "            \"value\": \"Positif\"\n" +
                "          }\n" +
                "        ]\n" +
                "      }\n" +
                "    }\n" +
                "  }\n" +
                "}";
            TwoDDocHealthCertResponseFacade facade = getFacadeFromJsonString(json);

            CannotParse2DDocException exception = assertThrows(CannotParse2DDocException.class, facade::getAttestationType);
            assertEquals("The same variable has been found more than once in the 2DDoc", exception.getMessage());
        }

        @Test
        void givenTwoDDocIsTestWithUnexpectedResult_shouldThrowException() throws JsonProcessingException {
            String json = "{\n" +
                "  \"data\": {\n" +
                "    \"static\": {\n" +
                "      \"message\": {\n" +
                "        \"type\": \"B2\",\n" +
                "        \"fields\": [\n" +
                "          {\n" +
                "            \"label\": \"Résultat de l'analyse\",\n" +
                "            \"name\": \"F5\",\n" +
                "            \"value\": \"foobar\"\n" +
                "          }\n" +
                "        ]\n" +
                "      }\n" +
                "    }\n" +
                "  }\n" +
                "}";
            TwoDDocHealthCertResponseFacade facade = getFacadeFromJsonString(json);

            Invalid2DDocInputValueException exception = assertThrows(Invalid2DDocInputValueException.class, facade::getAttestationType);
            assertEquals("Test result is not supported", exception.getMessage());
        }

    }

    private TwoDDocHealthCertResponseFacade getFacadeFromJsonString(String json) throws JsonProcessingException {
        DocumentDto twoDDoc = new ObjectMapper().readValue(json, DocumentDto.class);
        return new TwoDDocHealthCertResponseFacade(twoDDoc);
    }

    @Nested
    public class GivenTwoDDocIsTestAttestation {

        @Test
        void givenTwoDDocIsPositiveTest_attestationTypeShouldBeTest() throws JsonProcessingException {
            String json = "{\n" +
                "  \"data\": {\n" +
                "    \"static\": {\n" +
                "      \"message\": {\n" +
                "        \"type\": \"B2\",\n" +
                "        \"fields\": [\n" +
                "          {\n" +
                "            \"label\": \"Code analyse\",\n" +
                "            \"name\": \"F4\",\n" +
                "            \"value\": \"94558-4 Antigénique COVID\"\n" +
                "          },\n" +
                "          {\n" +
                "            \"label\": \"Résultat de l'analyse\",\n" +
                "            \"name\": \"F5\",\n" +
                "            \"value\": \"Positif\"\n" +
                "          }\n" +
                "        ]\n" +
                "      }\n" +
                "    }\n" +
                "  }\n" +
                "}";
            TwoDDocHealthCertResponseFacade facade = getFacadeFromJsonString(json);

            assertEquals(AttestationType.TEST, facade.getAttestationType());
        }

        @Test
        void givenTwoDDocIsPositiveTest_attestationTypeShouldBeRecovery() throws JsonProcessingException {
            String json = "{\n" +
                "  \"data\": {\n" +
                "    \"static\": {\n" +
                "      \"message\": {\n" +
                "        \"type\": \"B2\",\n" +
                "        \"fields\": [\n" +
                "          {\n" +
                "            \"label\": \"Code analyse\",\n" +
                "            \"name\": \"F4\",\n" +
                "            \"value\": \"94500-6 PCR COVID\"\n" +
                "          },\n" +
                "          {\n" +
                "            \"label\": \"Résultat de l'analyse\",\n" +
                "            \"name\": \"F5\",\n" +
                "            \"value\": \"Positif\"\n" +
                "          }\n" +
                "        ]\n" +
                "      }\n" +
                "    }\n" +
                "  }\n" +
                "}";
            TwoDDocHealthCertResponseFacade facade = getFacadeFromJsonString(json);

            assertEquals(AttestationType.RECOVERY, facade.getAttestationType());
        }

        @Test
        void givenTwoDDocIsNegativeTest_attestationTypeShouldBeTest() throws JsonProcessingException {
            String json = "{\n" +
                "  \"data\": {\n" +
                "    \"static\": {\n" +
                "      \"message\": {\n" +
                "        \"type\": \"B2\",\n" +
                "        \"fields\": [\n" +
                "          {\n" +
                "            \"label\": \"Résultat de l'analyse\",\n" +
                "            \"name\": \"F5\",\n" +
                "            \"value\": \"Négatif\"\n" +
                "          }\n" +
                "        ]\n" +
                "      }\n" +
                "    }\n" +
                "  }\n" +
                "}";
            TwoDDocHealthCertResponseFacade facade = getFacadeFromJsonString(json);

            assertEquals(AttestationType.TEST, facade.getAttestationType());
        }

        @Test
        void givenTwoDDocIsCompleteNegativeTest_attestationTypeShouldBeTest() throws JsonProcessingException {
            String json = "{\n" +
                "  \"data\": {\n" +
                "    \"static\": {\n" +
                "      \"message\": {\n" +
                "        \"type\": \"B2\",\n" +
                "        \"fields\": [\n" +
                "          {\n" +
                "            \"label\": \"Code analyse\",\n" +
                "            \"name\": \"F4\",\n" +
                "            \"value\": \"3333\"\n" +
                "          },\n" +
                "          {\n" +
                "            \"label\": \"Résultat de l'analyse\",\n" +
                "            \"name\": \"F5\",\n" +
                "            \"value\": \"Négatif\"\n" +
                "          },\n" +
                "          {\n" +
                "            \"label\": \"Date et heure du prélèvement\",\n" +
                "            \"name\": \"F6\",\n" +
                "            \"value\": \"04/04/2021 23:59\"\n" +
                "          }\n" +
                "        ]\n" +
                "      },\n" +
                "      \"signature\": {\n" +
                "        \"isValid\": true,\n" +
                "        \"status\": \"Valide\"\n" +
                "      }\n" +
                "    }\n" +
                "  }\n" +
                "}";
            TwoDDocHealthCertResponseFacade facade = getFacadeFromJsonString(json);

            assertEquals("3333", facade.getTestType());
            assertEquals("04/04/2021 23:59", facade.getTestDate());
            assertTrue(facade.isSignatureValid());
        }

        @Test
        void givenTwoDDocIsTest_shouldSetDateOfBirth() throws JsonProcessingException {
            String json = "{\n" +
                "  \"data\": {\n" +
                "    \"static\": {\n" +
                "      \"message\": {\n" +
                "        \"type\": \"B2\"\n" +
                "      }\n" +
                "    }\n" +
                "  }\n" +
                "}";
            TwoDDocHealthCertResponseFacade facade = getFacadeFromJsonString(json);

            facade.setTestDateOfBirth("2015-10-21");

            assertEquals("2015-10-21", facade.getTestDateOfBirth());
        }

        @Test
        void givenTwoDDocIsAntibodyTest_shouldThrowException() throws JsonProcessingException {
            String json = "{\n" +
                "  \"data\": {\n" +
                "    \"static\": {\n" +
                "      \"message\": {\n" +
                "        \"type\": \"B2\",\n" +
                "        \"fields\": [\n" +
                "          {\n" +
                "            \"label\": \"Code analyse\",\n" +
                "            \"name\": \"F4\",\n" +
                "            \"value\": \"94558-4 Antigénique COVID\"\n" +
                "          },\n" +
                "          {\n" +
                "            \"label\": \"Résultat de l'analyse\",\n" +
                "            \"name\": \"F5\",\n" +
                "            \"value\": \"Négatif\"\n" +
                "          },\n" +
                "          {\n" +
                "            \"label\": \"Date et heure du prélèvement\",\n" +
                "            \"name\": \"F6\",\n" +
                "            \"value\": \"04/04/2021 23:59\"\n" +
                "          }\n" +
                "         ]\n" +
                "        }\n" +
                "      }\n" +
                "    }\n" +
                "}";

            DocumentDto twoDDoc = new ObjectMapper().readValue(json, DocumentDto.class);
            GreenCertificateRequestAdapter greenCertificateRequestAdapter = new GreenCertificateRequestAdapter(twoDDoc);

            Invalid2DDocInputValueException exception = assertThrows(Invalid2DDocInputValueException.class, greenCertificateRequestAdapter::createEudgcRequest);
            assertEquals("Antibody test type is not supported", exception.getMessage());

        }

    }

    @Nested
    public class GivenTwoDDocIsVaccinationAttestation {

        @Test
        void givenTwoDDocIsVaccination_attestationTypeShouldBeVaccination() throws JsonProcessingException {
            String json = "{\n" +
                "  \"data\": {\n" +
                "    \"static\": {\n" +
                "      \"message\": {\n" +
                "        \"type\": \"L1\"" +
                "\n" +
                "      }\n" +
                "    }\n" +
                "  }\n" +
                "}";
            TwoDDocHealthCertResponseFacade facade = getFacadeFromJsonString(json);

            assertEquals(AttestationType.VACCINATION, facade.getAttestationType());
        }

        @Test
        void givenTwoDDocIsCompleteVaccination_attestationTypeShouldBeVaccination() throws JsonProcessingException {
            String json = "{\n" +
                "  \"data\": {\n" +
                "    \"static\": {\n" +
                "      \"message\": {\n" +
                "        \"type\": \"L1\",\n" +
                "        \"fields\": [\n" +
                "          {\n" +
                "            \"label\": \"Nom du vaccin\",\n" +
                "            \"name\": \"L5\",\n" +
                "            \"value\": \"COMIRNATY PFIZER/BIONTECH Vaccine name\"\n" +
                "          },\n" +
                "          {\n" +
                "            \"label\": \"Fabriquant du vaccin\",\n" +
                "            \"name\": \"L6\",\n" +
                "            \"value\": \"COMIRNATY PFIZER/BIONTECH Manufacturer name\"\n" +
                "          },\n" +
                "          {\n" +
                "            \"label\": \"Rang du dernier état de vaccination effectué\",\n" +
                "            \"name\": \"L7\",\n" +
                "            \"value\": \"1\"\n" +
                "          },\n" +
                "          {\n" +
                "            \"label\": \"Nombre de doses attendues pour un cycle complet\",\n" +
                "            \"name\": \"L8\",\n" +
                "            \"value\": \"2\"\n" +
                "          },\n" +
                "          {\n" +
                "            \"label\": \"Date du dernier état du cycle de vaccination\",\n" +
                "            \"name\": \"L9\",\n" +
                "            \"value\": \"01/03/2021\"\n" +
                "          },\n" +
                "          {\n" +
                "            \"label\": \"Etat du cycle de vaccination\",\n" +
                "            \"name\": \"LA\",\n" +
                "            \"value\": \"En cours\"\n" +
                "          }\n" +
                "        ]\n" +
                "      }\n" +
                "    }\n" +
                "  }\n" +
                "}";
            TwoDDocHealthCertResponseFacade facade = getFacadeFromJsonString(json);

            assertEquals("COMIRNATY PFIZER/BIONTECH Vaccine name", facade.getVaccineName());
            assertEquals("COMIRNATY PFIZER/BIONTECH Manufacturer name", facade.getVaccineManufacturerName());
            assertEquals("1", facade.getVaccineDoseNumber());
            assertEquals("2", facade.getVaccineTotalDoses());
            assertEquals("01/03/2021", facade.getVaccinationDate());
            assertEquals("En cours", facade.getVaccinationCycleState());
        }

        @Test
        void givenTwoDDocIsVaccination_shouldSetDateOfBirth() throws JsonProcessingException {
            String json = "{\n" +
                    "  \"data\": {\n" +
                    "    \"static\": {\n" +
                    "      \"message\": {\n" +
                    "        \"type\": \"L1\"\n" +
                    "      }\n" +
                    "    }\n" +
                    "  }\n" +
                    "}";
            TwoDDocHealthCertResponseFacade facade = getFacadeFromJsonString(json);

            facade.setVaccineDateOfBirth("2015-10-21");

            assertEquals("2015-10-21", facade.getVaccineDateOfBirth());
        }

    }

}
