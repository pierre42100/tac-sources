package com.ingroupe.healthdocconvertor.convertor.impl;

import com.ingroupe.healthdocconvertor.connector.deuxddoc.dto.*;
import com.ingroupe.healthdocconvertor.exception.BlacklistedTwoDDocException;
import com.ingroupe.healthdocconvertor.exception.Invalid2DDocException;
import com.ingroupe.healthdocconvertor.exception.Invalid2DDocInputValueException;
import com.ingroupe.healthdocconvertor.exception.Invalid2DDocSignatureException;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static com.ingroupe.healthdocconvertor.convertor.impl.VaccinationCycleState.COMPLETE;
import static com.ingroupe.healthdocconvertor.convertor.impl.VaccinationCycleState.INCOMPLETE;
import static org.junit.jupiter.api.Assertions.*;

class TwoDDocValidatorTest {

    @Test
    void givenNull_shouldInvalid2DDocExceptionThrowException() {
        TwoDDocValidator validator = new TwoDDocValidator(null);

        assertThrows(Invalid2DDocException.class, validator::validate);
    }

    @Test
    void givenSignatureIsInvalid_shouldThrowException() {
        TwoDDocValidator validator = new TwoDDocValidator(buildTwoDDocWithCustomSignature(false));
        assertThrows(Invalid2DDocSignatureException.class, validator::validate);
    }

    @Test
    void givenSignatureIsBlackListed_shouldThrowException() {
        TwoDDocValidator validator = new TwoDDocValidator(buildTwoDDocWithBlackListedSignature());
        assertThrows(BlacklistedTwoDDocException.class, validator::validate);
    }


    private DocumentDto buildTwoDDocWithCustomSignature(Boolean customSignature) {
        DocumentDto twoDDoc = new DocumentDto();

        twoDDoc.setData(new DataDto());
        twoDDoc.getData().setStaticData(new Barcode2DDocDto());
        twoDDoc.getData().getStaticData().setSignature(new Signature());
        twoDDoc.getData().getStaticData().getSignature().setValid(customSignature);

        return twoDDoc;
    }

    private DocumentDto buildTwoDDocWithBlackListedSignature() {
        DocumentDto twoDDoc = buildTwoDDocWithVaccinationCycle(COMPLETE.getTwoDDocState(), "2", "2");
        twoDDoc.getData().getStaticData().setIsBlacklisted(true);
        return twoDDoc;
    }

    @Test
    void givenVaccinationCycleStateIsCompletedButInconsistentNumberOfDose_shouldThrowException() {
        DocumentDto twoDDoc = buildTwoDDocWithVaccinationCycle(COMPLETE.getTwoDDocState(), "1", "2");
        TwoDDocValidator validator = new TwoDDocValidator(twoDDoc);

        Invalid2DDocInputValueException exception =
            assertThrows(Invalid2DDocInputValueException.class, validator::validate);
        assertEquals(COMPLETE, exception.getVaccinationCycleState());
        assertEquals(1, exception.getInjectedDoseNumber());
        assertEquals(2, exception.getCompleteVaccinationCycleDoseNumber());
        assertEquals("The vaccination state is not consistent with the number of injections",
            exception.getMessage());
    }

    @Test
    void givenVaccinationCycleStateIsIncompleteButInconsistentNumberOfDose_shouldNotThrowException() {
        DocumentDto twoDDoc = buildTwoDDocWithVaccinationCycle(INCOMPLETE.getTwoDDocState(), "2", "2");
        TwoDDocValidator validator = new TwoDDocValidator(twoDDoc);

        assertDoesNotThrow(validator::validate);
    }

    private DocumentDto buildTwoDDocWithVaccinationCycle(String cycleState, String numberOfInjectedDose, String numberOfTotalNeededDose) {
        DocumentDto twoDDoc = buildTwoDDocWithCustomSignature(false);

        twoDDoc.setData(new DataDto());
        twoDDoc.getData().setStaticData(new Barcode2DDocDto());
        twoDDoc.getData().getStaticData().setIsBlacklisted(false);
        twoDDoc.getData().getStaticData().setSignature(new Signature());
        twoDDoc.getData().getStaticData().getSignature().setValid(true);

        twoDDoc.getData().getStaticData().setMessage(new ZoneDto());
        twoDDoc.getData().getStaticData().getMessage().setType("L1");
        twoDDoc.getData().getStaticData().getMessage().setFields(new ArrayList<>());

        FieldDto vaccinationDoseNumber = new FieldDto();
        vaccinationDoseNumber.setName("L7");
        vaccinationDoseNumber.setValue(numberOfInjectedDose);
        twoDDoc.getData().getStaticData().getMessage().getFields().add(vaccinationDoseNumber);

        FieldDto vaccinationTotalDose = new FieldDto();
        vaccinationTotalDose.setName("L8");
        vaccinationTotalDose.setValue(numberOfTotalNeededDose);
        twoDDoc.getData().getStaticData().getMessage().getFields().add(vaccinationTotalDose);

        FieldDto vaccinationCycleState = new FieldDto();
        vaccinationCycleState.setName("LA");
        vaccinationCycleState.setValue(cycleState);
        twoDDoc.getData().getStaticData().getMessage().getFields().add(vaccinationCycleState);

        return twoDDoc;
    }

}
