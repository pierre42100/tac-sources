package com.ingroupe.healthdocconvertor.convertor.impl;

import com.ingroupe.healthdocconvertor.controller.HealthDocumentConvertorController;
import com.ingroupe.healthdocconvertor.service.HealthDocumentConvertorService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = HealthDocumentConvertorController.class)
public class HealthDocumentConvertorControllerTest {

    final String JSON_CORRECT          ="{\"source\": \"DEUX_D_DOC\",\"chainEncoded\": \"DC04FR0000011E6A1E6AL101FRL0THEOULE SUR MER%1DL1JEAN PIERRE%1DL230041962L3COVID-19%1DL4J07BX03%1DL5COMIRNATY PFIZER/BIONTECH%1DL6COMIRNATY PFIZER/BIONTECH%1DL71L82L901032021LACO%1FV5Y2ZQDNE5J3O7QKDQI2ZHKPEBLWTIFV6DS2AJXSVVU2MBJS3K6E5AR2CNNVBLE5ODIL2RFIJJUIM64HTMNPAB5TZYCW7RFVHW3J7IQ\",\"destination\": \"DGCA\"}";
    final String JSON_WRONG_SOURCE     ="{\"source\": \"RANDOMSOURCE\",\"chainEncoded\": \"DC04FR0000011E6A1E6AL101FRL0THEOULE SUR MER%1DL1JEAN PIERRE%1DL230041962L3COVID-19%1DL4J07BX03%1DL5COMIRNATY PFIZER/BIONTECH%1DL6COMIRNATY PFIZER/BIONTECH%1DL71L82L901032021LACO%1FV5Y2ZQDNE5J3O7QKDQI2ZHKPEBLWTIFV6DS2AJXSVVU2MBJS3K6E5AR2CNNVBLE5ODIL2RFIJJUIM64HTMNPAB5TZYCW7RFVHW3J7IQ\",\"destination\": \"DGCA\"}";
    final String JSON_WRONG_DESTINATION="{\"source\": \"DEUX_D_DOC\",\"chainEncoded\": \"DC04FR0000011E6A1E6AL101FRL0THEOULE SUR MER%1DL1JEAN PIERRE%1DL230041962L3COVID-19%1DL4J07BX03%1DL5COMIRNATY PFIZER/BIONTECH%1DL6COMIRNATY PFIZER/BIONTECH%1DL71L82L901032021LACO%1FV5Y2ZQDNE5J3O7QKDQI2ZHKPEBLWTIFV6DS2AJXSVVU2MBJS3K6E5AR2CNNVBLE5ODIL2RFIJJUIM64HTMNPAB5TZYCW7RFVHW3J7IQ\",\"destination\": \"RANDOMDEST\"}";

    final String JSON_RETURN_WRONG_SOURCE     ="{\"codeError\":\"INCORRECT_PARAMETER\"}";
    final String JSON_RETURN_WRONG_DESTINATION="{\"codeError\":\"INCORRECT_PARAMETER\"}";
    final String JSON_RETURN_NO_INPUT ="{\"codeError\":\"INCORRECT_PARAMETER\"}";

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private HealthDocumentConvertorService healthDocumentConvertorService;

    @Test
    void whenValidInput_thenReturns200() throws Exception {
        mockMvc.perform(post("/decode/decodeDocument")
                .contentType(MediaType.APPLICATION_JSON)
                .content(JSON_CORRECT)
            ).andExpect(status().isOk());
    }

    @Test
    void givenIncorrectSourceField_shouldThrowException() throws Exception {
        mockMvc.perform(post("/decode/decodeDocument")
                .contentType(MediaType.APPLICATION_JSON)
                .content(JSON_WRONG_SOURCE)
        )
            .andExpect(status().is4xxClientError())
            .andExpect(result -> assertEquals( JSON_RETURN_WRONG_SOURCE, result.getResponse().getContentAsString() ));
    }

    @Test
    void givenIncorrectDestinationField_shouldThrowException() throws Exception {
        mockMvc.perform(post("/decode/decodeDocument")
                .contentType(MediaType.APPLICATION_JSON)
                .content(JSON_WRONG_DESTINATION)
            )
            .andExpect(status().is4xxClientError())
            .andExpect(result -> assertEquals(JSON_RETURN_WRONG_DESTINATION , result.getResponse().getContentAsString() ));
    }

    @Test
    void givenEmptyJsonInput_shouldThrowException() throws Exception {
        mockMvc.perform(post("/decode/decodeDocument")
                .contentType(MediaType.APPLICATION_JSON)
                .content("")
            )
            .andExpect(status().is4xxClientError())
            .andExpect(result -> assertEquals(JSON_RETURN_NO_INPUT, result.getResponse().getContentAsString() ));
    }


}
