package com.ingroupe.healthdocconvertor.exception;

public class BlacklistedTwoDDocException extends RuntimeException {

    public BlacklistedTwoDDocException() {
        super("The input document is blacklisted");
    }

}
