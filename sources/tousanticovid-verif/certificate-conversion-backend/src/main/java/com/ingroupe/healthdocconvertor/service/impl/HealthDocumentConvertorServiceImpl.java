package com.ingroupe.healthdocconvertor.service.impl;

import com.ingroupe.healthdocconvertor.convertor.HdcStrategy;
import com.ingroupe.healthdocconvertor.convertor.impl.Hdc2dDocToDgcaStrategy;
import com.ingroupe.healthdocconvertor.convertor.impl.HdcIcaoToDgcaStrategy;
import com.ingroupe.healthdocconvertor.dto.DocumentRequest;
import com.ingroupe.healthdocconvertor.exception.CannotParse2DDocException;
import com.ingroupe.healthdocconvertor.exception.Invalid2DDocPayloadException;
import com.ingroupe.healthdocconvertor.helper.HealthDocumentConvertorHelper;
import com.ingroupe.healthdocconvertor.service.GuardService;
import com.ingroupe.healthdocconvertor.service.HealthDocumentConvertorService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

@Service
@Slf4j
public class HealthDocumentConvertorServiceImpl implements HealthDocumentConvertorService, InitializingBean {

    @Autowired
    private ApplicationContext context;
    @Autowired
    private HealthDocumentConvertorHelper hdcHelper;
    @Autowired
    GuardService guardService;

    @Value("${hdc.guard.enabled}")
    private boolean guardEnabled;

    private final Map<String, Class<?>> legalConversions = new HashMap<>();

    private final String HASH_EXTRACTION_ERROR = "2DDOC hash extraction error";


    @Override
    public String decodeDocument(DocumentRequest documentRequest) throws CannotParse2DDocException {
        Class<?> strategyClass = obtainStrategy(documentRequest);
        // Validation
        if (documentRequest.getChainEncoded().isEmpty())
            throw new Invalid2DDocPayloadException();

        if (strategyClass == null) {
            log.error( "Health document conversion error : Invalid conversion type ");
            throw new CannotParse2DDocException("Invalid conversion type");
        }
        // Strategy context running
        HdcStrategy strategy = (HdcStrategy) context.getBean(strategyClass);

        // if activated, repeated input conversion fraud check
        if ( guardEnabled && strategy instanceof Hdc2dDocToDgcaStrategy ){
            String hash = getSignatureFrom2DDoc(documentRequest.getChainEncoded());
            guardService.getHash(hash);

            String qrCode=strategy.decodeDocument(documentRequest.getChainEncoded());

            guardService.saveHash(hash);
            return qrCode;
        }

        return strategy.decodeDocument(documentRequest.getChainEncoded());
    }

    private Class<?> obtainStrategy(DocumentRequest documentRequest) {
        String key = hdcHelper.genereteKey(documentRequest.getSource(), documentRequest.getDestination());
        return legalConversions.get(key);
    }

    @Override
    public void afterPropertiesSet() {
        log.debug("afterPropertiesSet loading rules for health documents conversion type supported");
        // load all convertion type supported with our strategies
        legalConversions.put(
            hdcHelper.genereteKey(DocumentRequest.DocumentType.DEUX_D_DOC, DocumentRequest.DocumentType.DGCA),
            Hdc2dDocToDgcaStrategy.class);
        legalConversions.put(
            hdcHelper.genereteKey(DocumentRequest.DocumentType.ICAO, DocumentRequest.DocumentType.DGCA),
            HdcIcaoToDgcaStrategy.class);
    }

    public String getSignatureFrom2DDoc(String chainEncoded) {
        try{
            String decodedString = URLDecoder.decode(chainEncoded, StandardCharsets.UTF_8);
            String signedData = StringUtils.substringBeforeLast(decodedString, "\u001F" );
            return DigestUtils.sha256Hex(signedData);
        }catch (Exception e){
            throw new CannotParse2DDocException(HASH_EXTRACTION_ERROR);
        }
    }

}
