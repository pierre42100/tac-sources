package com.ingroupe.healthdocconvertor.service;

import com.ingroupe.healthdocconvertor.dto.DocumentRequest;
import com.ingroupe.healthdocconvertor.exception.CannotParse2DDocException;

public interface HealthDocumentConvertorService {

    String decodeDocument(DocumentRequest documentRequest) throws CannotParse2DDocException;

}
