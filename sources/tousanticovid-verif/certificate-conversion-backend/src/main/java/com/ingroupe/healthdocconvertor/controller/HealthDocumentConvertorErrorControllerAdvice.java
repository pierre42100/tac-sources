package com.ingroupe.healthdocconvertor.controller;

import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import com.ingroupe.healthdocconvertor.dto.ErrorResponse;
import com.ingroupe.healthdocconvertor.exception.*;

import feign.FeignException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

@ControllerAdvice
@Slf4j
public class HealthDocumentConvertorErrorControllerAdvice {

    @Value("${hdc.connector.dgcIssuance.path}")
    private String issuanceEndpoint;

    @Value("${hdc.connector.deuxDDoc.baseUrl}")
    private String documentVerifierEndpoint;


    // Parameter errors
    @ExceptionHandler(HttpMessageNotReadableException.class)
    public @ResponseBody ResponseEntity<ErrorResponse> handleHttpMessageNotReadableException(HttpMessageNotReadableException e) {
        ErrorResponse errorResponse = buildUnexpectedMessage(e);

        if( e.getCause() instanceof InvalidFormatException) {
            InvalidFormatException exceptionCause = (InvalidFormatException) e.getCause();
            String invalidField = exceptionCause.getPath().get(0).getFieldName();

            if ( invalidField.equals("source") || invalidField.equals("destination") )
                errorResponse = buildErrorParameterMessage(invalidField);
        }

        if( e.getMessage() != null && e.getMessage().startsWith("Required request body is missing") )
                 errorResponse = buildNullInputMessage();

        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
            .body(errorResponse);
    }

    @ExceptionHandler({Invalid2DDocPayloadException.class})
    public ResponseEntity<ErrorResponse> handlerInvalidTwoDDocPayloadException(Invalid2DDocPayloadException e) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
            .body(buildErrorResponse("INCORRECT_PARAMETER","The value of the payload is not supported"));
    }


    // Parsing errors
    @ExceptionHandler(Invalid2DDocSignatureException.class)
    public ResponseEntity<ErrorResponse> handleInvalidTwoDDocSignatureException(Invalid2DDocSignatureException e) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
            .body(buildErrorResponse("INCORRECT_PAYLOAD_SIGNATURE", "The signature of the payload is not valid"));
    }

    @ExceptionHandler(CannotParse2DDocException.class)
    public ResponseEntity<ErrorResponse> handleCannotParseTwoDDocException(CannotParse2DDocException e) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
            .body(buildErrorResponse("CANNOT_PARSE_2DDOC", e.getMessage()));
    }


    // Functional errors
    @ExceptionHandler({Invalid2DDocInputValueException.class})
    public ResponseEntity<ErrorResponse> handlerInvalid2DDocInputValue(Invalid2DDocInputValueException e) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
            .body(buildErrorResponse("INCORRECT_INPUT_VALUE",e.getMessage()));
    }

    @ExceptionHandler({BlacklistedTwoDDocException.class})
    public ResponseEntity<ErrorResponse> handlerBlacklistedTwoDDocException(BlacklistedTwoDDocException e) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
            .body(buildErrorResponse("BLACKLISTED_DOCUMENT",e.getMessage()));
    }

    @ExceptionHandler({TooManyRequestsException.class})
    public ResponseEntity<ErrorResponse> handlerIllegalInputException(TooManyRequestsException e) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND)
            .body(buildErrorResponse("TOO_MANY_REQUESTS",e.getMessage()));
    }


    // Other errors
    @ExceptionHandler(Exception.class)
    public ResponseEntity<ErrorResponse> handleException(Exception e) {
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
            .body( buildUnexpectedMessage(e) );
    }

    @ExceptionHandler(FeignException.class)
    public ResponseEntity<ErrorResponse> handleFeignException(FeignException e) {
        log.error("Error during conversion: response status [{}] from [{}]", e.status(), e.request().requestTemplate().path());

        if ( e.request().requestTemplate().path().contains(issuanceEndpoint) )
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body( buildErrorResponse( "ISSUANCE_API_ERROR", "Issuance external service response error" ));

        if ( e.request().requestTemplate().path().contains(documentVerifierEndpoint) )
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body( buildErrorResponse( "VERIFICATION_API_ERROR", "Verification external service response error" ));

        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
            .body( buildUnexpectedMessage(e) );
    }


    private ErrorResponse buildErrorParameterMessage(String parameterName) {
        return buildErrorResponse("INCORRECT_PARAMETER", "The value of the "+parameterName+" parameter is not supported");
    }

    private ErrorResponse buildNullInputMessage() {
        return buildErrorResponse("INCORRECT_PARAMETER", "The value of the 2DDoc parameter is null");
    }

    private ErrorResponse buildUnexpectedMessage(Exception e) {
        e.printStackTrace();
        return buildErrorResponse("UNEXPECTED_ERROR", "Error during conversion");
    }

    private ErrorResponse buildErrorResponse(String codeError, String msgError) {
        ErrorResponse errorResponse = new ErrorResponse();
        errorResponse.setCodeError(codeError);
        log.error(msgError);
    return errorResponse;
    }


}
