package com.ingroupe.healthdocconvertor.connector.deuxddoc.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Signature {
    @JsonProperty("isValid")
    private boolean valid;
    private String status;
}
