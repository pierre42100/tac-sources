package com.ingroupe.healthdocconvertor.controller;

import com.ingroupe.healthdocconvertor.dto.HdcEntry;
import com.ingroupe.healthdocconvertor.service.GuardService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@Slf4j
@ConditionalOnProperty(value="hdc.guard.enabled", havingValue="true")
public class GuardController {

    @Autowired
    private GuardService guardService;

    @GetMapping(value="guard/{hash}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HdcEntry> getHash(@PathVariable String hash) {
        HdcEntry response = guardService.getHash(hash);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PutMapping(value="guard/{hash}")
    public ResponseEntity<HdcEntry> saveHash(@PathVariable String hash) {
        return new ResponseEntity<>(guardService.saveHash(hash), HttpStatus.OK);
    }

    @DeleteMapping(value="guard/{hash}")
    public ResponseEntity<?> deleteHash(@PathVariable String hash) {
        HdcEntry hdcEntry = guardService.deleteHash(hash);
        if ( hdcEntry != null && hdcEntry.getHash().equals(hash) )
            return new ResponseEntity<>("", HttpStatus.OK);
        else
            return new ResponseEntity<>("", HttpStatus.NOT_FOUND);
    }

    @DeleteMapping(value="guard/")
    public ResponseEntity<?> deleteAll() {
        guardService.deleteAll();
        return new ResponseEntity<>("", HttpStatus.OK);
    }
}
