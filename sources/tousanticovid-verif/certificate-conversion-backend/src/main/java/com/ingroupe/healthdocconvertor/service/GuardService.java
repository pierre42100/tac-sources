package com.ingroupe.healthdocconvertor.service;

import com.ingroupe.healthdocconvertor.dto.HdcEntry;
import com.ingroupe.healthdocconvertor.exception.TooManyRequestsException;
import com.ingroupe.healthdocconvertor.repository.GuardRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class GuardService {

    private final GuardRepository guardRepository;

    @Value("${hdc.guard.maxInputConversions}")
    private int maxInputConversions;

    private final String MAX_THRESHOLD_EXCEEDED = "2DDOC conversion exceeded threshold";


    public HdcEntry getHash(String hash){
        HdcEntry hdcEntry=guardRepository.findByHash(hash);
        return (hdcEntry== null) ? new HdcEntry() : hdcEntry;
    }

    public HdcEntry saveHash(String hash){
        HdcEntry hdcEntry=guardRepository.findByHash(hash);

        if (hdcEntry == null)
            hdcEntry = new HdcEntry();
        else if ( hdcEntry.getSuccessfulConversions() >= maxInputConversions )
            throw new TooManyRequestsException(MAX_THRESHOLD_EXCEEDED);

        hdcEntry.setHash(hash);
        hdcEntry.updateDate();
        hdcEntry.incrementSuccessfulConversions();
        guardRepository.save(hdcEntry);

        return hdcEntry;
    }

    public HdcEntry deleteHash(String hash){
        HdcEntry hdcEntry=guardRepository.deleteByHash(hash);
        return (hdcEntry == null) ? new HdcEntry() : hdcEntry;
    }

    public void deleteAll(){
        guardRepository.deleteAll();
    }

}
