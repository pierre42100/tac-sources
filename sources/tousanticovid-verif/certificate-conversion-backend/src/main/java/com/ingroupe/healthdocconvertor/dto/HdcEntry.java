package com.ingroupe.healthdocconvertor.dto;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Getter
@Document(collection = "hdc-guard")
public class HdcEntry {

    @Id
    @Setter
    private String hash;

    private int successfulConversions;

    private String lastConversionDate;

    public HdcEntry(){
        this.hash="";
        this.successfulConversions=0;
        this.lastConversionDate="";
    }

    public void updateDate(){
        LocalDateTime now = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        lastConversionDate = now.format(formatter);
    }

    public void incrementSuccessfulConversions() {
        this.successfulConversions++;
    }

}
