package com.ingroupe.healthdocconvertor.connector.deuxddoc.dto;

public enum AttestationType {

    RECOVERY,
    TEST,
    VACCINATION

}
