package com.ingroupe.healthdocconvertor.exception;

import com.ingroupe.healthdocconvertor.connector.deuxddoc.dto.DocumentDto;

public class CannotParse2DDocException extends RuntimeException {

    private final DocumentDto inputTwoDDoc;

    public CannotParse2DDocException(String message) {
        super(message);
        this.inputTwoDDoc = null;
    }

    public CannotParse2DDocException(DocumentDto inputTwoDDoc, String message) {
        super(message);
        this.inputTwoDDoc = inputTwoDDoc;
    }

    public DocumentDto getInputTwoDDoc() {
        return inputTwoDDoc;
    }

}
