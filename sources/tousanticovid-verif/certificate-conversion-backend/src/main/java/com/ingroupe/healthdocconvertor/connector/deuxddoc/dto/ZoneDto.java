package com.ingroupe.healthdocconvertor.connector.deuxddoc.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ZoneDto {
    private String version;

    private String type;

    private String label;

    private String error;

    private List<FieldDto> fields = new ArrayList<>();
}
