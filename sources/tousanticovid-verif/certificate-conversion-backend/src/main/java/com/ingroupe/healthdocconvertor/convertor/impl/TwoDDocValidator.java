package com.ingroupe.healthdocconvertor.convertor.impl;

import com.ingroupe.healthdocconvertor.connector.deuxddoc.dto.AttestationType;
import com.ingroupe.healthdocconvertor.connector.deuxddoc.dto.DocumentDto;
import com.ingroupe.healthdocconvertor.connector.deuxddoc.dto.TwoDDocHealthCertResponseFacade;
import com.ingroupe.healthdocconvertor.exception.BlacklistedTwoDDocException;
import com.ingroupe.healthdocconvertor.exception.Invalid2DDocException;
import com.ingroupe.healthdocconvertor.exception.Invalid2DDocInputValueException;
import com.ingroupe.healthdocconvertor.exception.Invalid2DDocSignatureException;

import static com.ingroupe.healthdocconvertor.convertor.impl.VaccinationCycleState.COMPLETE;
import static com.ingroupe.healthdocconvertor.convertor.impl.VaccinationCycleState.forTwoDDocState;


public class TwoDDocValidator {

    private final DocumentDto twoDDoc;

    public TwoDDocValidator(DocumentDto twoDDoc) {
        this.twoDDoc = twoDDoc;
    }

    void validate() {
        if (twoDDoc == null)
            throw new Invalid2DDocException();

        TwoDDocHealthCertResponseFacade facade = new TwoDDocHealthCertResponseFacade(twoDDoc);

        if (!facade.isSignatureValid())
            throw new Invalid2DDocSignatureException();

        if (facade.isSignatureBlackListed())
            throw new BlacklistedTwoDDocException();

        if (facade.getAttestationType() == AttestationType.VACCINATION) {
            VaccinationCycleState cycleState = forTwoDDocState(facade.getVaccinationCycleState());
            if (cycleState == COMPLETE && !injectedTheRequiredDoseNumber(facade))
                throw new Invalid2DDocInputValueException(cycleState,
                    Integer.parseInt(facade.getVaccineDoseNumber()),
                    Integer.parseInt(facade.getVaccineTotalDoses()), "The vaccination state is not consistent with the number of injections");
        }
    }


    private boolean injectedTheRequiredDoseNumber(TwoDDocHealthCertResponseFacade facade) {
        return facade.getVaccineDoseNumber().equals(facade.getVaccineTotalDoses());
    }

}
