package com.ingroupe.healthdocconvertor.convertor.impl;

import com.ingroupe.healthdocconvertor.exception.Invalid2DDocInputValueException;
import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;

public enum VaccineType {

    COMIRNATY("COMIRNATY", "EU/1/20/1528", "ORG-100030215"),
    MODERNA("MODERNA", "EU/1/20/1507", "ORG-100031184"),
    ASTRA_ZENECA("ASTRAZENECA", "EU/1/21/1529", "ORG-100001699"),
    JANSSEN("JANSSEN", "EU/1/20/1525", "ORG-100001417");

    private final String twoDDocName;
    private final String dgcName;
    private final String manufacturerName;

    VaccineType(String twoDDocName, String dgcName, String manufacturerName) {
        this.twoDDocName = twoDDocName;
        this.dgcName = dgcName;
        this.manufacturerName = manufacturerName;
    }

    public static VaccineType forTwoDDocName(String twoDDocName) {
        return Arrays.stream(VaccineType.values())
            .filter(v -> StringUtils.containsIgnoreCase(twoDDocName, v.twoDDocName))
            .findFirst()
            .orElseThrow(() ->
                new Invalid2DDocInputValueException("No corresponding type has been found for the vaccine name"));
    }

    public String getTwoDDocName() {
        return twoDDocName;
    }

    public String getDgcName() {
        return dgcName;
    }

    public String getManufacturerName() {
        return manufacturerName;
    }
}
