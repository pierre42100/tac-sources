package com.ingroupe.healthdocconvertor.util;

import java.time.ZoneOffset;

public class DateUtils {

    public static java.time.LocalDate javaLocalDateOf(kotlinx.datetime.LocalDate input) {
        return java.time.LocalDate.of(input.getYear(), input.getMonth(), input.getDayOfMonth());
    }

    public static java.time.LocalDateTime javaLocalDateTimeOf(kotlinx.datetime.Instant input) {
        return java.time.LocalDateTime.ofEpochSecond(input.getEpochSeconds(), 0, ZoneOffset.UTC);
    }

    public static java.time.ZonedDateTime javaZonedDateTimeOf(kotlinx.datetime.Instant input) {
        return javaLocalDateTimeOf(input).atZone(ZoneOffset.UTC);
    }

    public static kotlinx.datetime.LocalDate kotlinLocalDateTimeOf(java.time.LocalDate date) {
        return new kotlinx.datetime.LocalDate(date);
    }

    public static kotlinx.datetime.Instant kotlinInstantOf(java.time.OffsetDateTime date) {
        return kotlinx.datetime.Instant.Companion.fromEpochSeconds(date.toEpochSecond(), 0);
    }

}
