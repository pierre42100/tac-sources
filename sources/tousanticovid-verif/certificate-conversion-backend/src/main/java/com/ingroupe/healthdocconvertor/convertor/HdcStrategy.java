package com.ingroupe.healthdocconvertor.convertor;

import com.ingroupe.healthdocconvertor.exception.CannotParse2DDocException;

public interface HdcStrategy {

    String decodeDocument(String chainEncoded) throws CannotParse2DDocException;

}
