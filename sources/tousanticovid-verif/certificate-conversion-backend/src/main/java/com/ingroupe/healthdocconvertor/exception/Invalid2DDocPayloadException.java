package com.ingroupe.healthdocconvertor.exception;

public class Invalid2DDocPayloadException extends RuntimeException {
    public Invalid2DDocPayloadException()
    {
        super("The value of the payload is not supported");
    }
}
