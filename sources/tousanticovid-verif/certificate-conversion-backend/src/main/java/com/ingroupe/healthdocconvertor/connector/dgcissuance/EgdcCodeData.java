package com.ingroupe.healthdocconvertor.connector.dgcissuance;

import lombok.Data;

@Data
public class EgdcCodeData {
    String dgci;
    String qrCode;
    String tan;
}
