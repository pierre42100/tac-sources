package com.ingroupe.healthdocconvertor.controller;

import com.ingroupe.healthdocconvertor.dto.DocumentRequest;
import com.ingroupe.healthdocconvertor.service.HealthDocumentConvertorService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequestMapping("/decode")
@Slf4j
public class HealthDocumentConvertorController {

    @Autowired
    private HealthDocumentConvertorService healthDocumentConvertorService;

    @Operation(summary = "Decode different documents (2D_DOC, ICAO) to DGCA, ICAO...", description = "Validate and decode different documents (2D_DOC, ICAO) to DGCA, ICAO")
    @PostMapping(value = "decodeDocument", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "chain correctly decoded and converted"),
            @ApiResponse(responseCode = "400", description = "Bad request : Invalid chain to be encoded, Invalid type of conversion )"),
            @ApiResponse(responseCode = "500", description = "Internal server error)")
    })
    public ResponseEntity<?> decodeDocument(@RequestBody DocumentRequest documentRequest) {
        String response;
        try
        {
            MDC.put("reqId", UUID.randomUUID().toString());
            log.info("Received request");
            response = healthDocumentConvertorService.decodeDocument(documentRequest);
            log.info("Conversion executed successfully");
        }
        finally
        {
            MDC.clear();
        }

        return new ResponseEntity<>(response, HttpStatus.OK);

    }

}
