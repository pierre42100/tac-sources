package com.ingroupe.healthdocconvertor.connector.deuxddoc;

import lombok.RequiredArgsConstructor;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Configuration;

@Configuration
@RequiredArgsConstructor
@EnableFeignClients
public class TwoDDocDecoderConnectorConfig {

}
