package com.ingroupe.healthdocconvertor.connector.deuxddoc.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ErrorDto {
    private String documentNumber;

    private String error;

    private String message;
}
