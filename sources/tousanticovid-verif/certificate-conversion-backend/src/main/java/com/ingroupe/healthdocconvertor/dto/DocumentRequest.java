package com.ingroupe.healthdocconvertor.dto;

import lombok.Data;

@Data
public class DocumentRequest {

    DocumentType source;

    private String chainEncoded;

    DocumentType destination;

    public enum DocumentType {
        DGCA, DEUX_D_DOC, ICAO
    }

}
