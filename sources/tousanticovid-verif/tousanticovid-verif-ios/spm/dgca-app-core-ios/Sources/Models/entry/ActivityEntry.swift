//
//  ActivityEntry.swift
//  
//
//

import Foundation
import SwiftyJSON

struct ActivityEntry: HCertEntry {
    
    var info: [InfoSection] {
        []
    }
    
    var walletInfo: [InfoSection] {
        []
    }
    
    var validityFailures: [String] {
        return [String]()
    }
    
    enum Fields: String {
        case uvci = "ci"
    }
    
    init?(body: JSON) {
        guard
            let uvci = body[Fields.uvci.rawValue].string
        else {
            return nil
        }
        self.uvci = uvci
        self.typeAddon = ""
    }
    var uvci: String
    var typeAddon: String
    
}
