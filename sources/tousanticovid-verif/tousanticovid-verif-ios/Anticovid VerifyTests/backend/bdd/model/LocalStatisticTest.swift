//
//  LocalStatisticTest.swift
//  Anticovid VerifyTests
//
//

import Foundation
import CoreData
import XCTest
@testable import Anticovid_Verify

class LocalStatisticTest: XCTestCase {

    let context = TestCoreDataStack().persistentStatContainer.newBackgroundContext()
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }

    func testCalculateNumberOfScan() {
        let localStatistic = LocalStatistic(context: context)
        localStatistic.initialize(date: Date())

        XCTAssert(localStatistic.calculateNumberOfScan() == 0)

        localStatistic.invalid = 1
        localStatistic.double = 6 //Not to be taken into consideration in arithmetic
        localStatistic.valid = 6
        localStatistic.toDecide = 4

        XCTAssert(localStatistic.calculateNumberOfScan() == 11)
    }
}
