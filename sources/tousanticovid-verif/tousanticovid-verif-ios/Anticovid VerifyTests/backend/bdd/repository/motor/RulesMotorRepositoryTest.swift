//
//  RulesMotorRepositoryTest.swift
//  Anticovid VerifyTests
//
//

import Foundation
import CoreData
import XCTest
@testable import Anticovid_Verify

class RulesMotorRepositoryTest: XCTestCase {
    
    let mockBDDConfig = MockBDDConfig()
    
    var context: NSManagedObjectContext!

    var rulesMotorRepository: RulesMotorRepository! = nil
    
    override func setUp() {
        super.setUp()
        self.context = mockBDDConfig.getMotorContext()
        try? self.context.save()
        
        rulesMotorRepository = RulesMotorRepository(bddConfig: mockBDDConfig)
    }
    
    //    func testSaveReturnRulesMotor() {
    //        let mockData = getMockData()
    //        let rulerMotor = rulesMotorRepository.save(
    //            validity: mockData.validity,
    //            specificValueList: mockData.specificValueList
    //        )
    //
    //        XCTAssertNotNil(rulerMotor)
    //    }
    
    //    func testGetMotorRulesMotorWhenAllIsGood() {
    //        let mockData = getMockData()
    //        let rulerMotorEntity = rulesMotorRepository.save(
    //            validity: mockData.validity,
    //            specificValueList: mockData.specificValueList
    //        )
    //
    //        XCTAssertNotNil(rulerMotorEntity)
    //
    //        let rulesMotor = rulesMotorRepository.getMotorRules()
    //
    //        XCTAssertNotNil(rulesMotor)
    //    }
    
    func testDeletePreviousRulesMotorWhenAllIsGood() {
        let mockData = getMockData()
        let rulerMotorEntity = try? rulesMotorRepository.save(
            notGreenCountries: [], validity: mockData.validity,
            specificValueList: mockData.specificValueList
        )
        
        XCTAssertNotNil(rulerMotorEntity)
        
        let isDeleted = rulesMotorRepository.deletePreviousRules()
        
        XCTAssertTrue(isDeleted)
        
        let rulesMotor = rulesMotorRepository.getMotorRules()
        
        XCTAssertNil(rulesMotor)
    }
    
    private func getMockData() -> (validity: Validity, specificValueList: SpecificValueList) {
        let validity = Validity(
            testNegativePcrEndHour: 0,
            testNegativeAntigenicEndHour: 0,
            testPositivePcrEndDay: 0,
            testPositivePcrStartDay: 0,
            testPositiveAntigenicStartDay: 0,
            testPositiveAntigenicEndDay: 0,
            recoveryStartDay: 0,
            recoveryEndDay: 0,
            vaccineDelay: 0,
            vaccineDelayJanssen: 0,
            vaccineDelayMax: 0,
            vaccineDelayMaxJanssen: 0,
            vaccineBoosterDelay: 0,
            vaccineBoosterDelayNew: 0,
            vaccineBoosterDelayMax: 0,
            vaccineBoosterAge: 0,
            vaccineBoosterDelayUnderAge: 0,
            vaccineBoosterDelayUnderAgeNew:0,
            vaccineDelayMaxRecovery: 0,
            vaccineBoosterToggleDate: ""
        )

        
        let specificValueList = SpecificValueList(
            testPcrList: [:],
            testAntigenicList: [:],
            vaccineMedicalProductList: [:],
            vaccineProphylaxisList: [:],
            vaccineManufacturerList: [:],
            testManufacturerList: [:]
        )
        
        return (validity, specificValueList)
    }
}
