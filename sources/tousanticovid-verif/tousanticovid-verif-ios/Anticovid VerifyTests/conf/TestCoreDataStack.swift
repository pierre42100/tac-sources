//
//  TestCoreDataStack.swift
//  Anticovid VerifyTests
//
//

import Foundation
import CoreData
@testable import Anticovid_Verify

class TestCoreDataStack: NSObject {
    lazy var persistentStatContainer: NSPersistentContainer = {
        let description = NSPersistentStoreDescription()
        description.url = URL(fileURLWithPath: "/dev/null")
        let container = NSPersistentContainer(name: "Statistic")
        container.persistentStoreDescriptions = [description]
        container.loadPersistentStores { _, error in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        }
        return container
    }()
    
    lazy var persistentMotorContainer: NSPersistentContainer = {
        let description = NSPersistentStoreDescription()
        description.url = URL(fileURLWithPath: "/dev/null")
        let container = NSPersistentContainer(name: "MotorV3")
        container.persistentStoreDescriptions = [description]
        container.loadPersistentStores { _, error in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        }
        return container
    }()
}

class MockBDDConfig: BDDConfig {

    let testCoreDataStack = TestCoreDataStack()
    
    override func getStatContext() -> NSManagedObjectContext {
        return testCoreDataStack.persistentStatContainer.viewContext
    }
    
    override func getMotorContext() -> NSManagedObjectContext {
        return testCoreDataStack.persistentMotorContainer.viewContext
    }
}
