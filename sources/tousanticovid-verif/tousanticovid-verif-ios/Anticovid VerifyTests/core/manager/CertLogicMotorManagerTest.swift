//
//  CertLogicMotorManagertest.swift
//  Anticovid VerifyTests
//
//

import Foundation
import XCTest
import CertLogic
import SwiftDGC
import SwiftyJSON
@testable import Anticovid_Verify

class CertLogicMotorManagerTest: XCTestCase {
    
    var certLogicMotorManager: CertLogicMotorManager!
    
    var motorRulesRepository: MockCertMotorRulesRepository!
    
    override func setUp() {
        super.setUp()
        
        motorRulesRepository = MockCertMotorRulesRepository()

        certLogicMotorManager = CertLogicMotorManager(
            certLogicMotorRepository: motorRulesRepository
        )
    }
    
    func testCanUseCertLogicEngineReturnTrueIfMotorRulesRepositoryReturnFillArrayAndSetValueReturnFillArray() {
        motorRulesRepository.rules = [getMockRule()]
        motorRulesRepository.valueSet = [getValueSet()]
        
        XCTAssertTrue(certLogicMotorManager.canUseCertLogicEngine())
    }
    
    func testCanUseCertLogicEngineReturnFalseIfMotorRulesRepositoryReturnEmptyArrayAndSetValueReturnEmptyArray() {
        motorRulesRepository.rules = []
        motorRulesRepository.valueSet = []
        
        XCTAssertFalse(certLogicMotorManager.canUseCertLogicEngine())
    }
    
    func testCanUseCertLogicEngineReturnFalseIfMotorRulesRepositoryReturnEmptyArrayAndSetValueReturnFillArray() {
        motorRulesRepository.rules = []
        motorRulesRepository.valueSet = [getValueSet()]
        
        XCTAssertFalse(certLogicMotorManager.canUseCertLogicEngine())
    }
    
    func testCanUseCertLogicEngineReturnFalseIfMotorRulesRepositoryReturnFillArrayAndSetValueReturnEmptyArray() {
        motorRulesRepository.rules = [getMockRule()]
        motorRulesRepository.valueSet = []
        
        XCTAssertFalse(certLogicMotorManager.canUseCertLogicEngine())
    }
    
    func testGetCertificationTypeReturnRecoveryWhenHCertIsRecovery() {
        XCTAssertEqual(certLogicMotorManager.getCertificationType(type: .recovery), .recovery)
    }
    
    func testGetCertificationTypeReturnTestWhenHCertIsTest() {
        XCTAssertEqual(certLogicMotorManager.getCertificationType(type: .test), .test)
    }
    
    func testGetCertificationTypeReturnVaccinationWhenHCertIsVaccine() {
        XCTAssertEqual(certLogicMotorManager.getCertificationType(type: .vaccine), .vaccination)
    }
    
    func testGetCertificationTypeReturnGeneralWhenHCertIsUnknow() {
        XCTAssertEqual(certLogicMotorManager.getCertificationType(type: .unknown), .general)
    }
    
//    func testSetRulesAndSetValues() {
//        certLogicMotorManager.setRulesAndSetValues(with: [getMockRule()], and: [])
//
//        XCTAssertTrue(motorRulesRepository.deletePreviouValueSetCall)
//        XCTAssertTrue(motorRulesRepository.saveValueSetCall)
//
//        XCTAssertTrue(motorRulesRepository.deletePreviousRulesCall)
//        XCTAssertTrue(motorRulesRepository.saveRulesCall)
//    }
    
    func testGetRuleDetailsError() {
        let result = certLogicMotorManager.getRuleDetailsError(
            rule: getMockRule(),
            filter: FilterParameter(validationClock: Date(), countryCode: "", certificationType: .general)
        )
        
        XCTAssertNotNil(result)
    }

    private func getMockRule() -> Rule {
        return Rule(
            identifier: "",
            type: "",
            version: "",
            schemaVersion: "",
            engine: "",
            engineVersion: "",
            certificateType: "",
            description: [],
            validFrom: "",
            validTo: "",
            affectedString: [],
            logic: JSON(),
            countryCode: ""
        )
    }
    
    private func getValueSet() -> ValueSet {
        return ValueSet(valueSetId: "", valueSetDate: "", valueSetValues: [:])
    }
}


class MockCertMotorRulesRepository: CertLogicMotorRepository {
    
    // ValueSet
    var valueSet: [ValueSet] = []
    override func getValueSets() -> [ValueSet] {
        return valueSet
    }
    
//    var deletePreviouValueSetCall: Bool = false
//    override func deletePreviousValueSet() -> Bool {
//        deletePreviouValueSetCall.toggle()
//        return deletePreviouValueSetCall
//    }
    
    var saveValueSetCall: Bool = false
    override func save(valueSets: [CertLogic.ValueSet]) throws -> [CertLogicValueSetMotor] {
        saveValueSetCall.toggle()
        return []
    }
    
    // Rules
    var rules: [Rule] = []
    override func getRules() -> [Rule] {
        return rules
    }

//    var deletePreviousRulesCall: Bool = false
//    override func deletePreviousRules() -> Bool {
//        deletePreviousRulesCall.toggle()
//        return deletePreviousRulesCall
//    }
    
    var saveRulesCall: Bool = false
    override func save(rules: [CertLogic.Rule]) throws -> [CertLogicRulesMotor] {
        saveRulesCall.toggle()
        return []
    }
}
