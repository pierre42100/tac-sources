//
//  MockNotificationCenter.swift
//  Anticovid VerifyTests
//
//

import Foundation
@testable import Anticovid_Verify

class MockNotificationCenter: NotificationCenter {

    var notificationName: String?
    override func post(name aName: NSNotification.Name, object anObject: Any?) {
        self.notificationName = aName.rawValue
    }
}


