//
//  Const+Stat.swift
//  Anticovid Verify
//
//

import Foundation

class ConstStat {
    static let SEPARATOR = "|"
    static let TACV_2DDOC_IOS_LITE = "TACV_2DDOC_IOS_LITE"
    static let TACV_2DDOC_IOS_OT = "TACV_2DDOC_IOS_OT"
    static let TACV_2DDOC_IOS_PLUS = "TACV_2DDOC_IOS_PLUS"
    static let D_DOC_B2 = "2D-DOC_B2"
    static let D_DOC_L1 = "2D-DOC_L1"
    static let DCC_TEST = "DCC_TEST"
    static let DCC_VACCINATION = "DCC_VACCINATION"
    static let DCC_RECOVERY = "DCC_RECOVERY"
    static let DCC_EXEMPTION = "DCC_EXEMPTION"
    static let DCC_ACTIVITY = "DCC_ACTIVITY"
}
