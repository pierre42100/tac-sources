//
//  Const+Conf.swift
//  Anticovid Verify
//
//

import Foundation

class ConstConf {
    static let idApp = Bundle.main.infoDictionary?["id_app"] as? String ?? ""
    static let build = Bundle.main.infoDictionary?["CFBundleVersion"] as? String ?? ""
    static let urlWs = Bundle.main.infoDictionary?["url_ws"] as? String
    static let urlAssets = Bundle.main.infoDictionary?["url_as"] as? String ?? ""
    static let urlLegal = Bundle.main.infoDictionary?["url_le"] as? String ?? ""
    static let publicKey: String = (Bundle.main.infoDictionary?["public_key"] as? String) ?? ""
    static let tokenLite: String = (Bundle.main.infoDictionary?["token_lite"] as? String) ?? ""
    static let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
    static let licSC = Bundle.main.infoDictionary?["lic_sc"] as? String ?? ""
    static let filelogEnabled = Bundle.main.infoDictionary?["filelogEnabled"] as? Bool ?? false
    static let canScreen = Bundle.main.infoDictionary?["canScreen"] as? Bool ?? false
    static let passCovidDocumentTypes = ["B2", "L1"]
    static let clientControlTypeLite = ""
    static let clientControlTypeOT = ""
    static let cgu = "\(ConstConf.urlAssets)v1608654186/IN%20Verify%20CGU/tac-cgu-application-mobile-verify.pdf"
    static let privacyPolicy = "\(ConstConf.urlAssets)tac-boqpocuqwwl9zywnhewl.pdf"
    static let decret = "\(ConstConf.urlLegal)JORFDOLE000043426698/?detailType=CONTENU&detailId=1"
} 
