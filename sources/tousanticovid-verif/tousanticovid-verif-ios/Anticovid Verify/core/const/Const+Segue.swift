//
//  Const+Segue.swift
//  Anticovid Verify
//
//

import Foundation

struct ConstSegue {
    static let tutorialResultSegue = "tutorialResultSegue"
    static let resultSegue = "resultSegue"
}
