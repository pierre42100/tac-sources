//
//  ResultState.swift
//  Anticovid Verify
//
//

import Foundation
import UIKit

enum CertificateState {
    case valid
    case nonValid
    case testExpired
    
    case undetermined
    case notConform
    case DDOCNotAuthentic
    case DCCNotauthentic
    case inprogress
    case finished
    
    case negative
    case positiveValid
    case positiveNotValid
    case testTypeNonValid
    case testPcrNegativeDetail
    case testPcrPositiveDetail
    case testAntigenicNegativeDetail
    case testAntigenicPositiveDetail
    case testAntigenicNegativeInvalid
    case testAntigenicPositiveInvalid
    
    case recoveryValid
    case recoveryNotValid
    
    case exampted
    case activity
    
    case vaccinalCycleComplet
    case vaccinalCycleNotConform
    
    case expired
    case inactive
    
    case blacklisted
}

extension CertificateState {
    
    func getImage() -> UIImage? {
        switch self {
        case .negative, .positiveValid, .finished, .valid :
            return UIImage(named: "check")
        case .testExpired, .positiveNotValid, .undetermined, .notConform, .DDOCNotAuthentic, .DCCNotauthentic, .testTypeNonValid, .inprogress, .nonValid, .recoveryNotValid, .vaccinalCycleNotConform, .expired, .inactive, .blacklisted :
            return UIImage(named: "wrong")
        default:
            return nil
        }
    }
    
    func getColor() -> UIColor {
        switch self {
        case .negative, .positiveValid, .finished, .valid :
            return .greenIn
        case .testPcrNegativeDetail,
             .testPcrPositiveDetail,
             .testAntigenicNegativeDetail,
             .testAntigenicPositiveDetail,
             .testAntigenicNegativeInvalid,
             .testAntigenicPositiveInvalid,
             .recoveryValid,
             .exampted,
             .activity,
             .vaccinalCycleComplet:
            return .blueIn
        default:
            return .redIn
        }
    }
    
    func getName() -> String {
        switch self {
        case .valid: return "Valide"
        case .nonValid: return "Non valide"
        case .testExpired: return "Test périmé"
            
        case .undetermined: return "Résultat indéterminé"
        case .notConform: return "Prélévement non conforme"
        case .DDOCNotAuthentic: return "2D-DOC non valide\nDonnées non certifiées"
        case .DCCNotauthentic: return "QR Code non valide\nDonnées non certifiées"

        case .inprogress, .vaccinalCycleNotConform: return "Cycle vaccinal non conforme"
        case .finished: return "Cycle vaccinal valide"
            
        case .negative: return "Test négatif"
        case .positiveValid: return "Test positif valide"
        case .positiveNotValid: return "Test positif non valide"
        case .testTypeNonValid: return "Type de test non valide"
        case .testPcrNegativeDetail: return "Test PCR négatif (voir durée)"
        case .testPcrPositiveDetail: return "Test PCR positif (voir durée - Invalide hors France)"
        case .testAntigenicNegativeDetail: return "TAg négatif (voir durée)"
        case .testAntigenicPositiveDetail: return "TAg positif (voir durée - Invalide hors France)"
        case .testAntigenicNegativeInvalid: return "TAg négatif (voir durée - Invalide hors France)"
        case .testAntigenicPositiveInvalid: return "TAg positif (voir durée - Invalide hors France)"

        case .recoveryValid: return "Test rétablissement (voir durée)"
        case .recoveryNotValid: return "Test rétablissement non valide"
        
        case .vaccinalCycleComplet: return "Cycle vaccinal complet (voir durée)"
        case .exampted: return "Pass contre-indication (voir dates - Invalide hors France)"
        case .activity: return "Pass éphémère (voir dates - Invalide hors France)"
        
        case .expired: return "Certificat expiré, données non certifiées"
        case .inactive: return "Certificat inactif"
        
        case .blacklisted: return "Certificat Frauduleux"
        }
    }
}
