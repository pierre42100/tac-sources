//
//  CertificateAdminState.swift
//  Anticovid Verify
//
//

import Foundation

enum CertificateAdminState: String {
    case blacklist = "Blacklist"
    case double = "Doublon"
    case blackListAndDouble = "BlacklistDoublon"
    
    func getTextInformation() -> (String, String) {
        switch self {
        case .blacklist, .blackListAndDouble:
            return UIDataManager.shared.getBlacklist()
        case .double:
            return UIDataManager.shared.getDuplicate()
        }
    }
}
