//
//  RemindStep.swift
//  Anticovid Verify
//
//

import Foundation

enum RemindStep {
    case none
    case low
    case medium
    case high
    
    var step: Int {
        switch self {
        case .none:
            return 0
        case .low:
            return 1
        case .medium:
            return 2
        case .high:
            return 3
        }
    }
}
