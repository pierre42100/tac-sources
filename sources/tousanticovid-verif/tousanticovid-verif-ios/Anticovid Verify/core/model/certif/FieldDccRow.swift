//
//  ResultRow.swift
//  Anticovid Verify
//
//

import UIKit
import CertLogic

enum ResultType : String {
    case admin, info, certLogic
}
    
enum ResultPicto : String {
    case none, eye = "eye", valid = "check",notValid = "wrong"
}

enum ResultTextColor : String {
    case normal, green = "greenIn", red = "redIn"
}

enum FieldRow {
    case Admin(
        title: String,
        content: String?
    )
    case Field(
        title: String,
        subtitle: String,
        picto: ResultPicto?
    )
    case Header(label: String)
    case Rule(label: String?, color: UIColor)
}

enum RulesValidation {
    case validate, neutral, invalidate
    
    func getIcon(with typeValidate: RulesValidation) -> String {
        switch typeValidate {
        case .validate:
            return "check_ok"
        case .neutral:
            return "neutral"
        case .invalidate:
            return "check_ko"
        }
    }
}

enum SectionRow: Hashable {
    case Label(title: String, color: UIColor? = nil, font: UIFont? = nil)
    case emptyHeader(id: String = UUID().uuidString)
    case Rules(country: String, validation: RulesValidation?)
}
