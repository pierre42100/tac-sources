//
//  LocalStatData.swift
//  Anticovid Verify
//
//

import Foundation

struct LocalStatData {
    let identifiant: String
    let typeValidation: DataTypeValidation
    let certificateAdminState: CertificateAdminState?
}

enum DataTypeValidation: String {
    case valid = "true"
    case invalid = "false"
    case to_decide = "null"
}
