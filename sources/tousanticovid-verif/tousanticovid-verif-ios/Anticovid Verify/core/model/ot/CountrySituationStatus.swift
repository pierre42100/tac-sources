//
//  CountrySituationStatus.swift
//  Anticovid Verify
//

import Foundation

enum CountrySituationStatus: CaseIterable {
    case green
    case orange
    case red
}
