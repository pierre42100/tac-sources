//
//  Formatter.swift
//  Anticovid Verify
//
//

import Foundation
import SwiftyJSON

class Formatter {
    
    static let shared = Formatter()
    
    let rulesMotorService: RulesService
    
    init(rulesMotorService: RulesService = .shared) {
        self.rulesMotorService = rulesMotorService
    }
    
    func vaccineName(from vaccineCode: String) -> String {
        guard let rules = rulesMotorService.getRules() else { return vaccineCode }
        
        if let vaccineName = rules.vaccineMedicalProductList?.stringValueForKeyInsensitive(key: vaccineCode) {
            return vaccineName
        }
        
        return vaccineCode
    }
    
    func analysisCodeFormatter(analysisCode: String?) -> String {
        guard let rules = rulesMotorService.getRules(), let analysisCode = analysisCode else { return analysisCode ?? "" }
        
        if let analysisCode = rules.testPcrList?.stringValueForKeyInsensitive(key: analysisCode) {
            return analysisCode
        }

        if let analysisCode = rules.testAntigenicList?.stringValueForKeyInsensitive(key: analysisCode) {
            return analysisCode
        }
        
        return analysisCode
    }
    
    func analysisResultFormatter(analysisResult: String?) -> String {
        switch analysisResult?.uppercased() {
        case "N":
            return "Négatif"
        case "P":
            return "Positif"
        case "X":
            return "Prélèvement non conforme"
        case "I":
            return "Indéterminé"
        case "260415000":
            return "Négatif"
        case "260373001":
            return "Positif"
        default:
            return ""
        }
    }
    
    func countryName(from countryCode: String) -> String {
        if let name = (Locale.current as NSLocale).displayName(forKey: .countryCode, value: countryCode) {
            return name
        } else {
            return countryCode
        }
    }
    
    func testTypeName(from testType: String) -> String {
        guard let rules = rulesMotorService.getRules() else { return testType }
        
        if rules.testPcrList?.stringValueForKeyInsensitive(key: testType) != nil {
            return "Test PCR Covid"
        }

        if rules.testAntigenicList?.stringValueForKeyInsensitive(key: testType) != nil {
            return "Test Antigénique Covid"
        }
        
        return testType
    }
    
    func diseaseName(from diseaseCode: String) -> String {
        switch diseaseCode {
        case "840539006":
            return "COVID-19"
        default:
            return diseaseCode
        }
    }
    
    func prophylacticAgentName(from prophylacticAgent: String) -> String {
        if let prophylacticAgentName = rulesMotorService.getRules()?.vaccineProphylaxisList?.stringValueForKeyInsensitive(key: prophylacticAgent) {
            return prophylacticAgentName
        }

        return prophylacticAgent
    }
    
    func manufacturerName(from manufacturerCode: String) -> String {
        guard let rules = rulesMotorService.getRules() else { return manufacturerCode }
        
        if let vaccinManufacturer = rules.vaccineManufacturerList?.stringValueForKeyInsensitive(key: manufacturerCode) {
            return vaccinManufacturer
        }
        
        if let testManufacturer = rules.testManufacturerList?[manufacturerCode] {
            return testManufacturer
        }

        return manufacturerCode
    }
    
}
