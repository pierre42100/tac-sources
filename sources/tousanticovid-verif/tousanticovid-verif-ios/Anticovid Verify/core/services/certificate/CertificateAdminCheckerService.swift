//
//  CertificateAdminCheckerService.swift
//  Anticovid Verify
//

import Foundation

class CertificateAdminCheckerService {
    static let shared = CertificateAdminCheckerService()
    
    let localStatRepository: LocalStatRepository
    let blackListRepository: BlackListRepository
    
    init(blackListRepository: BlackListRepository = .shared, localStatRepository: LocalStatRepository = .shared) {
        self.blackListRepository = blackListRepository
        self.localStatRepository = localStatRepository
    }
    
    public func getCertificateAdminState(for certHash: String, isDCC: Bool? = nil) -> CertificateAdminState? {
        try? localStatRepository.removeOldData()
        
        let blackList = self.blackListRepository.isBlackListed(certHash: certHash, isDCC: isDCC)
        let double = self.localStatRepository.certificateAlreadyScan(idHash: certHash) != nil
        
        if blackList && double { return .blackListAndDouble }
        if blackList { return .blacklist }
        if double { return .double }

        return nil
    }
}
