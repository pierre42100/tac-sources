//
//  RulesMotorService.swift
//  Anticovid Verify
//
//

import Foundation

typealias TestNegativeDelay = (
    pcr: Int,
    antigenic: Int,
    primoPcr: Int,
    primoAntigenic: Int
)

typealias TestPositiveDelay = (
    pcrStartDay: Int,
    pcrEnDay: Int,
    antigenicStartDay: Int,
    antigenicEndtDay: Int
)

typealias RecoveryDelay = (
    recoveryStartDay: Int,
    recoveryEndDay: Int
)

class RulesService {
    
    static var shared = RulesService()
    
    private let rulesMotorRepository: RulesMotorRepository
    
    private var rules: RulesMotor?
    
    init(rulesMotorRepository: RulesMotorRepository = .shared) {
        self.rulesMotorRepository = rulesMotorRepository
    }
    
    func getRules() -> RulesMotor? {
        if rules == nil {
            rules = self.rulesMotorRepository.getMotorRules()
        }
        return rules
    }
    
    func vaccinePassVerifierConfiguration() -> VaccinePassValidity? {
        _ = self.getRules()
        
        if let vaccinePassData = rules?.vaccinePassConfiguration,
           let config = try? JSONDecoder().decode(VaccinePassValidity.self, from: vaccinePassData) {
            return config
        }

        return nil
    }
    
    func healthPassVerifierConfiguration() -> HealthPassValidity? {
        _ = self.getRules()
        
        if let healthPassData = rules?.healthPassConfiguration,
           let config = try? JSONDecoder().decode(HealthPassValidity.self, from: healthPassData) {
            return config
        }

        return nil
    }
    
    func plusValidityConfiguration() -> PlusValidity? {
        _ = self.getRules()
        
        if let plusPassData = rules?.plusValidityConfiguration,
           let config = try? JSONDecoder().decode(PlusValidity.self, from: plusPassData) {
            return config
        }

        return nil
    }
    
    func save(specificValues: SpecificValue) throws {
        rulesMotorRepository.deletePreviousRules()
        rules = nil
        
        rules = try rulesMotorRepository.save(
            notGreenCountries: specificValues.notGreenCountries,
            validity: specificValues.validity,
            specificValueList: SpecificValueList(
                testPcrList: specificValues.testPcrList,
                testAntigenicList: specificValues.testAntigenicList,
                vaccineMedicalProductList: specificValues.vaccineMedicalProductList,
                vaccineProphylaxisList: specificValues.vaccineProphylaxisList,
                vaccineManufacturerList: specificValues.vaccineManufacturerList,
                testManufacturerList: specificValues.testManufacturerList
            )
        )
    }
    
    func isVaccineProphylaxis(prophylacticAgent: String) -> Bool {
        _ = self.getRules()
        let vaccineProphylaxisList = self.rules?.vaccineProphylaxisList ?? ConstRules.vaccineProphylaxisList
        return vaccineProphylaxisList.stringValueForKeyInsensitive(key: prophylacticAgent) != nil
    }
    
    func isVaccineMedicinalProduct(vaccineName: String) -> Bool {
        _ = self.getRules()
        let vaccineMedicalProductList = self.rules?.vaccineMedicalProductList ?? ConstRules.vaccineMedicalProductList
        return vaccineMedicalProductList.stringValueForKeyInsensitive(key: vaccineName) != nil
    }
    
    func vaccineMahManfContains(manufacturer: String) -> Bool {
        _ = self.getRules()
        let vaccineManufacturerList = self.rules?.vaccineManufacturerList ?? ConstRules.vaccineManufacturerList
        return vaccineManufacturerList.stringValueForKeyInsensitive(key: manufacturer) != nil
    }
    
    func testManfContains(manufacturer: String) -> Bool {
        _ = self.getRules()
        let testManufacturerList = self.rules?.testManufacturerList ?? ConstRules.testManufacturerList
        return testManufacturerList.stringValueForKeyInsensitive(key: manufacturer) != nil

    }
    
    func isTest(loinc: String) -> Bool {
        return isPcr(loinc: loinc) || isAntigenic(loinc: loinc)
    }
    
    func isPcr(loinc: String) -> Bool {
        _ = self.getRules()
        let testPcrList = self.rules?.testPcrList ?? ConstRules.testPcrList
        return testPcrList.stringValueForKeyInsensitive(key: loinc) != nil
    }
    
    func isAntigenic(loinc: String) -> Bool {
        _ = self.getRules()
        let testAntigenicList = self.rules?.testAntigenicList ?? ConstRules.testAntigenicList
        return testAntigenicList.stringValueForKeyInsensitive(key: loinc) != nil
    }
}
