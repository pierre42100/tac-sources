
//  DetailedResultVerifierService.swift
//  Anticovid Verify
//

import Foundation
import SwiftDGC

class DetailedResultVerifierService {
    static let shared = DetailedResultVerifierService()

    let userData: UserDataManager
    let statService: StatService
    let rulesMotorService: RulesService
    let premiumManager: PremiumManager
    
    /**
     Pass configuration to use for verification
     */
    lazy var configuration: PlusValidity = { rulesMotorService.plusValidityConfiguration() ?? ConstRules.plusValidity }() 
    
    init(userdata: UserDataManager = .sharedInstance,
         statService: StatService = .shared,
         rulesMotorService: RulesService = .shared,
         premiumManager: PremiumManager = .shared) {
        self.userData = userdata
        self.rulesMotorService = rulesMotorService
        self.statService = statService
        self.premiumManager = premiumManager
    }
    
    func verifyDCC(certificate: HCert, for date: Date) -> VerifierResult? {
        if let res = verifyEuCertificate(certificate: certificate, date: date) {
            return res
        } else if let res = verifyFrCertificate(certificate: certificate, date: date) {
            return res
        } else {
            return nil
        }
    }
    
    func verify2DDOC(certificate: WalletCertificate, for date: Date) -> VerifierResult? {
        if let certificate = certificate as? SanitaryCertificate {
            return verifyTestPCR(certificate: certificate)
        } else if let certificate = certificate as? VaccinationCertificate {
            return verifyVaccin(certificate: certificate)
        } else {
            return nil
        }
    }
}

private extension DetailedResultVerifierService {
    func verifyTestPCR(certificate: SanitaryCertificate) -> VerifierResult? {
        var globalValidity: DataTypeValidation = .invalid
        var resultState: CertificateState? = nil

        guard let result = certificate.fields["F5"] else {
            return nil
        }
        
        guard certificate.analysisDate != nil else {
            return nil
        }
        
        let validity = WalletManager.shared.checkCertificateSignature(certificate)
        let isPCR = rulesMotorService.isPcr(loinc: certificate.analysisCode ?? "")
        let resultValue = result.uppercased()
        
        if !validity {
            resultState = .DDOCNotAuthentic
            globalValidity = .invalid
        } else if let loinc = certificate.analysisCode, !rulesMotorService.isTest(loinc: loinc) {
            resultState = .testTypeNonValid
            globalValidity = .invalid
        } else {
            switch resultValue {
            case "N" :
                resultState = isPCR ? .testPcrNegativeDetail : .testAntigenicNegativeDetail
                globalValidity = .to_decide
            case "P" :
                resultState = isPCR ? .testPcrPositiveDetail : .testAntigenicPositiveDetail
                globalValidity = .to_decide
            case "X" :
                resultState = .notConform
                globalValidity = .invalid
            default :
                resultState = .undetermined
                globalValidity = .invalid
            }
        }
        
        let controlType = premiumManager.modeSelected == .OT ?
        ConstStat.TACV_2DDOC_IOS_OT :
        ConstStat.TACV_2DDOC_IOS_PLUS
        
        return VerifierResult(certificateState: resultState!, statInformation: StatInformation(globalValidity: globalValidity, controlType: controlType, ressourceType: ConstStat.D_DOC_B2), certHash: certificate.getIdHash())
    }
    
    func verifyVaccin(certificate: VaccinationCertificate) -> VerifierResult? {
        var globalValidity: DataTypeValidation = .invalid
        var resultState: CertificateState? = nil
        
        // Verifier le Nom du vaccin et le nombre d'injection
        guard certificate.lastVaccinationDate != nil else {
            return nil
        }

        let isFinished = certificate.vaccinationCycleState?.lowercased() == "te"
        let validity = WalletManager.shared.checkCertificateSignature(certificate)
        
        if !validity {
            resultState = .DDOCNotAuthentic
            globalValidity = .invalid
        } else if isFinished  {
            resultState = .vaccinalCycleComplet
            globalValidity = .to_decide
        } else {
            resultState = .inprogress
            globalValidity = .invalid
        }
        
        let controlType = premiumManager.modeSelected == .OT ?
        ConstStat.TACV_2DDOC_IOS_OT :
        ConstStat.TACV_2DDOC_IOS_PLUS
        
        return VerifierResult(certificateState: resultState!, statInformation: StatInformation(globalValidity: globalValidity, controlType: controlType, ressourceType:  ConstStat.D_DOC_L1), certHash: certificate.getIdHash())
    }
    
    func verifyEuCertificate(certificate: HCert, date: Date) -> VerifierResult? {
        var globalValidity: DataTypeValidation = .invalid
        var resultState: CertificateState? = nil
        
        switch certificate.type {
        case .vaccine:
            guard certificate.body["v"][0]["dt"].string != nil else {
                return nil
            }
            
            guard let dosesCount = certificate.body["v"][0]["dn"].int,
                  let expectedDosesCount = certificate.body["v"][0]["sd"].int,
                  let vaccineName = certificate.body["v"][0]["mp"].string,
                  let prophylacticAgent = certificate.body["v"][0]["vp"].string,
                  let manufacturer = certificate.body["v"][0]["ma"].string else {
                return nil
            }
                        
            if !certificate.cryptographicallyValid {
                resultState = .DCCNotauthentic
                globalValidity = .invalid
            } else if certificate.iat > date {
                resultState = .inactive
                globalValidity = .invalid
            } else if certificate.exp < date && ConfDataManager.shared.isExpirationDateCheckedForVaccine {
                resultState = .expired
                globalValidity = .invalid
            } else if dosesCount >= expectedDosesCount,
                      rulesMotorService.isVaccineProphylaxis(prophylacticAgent: prophylacticAgent),
                      rulesMotorService.isVaccineMedicinalProduct(vaccineName: vaccineName),
                      rulesMotorService.vaccineMahManfContains(manufacturer: manufacturer) {
                resultState = .vaccinalCycleComplet
                globalValidity = .to_decide
            } else {
                resultState = .vaccinalCycleNotConform
                globalValidity = .invalid
            }
            
            let controlType = premiumManager.modeSelected == .OT ?
            ConstStat.TACV_2DDOC_IOS_OT :
            ConstStat.TACV_2DDOC_IOS_PLUS

            return VerifierResult(certificateState: resultState!, statInformation: StatInformation(globalValidity: globalValidity, controlType: controlType, ressourceType: ConstStat.DCC_VACCINATION), certHash: certificate.getIdHash(forCertField: "v"))
            
        case .test:
            
            guard let testType = certificate.body["t"][0]["tt"].string,
                  let testResult = certificate.body["t"][0]["tr"].string else {
                return nil
            }

            let isTest = rulesMotorService.isTest(loinc: testType)
            let isPCR = rulesMotorService.isPcr(loinc: testType)
            let isPositive = testResult == "260373001"
            
            if !certificate.cryptographicallyValid {
                resultState = .DCCNotauthentic
                globalValidity = .invalid
            } else if certificate.iat > date {
                resultState = .inactive
                globalValidity = .invalid
            } else if (certificate.exp < date &&
                        ConfDataManager.shared.isExpirationDateCheckedForTest) {
                resultState = .expired
                globalValidity = .invalid
            } else if !isTest {
                resultState = .testTypeNonValid
                globalValidity = .invalid
            } else {
                let manufacturer = certificate.body["t"][0]["ma"].stringValue
                
                if isPCR {
                    resultState = isPositive ? .testPcrPositiveDetail : .testPcrNegativeDetail
                    globalValidity = .to_decide
                } else {
                    if rulesMotorService.testManfContains(manufacturer: manufacturer) {
                        resultState = isPositive ? .testAntigenicPositiveDetail : .testAntigenicNegativeDetail
                        globalValidity = .to_decide
                    } else {
                        resultState = isPositive ? .testAntigenicPositiveInvalid  : .testAntigenicNegativeInvalid
                        globalValidity = .to_decide
                    }
                }
            }
            
            let controlType = premiumManager.modeSelected == .OT ?
            ConstStat.TACV_2DDOC_IOS_OT :
            ConstStat.TACV_2DDOC_IOS_PLUS
            
            return VerifierResult(
                certificateState: resultState!,
                statInformation: StatInformation(
                    globalValidity: globalValidity,
                    controlType: controlType,
                    ressourceType: ConstStat.DCC_TEST,
                    result: isPositive ? "TEST POSITIF" : "TEST NEGATIF",
                    resultInfo: certificate.body["t"][0]["tc"].string?.checkAndReturnCurrentPrefix(in: ConstRules.testPrefixes) ?? ""
                ),
                certHash: certificate.getIdHash(forCertField: "t")
            )
        case .recovery:
            guard let firstSamplingDateString = certificate.body["r"][0]["fr"].string,
                  let firstSamplingDate = Date(dateString: firstSamplingDateString)?.roundingToEndOfDay() else {
                return nil
            }
            
            let minimumValidity = firstSamplingDate.dateByAddingDays(configuration.recoveryStartDay)
            let maximumValidity = firstSamplingDate.dateByAddingDays(configuration.recoveryEndDay)
            
            if !certificate.cryptographicallyValid {
                resultState = .DCCNotauthentic
                globalValidity = .invalid
            } else if certificate.iat > date {
                resultState = .inactive
                globalValidity = .invalid
            } else if (certificate.exp < date &&
                       ConfDataManager.shared.isExpirationDateCheckedForRecovery) {
                resultState = .expired
                globalValidity = .invalid
            } else if date >= minimumValidity && date < maximumValidity {
                resultState = .recoveryValid
                globalValidity = .to_decide
            } else {
                resultState = .recoveryNotValid
                globalValidity = .invalid
            }
            
            let controlType = premiumManager.modeSelected == .OT ?
            ConstStat.TACV_2DDOC_IOS_OT :
            ConstStat.TACV_2DDOC_IOS_PLUS
            
            return VerifierResult(
                certificateState: resultState!,
                statInformation: StatInformation(
                    globalValidity: globalValidity,
                    controlType: controlType,
                    ressourceType: ConstStat.DCC_RECOVERY,
                    resultInfo: certificate.body["r"][0]["is"].string?.checkAndReturnCurrentPrefix(in: ConstRules.testPrefixes) ?? ""
                ),
                certHash: certificate.getIdHash(forCertField: "r")
            )
        default:
          return nil
        }
    }
    
    func verifyFrCertificate(certificate: HCert, date: Date) -> VerifierResult? {
        var globalValidity: DataTypeValidation = .invalid
        var resultState: CertificateState? = nil
        
        switch certificate.type {
        case .exampted:
            guard certificate.body["ex"]["df"].string != nil,
                  certificate.body["ex"]["du"].string != nil else {
                return nil
            }
                        
            if !certificate.cryptographicallyValid {
                resultState = .DCCNotauthentic
                globalValidity = .invalid
            } else if certificate.iat > date {
                resultState = .inactive
                globalValidity = .invalid
            } else if certificate.exp < date &&
                        ConfDataManager.shared.isExpirationDateCheckedForExemption {
                resultState = .expired
                globalValidity = .invalid
            } else {
                resultState = .exampted
                globalValidity = .to_decide
            }
            
            let controlType = premiumManager.modeSelected == .OT ?
            ConstStat.TACV_2DDOC_IOS_OT :
            ConstStat.TACV_2DDOC_IOS_PLUS
            
            return VerifierResult(certificateState: resultState!, statInformation: StatInformation(globalValidity: globalValidity, controlType: controlType, ressourceType: ConstStat.DCC_EXEMPTION), certHash: certificate.getIdHash(forCertField: "ex"))
        case .activity:
            
            resultState = .nonValid
            globalValidity = .invalid
            
            let controlType = premiumManager.modeSelected == .OT ?
            ConstStat.TACV_2DDOC_IOS_OT :
            ConstStat.TACV_2DDOC_IOS_PLUS
            
            return VerifierResult(certificateState: resultState!, statInformation: StatInformation(globalValidity: globalValidity, controlType: controlType, ressourceType: ConstStat.DCC_EXEMPTION), certHash: certificate.getIdHash(forCertField: "a"))
        case .unknown, .recovery, .test, .vaccine:
            return nil
        }
    }
}
