// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  WalletManager.swift
//  TousAntiCovid
//
//

import UIKit

protocol WalletChangesObserver: AnyObject {
    
    func walletCertificatesDidUpdate()
    
}

final class WalletObserverWrapper: NSObject {
    
    weak var observer: WalletChangesObserver?
    
    init(observer: WalletChangesObserver) {
        self.observer = observer
    }
    
}

final class WalletManager {
    
    static let shared: WalletManager = WalletManager()
    
    var isWalletActivated: Bool {
        true//ParametersManager.shared.displaySanitaryCertificatesWallet
    }

    private var observers: [WalletObserverWrapper] = []
    
    static func certificateType(value: String) -> WalletConstant.CertificateType? {
        if value.count < 22 {
            return nil
        }
        
        let start = value.index(value.startIndex, offsetBy: 20)
        let end = value.index(value.startIndex, offsetBy: 22)
        let documentType = String(value[start..<end])
        
        switch documentType {
        case "B2":
            return WalletConstant.CertificateType.sanitary
        case "L1":
            return WalletConstant.CertificateType.vaccination
        default:
            return nil
        }
    }

    static func certificateTypeFromHeaderInUrl(_ url: URL) -> WalletConstant.CertificateType? {
        guard let urlComponents = URLComponents(url: url, resolvingAgainstBaseURL: false) else { return nil }
        guard let completeMessage = urlComponents.queryItems?.first(where: { $0.name == "v" })?.value else { return nil }
        var type: WalletConstant.CertificateType?
        WalletConstant.CertificateType.allCases.forEach {
            guard completeMessage ~= $0.headerDetectionRegex else { return }
            type = $0
        }
        return type
    }
    
    
    func isCertificateUrlValid(_ url: URL) -> Bool {
        do {
            try extractCertificateFrom(url: url)
            return true
        } catch {
            return false
        }
    }
    
    private func addObservers() {
        
    }
    
    @objc private func walletCertificateDataDidUpdate() {
        notifyObservers()
    }
    
    func processWalletUrl(_ url: URL) throws {
        let certificate: WalletCertificate = try extractCertificateFrom(url: url)
        saveCertificate(certificate)
    }
    
    func extractCertificateFrom(doc: String) throws -> WalletCertificate {
        guard let certificate = WalletCertificate.from(doc: doc) else {
            throw WalletError.parsing.error
        }
        return certificate
    }
    
    @discardableResult
    func extractCertificateFrom(url: URL) throws -> WalletCertificate {
        guard let urlComponents = URLComponents(url: url, resolvingAgainstBaseURL: false) else { throw WalletError.parsing.error }
        guard let completeMessage = urlComponents.queryItems?.first(where: { $0.name == "v" })?.value else { throw WalletError.parsing.error }
        return try extractCertificateFrom(doc: completeMessage)
    }
    
    func saveCertificate(_ certificate: WalletCertificate) {
        
    }
    
    public func checkCertificateSignature(_ certificate: WalletCertificate) -> Bool {
        guard let publicKey = certificate.publicKey?.cleaningPEMStrings() else { return false }
        guard let publicKeyData = Data(base64Encoded: publicKey) else { return false }
        guard let messageData = certificate.message else { return false }
        guard let rawSignatureData = certificate.signature else { return false }
        do {
            let publicSecKey: SecKey = try SecKey.publicKeyfromDer(data: publicKeyData)
            let algorithm: SecKeyAlgorithm = .ecdsaSignatureMessageX962SHA256
            let canVerify: Bool = SecKeyIsAlgorithmSupported(publicSecKey, .verify, algorithm)
            guard canVerify else { return false }
            
            let encodedSignatureData: Data = certificate.isSignatureAlreadyEncoded ? rawSignatureData : try rawSignatureData.derEncodedSignature()
            
            var error: Unmanaged<CFError>?
            let isSignatureValid: Bool = SecKeyVerifySignature(publicSecKey,
                                                               algorithm,
                                                               messageData as CFData,
                                                               encodedSignatureData as CFData,
                                                               &error)
            return isSignatureValid
        } catch {
            return false
        }
    }
    
}

extension WalletManager {
    
    func addObserver(_ observer: WalletChangesObserver) {
        guard observerWrapper(for: observer) == nil else { return }
        observers.append(WalletObserverWrapper(observer: observer))
    }
    
    func removeObserver(_ observer: WalletChangesObserver) {
        guard let wrapper = observerWrapper(for: observer), let index = observers.firstIndex(of: wrapper) else { return }
        observers.remove(at: index)
    }
    
    private func observerWrapper(for observer: WalletChangesObserver) -> WalletObserverWrapper? {
        observers.first { $0.observer === observer }
    }
    
    private func notifyObservers() {
        observers.forEach { $0.observer?.walletCertificatesDidUpdate() }
    }
    
}
