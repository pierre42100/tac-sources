//
//  Double+Ext.swift
//  Anticovid Verify
//
//

import Foundation

extension Double {
    
    func toInt() -> Int {
        return Int(self)
    }
}
