// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  Date+Extension.swift
//  TousAntiCovid
//
//

import Foundation

extension Date {
    
    static let isoFormatter = formatter(for: "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
    static let dateFormatter = formatter(for: "yyyy-MM-dd")
    static let dateFormatterYM = formatter(for: "yyyy-MM")
    static let dateFormatterY = formatter(for: "yyyy")
    static let dateFormatterFull = formatter(for: "yyyy-MM-dd'T'HH:mm:ss")
    static let dateTimeFormatter = formatter(for: "dd/MM/yyyy HH:mm", utcPosix: false)
    static let dateFormatterOffset = formatter(for: "yyyy-MM-dd'T'HH:mm:ssZZZZZ")
    static let dateFormatterFractional = formatter(for: "yyyy-MM-dd'T'HH:mm:ss.SSSXXXXX")
    static let dateFrFormatter = formatter(for: "dd/MM/yyyy")
    static let certificateDateOfBirthFormatter = formatter(for: "yyyy-MM-dd")
    static let timeFormatter = formatter(for: "HH:mm:ssZZZZZ")
    
    init?(dateString: String) {
        if let date = Date.dateFormatter.date(from: dateString) {
            self = date
        } else if let date = Date.dateFormatterYM.date(from: dateString) {
            self = date
        } else if let date = Date.dateFormatterY.date(from: dateString) {
            self = date
        } else if let date = Date.dateFormatterFull.date(from: dateString) {
            self = date
        } else if let date = Date.dateFormatterOffset.date(from: dateString) {
            self = date
        } else if let date = Date.dateFormatterFractional.date(from: dateString) {
            self = date
        } else {
            return nil
        }
    }
    
    static func formatter(for locale: String, utcPosix: Bool = true, utc: Bool = false) -> DateFormatter {
        let dateTimeFormatter = DateFormatter()
        dateTimeFormatter.dateFormat = locale
        dateTimeFormatter.timeZone = TimeZone.current
        dateTimeFormatter.locale = Locale.current
        if utcPosix || utc {
            dateTimeFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        }
        if utcPosix {
            dateTimeFormatter.locale = Locale(identifier: "en_US_POSIX")
        }
        return dateTimeFormatter
    }
    
    var dateTimeString: String {
      Date.dateTimeFormatter.string(from: self)
    }
    
    public var dateString: String {
        Date.dateFrFormatter.string(from: self)
    }
    
    func timeFormatted() -> String {
        let formatter: DateFormatter = DateFormatter()
        formatter.dateStyle = .none
        formatter.timeStyle = .short
        return formatter.string(from: self)
    }
    
    func shortDateFormatted() -> String {
        let formatter: DateFormatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        return formatter.string(from: self).uppercased()
    }
    
    func shortTimeFormatted() -> String {
        let formatter: DateFormatter = DateFormatter()
        formatter.dateFormat = "HH:mm"
        return formatter.string(from: self).uppercased()
    }
    
    func dayMonthFormatted() -> String {
        let formatter: DateFormatter = DateFormatter()
        formatter.setLocalizedDateFormatFromTemplate("dMMMM")
        return formatter.string(from: self)
    }
    
    func dayShortMonthFormatted() -> String {
        let formatter: DateFormatter = DateFormatter()
        formatter.setLocalizedDateFormatFromTemplate("dMMM")
        return formatter.string(from: self)
    }
    
    func dayShortMonthYearFormatted() -> String {
        let formatter: DateFormatter = DateFormatter()
        formatter.setLocalizedDateFormatFromTemplate("dMMMyyyy")
        return formatter.string(from: self)
    }
    
    func dayShortMonthYearTimeFormatted() -> String {
        let formatter: DateFormatter = DateFormatter()
        formatter.setLocalizedDateFormatFromTemplate("dMMMyyyyHHmm")
        return formatter.string(from: self)
    }
    
    func shortDayAndMonthFormatted() -> String {
        let formatter: DateFormatter = DateFormatter()
        formatter.dateFormat = "dd/MM"
        return formatter.string(from: self).uppercased()
    }
    
    func fullDayMonthFormatted() -> String {
        let formatter: DateFormatter = DateFormatter()
        formatter.dateFormat = "EEEE d MMMM"
        return formatter.string(from: self)
    }
    
    func dayMonthYearFormatted() -> String {
        let formatter: DateFormatter = DateFormatter()
        formatter.setLocalizedDateFormatFromTemplate("dMMMMyyyy")
        return formatter.string(from: self)
    }
    
    func fullTextFormatted() -> String {
        let formatter: DateFormatter = DateFormatter()
        formatter.dateStyle = .short
        formatter.timeStyle = .short
        return formatter.string(from: self)
    }
    
    func fullDateTimeFormatted(withSeconds: Bool = true) -> String {
        let formatter: DateFormatter = DateFormatter()
        formatter.dateStyle = .short
        formatter.timeStyle = withSeconds ? .medium : .short
        return formatter.string(from: self)
    }

    func dateByAddingHours(_ hours: Int) -> Date {
        addingTimeInterval(Double(hours) * 3600.0)
    }

    func dateByAddingDays(_ days: Int) -> Date {
        addingTimeInterval(Double(days) * 24.0 * 3600.0)
    }
    
    func dateByAddingMonth(_ month: Int) -> Date {
        var dateComponent: DateComponents = DateComponents()
        dateComponent.month = month
        return Calendar.current.date(byAdding: dateComponent, to: self)!
    }
    
    func dateByAddingYears(_ years: Int) -> Date {
        var dateComponent: DateComponents = DateComponents()
        dateComponent.year = years
        return Calendar.current.date(byAdding: dateComponent, to: self)!
    }
    
    func fullDateFormatted() -> String {
        let formatter: DateFormatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy HH:mm"
        return formatter.string(from: self)
    }
    
    func testCodeFormatted() -> String {
        let formatter: DateFormatter = DateFormatter()
        formatter.dateFormat = "dMMMyyyy"
        return formatter.string(from: self).uppercased()
    }
    
    func underscoreDateFormatted() -> String {
        let formatter: DateFormatter = DateFormatter()
        formatter.dateFormat = "dd_MM_yyyy"
        return formatter.string(from: self).uppercased()
    }
    
    func universalDateFormatted() -> String {
        let formatter: DateFormatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        return formatter.string(from: self)
    }
    
    func fullDateFormattedForHuman() -> String {
        let formatter: DateFormatter = DateFormatter()
        formatter.dateFormat = "'Le' dd/MM/yyyy à HH:mm"
        return formatter.string(from: self)
    }
    
    #if !WIDGET
    func relativelyFormatted(prefixStringKey: String = "myHealthController.notification.update", displayYear: Bool = false) -> String {
        if Calendar.current.isDateInToday(self) {
            let lastPart: String = String(format: "\("common.today".localized), %@", timeFormatted())
            return prefixStringKey.isEmpty ? lastPart : String(format: prefixStringKey.localized, lastPart)
        } else if Calendar.current.isDateInYesterday(self) {
            let lastPart: String = String(format: "\("common.yesterday".localized), %@", timeFormatted())
            return prefixStringKey.isEmpty ? lastPart : String(format: prefixStringKey.localized, lastPart)
        } else {
            return prefixStringKey.isEmpty ? String(format: "%@, %@", displayYear ? dayMonthYearFormatted() : dayMonthFormatted(), timeFormatted()) : String(format: "\(prefixStringKey.localized), %@", dayMonthFormatted(), timeFormatted())
        }
    }
    
    func relativelyFormattedDay(prefixStringKey: String = "myHealthController.notification.update") -> String {
        if Calendar.current.isDateInToday(self) {
            let lastPart: String = String(format: "\("common.today".localized)")
            return String(format: prefixStringKey.localized, lastPart)
        } else if Calendar.current.isDateInYesterday(self) {
            let lastPart: String = String(format: "\("common.yesterday".localized)")
            return String(format: prefixStringKey.localized, lastPart)
        } else {
            return String(format: "\(prefixStringKey.localized)", dayMonthFormatted())
        }
    }
    
    func accessibilityRelativelyFormattedDate(prefixStringKey: String = "myHealthController.notification.update") -> String {
        let hourComponents: DateComponents = Calendar.current.dateComponents([.hour, .minute], from: self)
        let accessibilityHour: String = DateComponentsFormatter.localizedString(from: hourComponents, unitsStyle: .spellOut) ?? ""
        if Calendar.current.isDateInToday(self) {
            let lastPart: String = String(format: "\("common.today".localized), %@", accessibilityHour)
            return String(format: prefixStringKey.localized, lastPart)
        } else if Calendar.current.isDateInYesterday(self) {
            let lastPart: String = String(format: "\("common.yesterday".localized), %@", accessibilityHour)
            return String(format: prefixStringKey.localized, lastPart)
        } else {
            let accessibilityDate: String = DateFormatter.localizedString(from: self, dateStyle: .medium, timeStyle: .none)
            return String(format: "\(prefixStringKey.localized), %@", accessibilityDate, accessibilityHour)
        }
    }
    
    
    func relativelyFormattedForWidget() -> String {
        String(format: "\("myHealthController.notification.update".localized), %@", dayMonthFormatted(), timeFormatted())
    }
    
    func agoFormatted() -> String {
        let elapsedTime: Int = Int(Date().timeIntervalSince1970 - timeIntervalSince1970)
        let secondsInADay: Int = 24 * 3600
        switch elapsedTime {
        case 0..<60:
            return "common.justNow".localized
        case 60..<3600:
            // Minutes count.
            return String(format: "common.ago".localized, "")
        case 3600..<secondsInADay:
            // Hours count.
            return String(format: "common.ago".localized, "")
        case secondsInADay..<(7 * secondsInADay):
            // Days count.
            return String(format: "common.ago".localized, "")
        default:
            // Date + hours
            return ""
        }
    }
    #endif
    
    func roundingToBeginningOfDay() -> Date {
        var components: DateComponents = Calendar.current.dateComponents([.year, .month, .day, .hour, .minute, .second], from: self)
        components.hour = 0
        components.minute = 0
        components.second = 0
        return Calendar.current.date(from: components)!
    }
    
    func roundingToEndOfDay() -> Date {
        var components: DateComponents = Calendar.current.dateComponents([.year, .month, .day, .hour, .minute, .second], from: self)
        components.hour = 23
        components.minute = 59
        components.second = 59
        return Calendar.current.date(from: components)!
    }
    
    func formatForTestOT() -> String?{
        let time = Date().timeIntervalSince(self)
        guard time > 0 else { return nil }
        if time > (96 * 3600) {
            let days : Int = Int(time) / (3600*24)
            let hours = Int(time / 3600) % 24
            return String(days) + " jours " + String(hours) + " heures"
        } else {
            let hours = Int(time / 3600)
            let minutes = Int(time / 60) % 60
            return String(hours) + " heures " + String(minutes) + " minutes"
        }
    }
    
    func formatForRecoveryOT() -> String?{
        let startDateComponents = Calendar.current.dateComponents([.year, .month,.day,.hour], from: self)
        let endDateComponents = Calendar.current.dateComponents([.year, .month,.day,.hour], from: Date())
        let components = Calendar.current.dateComponents([.month,.day,.hour], from: startDateComponents, to: endDateComponents)
        if let months = components.month, let days = components.day, let hours = components.hour {
            if (months > 0){
                return String(months) + " mois " + String(days) + " jours"
            } else {
                return String(days) + " jours " + String(hours) + " heures"
            }
        }
        return nil
    }
    func formatForVaccinOT() -> String?{
        let startDateComponents = Calendar.current.dateComponents([.year, .month,.day], from: self)
        let endDateComponents = Calendar.current.dateComponents([.year, .month,.day], from: Date())
        let components = Calendar.current.dateComponents([.month,.day], from: startDateComponents, to: endDateComponents)
        if let months = components.month, let days = components.day {
            if (months > 0) {
                return String(months) + " mois " + String(days) + " jours"
            } else {
                return String(days) + " jours"
            }
        }
        return nil
    }
    
    static func getDates(forLastNDays nDays: Int) -> [Date] {
        let date = Date()

        var arrDates = [Date]()

        for x in 0 ... nDays {
            arrDates.append(date.dateByAddingDays(x * -1))
        }
        return arrDates
    }
}

extension Date {
    
    var timeIntervalSince1900: Int {
        return Int(timeIntervalSince1970) + 2208988800
    }
    
    init(timeIntervalSince1900: Int) {
        self.init(timeIntervalSince1970: Double(timeIntervalSince1900 - 2208988800))
    }
    
    func svDateByAddingDays(_ days: Int) -> Date {
        addingTimeInterval(Double(days) * 24.0 * 3600.0)
    }
    
    func checkEqualCurrentDate() -> Bool {
        let previousDate = Date().addingTimeInterval(-10)
        let nextDate = Date().addingTimeInterval(10)
        return self >= previousDate && self <= nextDate
    }
    
    func getAgeFromDate() -> Int? {
        let calendar = NSCalendar(calendarIdentifier: .gregorian)
        let ageComponents = calendar?.components(.year, from: self, to: Date(), options: NSCalendar.Options(rawValue: 0))
        return ageComponents?.year
    }
}
