//
//  DateComponents+Extension.swift
//  Anticovid Verify
//

import Foundation

extension DateComponents {
    init(ISO8601String: String) throws {
        guard let unitValues = ISO8601String.durationUnitValues else {
            throw ISO8601DateComponentsFormatError()
        }

        self.init()
        for (unit, value) in unitValues {
            setValue(value, for: unit)
        }
    }
    
    func opposite() -> DateComponents {
        var newYear = year
        if let newYear2 = newYear {
            newYear = -newYear2
        }
        var newMonth = month
        if let newMonth2 = newMonth {
            newMonth = -newMonth2
        }
        var newDay = day
        if let newDay2 = newDay {
            newDay = -newDay2
        }
        
        return DateComponents(calendar: calendar, timeZone: timeZone, era: era, year: newYear, month: newMonth, day: newDay, hour: hour, minute: minute, second: second, nanosecond: nanosecond, weekday: weekday, weekdayOrdinal: weekdayOrdinal, quarter: quarter, weekOfMonth: weekOfMonth, weekOfYear: weekOfYear, yearForWeekOfYear: yearForWeekOfYear)
    }
}

struct ISO8601DateComponentsFormatError: Error {}

private extension String {
    private static let dateUnitMapping: [Character: Calendar.Component] = [
        "Y": .year,
        "M": .month,
        "W": .weekOfYear,
        "D": .day
    ]

    private static let timeUnitMapping: [Character: Calendar.Component] = [
        "H": .hour,
        "M": .minute,
        "S": .second
    ]

    var durationUnitValues: [(Calendar.Component, Int)]? {
        guard hasPrefix("P") else {
            return nil
        }

        let duration = String(dropFirst())

        guard let separatorRange = duration.range(of: "T") else {
            return duration.unitValues(withMapping: String.dateUnitMapping)
        }

        let date = String(duration[..<separatorRange.lowerBound])
        let time = String(duration[separatorRange.upperBound...])

        guard let dateUnits = date.unitValues(withMapping: String.dateUnitMapping),
            let timeUnits = time.unitValues(withMapping: String.timeUnitMapping) else {
            return nil
        }

        return dateUnits + timeUnits
    }

    func unitValues(withMapping mapping: [Character: Calendar.Component]) -> [(Calendar.Component, Int)]? {
        if isEmpty {
            return []
        }

        var components: [(Calendar.Component, Int)] = []

        let identifiersSet = CharacterSet(charactersIn: String(mapping.keys))

        let scanner = Scanner(string: self)
        while !scanner.isAtEnd {
            var value: Int = 0
            if !scanner.scanInt(&value) {
                return nil
            }
            let identifier = scanner.scanCharacters(from: identifiersSet)
            if identifier?.count != 1 {
                return nil
            }
            // swiftlint:disable:next force_unwrapping
            let unit = mapping[Character(identifier! as String)]!
            components.append((unit, value))
        }
        return components
    }
}
