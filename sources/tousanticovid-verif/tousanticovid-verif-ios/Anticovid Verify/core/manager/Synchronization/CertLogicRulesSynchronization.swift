//
//  CertLogicRulesSynchronization.swift
//  Anticovid Verify
//

import Foundation
import INCommunication
import INCore
import os.log
import Combine
import CertLogic

class CertLogicRulesSynchronization {
    private let certLogicMotorManager: CertLogicMotorManager
    private let countriesManager: CountriesManager
    private let maxRulesToFetchInOneRequest = 200
    
    init(certLogicMotorManager: CertLogicMotorManager = .shared,
         countriesManager: CountriesManager = .shared) {
        self.certLogicMotorManager = certLogicMotorManager
        self.countriesManager = countriesManager
    }
    
    func synchronizeRules() -> AnyPublisher<Void, Error> {
        fetchRuleBases()
            .flatMap(fetchRulesInPackets(ruleIds:))
            .tryMap {
                try self.countriesManager.loadLocalCountriesFromResourcesAndRules()
            }
            .eraseToAnyPublisher()
    }
}

private extension CertLogicRulesSynchronization {
    func fetchRuleBases() -> AnyPublisher<[RuleBaseResponse], Error> {
        os_log("%@", type: .info, "⚠️ Fetching rule identifiers...")

        let service = ServiceRouter.ruleIds

        return INRequestService.shared.executeRequest(service, type: [RuleBaseResponse].self, pinCertificates: service.getPinningCerts())
            .tryMap { [unowned self] response in
                try self.certLogicMotorManager.handleRuleBasesResponse(response)
                    .rulesToAdd
            }
            .eraseToAnyPublisher()
    }
    
    func fetchRulesInPackets(ruleIds: [RuleBaseResponse]) -> AnyPublisher<Void, Error> {
        let packets = ruleIds.chunks(ofCount: maxRulesToFetchInOneRequest)
        
        let initPublisher = AnyPublisher<Void, Error>(Just(()).setFailureType(to: Error.self))
        
        return packets.reduce(initPublisher, { partialResult, chunk in
            return partialResult
                .flatMap { [weak self] _ -> AnyPublisher<Void, Error> in
                    guard let self = self else {
                        return Fail(error: SyncError.referenceLost)
                            .eraseToAnyPublisher()
                    }
                    return self.fetchRules(ruleIds: Array(chunk))
                }
                .eraseToAnyPublisher()
        })
    }
    
    func fetchRules(ruleIds: [RuleBaseResponse]) -> AnyPublisher<Void, Error> {
        os_log("%@", type: .info, "⚠️ Fetching rules... with count: \(ruleIds.count)")

        let service = ServiceRouter.rulesDetails(ruleHashes: ruleIds.map { $0.hash })
        
        return INRequestService.shared.executeRequest(service, type: [CertLogic.Rule].self,
                                                   pinCertificates: service.getPinningCerts())
            .tryMap { [unowned self, ruleIds] response in
                try self.certLogicMotorManager.handleRulesResponse(response, withIds: ruleIds)
            }
            .eraseToAnyPublisher()
        
    }
}
