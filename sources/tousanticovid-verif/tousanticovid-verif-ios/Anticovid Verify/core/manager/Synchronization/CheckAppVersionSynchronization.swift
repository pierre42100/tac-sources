//
//  CheckAppVersionSynchronization.swift
//  Anticovid Verify
//

import Foundation
import INCommunication
import INCore
import os.log
import Combine

class CheckAppVersionSynchronization {
    
    private let userDataManager = UserDataManager.sharedInstance
    
    func synchronize() -> AnyPublisher<Void, Error> {
        let service = ServiceRouter.checkMinVersion
        
        return INRequestService.shared
            .executeRequest(service, type: AppConf.self)
            .flatMap(handleCheckAppVersionResponse(response:))
            .eraseToAnyPublisher()
    }
}

private extension CheckAppVersionSynchronization {
    func handleCheckAppVersionResponse(response: AppConf) -> AnyPublisher<Void, Error> {
        userDataManager.appLastMajorVersionAccepted = response.about.iosConfiguration.lastMajorVersion.toInt()
        userDataManager.appLastMinorVersionAccepted = response.about.iosConfiguration.lastMinorVersion.toInt()
        FileLogger.shared.write(text: "lastMajorVersion \(userDataManager.appLastMajorVersionAccepted ?? -1)")
        FileLogger.shared.write(text: "lastMinorVersion \(userDataManager.appLastMinorVersionAccepted ?? -1)")
        NotificationCenter.default.post(name: ConstNotification.updateApplicationRequired,
                                        object: nil)
        NotificationCenter.default.post(name: ConstNotification.updateApplicationAvailable,
                                        object: nil)
        
        return Just(()).setFailureType(to: Error.self).eraseToAnyPublisher()
    }
}
