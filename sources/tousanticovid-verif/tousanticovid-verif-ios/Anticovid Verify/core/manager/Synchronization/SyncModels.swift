//
//  SyncModels.swift
//  Anticovid Verify
//

import Foundation

enum SyncStatus {
    case unknown, inProgress, neverSynced, synced
}

indirect enum SyncingFunctionnality {
    case baseConfiguration(chainWith: SyncingFunctionnality? = nil)
    case blacklist(chainWith: SyncingFunctionnality? = nil, force: Bool = false)
    case OTModeConfiguration(chainWith: SyncingFunctionnality? = nil)
    case checkAppVersion(chainWith: SyncingFunctionnality? = nil)
    case stats(chainWith: SyncingFunctionnality? = nil)
    
    var chain: [SyncingFunctionnality] {
        var result = [SyncingFunctionnality]()
        var optionalPending = chained
        while let pending = optionalPending {
            result.append(pending)
            optionalPending = pending.chained
        }
        return result
    }
    
    private var chained: SyncingFunctionnality? {
        switch self {
        case .baseConfiguration(let chained):
            return chained
        case .OTModeConfiguration(let chained):
            return chained
        case .blacklist(let chained, _):
            return chained
        case .checkAppVersion(let chained):
            return chained
        case .stats(let chained):
            return chained
        }
    }
}

enum SyncError: Error {
    case unknown
    case referenceLost
    case notABlacklistService
    case cannotFetchBlacklistAtCurrentTime
    case blacklistAlreadyUpToDate
    case noNeedToSync
    case syncAlreadyInProgress
    case ruleHasNoLogic
    case webserviceDataBroken
}
