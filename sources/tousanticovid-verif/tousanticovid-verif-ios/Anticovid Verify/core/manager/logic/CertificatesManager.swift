//
//  CertificatesManager.swift
//  Anticovid Verify
//
//

import Foundation
import INCore

class CertificatesManager {

    static let shared: CertificatesManager = CertificatesManager()
    static let certificatesLastUpdateKey = "certificates.synchronization.date"
    static let certificatesDCCKey = "certificates.synchronization.dcc"
    static let certificates2DDocKey = "certificates.synchronization.2ddoc"
    static let certificatesRemindStep1Key = "certificates.synchronization.remind.step1"
    static let certificatesRemindStep2Key = "certificates.synchronization.remind.step2"
    static let certificatesRemindStep3Key = "certificates.synchronization.remind.step3"
    static let certificatesFrequencyKey = "certificates.synchronization.frequency"
    static let NUMBER_MIN_OF_CERTIFICATE = 50
    
    var dccCertificates : [String:String]? {
        get{
            return getCertsForKey(key: CertificatesManager.certificatesDCCKey)
        }
        set(value){
            let encoder = JSONEncoder()
            if let encoded = try? encoder.encode(value) {
                UserDefaults.standard.set(encoded, forKey: CertificatesManager.certificatesDCCKey)
            }
        }
    }

    var ddocCertificates : [String:String]? {
        get{
            return getCertsForKey(key: CertificatesManager.certificates2DDocKey)
        }
        set(value){
            let encoder = JSONEncoder()
            if let encoded = try? encoder.encode(value) {
                UserDefaults.standard.set(encoded, forKey: CertificatesManager.certificates2DDocKey)
            }
        }
    }
    
    private func getCertsForKey(key: String) -> [String: String]?{
        if let certsJson = UserDefaults.standard.object(forKey: key) as? Data{
            let decoder = JSONDecoder()
            return try? decoder.decode([String:String].self, from: certsJson)
        }
        return nil
    }
    
    func loadLocalCertificates() -> CertificatesUpdateResponse? {
        if let jsonPath = Bundle.main.url(forResource: "sync_certs", withExtension: "json") {
            do {
                let contents = try Data(contentsOf: jsonPath)
                let decoder = JSONDecoder()
                decoder.dateDecodingStrategy = .formatted(Date.dateFormatter)
                let response = try decoder.decode(CertificatesUpdateResponse.self, from: contents)
                return response
            } catch let error {
                print(error)
                return nil
            }
        }
        return nil
    }
    
    func saveCertificates(certs: CertificatesUpdateResponse){
        dccCertificates = certs.certificatesDCC
        ddocCertificates = certs.certificates2DDoc
    }
}
