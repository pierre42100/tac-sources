//
//  CustomFont.swift
//  Anticovid Verify
//
//

import Foundation
import UIKit

class INStyles: NSObject {
    
  static func fontForStyle(style:String)->UIFont{
    switch style{
    case "title":
      return UIFont(name: "Avenir-Black", size: 22)!
    case "result":
      return UIFont(name: "Avenir-Black", size: 26)!
    case "subtitle":
        return UIFont(name: "Avenir-Heavy", size: 18)!
    case "text":
        return UIFont(name: "Avenir-Medium", size: 18)!
    case "textLittle":
        return UIFont(name: "Avenir-Medium", size: 16)!
    case "navTitle":
        return UIFont(name: "Avenir-Heavy", size: 20)!
    case "navSubtitle":
        return UIFont(name: "Avenir-Medium", size: 18)!
    default:
        return INStyles.fontForStyle(style: "title");
    }
  }
    
}

@IBDesignable class INLabel: UILabel {
    
  @IBInspectable var style:String="title"{
    didSet{
        self.font = INStyles.fontForStyle(style: style)
        
    }
  }
    
}

