//
//  ViewControllerIntanciator.swift
//  Anticovid Verify
//
//

import Foundation
import UIKit

class ViewControllerIntanciator {
    
    // MARK: - WebView
    
    public static func getWKWebViewController(contentOnLoad: String, transparent: Bool = false) -> WkWebViewViewContoller {
        let storyboard = UIStoryboard(name: "WebViewController" , bundle: nil)
        let viewController: WkWebViewViewContoller =
            (storyboard.instantiateViewController(withIdentifier: "wkWebViewViewContoller") as? WkWebViewViewContoller)!
        viewController.wkWebViewData = WkWebViewData(value: contentOnLoad, type: .HTLM)
        viewController.transparent = transparent
        return viewController
    }

    public static func getWKWebViewController(urlOnLoad: String, transparent: Bool = false) -> WkWebViewViewContoller {
        let storyboard = UIStoryboard(name: "WebViewController" , bundle: nil)
        let viewController: WkWebViewViewContoller =
            (storyboard.instantiateViewController(withIdentifier: "wkWebViewViewContoller") as? WkWebViewViewContoller)!
        viewController.wkWebViewData = WkWebViewData(value: urlOnLoad, type: .URL)
        viewController.transparent = transparent
        return viewController
    }
    
    public static func getWKWebViewContainer(wkWebViewData: WkWebViewData, titleValue: String = "") -> UIViewController {
        let storyboard = UIStoryboard(name: "WebViewController" , bundle: nil)
        let viewController: WkWebViewContainerController =
            (storyboard.instantiateViewController(withIdentifier: "wkWebViewContainerController") as? WkWebViewContainerController)!
        viewController.wkWebViewData = wkWebViewData
        viewController.titleValue = titleValue
        return UINavigationController(rootViewController: viewController)
    }
    // MARK: - Tuto
    
    public static func getPageableViewController(with viewControllers: [UIViewController]) -> UIViewController {
        let storyboard = UIStoryboard(name: "Tutorial", bundle: nil)
        let viewController: PageViewController = ((storyboard.instantiateViewController(withIdentifier: "PageViewController") as? PageViewController)!)
        viewController.viewControllers = viewControllers
        return viewController
    }
    
    public static func getFirstTutoViewController() -> UIViewController {
        let storyboard = UIStoryboard(name: "Tutorial", bundle: nil)
        let viewController: UIViewController = ((storyboard.instantiateViewController(withIdentifier: "FirstTutorialViewController")))
        return viewController
    }
    
    public static func getSecondTutoViewController() -> UIViewController {
        let storyboard = UIStoryboard(name: "Tutorial", bundle: nil)
        let viewController: UIViewController = ((storyboard.instantiateViewController(withIdentifier: "SecondTutorialViewController")))
        return viewController
    }
}
