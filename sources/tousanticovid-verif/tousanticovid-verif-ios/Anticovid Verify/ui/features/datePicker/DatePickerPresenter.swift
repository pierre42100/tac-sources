//
//  DatePickerPresenter.swift
//  Anticovid Verify
//
//

import Foundation

class DatePickerPresenter{
    
    private let otDataManager : OtDataManager
    
    init(otDataManager: OtDataManager = .shared) {
        self.otDataManager = otDataManager
    }
    
    func saveNewCheckDate(with otMode: OtState, newDate: Date) {
        if(otMode == .goOutState) {
            otDataManager.goOutData?.currentCheckDate = newDate
        } else {
            otDataManager.goInData?.currentCheckDate = newDate
        }
    }
}

