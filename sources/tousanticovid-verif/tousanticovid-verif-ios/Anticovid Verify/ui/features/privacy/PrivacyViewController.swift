//
//  PrivacyViewController.swift
//  Anticovid Verify
//
//

import UIKit

class PrivacyViewController: INViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.isModalInPresentation = true
    }
    
    @IBAction func onTouchPrivacyPolicy(_ sender: Any) {
        if let url = URL(string: ConstConf.privacyPolicy) {
            UIApplication.shared.open(url)
        }
    }
    
    @IBAction func onTouchCGU(_ sender: Any) {
        if let url = URL(string: ConstConf.cgu) {
            UIApplication.shared.open(url)
        }
    }
    
    @IBAction func onTouchStart(_ sender: Any) {
        UserDataManager.sharedInstance.shouldShowPrivacy = false
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onTouchDecret(_ sender: Any) {
        if let url = URL(string: ConstConf.decret) {
            UIApplication.shared.open(url)
        }
    }
}
