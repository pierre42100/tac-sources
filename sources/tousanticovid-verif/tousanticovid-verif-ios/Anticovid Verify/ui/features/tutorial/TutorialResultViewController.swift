//
//  TutorialResultViewController.swift
//  Anticovid Verify
//
//

import UIKit

class TutorialResultViewController: INViewController {
    
    private let resultSegue = "resultSegue"
    
    var fromHelp = false
    var encodedBarcode: String?
    var typeBarcode: Int?
    var modeSelected: ModeSelected?
    
    @IBOutlet weak var showTutorialSwitch: UISwitch!
    @IBOutlet weak var showTutorialLabel: INLabel!
    @IBOutlet weak var showTutorialButton: RoundedButton!
    
    @IBOutlet weak var container: UIView!
    
    @IBOutlet weak var containerBottomConstraint: NSLayoutConstraint!
    
    @IBAction func onSwitchValueChange(_ sender: Any) {
        UserDataManager.sharedInstance.shouldShowResultTutorial = !showTutorialSwitch.isOn
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if fromHelp {
            showTutorialSwitch.isHidden = true
            showTutorialLabel.isHidden = true
            showTutorialButton.setTitle("Fermer", for: .normal)
            containerBottomConstraint.constant = -20
        }
        
        initView()
    }
    
    @IBAction func onClose(_ sender: Any) {
        if fromHelp {
            self.dismiss(animated: true, completion: nil)
        } else {
            if let encoded2DDoc = encodedBarcode {
                performSegue(withIdentifier: resultSegue, sender: encoded2DDoc)
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == resultSegue, let vc = segue.destination as? ResultContainerViewController, let encoded2DDoc = encodedBarcode {
            vc.encodedBarcode = encoded2DDoc
            vc.typeBarcode = typeBarcode
        }
    }
    
    private func replaceChildView(with uiViewController: UIViewController) {
        let currentChild = self.children.first
        
        currentChild?.willMove(toParent: nil)
        container.frame = uiViewController.view.frame
        uiViewController.view.alpha = 0
        
        self.addChild(uiViewController)
        self.view.isUserInteractionEnabled = false
        
        self.transition(from: currentChild!, to: uiViewController, duration: 0.25, options: [.curveEaseInOut], animations: {
            uiViewController.view.alpha = 1
            currentChild?.view.alpha = 0
        }, completion: { _ in
            currentChild?.removeFromParent()
            uiViewController.didMove(toParent: self)
            self.view.isUserInteractionEnabled = true
            self.container.isHidden = false
        })
    }
    
    private func initView()  {
        guard let modeSelected = modeSelected else {
            return
        }

        var content = ""
        var shouldHide = false
        
        switch modeSelected {
        case .tacVerif(let passSelected, _):
            switch passSelected {
            case .vaccine:
                content = UIDataManager.shared.tutorialVaccinePass?.base64ToString() ?? ""
            case .health:
                content = UIDataManager.shared.tutorialHealthPass?.base64ToString() ?? ""

            }
        case .tacVerifPlus, .OT:
            shouldHide = true
            content = UIDataManager.shared.getOtTutorial()
        }
        
        if !shouldHide  {
            replaceChildView(with: ViewControllerIntanciator.getWKWebViewController(contentOnLoad: content, transparent: true))
        } else {
            container.isHidden = false
        }
    }
}
