//
//  TutorialScanViewController.swift
//  Anticovid Verify
//
//

import UIKit

class TutorialScanViewController: INViewController {
    
    private let scanSegue = "scanSegue"
    
    var fromHelp = false
    
    @IBOutlet weak var showTutorialSwitch: UISwitch!
    @IBOutlet weak var showTutorialLabel: INLabel!
    @IBOutlet weak var showTutorialButton: RoundedButton!
    
    @IBOutlet weak var content: UIView!
    
    let viewControllers = [
        ViewControllerIntanciator.getFirstTutoViewController(),
        ViewControllerIntanciator.getSecondTutoViewController()
    ]
    
    @IBAction func onSwitchValueChange(_ sender: Any) {
        if let choice = sender as? UISwitch {
            UserDataManager.sharedInstance.shouldShowScanTutorial = !choice.isOn
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        if fromHelp {
            showTutorialSwitch.isHidden = true
            showTutorialLabel.isHidden = true
            showTutorialButton.setTitle("Fermer", for: .normal)
        }
    }

    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        replaceChildView(with: ViewControllerIntanciator.getPageableViewController(with: viewControllers))
    }
    
    private func replaceChildView(with uiViewController: UIViewController) {
        let currentChild = self.children.first
        
        currentChild?.willMove(toParent: nil)
        content.translatesAutoresizingMaskIntoConstraints = false
        content.frame = uiViewController.view.frame
        uiViewController.view.alpha = 0
        
        self.addChild(uiViewController)
        self.view.isUserInteractionEnabled = false
        guard let current = currentChild else {
            uiViewController.didMove(toParent: self)
            uiViewController.view.alpha = 1
            self.view.isUserInteractionEnabled = true
            self.content.isHidden = false
            return
        }
        self.transition(from: current, to: uiViewController, duration: 0.25, options: [.curveEaseInOut], animations: {
            uiViewController.view.alpha = 1
            current.view.alpha = 0
        }, completion: { _ in
            current.removeFromParent()
            uiViewController.didMove(toParent: self)
            self.view.isUserInteractionEnabled = true
            self.content.isHidden = false
        })
    }

    @IBAction func onClose(_ sender: Any) {
        if fromHelp {
            self.dismiss(animated: true, completion: nil)
        } else {
            performSegue(withIdentifier: scanSegue, sender: nil)
        }
    }
}
