//
//  LocalStatisticViewController.swift
//  Anticovid Verify
//
//

import Foundation
import UIKit
import Charts

class XFormatter: AxisValueFormatter {
    
    func stringForValue(_ value: Double, axis: AxisBase?) -> String {
        let numberOfDay = abs(value - 15)
        let date = Date().dateByAddingDays(Int(numberOfDay * -1))
        return date.shortDayAndMonthFormatted()
    }
}

class YFormatter: ValueFormatter {
    
    func stringForValue(_ value: Double, entry: ChartDataEntry, dataSetIndex: Int, viewPortHandler: ViewPortHandler?) -> String {
        return String(Int(value))
    }
}

class LocalStatisticViewController: INViewController {

    var localStatisticPresenter: LocalStatisticPresenter!
    
    var historyData = [HistoryData]()
    
    var chartEntries = [BarChartDataEntry]()
    
    @IBOutlet weak var scanOfTheDayLabel: INLabel!
    
    @IBOutlet weak var numberOfScanOfTheDayLabel: INLabel!
    
    @IBOutlet weak var numberOfDoubleOfTheDayLabel: INLabel!
    
    @IBOutlet weak var chartView: BarChartView!
    
    @IBOutlet weak var historyTableView: UITableView!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        localStatisticPresenter = LocalStatisticPresenter(self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureChart()
        self.localStatisticPresenter.fetchStatistics()
    }
    
    private func configureChart() {
        let leftAxis = chartView.leftAxis
        leftAxis.axisMinimum = 0
        leftAxis.granularity = 1
        
        let xAxis = chartView.xAxis
        xAxis.labelPosition = .bottom
        xAxis.drawGridLinesEnabled = false
        xAxis.valueFormatter = XFormatter()
        
        chartView.rightAxis.enabled = false
        chartView.legend.enabled = true
        
        chartView.data = getBarChartDataFromChartEntries()
    }
    
    private func getBarChartDataFromChartEntries() -> BarChartData {
        let chartEntriesSet = BarChartDataSet(entries: chartEntries, label: "")
        chartEntriesSet.colors = [
            NSUIColor(cgColor: UIColor.blueIn.withAlphaComponent(0.75).cgColor),
            NSUIColor(cgColor: UIColor.grayIn.withAlphaComponent(0.75).cgColor)
        ]
        
        chartEntriesSet.stackLabels = ["Scans hors doublons" , "Doublons"]
        
        let barChartData = BarChartData(dataSets: [chartEntriesSet])
        barChartData.setValueFormatter(YFormatter())
        return barChartData
    }
}

extension LocalStatisticViewController: ChartViewDelegate {}

extension LocalStatisticViewController: LocalStatisticView {
    
    func updateHistory(withData history: [HistoryData]) {
        self.historyData = history
        self.historyTableView.reloadData()
    }
    
    
    func updateChart(withData chartData: [ChartData]) {
        chartEntries = []

        for x in 0..<chartData.count {
            chartEntries.append(BarChartDataEntry(x: Double(x), yValues: [chartData[x].numberOfScan, chartData[x].numberOfDuplicate]))
        }

        chartView.data = getBarChartDataFromChartEntries()
        chartView.notifyDataSetChanged()
    }
    
    func updateHeader(with dayStatistic: LocalStatistic?) {
        self.scanOfTheDayLabel.text = "Nombre de scans du jour: \((dayStatistic?.date ?? Date()).dateString)"
        self.numberOfScanOfTheDayLabel.text = String(dayStatistic != nil ? dayStatistic!.calculateNumberOfScan() : 0)
        self.numberOfDoubleOfTheDayLabel.text = String(dayStatistic != nil ? dayStatistic!.double.toInt() : 0)
    }
}

extension LocalStatisticViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.historyData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: HistoryStatisticCell.identifier, for: indexPath) as? HistoryStatisticCell else {
            return UITableViewCell()
        }
        let historyData = historyData[indexPath.row]
        cell.makeCell(stat: historyData)
        cell.selectionStyle = .none
        return cell
    }
}

extension LegendEntry {
    
    static func generateLegendEntry(withLabel label: String, andColor color: UIColor) -> LegendEntry {
        let legendEntry = LegendEntry.init(label: label)
        legendEntry.formColor = color
        legendEntry.labelColor = UIColor.black
        return legendEntry
    }
}
