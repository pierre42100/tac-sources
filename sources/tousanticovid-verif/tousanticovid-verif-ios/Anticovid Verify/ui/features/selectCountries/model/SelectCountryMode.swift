//
//  SelectCoutryControllerMode.swift
//  Anticovid Verify
//
//

import Foundation

enum SelectCountryMode {
    case selectOneCountry(isNational: Bool)
    case selectCountries(numberMax: Int, isNational: Bool = false)
}
