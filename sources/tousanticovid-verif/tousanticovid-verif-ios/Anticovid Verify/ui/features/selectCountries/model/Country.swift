//
//  Country.swift
//  Anticovid Verify
//
//

import Foundation
import UIKit

public struct Country: Codable {
    
    var name: String
    var nameCode: String
    var isNational: Bool
    var isArrival: Bool = false
    var isDeparture: Bool = false
    var isFavorite: Bool = false
    
    private enum CodingKeys: String, CodingKey {
        case name
        case nameCode
        case isNational
        case isArrival
        case isDeparture
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        name = try container.decode(String.self, forKey: .name)
        nameCode = try container.decode(String.self, forKey: .nameCode)
        isNational = try container.decode(Bool.self, forKey: .isNational)
        isArrival = try container.decode(Bool.self, forKey: .isArrival)
        isDeparture = try container.decode(Bool.self, forKey: .isDeparture)
        isFavorite = false
    }
    
    
    init(name: String, nameCode: String, isNational: Bool, isArrival: Bool = false, isDeparture: Bool = false, isFavorite: Bool = false) {
        self.name = name
        self.nameCode = nameCode
        self.isNational = isNational
        self.isArrival = isArrival
        self.isDeparture = isDeparture
        self.isFavorite = isFavorite
    }

    var shortDisplayName: String {
        get {
            return nameCode.lowercased() == "_c" ? "Corse" : nameCode
        }
    }
    
    func getFlag() -> UIImage {
        if isNational {
            return (UIImage(named: "FR") ?? UIImage(named: "unknown")!)
        } else {
            return UIImage(named: nameCode) ?? UIImage(named: "unknown")!
        }
    }    
}

extension Country: Hashable {
    public func hash(into hasher: inout Hasher) {
        hasher.combine(nameCode.isEmpty ? "notEmpty" : nameCode.lowercased())
    }
}

extension Country: Equatable {
    public static func == (lhs: Country, rhs: Country) -> Bool {
        return lhs.nameCode.lowercased() == rhs.nameCode.lowercased()
    }
}
