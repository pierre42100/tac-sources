//
//  ResultViewPresenter.swift
//  Anticovid Verify
//
//

import Foundation
import INCommunication
import SwiftDGC
import OrderedCollections
import UIKit

protocol ResultView {
    func fillTableWithResults(results: OrderedDictionary<SectionRow, [FieldRow]>)
    func displayResultHeader(resultConfiguration: ResultHeaderConfiguration?)
    func displayBlacklistButton(title:String,content:String)
    func showError(error: BarcodeError)
    func startSchedule(with time: Int?)
}

class ResultPresenter {
    
    private let barcodeVerifier: BarcodeService
    
    private let premiumManager: PremiumManager
    
    private let otDataManager : OtDataManager
    
    private let walletManager: WalletManager
    
    private let internationalTravelVerifierService: InternationalTravelVerifierService
    
    private var view: ResultView?
    
    init(view: ResultView, otDataManager : OtDataManager = .shared, barcodeVerifier: BarcodeService = .shared, premiumManager: PremiumManager = .shared, walletManagaer: WalletManager = .shared, userDataManager: UserDataManager = .sharedInstance, internationalTravelVerifierService: InternationalTravelVerifierService = .shared) {
        self.view = view
        self.barcodeVerifier = barcodeVerifier
        self.premiumManager = premiumManager
        self.walletManager = walletManagaer
        self.otDataManager = otDataManager
        self.internationalTravelVerifierService = internationalTravelVerifierService
    }
    
    func getResult(barcode:String, typeBarcode: Int) {
        guard let view = self.view else { return }
        
        // get custom date for validation
        var date = Date()
        if premiumManager.modeSelected == .OT {
            switch otDataManager.selection {
            case .arrival(let data):
                date = data.currentCheckDate.getDate()
            case .departure(let data):
                date = data.currentCheckDate.getDate()
            }
        }
        
        // validate
        let barcodeResult = self.barcodeVerifier.analyze(barcode: barcode, forValidityAt: date)
        
        guard let certificate = barcodeResult.object,
              let resultState = barcodeResult.state else {
            view.showError(error: barcodeResult.error ?? .wrongDocument)
            return
        }
        
        // present result
        var uiInformation: OrderedDictionary<SectionRow, [FieldRow]> = [:]
        uiInformation = presentCustomDateResultIfNeeded(uiInformation)
        uiInformation = presentOTModeDataIfNeeded(uiInformation,
                                                  certificate: certificate,
                                                  state: resultState)
        uiInformation = presentCertificateInformation(uiInformation,
                                                      certificate: certificate, resultState: resultState)
        
        // compute time before closing view
        var exitViewTime = premiumManager.modeSelected == .OT ? 60 : 30 // 1 min or 30 sec
        if barcodeResult.adminState != nil {
            exitViewTime = 300 // 5 min
        }
        
        view.startSchedule(with: exitViewTime)
        presentHeaderIfNeeded(resultState: resultState, adminState: barcodeResult.adminState)
        view.fillTableWithResults(results: uiInformation)
        if let adminState = barcodeResult.adminState {
            let otherInformation = adminState.getTextInformation()
            view.displayBlacklistButton(title: otherInformation.0, content: otherInformation.1)
        }
        
    }
}

private extension ResultPresenter {
    func presentHeaderIfNeeded( resultState: CertificateState, adminState: CertificateAdminState?) {
        guard premiumManager.modeSelected == .OT else {
            view?.displayResultHeader(resultConfiguration: .init(title: resultState.getName(),
                                                                 image: resultState.getImage(),
                                                                 color: resultState.getColor()))
            return
        }
        
        if shouldShowErrorCertificateHeader(resultState: resultState) {
            view?.displayResultHeader(resultConfiguration: .init(title: resultState.getName(),
                                                             image: resultState.getImage(),
                                                             color: resultState.getColor()))
        } else {
            view?.displayResultHeader(resultConfiguration: nil)
        }
    }
    
    func presentOTModeDataIfNeeded(_ collection: OrderedDictionary<SectionRow, [FieldRow]>,
                                   certificate: BarcodeProtocolObject,
                                   state: CertificateState) -> OrderedDictionary<SectionRow, [FieldRow]> {
        guard !shouldShowErrorCertificateHeader(resultState: state) else { return collection }
        
        guard premiumManager.modeSelected == .OT,
              .inactive != state, .expired != state,
              .DCCNotauthentic != state, .DDOCNotAuthentic != state else { return collection
              }
        
        var uiinformation = collection
        
        guard let cert = certificate as? HCert,
              let OTResult = internationalTravelVerifierService
                .verify(cert,withSelection: otDataManager.selection) else {
                    return uiinformation
                }
        
        // ot headers labels
        switch otDataManager.selection {
        case .departure(let data):
            if internationalTravelVerifierService.countriesSituation[data.departureCountry] != .green {
                uiinformation[SectionRow.emptyHeader()] = [FieldRow.Admin(title: "Attention il est possible que la situation sanitaire de départ ne soit plus considérée comme verte vis à vis des autres pays.", content: "")]
            }

            if data.currentCheckDate.getDate() > Date() {
                uiinformation[SectionRow.emptyHeader()] = [FieldRow.Field(title: "DATE EFFECTIVE DU CONTROLE", subtitle: data.currentCheckDate.getDate().dateTimeString, picto: nil)]
            }
        case .arrival(let data):
            if data.currentCheckDate.getDate() > Date() {
                uiinformation[SectionRow.emptyHeader()] = [FieldRow.Field(title: "DATE EFFECTIVE DU CONTROLE", subtitle: data.currentCheckDate.getDate().dateTimeString, picto: nil)]
            }
        }
        
        let countryResultSection = OTResult.resultForCountry.filter {
            $0.country != InternationalTravelVerifierService.greenCountry &&
            $0.country != InternationalTravelVerifierService.orangeCountry &&
            $0.country != InternationalTravelVerifierService.redCountry
        }
        
        uiinformation = presentResultCountries(uiinformation,
                                               resultForCountries: countryResultSection,
                                               state: state)
        
        if case .arrival(let data) = otDataManager.selection  {
            data.favoritesCountries
                .filter { country in
                    !OTResult.resultForCountry.map { $0.country }.contains(country)
                }
                .forEach {
                    uiinformation[SectionRow.Rules(country: $0.name + " - Voir couleurs", validation: .none)] = []
                }
        }
        
        let coloredCountries = OTResult.resultForCountry.filter {
            $0.country == InternationalTravelVerifierService.greenCountry ||
            $0.country == InternationalTravelVerifierService.orangeCountry ||
            $0.country == InternationalTravelVerifierService.redCountry
        }
        
        if !coloredCountries.isEmpty {
            uiinformation[SectionRow.Label(title: "Autres résultats selon la situation sanitaire du pays de départ")] = []
        }
                
        uiinformation = presentResultCountries(uiinformation, resultForCountries: coloredCountries, state: state)
        
        return uiinformation
    }
    
    private func color(forValidation valid: RulesValidation) -> UIColor {
        switch valid {
        case .invalidate:
            return .redIn
        case .validate:
            return .greenIn
        case .neutral:
            return .blueIn
        }
    }
    
    private func presentCustomDateResultIfNeeded(_ collection: OrderedDictionary<SectionRow, [FieldRow]>) -> OrderedDictionary<SectionRow, [FieldRow]> {
        var result = collection
        
        if premiumManager.modeSelected == .OT {
            switch otDataManager.selection {
            case .departure(let data):
                if case .custom(let date) = data.currentCheckDate {
                    result[.emptyHeader()] = [FieldRow.Field(title: "DATE EFFECTIVE DU CONTROLE", subtitle: date.dateTimeString, picto: nil)]
                }
            case .arrival(let data):
                if case .custom(let date) = data.currentCheckDate {
                    result[.emptyHeader()] = [FieldRow.Field(title: "DATE EFFECTIVE DU CONTROLE", subtitle: date.dateTimeString, picto: nil)]
                }
            }
        }
        
        return result
    }
    
    private func presentResultCountries(_ collection: OrderedDictionary<SectionRow, [FieldRow]>,
                                        resultForCountries: [OtModeCountryResult],
                                        state: CertificateState) -> OrderedDictionary<SectionRow, [FieldRow]>  {
        var result = collection
        
        resultForCountries.forEach { resultForCountry in
            switch resultForCountry.result {
            case .passed:
                result[SectionRow.Rules(country: resultForCountry.country.name,
                                        validation: .validate)] = []
            case .open:
                result[SectionRow.Rules(country: resultForCountry.country.name,
                                        validation: .neutral)] = [FieldRow.Rule(label: state.getName(), color: color(forValidation: .neutral))]
            case .fail:
                let fields = resultForCountry.validations.compactMap() { info -> FieldRow? in
                    guard let rule = info.rule else { return nil }
                    var desc = rule.description.first { $0.lang.lowercased() == "fr" }
                    desc = desc ?? rule.description.first { $0.lang.lowercased() == "en" }
                    let descString = desc.map { $0.desc } ?? ""
                    
                    return FieldRow.Rule(label: "Règle KO : \(descString)",
                                         color: .redIn)
                }
                result[SectionRow.Rules(country: resultForCountry.country.name,
                                        validation: .invalidate)] = fields
            }
        }
        
        return result
    }
    
    private func presentCertificateInformation(_ collection: OrderedDictionary<SectionRow, [FieldRow]>,
                                               certificate: BarcodeProtocolObject,
                                               resultState: CertificateState) -> OrderedDictionary<SectionRow, [FieldRow]> {
        var result = collection
        
        certificate.getUIInformation(result: resultState, shouldShowFullResult: premiumManager.modeSelected == .tacVerifPlus || premiumManager.modeSelected == .OT)
            .forEach({ (key, value) in
                result[SectionRow.Label(title: key)] = value
            })
        
        return result
    }
    
    private func shouldShowErrorCertificateHeader(resultState: CertificateState) -> Bool {
        switch resultState {
        case .expired, .inactive, .DCCNotauthentic, .DDOCNotAuthentic, .blacklisted:
            return true
        default:
            return false
        }
    }
}
