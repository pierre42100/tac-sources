//
//  ResultState.swift
//  Anticovid Verify
//
//

import Foundation

@objc enum ResultState: Int {
    case rulesNotAvailable
    case rulesNotRespected
    case dccNotCertified
    case certificateValid
}

