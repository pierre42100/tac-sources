//
//  ResultContainerViewController.swift
//  Anticovid Verify
//
//

import Foundation
import UIKit
import OrderedCollections

class ResultContainerViewController : UIViewController {
    
    private let tutorialResultSegue = "tutorialResultSegue"

    @IBOutlet weak var container: UIView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var encodedBarcode: String!
    var typeBarcode: Int!
    var presenter : ResultPresenter!
    var resultViewController: ResultViewController!
    var currentAlert :UIAlertController?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.presenter = ResultPresenter(view:self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Résultat"
        
        if UIScreen.main.isCaptured && !ConstConf.canScreen {
            self.alert(title: "Enregistrement en cours", message: "Votre iPhone enregistre votre écran, veuillez désactiver l'enregistrement avant de réessayer")
        } else {
            self.presenter.getResult(barcode: self.encodedBarcode, typeBarcode: self.typeBarcode)
        }
        NotificationCenter.default.addObserver(self, selector:#selector(pop), name: UIApplication.willResignActiveNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(pop), name: UIScreen.capturedDidChangeNotification, object: nil)

        navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(systemName: "questionmark.circle"), style: .plain, target: self, action: #selector(onHelp))
    }

    @objc func onHelp() {
        if let vc = UIStoryboard(name: "Home", bundle: Bundle.main).instantiateViewController(identifier: "TutorialResultViewController") as? TutorialResultViewController {
            vc.fromHelp = true
            vc.modeSelected = PremiumManager.shared.modeSelected
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? ResultViewController{
            self.resultViewController = vc
        }
    }
    
    @objc func pop(){
        DispatchQueue.main.async { [self] in

            if let alert = self.currentAlert {
            alert.dismiss(animated: false, completion: {
                self.navigationController?.popToRootViewController(animated: true)
            })
            currentAlert = nil
        } else {
            self.navigationController?.popToRootViewController(animated: true)

        }
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
        self.currentAlert?.dismiss(animated: true, completion: nil)
        currentAlert = nil
    }
}

extension ResultContainerViewController : ResultView {
    func displayBlacklistButton(title: String, content: String) {
        resultViewController.displayBlacklistButton(title: title, content: content)
    }
    
    func showError(error: BarcodeError) {
        switch error {
            case .wrongDocument:
                alert(title:"Une erreur est survenue",message:"Le document n'est pas reconnu")
        }
    }
    
    func fillTableWithResults(results: OrderedDictionary<SectionRow, [FieldRow]>) {
        activityIndicator.stopAnimating()

        resultViewController.items = results
        container.fadeIn()
        container.isHidden = false
    }
    
    func startSchedule(with time: Int?) {
        resultViewController.startSchedule(with: time ?? 30)
    }
    
    func displayResultHeader(resultConfiguration: ResultHeaderConfiguration?) {
        resultViewController.displayResultHeader(resultConfiguration: resultConfiguration)
    }
    
    func alert(title:String,message:String){
        currentAlert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        if let currentAlert = self.currentAlert {
            currentAlert.addAction(UIAlertAction.init(title: "Ok", style: .destructive, handler: {[weak self] _ in
                self?.pop()
            }))
            self.present(currentAlert, animated: true, completion: nil)
        }
    }
}
