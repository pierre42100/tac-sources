//
//  ResultRuleCell.swift
//  Anticovid Verify
//
//

import UIKit

class ResultRuleCell: UITableViewCell {
    @IBOutlet weak var ruleDescription: UILabel!
    @IBOutlet weak var columnView: UIView!
}
