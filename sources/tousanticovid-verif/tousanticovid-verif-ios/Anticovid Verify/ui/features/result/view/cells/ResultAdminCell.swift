//
//  ResultAdminCell.swift
//  Anticovid Verify
//
//

import Foundation
import UIKit

import WebKit

class ResultAdminCell: UITableViewCell {
    
    @IBOutlet weak var informationLabel: UILabel!
    
    func makeCell(information: String, showDisclosure: Bool) {
        informationLabel.text = information
        accessoryType = showDisclosure ? .disclosureIndicator : .none
    }
    
    open override func layoutSubviews() {
        super.layoutSubviews()
        
        if let indicatorButton = allSubviews.compactMap({ $0 as? UIButton }).last {
            let image = indicatorButton.backgroundImage(for: .normal)?.withRenderingMode(.alwaysTemplate)
            indicatorButton.setBackgroundImage(image, for: .normal)
            indicatorButton.tintColor = .white
        }
    }
}

extension UIView {
    var allSubviews: [UIView] {
        return subviews.flatMap { [$0] + $0.allSubviews }
    }
}
