//
//  ResultCountryHeaderCell.swift
//  Anticovid Verify
//

import UIKit

class ResultCountryHeaderView: UITableViewHeaderFooterView {
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var containerBlurView: UIView!
    @IBOutlet weak var countryLabel: INLabel!
    @IBOutlet weak var validationImage: UIImageView!
    @IBOutlet weak var collaspeIndicator: UIImageView!
    
    static var identifier: String {
        return String(describing: self)
    }
    static var nib: UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        containerBlurView.alpha = 0.5
    }
    
    func makeCell(country: String, validation: RulesValidation?, collapseIsHidden: Bool, isCollaspe: Bool) {
        countryLabel.text = country
        validationImage.isHidden = false
        
        switch validation {
        case .neutral:
            validationImage.image = UIImage(named: "noData", in: Bundle.main, compatibleWith: nil)
        case .invalidate:
            validationImage.image = UIImage(named: "invalid", in: Bundle.main, compatibleWith: nil)
        case .validate:
            validationImage.image = UIImage(named: "valid", in: Bundle.main, compatibleWith: nil)
        case .none:
            validationImage.image = nil
        }
        
        collaspeIndicator.isHidden = collapseIsHidden
        collaspeIndicator.image = UIImage(systemName: isCollaspe ? "chevron.down" : "chevron.up")
    }
}
