//
//  MainViewPresenter.swift
//  Anticovid Verify
//

import Foundation
import Combine
import UIKit
import Network

protocol MainViewProtocol: AnyObject {
    func updateBannerView(text: String?)
    func displayAlert(_ alert: UIAlertController)
    func displaySwitchMode()
    func displaySyncAlert(didTapOnBanner: Bool)
    func displayLockViewsExeptBanner(_ value: Bool)
}

class MainViewPresenter {
    private weak var view: MainViewProtocol!
    private var bannerTypeEventQueue = Set<BannerType>() {
        // make it a publisher and observe it
        didSet {
            let mostPrioritized = bannerTypeEventQueue.sorted().first
            
            switch mostPrioritized {
            case .updateApplicationRequired:
                view.displayLockViewsExeptBanner(true)
            default:
                view.displayLockViewsExeptBanner(false)
            }
            
            view.updateBannerView(text: mostPrioritized?.rawValue)
        }
    }
    private var cancellables = [AnyCancellable]()
    private var pathMonitor: NWPathMonitor?
    private var networkQueue = DispatchQueue(label: "NetworkMonitoring", qos: .background)
    
    init(_ view: MainViewProtocol) {
        self.view = view
        subscribeToBannerEvents()
        subscribeToSwitchOTModeEvent()
        subscribeToScreenShotEvents()
        monitorNetworkChanges()
    }
    
    deinit {
        pathMonitor?.cancel()
    }
    
    func handleDidTapBannerView() {
        guard let bannerEvent = bannerTypeEventQueue.sorted().first else { return }
        
        switch bannerEvent {
        case .updateApplicationRequired, .updateApplicationAvailable:
            UIApplication.shared.goToAppStore()
        case .updateBannerSyncApp:
            view.displaySyncAlert(didTapOnBanner: true)
        }
    }
}

// MARK: Private funcs

private extension MainViewPresenter {
    func monitorNetworkChanges() {
        let pathMonitor = NWPathMonitor()
        pathMonitor.pathUpdateHandler = { pathMonitor in
            UserDataManager.sharedInstance.isNetworkConnected = pathMonitor.status == .satisfied
            NotificationCenter.default.post(name: ConstNotification.updateBannerSyncApp, object: nil)
        }
        pathMonitor.start(queue: networkQueue)
        self.pathMonitor = pathMonitor
    }
    
    func subscribeToBannerEvents() {
        // certificates sync
        NotificationCenter.default.publisher(for: ConstNotification.updateBannerSyncApp)
            .receive(on: DispatchQueue.main)
            .sink(receiveValue: { [weak self] _ in
                let remindStep = SyncManager.shared.actualAppSyncRemindStep()
                let isSynced = remindStep == .none
                let shouldShowSync = remindStep == .high || remindStep == .medium
                let bannerType = BannerType.updateBannerSyncApp
               
                if isSynced {
                    self?.bannerTypeEventQueue.remove(bannerType)
                } else {
                    self?.bannerTypeEventQueue.insert(bannerType)
                }
                
                if shouldShowSync {
                    self?.view.displaySyncAlert(didTapOnBanner: false)
                }
            })
            .store(in: &cancellables)
        
        // update app required
        NotificationCenter.default.publisher(for: ConstNotification.updateApplicationRequired)
            .receive(on: DispatchQueue.main)
            .sink(receiveValue: { [weak self] _ in
                guard let version = UserDataManager.sharedInstance.appLastMajorVersionAccepted,
                      ConstConf.build.toInt() <= version else {
                          self?.bannerTypeEventQueue.remove(.updateApplicationRequired)
                          return
                      }
                
                self?.bannerTypeEventQueue.insert(.updateApplicationRequired)
                
                let alert = UIAlertController(
                    title: "Mise à jour de l'application requise",
                    message: "Une nouvelle version de l'application est requise pour prendre en compte les dernières règles en vigueur",
                    preferredStyle: .alert
                )
                let action = UIAlertAction(title: "Ok", style: .destructive, handler: { handler in
                    UIApplication.shared.goToAppStore()
                    Timer.scheduledTimer(withTimeInterval: 0.2, repeats: false) { (timer) in
                        exit(0)
                    }
                })
                                                
                alert.addAction(action)
                self?.view.displayAlert(alert)
            })
            .store(in: &cancellables)
        // send value to init updateApplicationRequired
        NotificationCenter.default.post(name: ConstNotification.updateApplicationRequired,
                                        object: nil)
        
        // update app required
        NotificationCenter.default.publisher(for: ConstNotification.updateApplicationAvailable)
            .receive(on: DispatchQueue.main)
            .sink(receiveValue: { [weak self] _ in
                guard let version = UserDataManager.sharedInstance.appLastMinorVersionAccepted,
                      ConstConf.build.toInt() <= version else {
                          self?.bannerTypeEventQueue.remove(.updateApplicationAvailable)
                          return
                      }
                self?.bannerTypeEventQueue.insert(.updateApplicationAvailable)
            })
            .store(in: &cancellables)
        // send value to init updateApplicationAvailable
        NotificationCenter.default.post(name: ConstNotification.updateApplicationAvailable,
                                        object: nil)
    }
    
    func subscribeToSwitchOTModeEvent() {
        NotificationCenter.default.publisher(for: ConstNotification.navControllerAppareance)
            .receive(on: DispatchQueue.main)
            .sink { [weak self] notification in
                self?.view.displaySwitchMode()
            }
            .store(in: &cancellables)
    }
    
    func subscribeToScreenShotEvents() {
        NotificationCenter.default.publisher(for: UIApplication.userDidTakeScreenshotNotification)
            .receive(on: DispatchQueue.main)
            .sink(receiveValue: { [weak self] notification in
                let alert = UIAlertController(
                    title: "La capture d’écran n’est pas autorisée lors de l’utilisation de l’application TAC Verif.",
                    message: "En cas de non-respect de cet avertissement, vous vous exposez aux sanctions prévues par la Loi n° 78-17 du 6 janvier 1978 modifiée relative à l’informatique, aux fichiers et aux libertés, article 40 et suivants.",
                    preferredStyle: .alert
                )
                alert.addAction(UIAlertAction.init(title: "Ok", style: .destructive, handler:nil))
                self?.view.displayAlert(alert)
            })
            .store(in: &cancellables)
    }
}
