//
//  AppDelegate.swift
//  Anticovid Verify
//
//

import UIKit
import INCommunication
import Foundation
import BackgroundTasks
import os.log
import INCore
import Combine

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var networkView: UIView?
    private var cancellables = Set<AnyCancellable>()
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

        DispatchQueue.main.async {
            // Override point for customization after application launch.
            UINavigationController.updateNavigationBarTheme()
        }
        if SyncManager.shared.shouldStartLocalSync() {
            SyncManager.shared.startLocal()
                .map { true }
                .flatMap(SyncManager.shared.start(force:))
                .subscribe(on: DispatchQueue.global(qos: .background))
                .receive(on: DispatchQueue.global(qos: .background))
                .sink()
                .store(in: &self.cancellables)
        }
        
        BackgroundTaskManager.shared.register()
        
        BDDConfig.shared.saveContext()
        
        return true
    }
    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }
}
