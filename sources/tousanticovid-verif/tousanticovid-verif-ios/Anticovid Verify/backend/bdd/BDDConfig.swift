//
//  BDDConfig.swift
//  Anticovid Verify
//
//

import Foundation
import CoreData

class BDDConfig {
    
    static let shared = BDDConfig()
    
    lazy var statPersistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "Statistic")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    lazy var rulesMotorPersistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "MotorV3")
        /*let description = NSPersistentStoreDescription()
        description.shouldMigrateStoreAutomatically = false
        container.persistentStoreDescriptions = [description]*/
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    lazy var countriesPersistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "Countries")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    lazy var blacklistPersistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "Blacklist")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    func getStatContext() -> NSManagedObjectContext {
        Thread.isMainThread  ?
        statPersistentContainer.viewContext :
        statPersistentContainer.newBackgroundContext()
    }
    
    func getMotorContext() -> NSManagedObjectContext {
        let context = Thread.isMainThread ?
        rulesMotorPersistentContainer.viewContext :
        rulesMotorPersistentContainer.newBackgroundContext()
        
        context.mergePolicy = NSMergePolicy.overwrite
        
        return context
    }
    
    func getCountriesContext() -> NSManagedObjectContext {
        Thread.isMainThread ?
        countriesPersistentContainer.viewContext :
        countriesPersistentContainer.newBackgroundContext()
    }
    
    func getBlacklistContext() -> NSManagedObjectContext {
        Thread.isMainThread ?
        blacklistPersistentContainer.viewContext :
        blacklistPersistentContainer.newBackgroundContext()
    }
    
    func saveContext() {
        self.saveStatContext()
        self.saveRulesMotorContext()
        self.saveCountriesContext()
    }
    
    private func saveStatContext() {
        let context = statPersistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    private func saveRulesMotorContext() {
        let context = rulesMotorPersistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    private func saveCountriesContext() {
        let context = countriesPersistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
}
