//
//  ValueSetBaseResponse.swift
//  Anticovid Verify
//

import Foundation

struct ValueSetBaseResponse: Codable, Equatable {
    let id: String
    let hash: String
    
    static func == (lhs: Self, rhs: Self) -> Bool {
        lhs.id == rhs.id && lhs.hash == rhs.hash
    }
}
