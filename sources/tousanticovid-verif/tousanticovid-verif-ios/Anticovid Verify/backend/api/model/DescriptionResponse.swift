//
//  DescriptionResponse.swift
//  Anticovid Verify
//

import Foundation

struct DescriptionResponse: Codable {
    let lang: String
    let desc: String
}
