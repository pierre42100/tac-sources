![IN Groupe](http://www.w3.org/2000/svg)

Le code présenté ci-dessous est celui de l'application TAC Verif pour les OS Android et iOS
=====================
Ce code est publié sous une licence propre à IN Groupe disponible ici : [https://gitlab.inria.fr/tousanticovid-verif/tousanticovid-verif-ios/-/blob/ffffa948698ff38518de3fc9f1b7661cfa42e76c/LICENSE](https://gitlab.inria.fr/tousanticovid-verif/tousanticovid-verif-ios/-/blob/ffffa948698ff38518de3fc9f1b7661cfa42e76c/LICENSE), que toute personne s'engage à consulter et respecter avant toute utilisation du code objet de la présente publication. 


Usage et fonctionnalité
-------------------

Le code ainsi publié est celui de l'application TAC Verif dont l'usage est strictement réservé aux personnes habilitées et services autorisée dans le cadre de la Loi de Sortie de l'Etat d'Urgence Sanitaire du 2 juin 2021, article 1 et de ces décrets d'applications [https://www.legifrance.gouv.fr/jorf/id/JORFTEXT000043618403](https://www.legifrance.gouv.fr/jorf/id/JORFTEXT000043618403)


Cette application dispose de deux modes de fonctionnement :

    * Un mode lite (TAC Verif) destiné à des vérifications simples. Dans ce mode, sont affichés les noms, prénoms et date de naissance de la personne concernée par le justificatif, ainsi qu'un résultat positif ou négatif de détention d'un justificatif conforme.

    * Un mode détaillé (TAC Verif +) qui affiche plus de données correspondant aux éléments contenus dans le pass sanitaire (2D DOC ou QR européen) et réservé aux catégories de personnes dûment habilitées au titre de la réglementation en vigueur.


Applications & roadmap
-------------------

Les applications Android et iOS rendent les mêmes fonctionnalités. Elles ont été developpées à partir de sources distinctes qui peut expliquer les différences entre les deux codes (au delà des aspects techniques)

Les applications vont évoluer avec, entre autres, deux points déjà identifiés :

    * Le remplacement de la Librairie MLKit
    
    * Le remplacement du service AKAMAI par un service équivalent issue d'une entité européenne

Licences
-------------------

___Apache 2.0___

*Component*: EU Digital COVID Certificate App Core - iOS
*License Text URL*: [http://www.apache.org/licenses/LICENSE-2.0](http://www.apache.org/licenses/LICENSE-2.0)
*Source Code*: [https://github.com/eu-digital-green-certificates/dgca-app-core-io](https://github.com/eu-digital-green-certificates/dgca-app-core-io)

___MIT___

*Component*: SwiftJWT
*License Text URL*: [https://github.com/Kitura/Swift-JWT/blob/master/LICENSE](https://github.com/Kitura/Swift-JWT/blob/master/LICENSE)
*Source Code*: [https://github.com/Kitura/Swift-JWT](https://github.com/Kitura/Swift-JWT)


