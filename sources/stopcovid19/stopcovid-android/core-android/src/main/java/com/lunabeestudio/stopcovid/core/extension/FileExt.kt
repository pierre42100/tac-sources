/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2023/02/07 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.core.extension

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import com.lunabeestudio.stopcovid.core.UiConstants
import java.io.File

fun File.decodeToBitmap(): Bitmap? {
    val options = BitmapFactory.Options()
    options.inJustDecodeBounds = true
    BitmapFactory.decodeFile(path, options)
    options.inJustDecodeBounds = false
    options.inSampleSize = calculateInSampleSize(options)
    return BitmapFactory.decodeFile(path, options)
}

private fun calculateInSampleSize(options: BitmapFactory.Options): Int {
    // Raw height and width of image
    val (height: Int, width: Int) = options.run { outHeight to outWidth }
    var inSampleSize = 1

    if (height > UiConstants.ImageView.MaxSize || width > UiConstants.ImageView.MaxSize) {
        val halfHeight: Int = height / 2
        val halfWidth: Int = width / 2

        // Calculate the largest inSampleSize value that is a power of 2 and keeps both
        // height and width larger than the requested height and width.
        while (halfHeight / inSampleSize >= UiConstants.ImageView.MaxSize && halfWidth / inSampleSize >= UiConstants.ImageView.MaxSize) {
            inSampleSize *= 2
        }
    }

    return inSampleSize
}
