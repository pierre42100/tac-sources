/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2022/3/18 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.core.fastitem

import android.os.Parcelable
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.lunabeestudio.stopcovid.core.databinding.ItemHorizontalRecyclerviewBinding
import com.mikepenz.fastadapter.GenericItem
import com.mikepenz.fastadapter.adapters.GenericFastItemAdapter
import com.mikepenz.fastadapter.binding.AbstractBindingItem

class HorizontalRecyclerViewScrollSaver {
    private var recyclerViewState: Parcelable? = null
    private var recyclerView: RecyclerView? = null

    fun attachToRecycler(recyclerView: RecyclerView) {
        this.recyclerView = recyclerView
        recyclerView.layoutManager?.onRestoreInstanceState(recyclerViewState)
    }

    fun updateStateFromRecycler() {
        recyclerViewState = recyclerView?.layoutManager?.onSaveInstanceState()
    }

    fun detachRecycler() {
        recyclerView = null
    }
}

class HorizontalRecyclerViewItem(
    override val type: Int,
) : AbstractBindingItem<ItemHorizontalRecyclerviewBinding>() {
    var horizontalItems: List<GenericItem> = listOf()
    var viewPool: RecyclerView.RecycledViewPool? = null
    var horizontalRecyclerViewScrollSaver: HorizontalRecyclerViewScrollSaver? = null

    override fun createBinding(inflater: LayoutInflater, parent: ViewGroup?): ItemHorizontalRecyclerviewBinding {
        return ItemHorizontalRecyclerviewBinding.inflate(inflater, parent, false).apply {
            recyclerview.setRecycledViewPool(viewPool)
        }
    }

    override fun bindView(binding: ItemHorizontalRecyclerviewBinding, payloads: List<Any>) {
        super.bindView(binding, payloads)

        horizontalRecyclerViewScrollSaver?.attachToRecycler(binding.recyclerview)
        // Avoid clipping recycler when refresh
        @Suppress("UNCHECKED_CAST")
        var fastAdapter = binding.recyclerview.adapter as? GenericFastItemAdapter
        if (fastAdapter == null) {
            fastAdapter = GenericFastItemAdapter()
            binding.recyclerview.adapter = fastAdapter
        }
        fastAdapter.setNewList(horizontalItems)
    }

    override fun unbindView(binding: ItemHorizontalRecyclerviewBinding) {
        super.unbindView(binding)
        horizontalRecyclerViewScrollSaver?.updateStateFromRecycler()
    }
}

fun horizontalRecyclerViewItem(type: Int, block: (HorizontalRecyclerViewItem.() -> Unit)):
    HorizontalRecyclerViewItem = HorizontalRecyclerViewItem(type).apply(block)
