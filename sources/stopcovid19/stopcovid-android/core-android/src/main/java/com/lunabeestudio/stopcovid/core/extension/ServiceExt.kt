/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2021/27/8 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.core.extension

import android.app.Service
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.lunabeestudio.stopcovid.core.LocalizedApplication
import com.lunabeestudio.stopcovid.core.manager.LocalizedStrings
import com.lunabeestudio.stopcovid.core.utils.Event

val Service.strings: LocalizedStrings
    get() = (application as? LocalizedApplication)?.localizedStrings ?: emptyMap()

val Service.liveStrings: LiveData<Event<LocalizedStrings>>
    get() = (application as? LocalizedApplication)?.liveLocalizedStrings ?: MutableLiveData()
