/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2020/13/05 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.core.extension

import androidx.emoji.text.EmojiCompat
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import okhttp3.CacheControl
import okhttp3.OkHttpClient
import okhttp3.Request
import retrofit2.HttpException
import retrofit2.Response
import timber.log.Timber
import java.io.File
import java.util.IllegalFormatException
import java.util.concurrent.TimeUnit

@Deprecated("Use com.lunabeestudio.remote_android.ServerManager.saveTo")
@Suppress("BlockingMethodInNonBlockingContext")
suspend fun String.saveTo(okHttpClient: OkHttpClient, file: File): Boolean {
    return withContext(Dispatchers.IO) {
        val request: Request = Request.Builder().apply {
            headerAcceptJson()
            cacheControl(CacheControl.Builder().maxAge(10, TimeUnit.MINUTES).build())
            url(this@saveTo)
        }.build()

        val response = okHttpClient.newCall(request).execute()
        val body = response.body
        if (response.fromCacheOrEtags) {
            false
        } else if (response.isSuccessful && body != null) {
            body.byteStream().use { input ->
                file.outputStream().use { output ->
                    input.copyTo(output, 4 * 1024)
                }
            }
            true
        } else {
            throw HttpException(Response.error<Any>(body!!, response))
        }
    }
}

private fun Request.Builder.headerAcceptJson(): Request.Builder {
    header("Accept", "application/json")
    return this
}

fun CharSequence?.safeEmojiSpanify(): CharSequence? {
    return try {
        EmojiCompat.get().process(this ?: "")
    } catch (e: IllegalStateException) {
        this
    }
}

fun String.fixFormatter(): String = this
    .replace("%@", "%s")
    .replace(Regex("%\\d\\$@")) { matchResult ->
        matchResult.value.replace('@', 's')
    }

fun String?.formatOrNull(vararg args: Any?): String? {
    return this?.let {
        try {
            String.format(it, *args)
        } catch (e: IllegalFormatException) {
            Timber.e(e)
            it
        }
    }
}
