/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2022/3/18 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.core.fastitem

import android.content.res.ColorStateList
import android.graphics.drawable.GradientDrawable
import android.text.util.Linkify
import android.util.LayoutDirection
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.appcompat.view.ContextThemeWrapper
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.core.view.updateLayoutParams
import androidx.core.widget.TextViewCompat
import com.lunabeestudio.stopcovid.core.R
import com.lunabeestudio.stopcovid.core.databinding.ItemActionBinding
import com.lunabeestudio.stopcovid.core.databinding.ItemCardWithActionsBinding
import com.lunabeestudio.stopcovid.core.extension.fetchSystemColor
import com.lunabeestudio.stopcovid.core.extension.safeEmojiSpanify
import com.lunabeestudio.stopcovid.core.extension.setImageResourceOrHide
import com.lunabeestudio.stopcovid.core.extension.setOnClickListenerOrHideRipple
import com.lunabeestudio.stopcovid.core.extension.setTextOrHide
import com.lunabeestudio.stopcovid.core.extension.toDimensSize
import com.lunabeestudio.stopcovid.core.model.Action
import com.lunabeestudio.stopcovid.core.model.CardTheme
import com.mikepenz.fastadapter.binding.AbstractBindingItem

class CardWithActionsItem(private val cardTheme: CardTheme) : AbstractBindingItem<ItemCardWithActionsBinding>() {

    override val type: Int = cardTheme.name.hashCode()

    var cardTitle: String? = null

    @DrawableRes
    var cardTitleIcon: Int? = null

    @DrawableRes
    var mainTitleIcon: Int? = null

    @ColorRes
    var cardTitleColorRes: Int? = null

    @ColorRes
    var mainTitleColorRes: Int? = null

    // Gradient background, override theme
    private var gradientBackground: GradientDrawable? = null

    var mainTitle: String? = null
    private var mainHeader: String? = null
    var mainBody: CharSequence? = null
    var mainMaxLines: Int? = null
    var mainLayoutDirection: Int = LayoutDirection.INHERIT
    private var mainGravity: Int = Gravity.NO_GRAVITY

    @DrawableRes
    var mainImage: Int? = null

    var onCardClick: (() -> Unit)? = null
    var onDismissClick: (() -> Unit)? = null
    var contentDescription: String? = null
    var linkifyMainBody: Boolean = true

    var actions: List<Action>? = emptyList()

    override fun createBinding(inflater: LayoutInflater, parent: ViewGroup?): ItemCardWithActionsBinding {
        val context = inflater.context
        val themedInflater = LayoutInflater.from(ContextThemeWrapper(context, cardTheme.themeId))
        return ItemCardWithActionsBinding.inflate(themedInflater, parent, false)
    }

    override fun bindView(binding: ItemCardWithActionsBinding, payloads: List<Any>) {
        binding.cardTitleTextView.setTextOrHide(cardTitle) {
            cardTitleColorRes?.let {
                val color = ContextCompat.getColor(context, it)
                setTextColor(color)
                TextViewCompat.setCompoundDrawableTintList(this, ColorStateList.valueOf(color))
            }
            setCompoundDrawablesWithIntrinsicBounds(cardTitleIcon ?: 0, 0, 0, 0)
        }

        var mainLayoutVisible = false
        binding.mainHeaderTextView.setTextOrHide(mainHeader) { mainLayoutVisible = true }
        binding.mainTitleTextView.setTextOrHide(mainTitle) {
            mainLayoutVisible = true
            mainTitleColorRes?.let {
                val color = ContextCompat.getColor(context, it)
                setTextColor(color)
                TextViewCompat.setCompoundDrawableTintList(this, ColorStateList.valueOf(color))
            }
            setCompoundDrawablesWithIntrinsicBounds(mainTitleIcon ?: 0, 0, 0, 0)
        }
        binding.mainBodyTextView.setTextOrHide(mainBody) {
            mainLayoutVisible = true
            this@CardWithActionsItem.mainMaxLines?.let { maxLines = it }
            if (linkifyMainBody) {
                Linkify.addLinks(binding.mainBodyTextView, Linkify.WEB_URLS)
            }
        }
        binding.mainImageView.setImageResourceOrHide(mainImage)
        binding.mainHeaderTextView.gravity = mainGravity
        binding.mainTitleTextView.gravity = mainGravity
        binding.mainBodyTextView.gravity = mainGravity

        if (mainLayoutVisible) {
            binding.mainLayout.visibility = View.VISIBLE
            binding.mainLayout.layoutDirection = mainLayoutDirection
            binding.contentLayout.setOnClickListenerOrHideRipple(
                onCardClick?.let {
                    View.OnClickListener {
                        it()
                    }
                },
            )
        } else {
            binding.mainLayout.visibility = View.GONE
        }

        binding.contentLayout.isVisible = (mainLayoutVisible || binding.cardTitleTextView.isVisible)

        if (actions.isNullOrEmpty()) {
            binding.actionsLinearLayout.visibility = View.GONE
        } else {
            val count = actions?.size ?: 0
            val viewCount = binding.actionsLinearLayout.childCount

            if (count < viewCount) {
                repeat(viewCount - count) {
                    binding.actionsLinearLayout.removeViewAt(0)
                }
            } else if (count > viewCount) {
                repeat(count - viewCount) {
                    ItemActionBinding.inflate(
                        LayoutInflater.from(binding.root.context),
                        binding.actionsLinearLayout,
                        true,
                    )
                }
            }

            actions?.forEachIndexed { index, action ->
                action.run {
                    val actionBinding = ItemActionBinding.bind(binding.actionsLinearLayout.getChildAt(index))
                    actionBinding.actionDivider.isVisible = (index == 0 && mainLayoutVisible) || (index > 0)
                    actionBinding.textView.text = label.safeEmojiSpanify()
                    actionBinding.leftIconImageView.setImageResourceOrHide(icon)
                    actionBinding.badgeView.isVisible = showBadge
                    actionBinding.actionRootLayout.setOnClickListenerOrHideRipple(onClickListener)
                    actionBinding.arrowImageView.isVisible = onClickListener != null && showArrow
                    actionBinding.progressBar.isVisible = loading
                }
            }
            if (!binding.actionsLinearLayout.isVisible) {
                binding.actionsLinearLayout.visibility = View.VISIBLE
            }
        }

        gradientBackground?.let {
            binding.rootLayout.background = it
        }

        binding.rootLayout.background = gradientBackground ?: cardTheme.backgroundDrawableRes?.let {
            ContextCompat.getDrawable(binding.rootLayout.context, it)
        }

        binding.dismissImageView.setOnClickListener(
            onDismissClick?.let { onDismiss ->
                View.OnClickListener { onDismiss() }
            },
        )
        binding.dismissImageView.isVisible = onDismissClick != null

        if (onDismissClick != null && cardTitle == null) {
            setDismissButtonMargin(binding)
        }
    }

    private fun setDismissButtonMargin(binding: ItemCardWithActionsBinding) {
        (
            binding.mainHeaderTextView.takeIf { mainHeader != null }
                ?: binding.mainTitleTextView.takeIf { mainTitle != null }
                ?: binding.mainBodyTextView.takeIf { mainBody != null }
            )?.updateLayoutParams<ViewGroup.MarginLayoutParams> {
            marginEnd = R.dimen.min_touch_target_size.toDimensSize(binding.rootLayout.context).toInt()
            topMargin = R.dimen.spacing_medium.toDimensSize(binding.rootLayout.context).toInt()
        }
    }

    override fun unbindView(binding: ItemCardWithActionsBinding) {
        super.unbindView(binding)
        binding.mainBodyTextView.maxLines = Int.MAX_VALUE
        binding.mainBodyTextView.visibility = View.VISIBLE

        binding.cardTitleTextView.apply {
            val color = R.attr.colorAccent.fetchSystemColor(context)
            setTextColor(color)
            TextViewCompat.setCompoundDrawableTintList(this, ColorStateList.valueOf(color))
        }
        binding.mainTitleTextView.apply {
            val color = R.attr.colorOnSurface.fetchSystemColor(context)
            setTextColor(color)
            TextViewCompat.setCompoundDrawableTintList(this, ColorStateList.valueOf(color))
        }

        listOf(binding.mainHeaderTextView, binding.mainTitleTextView).forEach {
            it.updateLayoutParams<ViewGroup.MarginLayoutParams> {
                marginEnd = 0
                topMargin = 0
            }
        }
        binding.mainBodyTextView.updateLayoutParams<ViewGroup.MarginLayoutParams> {
            marginEnd = 0
            topMargin = R.dimen.spacing_small.toDimensSize(binding.rootLayout.context).toInt()
        }
        binding.mainBodyTextView.movementMethod = null
    }
}

fun cardWithActionItem(
    cardTheme: CardTheme = CardTheme.Default,
    block: (CardWithActionsItem.() -> Unit),
): CardWithActionsItem = CardWithActionsItem(cardTheme)
    .apply(block)
