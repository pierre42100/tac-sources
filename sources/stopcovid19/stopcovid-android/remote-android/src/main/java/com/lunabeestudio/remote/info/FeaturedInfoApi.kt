/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2023/03/08 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.remote.info

import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

internal interface FeaturedInfoApi {

    @GET("featured-{localSuffix}.json")
    suspend fun getFeaturedInfo(
        @Path("localSuffix") localSuffix: String,
    ): Response<List<ApiFeaturedInfo>>
}
