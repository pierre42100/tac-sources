/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2023/03/08 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.remote.certificate

import com.google.gson.Gson
import com.lunabeestudio.domain.di.DccBaseUrl
import com.lunabeestudio.domain.model.TacResult
import com.lunabeestudio.error.RemoteError
import com.lunabeestudio.remote.RetrofitClient
import com.lunabeestudio.remote.extension.toRemoteException
import com.lunabeestudio.repository.certificate.CertificateRemoteDataSource
import com.lunabeestudio.repository.crypto.SharedCryptoDataSource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import okhttp3.OkHttpClient
import timber.log.Timber
import javax.inject.Inject

class CertificateRemoteDataSourceImpl @Inject constructor(
    sharedCryptoDataSource: SharedCryptoDataSource,
    @DccBaseUrl baseUrl: String,
    okHttpClient: OkHttpClient,
) : CertificateRemoteDataSource {

    private val api: DccApi = RetrofitClient.getJsonService(baseUrl, DccApi::class.java, okHttpClient)
    private val serverKeyAgreementHelper = ServerKeyAgreementHelper(sharedCryptoDataSource)
    private val gson = Gson()
    override suspend fun generateMultipass(serverPublicKey: String, encodedCertificateList: List<String>): TacResult<String> {
        val serverKeyAgreementData = try {
            serverKeyAgreementHelper.encryptRequestData(serverPublicKey, encodedCertificateList)
        } catch (e: Exception) {
            Timber.e(e)
            return TacResult.Failure(
                RemoteError(RemoteError.Code.UNKNOWN, "Failed to encrypt certificate for dcc aggregation\n${e.message}"),
            )
        }

        val apiAggregateRQ = ApiAggregateRQ(
            serverKeyAgreementData.encodedLocalPublicKey,
            serverKeyAgreementData.encryptedData,
        )

        @Suppress("BlockingMethodInNonBlockingContext")
        return withContext(Dispatchers.IO) {
            try {
                val response = api.aggregate(apiAggregateRQ)
                if (response.isSuccessful) {
                    response.body()?.let {
                        val multipassJson = serverKeyAgreementHelper.decryptResponse(it.response)
                        val multipass = gson.fromJson(multipassJson, ApiAggregateCertificate::class.java).certificate
                        TacResult.Success(multipass)
                    } ?: TacResult.Failure(
                        RemoteError(RemoteError.Code.BACKEND, "Response successful but body is empty"),
                    )
                } else {
                    val error = response.errorBody()?.string()?.let { errorBody ->
                        gson.fromJson(errorBody, ApiAggregateError::class.java)
                    }
                    if (error != null) {
                        TacResult.Failure(
                            RemoteError(RemoteError.Code.BACKEND, error.message, errorCodes = error.errors.map { it.code }),
                        )
                    } else {
                        TacResult.Failure(RemoteError(RemoteError.Code.BACKEND))
                    }
                }
            } catch (e: Exception) {
                val remoteError = e.toRemoteException()
                TacResult.Failure(remoteError)
            }
        }
    }
}
