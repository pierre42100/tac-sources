/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2022/2/3 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.remote.keyfigures

import keynumbers.Keynumbers
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Path

internal interface KeyFiguresApi {
    @Headers(
        "Accept: application/x-protobuf",
    )
    @GET("v2{codePath}/key-figures-{localSuffix}.pb.gz")
    suspend fun getKeyFigure(
        @Path("localSuffix") localSuffix: String,
        @Path("codePath") codePath: String,
    ): Response<Keynumbers.KeyNumbersMessage>
}
