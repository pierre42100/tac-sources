/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2020/13/05 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.extension

import android.content.SharedPreferences
import androidx.core.content.edit
import com.lunabeestudio.stopcovid.Constants
import com.lunabeestudio.stopcovid.fastitem.StatePlay
import java.util.Date

val SharedPreferences.isOnBoardingDone: Boolean
    get() = getBoolean(Constants.SharedPrefs.ON_BOARDING_DONE, false)

var SharedPreferences.lastInfoCenterRefresh: Date
    get() = Date(getLong(Constants.SharedPrefs.LAST_INFO_CENTER_REFRESH, 0L))
    set(value) = edit { putLong(Constants.SharedPrefs.LAST_INFO_CENTER_REFRESH, value.time) }

var SharedPreferences.areInfoNotificationsEnabled: Boolean
    get() = getBoolean(Constants.SharedPrefs.ARE_INFO_NOTIFICATIONS_ENABLED, true)
    set(value) = edit { putBoolean(Constants.SharedPrefs.ARE_INFO_NOTIFICATIONS_ENABLED, value) }

var SharedPreferences.lastVersionCode: Int
    get() = getInt(Constants.SharedPrefs.LAST_VERSION_CODE, 0)
    set(value) = edit { putInt(Constants.SharedPrefs.LAST_VERSION_CODE, value) }

var SharedPreferences.lastVersionName: String?
    get() = getString(Constants.SharedPrefs.LAST_VERSION_NAME, null)
    set(value) = edit { putString(Constants.SharedPrefs.LAST_VERSION_NAME, value) }

var SharedPreferences.lastVersionMigration: Int
    get() = getInt(Constants.SharedPrefs.LAST_VERSION_MIGRATION, 0)
    set(value) = edit { putInt(Constants.SharedPrefs.LAST_VERSION_MIGRATION, value) }

var SharedPreferences.showSmartWallet: Boolean
    get() = getBoolean(Constants.SharedPrefs.SHOW_SMART_WALLET, true)
    set(value) = edit { putBoolean(Constants.SharedPrefs.SHOW_SMART_WALLET, value) }

var SharedPreferences.ratingsKeyFiguresOpening: Long
    get() = getLong(Constants.SharedPrefs.RATINGS_KEY_FIGURES_OPENING, 0L)
    set(value) = edit { putLong(Constants.SharedPrefs.RATINGS_KEY_FIGURES_OPENING, value) }

var SharedPreferences.ratingPopInShown: Boolean
    get() = getBoolean(Constants.SharedPrefs.RATINGS_SHOWN, false)
    set(value) = edit { putBoolean(Constants.SharedPrefs.RATINGS_SHOWN, value) }

var SharedPreferences.googleReviewShown: Boolean
    get() = getBoolean(Constants.SharedPrefs.GOOGLE_REVIEW_SHOWN, false)
    set(value) = edit { putBoolean(Constants.SharedPrefs.GOOGLE_REVIEW_SHOWN, value) }

var SharedPreferences.notificationVersionClosed: Int
    get() = getInt(Constants.SharedPrefs.NOTIFICATION_VERSION_CLOSED, 0)
    set(value) = edit { putInt(Constants.SharedPrefs.NOTIFICATION_VERSION_CLOSED, value) }

var SharedPreferences.lowStorageAlertShown: Boolean
    get() = getBoolean(Constants.SharedPrefs.LOW_STORAGE_ALERT_SHOWN, false)
    set(value) = edit { putBoolean(Constants.SharedPrefs.LOW_STORAGE_ALERT_SHOWN, value) }

var SharedPreferences.enableAutoFullscreenBrightness: Boolean
    get() = getBoolean(Constants.SharedPrefs.ENABLE_AUTO_FULLSCREEN_BRIGHTNESS, true)
    set(value) = edit { putBoolean(Constants.SharedPrefs.ENABLE_AUTO_FULLSCREEN_BRIGHTNESS, value) }

var SharedPreferences.keyFigureCompare1: String?
    get() = getString(Constants.SharedPrefs.COMPARE_KEY_FIGURE_LABEL_1, null)
    set(value) = edit { putString(Constants.SharedPrefs.COMPARE_KEY_FIGURE_LABEL_1, value) }

var SharedPreferences.keyFigureCompare2: String?
    get() = getString(Constants.SharedPrefs.COMPARE_KEY_FIGURE_LABEL_2, null)
    set(value) = edit { putString(Constants.SharedPrefs.COMPARE_KEY_FIGURE_LABEL_2, value) }

var SharedPreferences.notificationSent: Set<String>
    get() = getStringSet(Constants.SharedPrefs.SMART_WALLET_SENT_NOTIFICATIONS, null) ?: emptySet()
    set(value) = edit {
        putStringSet(Constants.SharedPrefs.SMART_WALLET_SENT_NOTIFICATIONS, value)
    }

var SharedPreferences.blacklistRefreshedAt: Date
    get() = Date(getLong(Constants.SharedPrefs.BLACKLIST_REFRESHED_AT, 0))
    set(value) = edit { putLong(Constants.SharedPrefs.BLACKLIST_REFRESHED_AT, value.time) }

var SharedPreferences.keyFigureMap: String?
    get() = getString(Constants.SharedPrefs.MAP_KEY_FIGURE_LABEL, null)
    set(value) = edit { putString(Constants.SharedPrefs.MAP_KEY_FIGURE_LABEL, value) }

var SharedPreferences.keyFigureState: StatePlay
    get() = getString(Constants.SharedPrefs.MAP_KEY_FIGURE_STATE, null)?.let { StatePlay.valueOf(it) } ?: StatePlay.PLAY
    set(value) = edit { putString(Constants.SharedPrefs.MAP_KEY_FIGURE_STATE, value.toString()) }

var SharedPreferences.keyFigureValue: Int
    get() = getInt(Constants.SharedPrefs.MAP_KEY_FIGURE_VALUE, 0)
    set(value) = edit { putInt(Constants.SharedPrefs.MAP_KEY_FIGURE_VALUE, value) }
