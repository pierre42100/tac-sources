/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2022/3/9 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.usecase

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Build
import androidx.core.app.NotificationCompat
import androidx.core.content.edit
import androidx.preference.PreferenceManager
import com.lunabeestudio.stopcovid.Constants
import com.lunabeestudio.stopcovid.TousAntiCovid
import com.lunabeestudio.stopcovid.activity.MainActivity
import com.lunabeestudio.stopcovid.core.UiConstants
import com.lunabeestudio.stopcovid.core.manager.StringsManager
import com.lunabeestudio.stopcovid.core.utils.ImmutablePendingIntentCompat
import com.lunabeestudio.stopcovid.extension.areInfoNotificationsEnabled

class SendInfoCenterNotificationUseCase(
    private val stringsManager: StringsManager,
    private val sharedPreferences: SharedPreferences,
    private val tousAntiCovid: TousAntiCovid,
) {
    suspend operator fun invoke() {
        if (sharedPreferences.areInfoNotificationsEnabled && !tousAntiCovid.isAppInForeground) {
            sendUpdateNotification()
        }
    }

    private suspend fun sendUpdateNotification() {
        PreferenceManager.getDefaultSharedPreferences(tousAntiCovid).edit {
            putBoolean(Constants.SharedPrefs.HAS_NEWS, true)
        }

        val notificationManager = tousAntiCovid.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        if (stringsManager.strings.isEmpty()) {
            stringsManager.initialize(tousAntiCovid)
        }
        val strings = stringsManager.strings

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(
                UiConstants.Notification.NEWS.channelId,
                strings["notification.channel.news.title"] ?: "News",
                NotificationManager.IMPORTANCE_DEFAULT,
            )
            notificationManager.createNotificationChannel(channel)
        }

        val notificationIntent = Intent(tousAntiCovid, MainActivity::class.java)
        val pendingIntent = ImmutablePendingIntentCompat.getActivity(
            tousAntiCovid,
            0,
            notificationIntent,
        )
        val notification = NotificationCompat.Builder(
            tousAntiCovid,
            UiConstants.Notification.NEWS.channelId,
        )
            .setContentTitle(strings["info.notification.newsAvailable.title"])
            .setPriority(NotificationCompat.PRIORITY_DEFAULT)
            .setAutoCancel(true)
            .setOnlyAlertOnce(true)
            .setSmallIcon(com.lunabeestudio.stopcovid.core.R.drawable.ic_notification_bar)
            .setStyle(
                NotificationCompat.BigTextStyle()
                    .bigText(strings["info.notification.newsAvailable.body"]),
            )
            .setContentIntent(pendingIntent)
            .build()
        notificationManager.notify(UiConstants.Notification.NEWS.notificationId, notification)
    }
}
