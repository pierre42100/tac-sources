/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2022/1/18 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.fragment

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.fragment.app.setFragmentResult
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.NavOptions
import androidx.navigation.fragment.navArgs
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.lunabeestudio.domain.model.TacResult
import com.lunabeestudio.domain.model.WalletCertificateType
import com.lunabeestudio.error.RemoteError
import com.lunabeestudio.stopcovid.R
import com.lunabeestudio.stopcovid.activity.MainActivity
import com.lunabeestudio.stopcovid.core.extension.findNavControllerOrNull
import com.lunabeestudio.stopcovid.core.fastitem.captionItem
import com.lunabeestudio.stopcovid.core.fastitem.dividerItem
import com.lunabeestudio.stopcovid.core.fastitem.spaceItem
import com.lunabeestudio.stopcovid.core.utils.Event
import com.lunabeestudio.stopcovid.extension.expirationString
import com.lunabeestudio.stopcovid.extension.injectionContainer
import com.lunabeestudio.stopcovid.extension.isSignatureExpired
import com.lunabeestudio.stopcovid.extension.multipassPickerCaption
import com.lunabeestudio.stopcovid.extension.openInExternalBrowser
import com.lunabeestudio.stopcovid.extension.safeNavigate
import com.lunabeestudio.stopcovid.extension.shortDateFormat
import com.lunabeestudio.stopcovid.extension.vaccineDose
import com.lunabeestudio.stopcovid.fastitem.selectionItem
import com.lunabeestudio.stopcovid.model.MultipassProfile
import com.lunabeestudio.stopcovid.viewmodel.MultipassCertificatesPickerUiModel
import com.lunabeestudio.stopcovid.viewmodel.MultipassCertificatesPickerViewModel
import com.lunabeestudio.stopcovid.viewmodel.MultipassCertificatesPickerViewModelFactory
import com.mikepenz.fastadapter.GenericItem
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

class MultipassCertificatesPickerFragment : BottomSheetMainFragment() {

    private val args by navArgs<MultipassCertificatesPickerFragmentArgs>()

    private val viewModel: MultipassCertificatesPickerViewModel by viewModels {
        MultipassCertificatesPickerViewModelFactory(
            profileId = args.profileId,
            getFilteredMultipassProfileFromIdUseCase = injectionContainer.getFilteredMultipassProfileFromIdUseCase,
            generateMultipassUseCase = injectionContainer.generateMultipassUseCase,
            configurationManager = injectionContainer.configurationManager,
            getSmartWalletStateUseCase = injectionContainer.getSmartWalletStateUseCase,
        )
    }

    override fun getTitleKey(): String = "multiPass.selectionScreen.title"

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.multipass.observe(viewLifecycleOwner) {
            refreshScreen()
        }

        viewModel.uiModel.observe(viewLifecycleOwner) { (bottomButtonState, profileNoDccMin, selectionMaxReached) ->
            when (bottomButtonState) {
                MultipassCertificatesPickerUiModel.ValidateButtonState.DISABLED -> setBottomButtonEnabled(false)
                MultipassCertificatesPickerUiModel.ValidateButtonState.ENABLED -> setBottomButtonEnabled(true)
            }

            if (profileNoDccMin?.getContentIfNotHandled(Event.NO_ID) != null) {
                findNavControllerOrNull()?.safeNavigate(
                    MultipassCertificatesPickerFragmentDirections
                        .actionMultipassCertificatesPickerFragmentToMultipassErrorFragment(
                            displayName = viewModel.multipass.value.getProfileDisplayText(),
                            errorKeys = emptyArray(),
                            notEnoughDcc = true,
                        ),
                    NavOptions.Builder().setPopUpTo(R.id.multipassCertificatesPickerFragment, true).build(),
                )
            }

            context?.let { ctx ->
                if (selectionMaxReached?.getContentIfNotHandled(Event.NO_ID) != null) {
                    MaterialAlertDialogBuilder(ctx)
                        .setTitle(strings["multiPass.selectionScreen.alert.maximumSelection.title"])
                        .setMessage(strings["multiPass.selectionScreen.alert.maximumSelection.subtitle"])
                        .setPositiveButton(strings["common.ok"], null)
                        .show()
                }
            }
        }
    }

    override suspend fun getItems(): List<GenericItem> {
        val items = ArrayList<GenericItem>()

        items += spaceItem {
            spaceRes = R.dimen.spacing_large
            identifier = items.size.toLong()
        }

        viewModel.multipass.value?.let { profile ->
            items += captionItem {
                text = stringsFormat("multiPass.selectionScreen.header.title", profile.getProfileDisplayText())
                identifier = "multiPass.selectionScreen.header.title".hashCode().toLong()
            }

            profile.certificates.forEachIndexed { index, certificate ->
                items += selectionItem {
                    title = if (certificate.type == WalletCertificateType.VACCINATION_EUROPE) {
                        certificate.greenCertificate.vaccineDose?.let { (first, second) ->
                            stringsFormat("wallet.proof.europe.vaccine.doses", first, second)
                        } ?: strings["enum.HCertType.${WalletCertificateType.VACCINATION_EUROPE.code}"]
                    } else {
                        strings["enum.HCertType.${certificate.type.code}"]
                    }?.uppercase()

                    caption = context?.let { ctx ->
                        certificate.multipassPickerCaption(
                            strings = strings,
                            context = ctx,
                            smartWalletState = viewModel.getSmartWalletState(certificate),
                            configuration = injectionContainer.configurationManager.configuration,
                        )
                    }

                    if (certificate.isSignatureExpired) {
                        footer = context?.let { ctx ->
                            certificate.expirationString(strings, shortDateFormat(ctx))
                        }
                    }

                    showSelection = viewModel.isCertificateSelected(certificate)
                    iconSelectionOff = R.drawable.ic_check_off
                    isEnabled = true
                    onClick = {
                        viewModel.toggleCertificate(certificate)
                        refreshScreen()
                    }
                    identifier = certificate.sha256.hashCode().toLong()
                }

                if (index < profile.certificates.size - 1) {
                    items += dividerItem {
                        identifier = items.size.toLong()
                    }
                }
            }
        }

        return items
    }

    private fun showMultipassConfirmationAlert() {
        context?.let {
            MaterialAlertDialogBuilder(it)
                .setTitle(strings["multiPass.CGU.alert.title"])
                .setMessage(strings["multiPass.CGU.alert.subtitle"])
                .setPositiveButton(strings["common.ok"]) { _, _ ->
                    requestMultipass()
                }
                .setNegativeButton(strings["common.cancel"], null)
                .setNeutralButton(strings["multiPass.CGU.alert.linkButton.title"]) { _, _ ->
                    context?.let { ctx -> strings["multiPass.CGU.alert.url"]?.openInExternalBrowser(ctx) }
                }
                .show()
        }
    }

    private fun requestMultipass() {
        viewModel.generateMultipass().onEach { result ->
            (activity as? MainActivity)?.showProgress(result is TacResult.Loading)

            when (result) {
                is TacResult.Loading -> { /* no-op */
                }
                is TacResult.Success -> {
                    context?.let { ctx ->
                        MaterialAlertDialogBuilder(ctx)
                            .setTitle(strings["multiPass.generation.successAlert.title"])
                            .setMessage(strings["multiPass.generation.successAlert.subtitle"])
                            .setPositiveButton(strings["multiPass.generation.successAlert.buttonTitle"]) { _, _ ->
                                setFragmentResult(
                                    GENERATE_MULTIPASS_RESULT_KEY,
                                    bundleOf(GENERATE_MULTIPASS_BUNDLE_KEY_CERTIFICATE_ID to result.successData.id),
                                )
                                findNavControllerOrNull()?.popBackStack()
                            }
                            .setCancelable(false)
                            .show()
                    }
                }
                is TacResult.Failure -> {
                    findNavControllerOrNull()?.safeNavigate(
                        MultipassCertificatesPickerFragmentDirections
                            .actionMultipassCertificatesPickerFragmentToMultipassErrorFragment(
                                displayName = viewModel.multipass.value.getProfileDisplayText(),
                                errorKeys = (result.throwable as? RemoteError)?.code?.errorCodes.orEmpty().toTypedArray(),
                                notEnoughDcc = false,
                            ),
                    )
                }
            }
        }.launchIn(lifecycleScope)
    }

    private fun MultipassProfile?.getProfileDisplayText(): String {
        return this?.let {
            firstname?.let { "$it " }.orEmpty() + lastname + formattedBirthdate?.let { " ($it)" }.orEmpty()
        }.orEmpty()
    }

    override fun onBottomSheetButtonClicked() {
        showMultipassConfirmationAlert()
    }

    override fun getBottomSheetButtonKey(): String = "multiPass.selectionScreen.button.validate"

    companion object {
        const val GENERATE_MULTIPASS_RESULT_KEY: String = "GENERATE_MULTIPASS_RESULT_KEY"
        const val GENERATE_MULTIPASS_BUNDLE_KEY_CERTIFICATE_ID: String = "GENERATE_MULTIPASS_BUNDLE_KEY_CERTIFICATE_ID"
    }
}
