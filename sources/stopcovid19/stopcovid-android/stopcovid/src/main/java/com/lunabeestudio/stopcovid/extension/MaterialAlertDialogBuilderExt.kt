/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2020/10/29 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.extension

import android.content.Context
import android.content.SharedPreferences
import android.os.Build
import android.view.LayoutInflater
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AlertDialog
import androidx.core.view.WindowCompat
import androidx.core.view.WindowInsetsCompat
import androidx.core.widget.doOnTextChanged
import androidx.lifecycle.lifecycleScope
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.lunabeestudio.common.extension.chosenPostalCode
import com.lunabeestudio.stopcovid.activity.MainActivity
import com.lunabeestudio.stopcovid.core.extension.viewLifecycleOwnerOrNull
import com.lunabeestudio.stopcovid.core.fragment.BaseFragment
import com.lunabeestudio.stopcovid.core.manager.LocalizedStrings
import com.lunabeestudio.stopcovid.databinding.DialogPasswordEditTextBinding
import com.lunabeestudio.stopcovid.databinding.DialogPostalCodeEditTextBinding
import kotlinx.coroutines.launch

fun MaterialAlertDialogBuilder.showPostalCodeDialog(
    layoutInflater: LayoutInflater,
    strings: LocalizedStrings,
    onPositiveButton: (String) -> Unit,
) {
    val postalCodeEditTextBinding = DialogPostalCodeEditTextBinding.inflate(layoutInflater)

    postalCodeEditTextBinding.textInputEditText.doOnTextChanged { _, _, _, _ ->
        postalCodeEditTextBinding.textInputLayout.error = null
    }

    postalCodeEditTextBinding.textInputLayout.hint = strings["home.infoSection.newPostalCode.alert.placeholder"]
    setTitle(strings["home.infoSection.newPostalCode.alert.title"])
    setMessage(strings["home.infoSection.newPostalCode.alert.subtitle"])
    setView(postalCodeEditTextBinding.textInputLayout)
    setPositiveButton(strings["common.confirm"]) { _, _ ->
        // Nothing to do here => we want to prevent dialog dismiss if error
    }
    setCancelable(false)
    setNegativeButton(strings["common.cancel"], null)
    val dialog = show()

    val positiveButton = dialog.getButton(AlertDialog.BUTTON_POSITIVE)

    postalCodeEditTextBinding.textInputEditText.setOnEditorActionListener { _, actionId, _ ->
        if (actionId == EditorInfo.IME_ACTION_DONE) {
            positiveButton.callOnClick()
            true
        } else {
            false
        }
    }

    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
        dialog.window?.let { window ->
            WindowCompat.getInsetsController(window, postalCodeEditTextBinding.root).show(WindowInsetsCompat.Type.ime())
        }
    } else {
        postalCodeEditTextBinding.textInputEditText.post {
            val inputMethodManager = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.showSoftInput(
                postalCodeEditTextBinding.textInputEditText,
                InputMethodManager.SHOW_IMPLICIT,
            )
        }
    }

    postalCodeEditTextBinding.textInputEditText.requestFocus()

    positiveButton.setOnClickListener {
        val result = postalCodeEditTextBinding.textInputEditText.text.toString()

        if (result.isPostalCode()) {
            onPositiveButton(postalCodeEditTextBinding.textInputEditText.text.toString())
            dialog.dismiss()
        } else {
            postalCodeEditTextBinding.textInputLayout.error = strings["home.infoSection.newPostalCode.alert.wrongPostalCode"]
        }
    }
}

fun MaterialAlertDialogBuilder.showPostalCodeDialog(
    layoutInflater: LayoutInflater,
    strings: LocalizedStrings,
    baseFragment: BaseFragment,
    sharedPrefs: SharedPreferences,
) {
    showPostalCodeDialog(layoutInflater, strings) { postalCode ->
        if (sharedPrefs.chosenPostalCode != postalCode) {
            sharedPrefs.chosenPostalCode = postalCode
            baseFragment.viewLifecycleOwnerOrNull()?.lifecycleScope?.launch {
                (baseFragment.activity as? MainActivity)?.showProgress(true)
                baseFragment.injectionContainer.keyFigureRepository.onAppForeground()
                (baseFragment.activity as? MainActivity)?.showProgress(false)
                baseFragment.refreshScreen()
            }
        }
    }
}

fun MaterialAlertDialogBuilder.showDbFailure(
    strings: LocalizedStrings,
    onRetry: (() -> Unit),
    onClear: Pair<String, (() -> Unit)>,
): AlertDialog {
    setTitle(strings["common.warning"])
    setMessage(strings["android.db.error"])
    setPositiveButton(strings["common.ok"], null)
    setNeutralButton(strings["common.retry"]) { _, _ -> onRetry() }
    setNegativeButton(strings[onClear.first]) { _, _ -> onClear.second() }
    setCancelable(false)
    return show()
}

fun MaterialAlertDialogBuilder.showRatingDialog(
    strings: LocalizedStrings,
    onConfirmation: (() -> Unit)?,
) {
    setTitle(strings["rating.alert.title"])
    setMessage(strings["rating.alert.message"])
    setPositiveButton(strings["common.ok"]) { _, _ ->
        onConfirmation?.invoke()
    }
    setNegativeButton(strings["common.cancel"], null)
    show()
}

fun MaterialAlertDialogBuilder.showPasswordDialog(
    layoutInflater: LayoutInflater,
    strings: LocalizedStrings,
    inError: Boolean,
    onPositiveButton: (String) -> Unit,
) {
    val dialogPasswordEditTextBinding = DialogPasswordEditTextBinding.inflate(layoutInflater)

    dialogPasswordEditTextBinding.textInputEditText.doOnTextChanged { _, _, _, _ ->
        dialogPasswordEditTextBinding.textInputLayout.error = null
    }

    dialogPasswordEditTextBinding.textInputLayout.hint = strings["pdfImport.protected.enterPassword.alert.placeholder"]
    setTitle(strings["pdfImport.protected.enterPassword.alert.title"])
    setMessage(strings["pdfImport.protected.enterPassword.alert.message"])
    setView(dialogPasswordEditTextBinding.textInputLayout)
    setPositiveButton(strings["common.confirm"]) { _, _ ->
        onPositiveButton(dialogPasswordEditTextBinding.textInputEditText.text.toString())
    }
    setCancelable(false)
    setNegativeButton(strings["common.cancel"], null)
    val dialog = show()

    val positiveButton = dialog.getButton(AlertDialog.BUTTON_POSITIVE)

    dialogPasswordEditTextBinding.textInputEditText.setOnEditorActionListener { _, actionId, _ ->
        if (actionId == EditorInfo.IME_ACTION_DONE) {
            positiveButton.callOnClick()
            true
        } else {
            false
        }
    }
    if (inError) {
        dialogPasswordEditTextBinding.textInputLayout.error = strings["pdfImport.protected.wrongPassword.alert.message"]
    }
}
