/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2022/1/18 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.fragment

import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.fragment.app.clearFragmentResultListener
import androidx.fragment.app.setFragmentResultListener
import androidx.fragment.app.viewModels
import androidx.navigation.NavController
import androidx.navigation.NavDestination
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.lunabeestudio.stopcovid.R
import com.lunabeestudio.stopcovid.activity.MainActivity
import com.lunabeestudio.stopcovid.core.extension.findNavControllerOrNull
import com.lunabeestudio.stopcovid.core.extension.findParentFragmentByType
import com.lunabeestudio.stopcovid.core.fastitem.cardWithActionItem
import com.lunabeestudio.stopcovid.core.fastitem.spaceItem
import com.lunabeestudio.stopcovid.core.model.Action
import com.lunabeestudio.stopcovid.extension.findNavControllerOrNull
import com.lunabeestudio.stopcovid.extension.injectionContainer
import com.lunabeestudio.stopcovid.extension.openInExternalBrowser
import com.lunabeestudio.stopcovid.extension.safeNavigate
import com.lunabeestudio.stopcovid.model.CloseMultipassProfiles
import com.lunabeestudio.stopcovid.model.MultipassProfileSelectionItemData
import com.lunabeestudio.stopcovid.viewmodel.WalletMultipassViewModel
import com.lunabeestudio.stopcovid.viewmodel.WalletMultipassViewModelFactory
import com.mikepenz.fastadapter.GenericItem

class WalletMultipassFragment : MainFragment(), PagerTabFragment {

    private val viewModel by viewModels<WalletMultipassViewModel> {
        WalletMultipassViewModelFactory(
            injectionContainer.getMultipassProfilesUseCase,
            injectionContainer.getCloseMultipassProfilesUseCase,
        )
    }

    private var closeProfiles: CloseMultipassProfiles? = null

    override fun getTitleKey(): String = "walletController.title"

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupMultipassPickerResultListener()
    }

    override fun refreshScreen() {
        super.refreshScreen()
        (activity as? MainActivity)?.binding?.tabLayout?.isVisible = true
    }

    override suspend fun getItems(): List<GenericItem> {
        val items = ArrayList<GenericItem>()

        items += spaceItem {
            spaceRes = R.dimen.spacing_large
            identifier = items.size.toLong()
        }

        if (closeProfiles?.isNotEmpty() == true) {
            items += cardWithActionItem {
                mainTitle = strings["multiPass.tab.similarProfile.title"]
                mainBody = stringsFormat(
                    "multiPass.tab.similarProfile.subtitle",
                    closeProfiles?.joinToString { profile ->
                        profile.firstname?.let { "$it " }.orEmpty() +
                            profile.lastname +
                            profile.formattedBirthdate?.let { " ($it)" }.orEmpty()
                    },
                )
                actions = listOfNotNull(
                    strings["multiPass.tab.similarProfile.url"]?.takeIf { it.isNotEmpty() }?.let { url ->
                        Action(label = strings["multiPass.tab.similarProfile.linkButton.title"]) {
                            context?.let { ctx -> url.openInExternalBrowser(ctx) }
                        }
                    },
                )
                identifier = "multiPass.tab.similarProfile.title".hashCode().toLong()
            }

            items += spaceItem {
                spaceRes = R.dimen.spacing_large
                identifier = items.size.toLong()
            }
        }

        items += cardWithActionItem {
            mainTitle = strings["multiPass.tab.explanation.title"]
            mainBody = strings["multiPass.tab.explanation.subtitle"]
            actions = listOfNotNull(
                strings["multiPass.tab.explanation.url"]?.takeIf { it.isNotEmpty() }?.let { url ->
                    Action(label = strings["multiPass.tab.explanation.linkButton.title"]) {
                        context?.let { ctx -> url.openInExternalBrowser(ctx) }
                    }
                },
            )
            identifier = "multiPass.tab.explanation.subtitle".hashCode().toLong()
        }

        items += spaceItem {
            spaceRes = R.dimen.spacing_large
            identifier = items.size.toLong()
        }

        return items
    }

    private fun setupMultipassPickerResultListener() {
        val requestKey = MultipassCertificatesPickerFragment.GENERATE_MULTIPASS_RESULT_KEY
        findParentFragmentByType<WalletContainerFragment>()?.let { walletContainerFragment ->
            walletContainerFragment.setFragmentResultListener(requestKey) { _, bundle ->
                walletContainerFragment.clearFragmentResultListener(requestKey)
                val certificateId = bundle.getString(
                    MultipassCertificatesPickerFragment.GENERATE_MULTIPASS_BUNDLE_KEY_CERTIFICATE_ID,
                    null,
                )
                if (certificateId != null) {
                    walletContainerFragment.binding?.walletNavHostFragment
                        ?.findNavControllerOrNull()
                        ?.currentBackStackEntry
                        ?.savedStateHandle?.set(
                            WalletPagerFragment.SAVED_STATE_HANDLE_SCROLL_TO_CERTIFICATE_KEY,
                            certificateId,
                        )
                }
            }
        }
    }

    override fun onTabSelected() {
        binding?.recyclerView?.let { (activity as? MainActivity)?.binding?.appBarLayout?.setLiftOnScrollTargetView(it) }

        findParentFragmentByType<WalletContainerFragment>()?.let { walletContainerFragment ->
            val multipassProfiles = viewModel.getMultipassProfiles()
            closeProfiles = viewModel.getCloseMultipassProfiles(multipassProfiles)
            // Check if we already have the view, or let onViewCreated call refreshScreen
            if (view != null) {
                refreshScreen()
            }

            walletContainerFragment.setupBottomAction(strings["multiPass.tab.generation.button.title"]) {
                when (multipassProfiles.size) {
                    0 -> context?.let { ctx ->
                        MaterialAlertDialogBuilder(ctx)
                            .setTitle(strings["multiPass.noProfile.alert.title"])
                            .setMessage(strings["multiPass.noProfile.alert.subtitle"])
                            .setPositiveButton(strings["common.ok"], null)
                            .show()
                    }
                    1 -> {
                        walletContainerFragment.findNavControllerOrNull()?.safeNavigate(
                            WalletContainerFragmentDirections.actionWalletContainerFragmentToNavNewMultipass(
                                multipassProfiles.first().id,
                            ),
                        )
                    }
                    else -> {
                        walletContainerFragment.setFragmentResultListener(
                            ChooseMultipassProfileBottomSheetDialogFragment.CHOOSE_MULTIPASS_PROFILE_RESULT_KEY,
                        ) { _, data ->
                            data.getString(
                                ChooseMultipassProfileBottomSheetDialogFragment.CHOOSE_MULTIPASS_PROFILE_BUNDLE_KEY_ID_SELECTED,
                            )?.let { profileId ->
                                walletContainerFragment.findNavControllerOrNull()?.addOnDestinationChangedListener(
                                    object : NavController.OnDestinationChangedListener {
                                        override fun onDestinationChanged(
                                            controller: NavController,
                                            destination: NavDestination,
                                            arguments: Bundle?,
                                        ) {
                                            if (controller.currentDestination?.id == R.id.walletContainerFragment) {
                                                walletContainerFragment.findNavControllerOrNull()?.safeNavigate(
                                                    WalletContainerFragmentDirections
                                                        .actionWalletContainerFragmentToNavNewMultipass(profileId),
                                                )
                                                controller.removeOnDestinationChangedListener(this)
                                            }
                                        }
                                    },
                                )
                            }
                        }

                        walletContainerFragment.findNavControllerOrNull()?.safeNavigate(
                            WalletContainerFragmentDirections
                                .actionWalletContainerFragmentToChooseMultipassProfileBottomSheetDialogFragment(
                                    multipassProfiles.map { multipassProfile ->
                                        val title = multipassProfile.firstname ?: multipassProfile.lastname
                                        val caption = multipassProfile.formattedBirthdate

                                        MultipassProfileSelectionItemData(
                                            id = multipassProfile.id,
                                            title = title,
                                            caption = caption,
                                        )
                                    }.toTypedArray(),
                                ),
                        )
                    }
                }
            }
        }
    }
}
