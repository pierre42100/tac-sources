/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2022/12/21 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.fragment

import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import com.lunabeestudio.stopcovid.R
import com.lunabeestudio.stopcovid.core.fastitem.captionItem
import com.lunabeestudio.stopcovid.core.fastitem.secondaryButtonItem
import com.lunabeestudio.stopcovid.core.fastitem.spaceItem
import com.lunabeestudio.stopcovid.core.fastitem.titleItem
import com.lunabeestudio.stopcovid.core.fragment.FastAdapterBottomSheetDialogFragment
import com.lunabeestudio.stopcovid.extension.collectDataWithLifecycle
import com.lunabeestudio.stopcovid.extension.injectionContainer
import com.lunabeestudio.stopcovid.fastitem.bigTitleItem
import com.lunabeestudio.stopcovid.viewmodel.DetailFigureMapBottomSheetViewModel
import com.lunabeestudio.stopcovid.viewmodel.DetailFigureMapBottomSheetViewModelFactory
import com.mikepenz.fastadapter.GenericItem

class DetailFigureMapBottomSheetFragment : FastAdapterBottomSheetDialogFragment() {

    val args: DetailFigureMapBottomSheetFragmentArgs by navArgs()

    val viewModel: DetailFigureMapBottomSheetViewModel by viewModels {
        DetailFigureMapBottomSheetViewModelFactory(
            injectionContainer.keyFigureRepository,
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.fetchKeyFigure(args.labelKey)

        viewModel.keyFigure.collectDataWithLifecycle(viewLifecycleOwner) {
            refreshScreen()
        }
    }

    override fun refreshScreen() {
        val keyFigure = viewModel.keyFigure.value
        val items = ArrayList<GenericItem>()
        items += spaceItem {
            spaceRes = R.dimen.spacing_medium
            identifier = items.size.toLong()
        }
        items += bigTitleItem {
            text = strings["${keyFigure?.labelKey}.label"]
            identifier = "label".hashCode().toLong()
        }
        items += captionItem {
            text = strings["${keyFigure?.labelKey}.description"]
            identifier = "description".hashCode().toLong()
        }

        strings["${keyFigure?.labelKey}.learnMore"]?.let {
            items += spaceItem {
                spaceRes = R.dimen.spacing_large
                identifier = items.size.toLong()
            }
            items += titleItem {
                text = strings["common.readMore"]
                identifier = "common.readMore".hashCode().toLong()
            }
            items += captionItem {
                text = it
                identifier = "learnMore".hashCode().toLong()
            }
        }

        items += spaceItem {
            spaceRes = R.dimen.spacing_medium
            identifier = items.size.toLong()
        }

        items += secondaryButtonItem {
            text = strings["common.ok"]
            identifier = "common.ok".hashCode().toLong()
            width = ViewGroup.LayoutParams.MATCH_PARENT
            onClickListener = View.OnClickListener {
                this@DetailFigureMapBottomSheetFragment.dismiss()
            }
        }

        items += spaceItem {
            spaceRes = R.dimen.spacing_medium
            identifier = items.size.toLong()
        }

        adapter.setNewList(items)
    }
}
