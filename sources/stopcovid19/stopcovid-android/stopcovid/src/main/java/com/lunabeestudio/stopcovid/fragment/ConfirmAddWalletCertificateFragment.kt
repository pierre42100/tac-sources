/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2022/3/18 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.fragment

import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.core.view.doOnNextLayout
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.navArgs
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.lunabeestudio.common.extension.safeEnumValueOf
import com.lunabeestudio.domain.model.TacResult
import com.lunabeestudio.domain.model.WalletCertificateType
import com.lunabeestudio.error.LocalError
import com.lunabeestudio.stopcovid.activity.MainActivity
import com.lunabeestudio.stopcovid.core.extension.appCompatActivity
import com.lunabeestudio.stopcovid.core.extension.findNavControllerOrNull
import com.lunabeestudio.stopcovid.core.extension.getApplicationLocale
import com.lunabeestudio.stopcovid.core.extension.safeEmojiSpanify
import com.lunabeestudio.stopcovid.core.extension.setTextOrHide
import com.lunabeestudio.stopcovid.core.fragment.BaseFragment
import com.lunabeestudio.stopcovid.databinding.FragmentConfirmAddWalletCertificateBinding
import com.lunabeestudio.stopcovid.extension.formatDccText
import com.lunabeestudio.stopcovid.extension.injectionContainer
import com.lunabeestudio.stopcovid.extension.raw
import com.lunabeestudio.stopcovid.extension.safeNavigate
import com.lunabeestudio.stopcovid.extension.showDbFailure
import com.lunabeestudio.stopcovid.extension.showUnknownErrorAlert
import com.lunabeestudio.stopcovid.extension.walletCertificateError
import com.lunabeestudio.stopcovid.model.EuropeanCertificate
import com.lunabeestudio.stopcovid.model.WalletCertificate
import com.lunabeestudio.stopcovid.viewmodel.ConfirmAddWalletCertificateViewModel
import com.lunabeestudio.stopcovid.viewmodel.ConfirmAddWalletCertificateViewModelFactory
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.text.SimpleDateFormat

class ConfirmAddWalletCertificateFragment : BaseFragment() {

    private val args: ConfirmAddWalletCertificateFragmentArgs by navArgs()

    private var binding: FragmentConfirmAddWalletCertificateBinding? = null

    private var dbFailureDialog: AlertDialog? = null

    private val viewModel by viewModels<ConfirmAddWalletCertificateViewModel> {
        ConfirmAddWalletCertificateViewModelFactory(
            injectionContainer.walletRepository,
        )
    }

    private var certificate: WalletCertificate? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        lifecycleScope.launch {
            certificate = validateAndGetCertificate(
                certificateCode = args.certificateCode,
                certificateFormat = args.certificateFormat?.let { safeEnumValueOf<WalletCertificateType.Format>(it) },
            )
            refreshScreen()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentConfirmAddWalletCertificateBinding.inflate(inflater, container, false)
        (activity as? MainActivity)?.showProgress(true)
        return binding?.root!!
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if ((activity as? MainActivity)?.binding?.tabLayout?.isVisible == true) {
            postponeEnterTransition()
            (activity as? MainActivity)?.binding?.appBarLayout?.doOnNextLayout {
                startPostponedEnterTransition()
            }
            (activity as? MainActivity)?.binding?.tabLayout?.isVisible = false
        }
    }

    override fun refreshScreen() {
        appCompatActivity?.supportActionBar?.title = strings["confirmWalletQrCodeController.title"]

        binding?.apply {
            certificate?.let { certificate ->
                walletCaptionTextView.textView.setTextOrHide(strings["confirmWalletQrCodeController.explanation.title"])
                walletCaptionTextView.textView.gravity = Gravity.CENTER

                walletTitleTextView.textSwitcher.setText(
                    strings["confirmWalletQrCodeController.explanation.subtitle"]?.safeEmojiSpanify(),
                )
                walletTitleTextView.textView1.gravity = Gravity.CENTER
                walletTitleTextView.textView2.gravity = Gravity.CENTER

                walletAddButton.setTextOrHide(strings["confirmWalletQrCodeController.confirm"])
                walletAddButton.setOnClickListener {
                    lifecycleScope.launch {
                        checkAndProcessCertificate(certificate)
                    }
                }

                walletCancelButton.setTextOrHide(strings["common.cancel"])
                walletCancelButton.setOnClickListener {
                    findNavControllerOrNull()?.navigateUp()
                }

                (activity as? MainActivity)?.showProgress(false)
                walletConfirmContentGroup.isVisible = true
            }
        }
    }

    private suspend fun checkAndProcessCertificate(certificate: WalletCertificate) {
        showDuplicateWarningIfNeeded(certificate) {
            showCloseProfileIfNeeded((certificate as? EuropeanCertificate)) {
                lifecycleScope.launch {
                    processCertificate(certificate)
                }
            }
        }
    }

    private suspend fun validateAndGetCertificate(
        certificateCode: String,
        certificateFormat: WalletCertificateType.Format?,
    ): WalletCertificate? {
        return try {
            injectionContainer.verifyAndGetCertificateCodeValueUseCase(
                certificateCode,
                certificateFormat,
            )
        } catch (e: Exception) {
            (activity as? MainActivity)?.showProgress(false)
            handleCertificateError(e, certificateCode, null)
            null
        }
    }

    private suspend fun processCertificate(certificate: WalletCertificate) {
        try {
            (activity as? MainActivity)?.showProgress(true)
            viewModel.saveCertificate(certificate, context)
            injectionContainer.debugManager.logSaveCertificates(certificate.raw, "from wallet")

            val shouldShowDccValidityFragment = (certificate as? EuropeanCertificate)?.let { europeanCertificate ->
                DccValidityFragment.shouldShowDccValidityFragment(
                    validity = injectionContainer.computeDccValidityUseCase(europeanCertificate),
                    europeanCertificate = europeanCertificate,
                )
            } ?: false

            if (shouldShowDccValidityFragment) {
                findNavControllerOrNull()?.safeNavigate(
                    ConfirmAddWalletCertificateFragmentDirections.actionConfirmAddWalletCertificateFragmentToDccValidityFragment(
                        certificate.id,
                    ),
                )
            } else {
                strings["walletController.addCertificate.addSucceeded"]?.let {
                    (activity as? MainActivity)?.showSnackBar(it)
                }
                findNavControllerOrNull()?.navigateUp()
            }
        } catch (e: Exception) {
            handleCertificateError(e, certificate.type.code, certificate)
        } finally {
            (activity as? MainActivity)?.showProgress(false)
        }
    }

    private suspend fun showDuplicateWarningIfNeeded(
        certificate: WalletCertificate,
        onContinue: () -> Unit,
    ) {
        if (viewModel.isDuplicated(certificate)) {
            withContext(Dispatchers.Main) {
                MaterialAlertDialogBuilder(requireContext())
                    .setTitle(strings["common.warning"])
                    .setMessage(strings["walletController.alert.duplicatedCertificate.subtitle"])
                    .setPositiveButton(strings["walletController.alert.duplicatedCertificate.confirm"]) { _, _ ->
                        onContinue()
                    }
                    .setNegativeButton(strings["common.cancel"]) { _, _ ->
                        findNavControllerOrNull()?.navigateUp()
                    }
                    .show()
            }
        } else {
            onContinue()
        }
    }

    private fun showCloseProfileIfNeeded(
        dcc: EuropeanCertificate?,
        onContinue: () -> Unit,
    ) {
        if (dcc != null && injectionContainer.hasCloseMultipassProfileUseCase(dcc)) {
            val applicationLocale = context.getApplicationLocale()
            val message = dcc.formatDccText(
                inputText = strings["walletController.addSimilarProfileCertificate.alert.message"],
                strings = strings,
                dateFormat = SimpleDateFormat.getDateInstance(SimpleDateFormat.SHORT, applicationLocale),
                dateTimeFormat = SimpleDateFormat.getDateTimeInstance(
                    SimpleDateFormat.SHORT,
                    SimpleDateFormat.SHORT,
                    applicationLocale,
                ),
                forceEnglish = false,
                smartWalletState = null,
            )

            MaterialAlertDialogBuilder(requireContext())
                .setTitle(strings["walletController.addSimilarProfileCertificate.alert.title"])
                .setMessage(message)
                .setPositiveButton(strings["walletController.addSimilarProfileCertificate.alert.add"]) { _, _ ->
                    onContinue()
                }
                .setNegativeButton(strings["common.cancel"], null)
                .show()
        } else {
            onContinue()
        }
    }

    private suspend fun handleCertificateError(
        error: Exception,
        certificateCode: String?,
        certificate: WalletCertificate?,
    ) {
        val certificateType = certificateCode?.let { WalletCertificate.getTypeFromValue(it) } ?: WalletCertificateType.VACCINATION_EUROPE
        handleCertificateError(error, certificateType, certificate)
    }

    private fun handleCertificateError(
        error: Exception,
        certificateType: WalletCertificateType,
        certificate: WalletCertificate?,
    ) {
        val certificateError = error.walletCertificateError()
        when {
            certificateError != null -> {
                findNavControllerOrNull()?.safeNavigate(
                    ConfirmAddWalletCertificateFragmentDirections.actionConfirmAddWalletCertificateFragmentToWalletCertificateErrorFragment(
                        certificateType,
                        certificateError,
                    ),
                )
            }
            (error as? LocalError)?.code == LocalError.Code.SECRET_KEY_ALREADY_GENERATED && certificate != null -> {
                // Dismiss db failure dialog which might be trigger by db read failure
                dbFailureDialog?.dismiss()
                dbFailureDialog = null
                showDbFailureIfNeeded(certificate)
            }
            else -> showUnknownErrorAlert(null)
        }
    }

    private fun showDbFailureIfNeeded(certificate: WalletCertificate?) {
        if (dbFailureDialog == null) {
            lifecycleScope.launch {
                val result = viewModel.certificates

                val resetKeystoreAndSaveCertificate = { certificate: WalletCertificate ->
                    kotlin.runCatching {
                        lifecycleScope.launch {
                            viewModel.resetWalletCryptoKeyGeneratedFlag()
                            viewModel.saveCertificate(certificate, context)
                            viewModel.deleteLostCertificates() // key has been override, no more chance to recover
                            processCertificate(certificate) // re-process without checks
                        }
                    }.onFailure {
                        showDbFailureIfNeeded(certificate)
                    }
                }

                // result will always be Success if the wallet is empty, so also check for certificate saving flow (certificate != null)
                if (result is TacResult.Failure) {
                    context?.let { ctx ->
                        dbFailureDialog = MaterialAlertDialogBuilder(ctx)
                            .setOnDismissListener {
                                if (it == dbFailureDialog) { // In case of concurrent dismiss, do not nullify another dialog instance
                                    dbFailureDialog = null
                                }
                            }
                            .showDbFailure(
                                strings,
                                onRetry = {
                                    dbFailureDialog?.dismiss()
                                    dbFailureDialog = null
                                    lifecycleScope.launch {
                                        viewModel.forceRefreshCertificates()
                                        if (certificate == null) {
                                            showDbFailureIfNeeded(null)
                                        } else {
                                            kotlin.runCatching {
                                                viewModel.saveCertificate(certificate, context)
                                            }.onFailure {
                                                showDbFailureIfNeeded(certificate)
                                            }
                                        }
                                    }
                                },
                                onClear = "android.db.error.clearWallet" to {
                                    dbFailureDialog?.dismiss()
                                    dbFailureDialog = null
                                    if (certificate != null) {
                                        resetKeystoreAndSaveCertificate(certificate)
                                    } else {
                                        viewModel.deleteLostCertificates()
                                    }

                                    injectionContainer.debugManager.logReinitializeWallet()
                                },
                            )
                    }
                } else if (result is TacResult.Success && certificate != null) {
                    // If the user wants to add a new certificate, but
                    //  • We lost the keystore
                    //  • There no certificate in the wallet (result is TacResult.Success)
                    // -> Silently reset the key generated flag and add the certificate. Else show the dbFailureDialog
                    resetKeystoreAndSaveCertificate(certificate)
                }
            }
        }
    }
}
