/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2022/3/18 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.extension

import android.content.Context
import android.text.Spannable
import android.text.style.ImageSpan
import androidx.core.content.ContextCompat
import com.lunabeestudio.stopcovid.R
import com.lunabeestudio.stopcovid.core.extension.toDimensSize
import kotlin.math.roundToInt

fun Spannable.transformHeartEmoji(context: Context) {
    val heartIndex = this.indexOf("❤️")
    if (heartIndex >= 0) {
        ContextCompat.getDrawable(context, R.drawable.ic_empty_heart_24)?.let { drawable ->
            val textHeight = R.dimen.caption_font_size.toDimensSize(context)
            val ratio = textHeight / drawable.intrinsicHeight.toFloat()
            drawable.setBounds(
                0,
                0,
                (drawable.intrinsicWidth * ratio).roundToInt(),
                (drawable.intrinsicHeight * ratio).roundToInt(),
            )
            this.setSpan(
                ImageSpan(drawable),
                heartIndex,
                heartIndex + 1,
                Spannable.SPAN_INCLUSIVE_EXCLUSIVE,
            )
        }
    }
}
