/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2023/01/17 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.model

import android.text.Spanned
import com.lunabeestudio.stopcovid.core.manager.LocalizedStrings
import com.lunabeestudio.stopcovid.extension.formatNumberIfNeeded
import com.lunabeestudio.stopcovid.extension.fromHtml
import java.text.NumberFormat
import java.util.Date
import kotlin.math.abs

data class KeyFigureComparisonData(
    val labelKeyFigure: String,
    val descriptionData: KeyFigureComparisonDescriptionData?,
    val keyFigureComparisonDayMinusSeven: KeyFigureComparisonRowData?,
    val keyFigureComparisonMinValue: KeyFigureComparisonRowData,
    val keyFigureComparisonMaxValue: KeyFigureComparisonRowData,
)

data class KeyFigureComparisonRowData(
    val date: List<Date>,
    val value: String,
    val labelRes: String,
)

sealed interface KeyFigureComparisonDescriptionData {
    val oldValue: String
    val percentEvolution: String

    fun formatString(strings: LocalizedStrings, numberFormat: NumberFormat): Spanned?

    data class Percentage(
        override val oldValue: String,
        override val percentEvolution: String,
        val pointsDiff: String,
    ) : KeyFigureComparisonDescriptionData {

        override fun formatString(strings: LocalizedStrings, numberFormat: NumberFormat): Spanned? {
            val suffix = if (abs(pointsDiff.toFloat()) < 2) ".singular" else ""
            return strings["keyFigureDetailController.section.comparison.description.percentage$suffix"]?.format(
                percentEvolution.formatNumberIfNeeded(numberFormat, true),
                pointsDiff.formatNumberIfNeeded(numberFormat, true),
                oldValue.formatNumberIfNeeded(numberFormat),
            )?.fromHtml()
        }
    }

    data class Float(
        override val oldValue: String,
        override val percentEvolution: String,
    ) : KeyFigureComparisonDescriptionData {

        override fun formatString(strings: LocalizedStrings, numberFormat: NumberFormat): Spanned? {
            return strings["keyFigureDetailController.section.comparison.description.float"]?.format(
                percentEvolution.formatNumberIfNeeded(numberFormat, true),
                oldValue.formatNumberIfNeeded(numberFormat),
            )?.fromHtml()
        }
    }
}

enum class KeyFigureComparisonDataType {
    MIN, MAX
}
