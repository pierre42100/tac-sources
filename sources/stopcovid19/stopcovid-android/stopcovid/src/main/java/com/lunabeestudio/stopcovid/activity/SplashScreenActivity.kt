/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2022/3/18 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.activity

import android.app.ActivityOptions
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.core.view.WindowInsetsControllerCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.preference.PreferenceManager
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.lunabeestudio.stopcovid.R
import com.lunabeestudio.stopcovid.TousAntiCovid
import com.lunabeestudio.stopcovid.core.extension.isNightMode
import com.lunabeestudio.stopcovid.core.extension.liveStrings
import com.lunabeestudio.stopcovid.core.extension.strings
import com.lunabeestudio.stopcovid.core.manager.LocalizedStrings
import com.lunabeestudio.stopcovid.core.utils.Event
import com.lunabeestudio.stopcovid.databinding.ActivitySplashScreenBinding
import com.lunabeestudio.stopcovid.extension.isOnBoardingDone
import kotlinx.coroutines.Job
import kotlinx.coroutines.cancel
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlin.system.exitProcess
import kotlin.time.Duration.Companion.seconds

// TODO androidx SplashScreen
@SuppressWarnings("CustomSplashScreen")
class SplashScreenActivity : BaseActivity() {

    private val sharedPreferences: SharedPreferences by lazy {
        PreferenceManager.getDefaultSharedPreferences(this)
    }
    private var splashLoadingJob: Job? = null
    private var noStringDialog: AlertDialog? = null
    private lateinit var splashScreenBinding: ActivitySplashScreenBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        splashScreenBinding = ActivitySplashScreenBinding.inflate(layoutInflater)

        // Wait 2 + 5 seconds to load strings from file or server. Show blocking error if we still don't have strings.
        splashLoadingJob = lifecycleScope.launchWhenResumed {
            delay(2.seconds)
            splashScreenBinding.progressBar.show()
            delay(5.seconds)
            splashScreenBinding.progressBar.hide()
            showNoStringsErrorDialog()
        }

        setContentView(splashScreenBinding.root)
        WindowInsetsControllerCompat(window, window.decorView).isAppearanceLightStatusBars = !isNightMode()

        if (strings.isEmpty()) {
            val stringsObserver = object : Observer<Event<LocalizedStrings>> {
                override fun onChanged(strings: Event<LocalizedStrings>) {
                    if (strings.peekContent().isNotEmpty()) {
                        liveStrings.removeObserver(this)
                        startOnBoardingOrMain(sharedPreferences.isOnBoardingDone)
                    }
                }
            }
            liveStrings.observe(this@SplashScreenActivity, stringsObserver)
        } else {
            startOnBoardingOrMain(sharedPreferences.isOnBoardingDone)
        }
    }

    private fun startOnBoardingOrMain(onBoardingDone: Boolean) {
        splashLoadingJob?.cancel("Starting on boarding")
        splashScreenBinding.progressBar.hide()
        noStringDialog?.dismiss()

        if (onBoardingDone) {
            val intent = Intent(this@SplashScreenActivity, MainActivity::class.java).apply {
                data = intent.data
                addFlags(intent.flags)
            }
            startActivity(
                intent,
                ActivityOptions
                    .makeCustomAnimation(
                        this@SplashScreenActivity,
                        R.anim.nav_default_enter_anim,
                        R.anim.nav_default_exit_anim,
                    )
                    .toBundle(),
            )
            finishAndRemoveTask()
        } else {
            val intent = Intent(this@SplashScreenActivity, OnBoardingActivity::class.java).apply {
                data = intent.data
            }
            startActivity(
                intent,
                ActivityOptions
                    .makeCustomAnimation(
                        this@SplashScreenActivity,
                        R.anim.nav_default_enter_anim,
                        R.anim.nav_default_exit_anim,
                    )
                    .toBundle(),
            )
            finishAndRemoveTask()
        }
    }

    private fun showNoStringsErrorDialog() {
        if (!isFinishing) {
            if (strings.isEmpty()) {
                noStringDialog = MaterialAlertDialogBuilder(this@SplashScreenActivity)
                    .setTitle(getString(R.string.splashScreenErrorDialog_title, getString(R.string.app_name)))
                    .setMessage(R.string.splashScreenErrorDialog_message)
                    .setPositiveButton(R.string.splashScreenErrorDialog_positiveButton) { _, _ ->
                        splashLoadingJob = lifecycleScope.launch {
                            splashScreenBinding.progressBar.show()
                            (application as? TousAntiCovid)?.injectionContainer?.stringsManager?.let { stringsManager ->
                                stringsManager.initialize(this@SplashScreenActivity)
                                stringsManager.onAppForeground(this@SplashScreenActivity)
                            }
                            splashScreenBinding.progressBar.hide()
                            showNoStringsErrorDialog()
                        }
                    }
                    .setNegativeButton(R.string.splashScreenErrorDialog_negativeButton) { _, _ ->
                        exitProcess(0)
                    }
                    .setOnDismissListener {
                        noStringDialog = null
                    }
                    .show()
            } else {
                startOnBoardingOrMain(sharedPreferences.isOnBoardingDone)
            }
        }
    }
}
