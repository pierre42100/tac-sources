/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2022/12/21 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.view.map.view

import android.content.Context
import android.graphics.Color
import android.graphics.RectF
import android.util.AttributeSet
import android.widget.FrameLayout
import androidx.annotation.ColorInt
import androidx.annotation.DrawableRes
import com.lunabeestudio.stopcovid.Constants
import com.lunabeestudio.stopcovid.view.map.richpath.RichPath
import com.lunabeestudio.stopcovid.view.map.richpath.RichPathView
import com.lunabeestudio.stopcovid.view.map.view.marker.KeyFigureMarkerView

class MapView @JvmOverloads constructor(
    context: Context, val attrs: AttributeSet? = null, defStyleAttr: Int = 0,
) : FrameLayout(context, attrs, defStyleAttr) {

    @DrawableRes
    var mapDrawable: Int? = null
    private lateinit var mapPath: RichPathView

    var getMarkerView: (() -> KeyFigureMarkerView)? = null
    var regions: List<Region> = listOf()
    private var selectedRegions: Region? = null
    private var marker: KeyFigureMarkerView? = null
    var textNoData: String? = null

    @ColorInt
    var defaultRegionBackgroundColor: Int = Color.LTGRAY

    @ColorInt
    var defaultRegionSelectedColor: Int = Color.GRAY

    @ColorInt
    var regionStrokeColor: Int = Color.WHITE

    init {
        addPathView()
    }

    private fun toggleSelectByClick() {
        mapPath.setOnPatchClickListener {
            it.toggle()
            updateMap()
        }
    }

    fun resetMap() {
        removeAllViews()
        addPathView()
        selectedRegions = null
        updateMap()
        toggleSelectByClick()
    }

    private fun addPathView() {
        mapPath = RichPathView(context)
        mapDrawable?.let { mapPath.setVectorDrawable(it) }
        addView(mapPath)
    }

    fun updateMap() {
        mapPath.findAllRichPaths().forEach { richPath ->
            val isRegionSelected = selectedRegions?.xmlName?.clearZoomString() == richPath.xmlName?.clearZoomString()
            richPath.fillColor = if (isRegionSelected) richPath.regionSelectedColor() else richPath.regionBackgroundColor()
            richPath.strokeColor = regionStrokeColor
        }
        // Update selected region with new one
        regions.firstOrNull { it.xmlName == selectedRegions?.xmlName }?.let {
            selectedRegions = it
        }
        // Update marker
        drawMarkers()
        selectedRegions?.let { selectedRegion ->
            val regionData = regions.firstOrNull { it.xmlName == selectedRegion.xmlName.clearZoomString() }
                ?: selectedRegion
            marker?.bindView(regionData)
        }
    }

    private fun RichPath.getRegionFromRichPath(): Region? {
        return regions.firstOrNull { it.xmlName == this.xmlName?.clearZoomString() }
    }

    private fun RichPath.regionBackgroundColor(): Int {
        val region = this.getRegionFromRichPath()
        return region?.backgroundColor ?: defaultRegionBackgroundColor
    }

    private fun RichPath.regionSelectedColor(): Int {
        val region = this.getRegionFromRichPath()
        return region?.colorSelected ?: defaultRegionSelectedColor
    }

    private fun RichPath.toggle() {
        selectedRegions = if (selectedRegions?.xmlName == this.xmlName) {
            null
        } else {
            regions.firstOrNull { it.xmlName == this.xmlName } ?: getRegionNoData(this.xmlName)
        }
    }

    private fun getRegionNoData(xmlName: String?): Region? {
        return xmlName?.let {
            Region(
                xmlName,
                textNoData,
                null,
                null,
                null,
                null,
                "",
            )
        }
    }

    private fun drawMarkers() {
        marker?.let { removeView(marker) }
        selectedRegions?.let { region ->
            val regionPath = mapPath.findRichPathByName(region.xmlName)
            val regionBounds = RectF()
            regionPath?.computeBounds(regionBounds, true)
            val markerView = getMarkerView?.invoke()
            markerView?.let { mv ->
                val regionData = regions.firstOrNull { it.xmlName == region.xmlName.clearZoomString() } ?: region
                mv.bindView(regionData)
                addView(mv)
                mv.measure(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT)
                val width: Int = mv.measuredWidth
                val height: Int = mv.measuredHeight
                mv.translationY = minOf(regionBounds.centerY(), this.height.toFloat() - height - Constants.Map.PADDING_MARKER_MAP_VIEW)
                mv.translationX = minOf(regionBounds.centerX(), this.width.toFloat() - width - Constants.Map.PADDING_MARKER_MAP_VIEW)
                marker = mv
            }
        }
    }
}

private fun String.clearZoomString(): String {
    return this.replace("-zoom", "")
}
