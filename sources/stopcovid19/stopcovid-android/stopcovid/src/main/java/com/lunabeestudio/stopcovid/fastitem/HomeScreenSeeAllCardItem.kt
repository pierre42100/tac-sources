/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2022/3/18 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.fastitem

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.lunabeestudio.stopcovid.R
import com.lunabeestudio.stopcovid.core.extension.setTextOrHide
import com.lunabeestudio.stopcovid.databinding.ItemHomeScreenSeeAllCardBinding
import com.mikepenz.fastadapter.binding.AbstractBindingItem

class HomeScreenSeeAllCardItem : AbstractBindingItem<ItemHomeScreenSeeAllCardBinding>() {
    override val type: Int = R.id.item_home_screen_see_all_figure_card

    var seeAllText: String? = null
    var onClick: View.OnClickListener? = null

    override fun createBinding(inflater: LayoutInflater, parent: ViewGroup?): ItemHomeScreenSeeAllCardBinding {
        return ItemHomeScreenSeeAllCardBinding.inflate(inflater, parent, false)
    }

    override fun bindView(binding: ItemHomeScreenSeeAllCardBinding, payloads: List<Any>) {
        super.bindView(binding, payloads)
        binding.apply {
            root.setOnClickListener(onClick)
            seeAllTextView.setTextOrHide(seeAllText)
        }
    }

    override fun unbindView(binding: ItemHomeScreenSeeAllCardBinding) {
        super.unbindView(binding)
        binding.apply {
            root.setOnClickListener(null)
            seeAllTextView.text = null
        }
    }
}

fun homeScreenSeeAllCardItem(
    block: (HomeScreenSeeAllCardItem.() -> Unit),
): HomeScreenSeeAllCardItem = HomeScreenSeeAllCardItem().apply(block)
