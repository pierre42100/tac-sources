/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2020/29/10 - for the STOP-COVID project
 */

package com.lunabeestudio.stopcovid.fragment

import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import com.lunabeestudio.common.ConfigConstant
import com.lunabeestudio.domain.model.WalletCertificateType
import com.lunabeestudio.stopcovid.R
import com.lunabeestudio.stopcovid.activity.MainActivity
import com.lunabeestudio.stopcovid.core.extension.findNavControllerOrNull
import com.lunabeestudio.stopcovid.core.extension.findParentFragmentByType
import com.lunabeestudio.stopcovid.core.fastitem.cardWithActionItem
import com.lunabeestudio.stopcovid.core.fastitem.spaceItem
import com.lunabeestudio.stopcovid.extension.injectionContainer
import com.lunabeestudio.stopcovid.extension.safeNavigate
import com.lunabeestudio.stopcovid.fastitem.logoItem
import com.lunabeestudio.stopcovid.fastitem.walletSingleDocumentCardItem
import com.lunabeestudio.stopcovid.viewmodel.WalletInfoViewModel
import com.lunabeestudio.stopcovid.viewmodel.WalletInfoViewModelFactory
import com.mikepenz.fastadapter.GenericItem
import java.io.File

class WalletInfoFragment : MainFragment(), PagerTabFragment {

    private val viewModel by viewModels<WalletInfoViewModel> {
        WalletInfoViewModelFactory(
            injectionContainer.walletRepository,
        )
    }

    override fun getTitleKey(): String = "walletController.title"

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.certificatesCount.observe(viewLifecycleOwner) { certificatesCount ->
            if (certificatesCount == 0 || parentFragment is WalletPagerFragment) {
                refreshScreen()

                if (certificatesCount == 0) {
                    onTabSelected() // init bottom button
                }
            } else if (certificatesCount != null) {
                findNavControllerOrNull()?.safeNavigate(
                    WalletInfoFragmentDirections.actionWalletInfoFragmentToWalletPagerFragment(),
                )
            }
        }
    }

    override fun refreshScreen() {
        super.refreshScreen()
        (activity as? MainActivity)?.binding?.tabLayout?.isVisible = parentFragment is WalletPagerFragment
    }

    override suspend fun getItems(): List<GenericItem> {
        val items = ArrayList<GenericItem>()

        items += logoItem {
            imageRes = R.drawable.wallet
            identifier = R.drawable.wallet.toLong()
        }

        items += cardWithActionItem {
            mainTitle = strings["walletController.howDoesItWork.title"]
            mainBody = strings["walletController.howDoesItWork.subtitle"]
            identifier = "walletController.howDoesItWork.subtitle".hashCode().toLong()
        }

        items += spaceItem {
            spaceRes = R.dimen.spacing_large
            identifier = items.size.toLong()
        }

        items += walletSingleDocumentCardItem {
            mainTitle = strings["walletController.documents.title"]
            mainBody = strings["walletController.documents.subtitle"]
            certificateFile = File(
                context?.filesDir,
                ConfigConstant.Wallet.VACCIN_EUROPE_CERTIFICATE_THUMBNAIL_FILE,
            )
            certificateDrawable = R.drawable.vaccin_europe_certificate
            onClick = {
                findParentFragmentByType<WalletContainerFragment>()?.findNavControllerOrNull()
                    ?.safeNavigate(
                        WalletContainerFragmentDirections.actionWalletContainerFragmentToCertificateDocumentExplanationFragment(
                            WalletCertificateType.VACCINATION_EUROPE,
                        ),
                    )
            }
            identifier = "walletController.documents.subtitle".hashCode().toLong()
        }

        items += spaceItem {
            spaceRes = R.dimen.spacing_large
            identifier = items.size.toLong()
        }

        return items
    }

    override fun onTabSelected() {
        binding?.recyclerView?.let { (activity as? MainActivity)?.binding?.appBarLayout?.setLiftOnScrollTargetView(it) }

        findParentFragmentByType<WalletContainerFragment>()?.let { walletContainerFragment ->
            if (
                injectionContainer.configurationManager.configuration.multipassConfig?.isEnabled == true &&
                (viewModel.certificatesCount.value ?: 0) > 0
            ) {
                walletContainerFragment.setupBottomAction(null, null)
            } else {
                walletContainerFragment.setupBottomAction(strings["walletController.addCertificate"]) {
                    walletContainerFragment.findNavControllerOrNull()
                        ?.safeNavigate(
                            WalletContainerFragmentDirections.actionWalletContainerFragmentToWalletQRCodeFragment(),
                        )
                }
            }
        }
    }
}
