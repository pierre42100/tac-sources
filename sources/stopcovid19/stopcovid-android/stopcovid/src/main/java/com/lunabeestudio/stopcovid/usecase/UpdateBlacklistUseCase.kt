/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2022/3/2 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.usecase

import android.content.SharedPreferences
import com.lunabeestudio.domain.model.RawWalletCertificate
import com.lunabeestudio.domain.model.TacResult
import com.lunabeestudio.domain.repository.BlacklistRepository
import com.lunabeestudio.domain.syncmanager.BlacklistSyncManager
import com.lunabeestudio.stopcovid.Constants
import com.lunabeestudio.stopcovid.extension.blacklistRefreshedAt
import com.lunabeestudio.stopcovid.extension.isExpired
import com.lunabeestudio.stopcovid.extension.raw
import com.lunabeestudio.stopcovid.manager.ConfigurationManager
import com.lunabeestudio.stopcovid.model.EuropeanCertificate
import com.lunabeestudio.stopcovid.repository.WalletRepository
import kotlinx.coroutines.flow.firstOrNull
import kotlinx.coroutines.flow.last
import kotlinx.coroutines.withTimeoutOrNull
import java.util.Date
import kotlin.time.Duration
import kotlin.time.Duration.Companion.milliseconds
import kotlin.time.Duration.Companion.seconds

class UpdateBlacklistUseCase(
    private val configurationManager: ConfigurationManager,
    private val walletRepository: WalletRepository,
    private val blacklistRepository: BlacklistRepository,
    private val blacklistSyncManager: BlacklistSyncManager,
    private val sharedPreferences: SharedPreferences,
    private val computeDccValidityUseCase: ComputeDccValidityUseCase,
) {
    suspend operator fun invoke(skipIndexUpdate: Boolean = false, certificateId: String? = null): TacResult<Unit> {
        val timeSinceLastRefresh = (System.currentTimeMillis() - sharedPreferences.blacklistRefreshedAt.time).milliseconds
        val blacklistRefreshFrequency = configurationManager.configuration.blacklistRefreshFrequency ?: Duration.INFINITE

        return if (timeSinceLastRefresh < blacklistRefreshFrequency && certificateId == null) {
            TacResult.Success(Unit)
        } else {
            val certificates = if (certificateId != null) {
                walletRepository.getCertificateById(certificateId)?.let(::listOf) ?: emptyList()
            } else {
                withTimeoutOrNull(5.seconds) {
                    walletRepository.walletCertificateFlow.firstOrNull { it !is TacResult.Loading }?.data
                } ?: emptyList()
            }

            val list = if (skipIndexUpdate) {
                certificates.filter { it.isBlacklisted == null }
            } else {
                val indexResult = blacklistSyncManager.syncIndexes().last()
                when {
                    // Index error, do not update blacklistRefreshedAt and retry next time
                    indexResult is TacResult.Failure -> return TacResult.Failure(indexResult.throwable)
                    // No change only check unknown certificate blacklist state
                    (indexResult is TacResult.Success && indexResult.successData == null) ->
                        certificates.filter { it.isBlacklisted == null }
                    // Index has changed, check every certificates
                    else -> certificates
                }
            }
            val filteredCertificates = list.filterNot { certificate ->
                // Do not check expired certificates
                (certificate as? EuropeanCertificate)?.let { dcc ->
                    val validity = computeDccValidityUseCase(dcc)
                    dcc.isExpired(configurationManager.configuration, validity)
                } ?: false
            }

            // Group by blacklisted prefix (or null)
            val groupedCertificates = filteredCertificates.groupBy {
                blacklistRepository.getMatchPrefix(it.sha256)
            }

            // Get hash list for each blacklisted prefix and update certificates if needed
            val certificatesToUpdate = mutableListOf<RawWalletCertificate>()
            groupedCertificates
                .forEach { (key, certificates) ->
                    val hashes = key?.let {
                        val prefixHashesResult = blacklistRepository.getPrefixHashes(it)
                        when (prefixHashesResult) {
                            is TacResult.Loading, // unexpected loading
                            is TacResult.Failure,
                            -> {
                                return@forEach // Skip prefix, it will retry next time the index changes
                            }
                            is TacResult.Success -> prefixHashesResult.successData
                        }
                    } ?: emptyList()

                    certificates.forEach { certificate ->
                        val isBlacklisted = hashes.contains(
                            certificate.sha256.take(Constants.Certificate.BLACKLIST_HASH_LENGTH),
                        )
                        if (certificate.isBlacklisted != isBlacklisted) {
                            certificatesToUpdate += certificate.raw.apply {
                                this.isBlacklisted = isBlacklisted
                            }
                        }
                    }
                }

            walletRepository.updateCertificate(certificatesToUpdate)
            sharedPreferences.blacklistRefreshedAt = Date()

            TacResult.Success(Unit)
        }
    }
}
