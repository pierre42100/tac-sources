/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by tarek360, Modified by Lunabee Studio / Date - 2022/12/21 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.view.map.richpath

import android.annotation.SuppressLint
import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import androidx.annotation.DrawableRes
import androidx.appcompat.widget.AppCompatImageView
import com.lunabeestudio.stopcovid.view.map.richpath.model.Vector
import com.lunabeestudio.stopcovid.view.map.richpath.util.XmlParser
import org.xmlpull.v1.XmlPullParserException
import java.io.IOException

class RichPathView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0,
) : AppCompatImageView(context, attrs, defStyleAttr) {

    private lateinit var vector: Vector
    private var richPathDrawable: RichPathDrawable? = null
    private var onPathClickListener: RichPath.OnPathClickListener? = null

    init {
        // Disable hardware
        setLayerType(View.LAYER_TYPE_SOFTWARE, null)
    }

    @SuppressLint("ResourceType")
    fun setVectorDrawable(@DrawableRes resId: Int) {
        val xpp = context.resources.getXml(resId)
        vector = Vector()
        try {
            XmlParser.parseVector(vector, xpp, context)
            richPathDrawable = RichPathDrawable(vector, scaleType)
            setImageDrawable(richPathDrawable)
        } catch (e: IOException) {
            e.printStackTrace()
        } catch (e: XmlPullParserException) {
            e.printStackTrace()
        }
    }

    fun findAllRichPaths(): Array<RichPath> {
        return richPathDrawable?.findAllRichPaths() ?: arrayOf()
    }

    fun findRichPathByName(name: String): RichPath? {
        return richPathDrawable?.findRichPathByName(name)
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent?): Boolean {
        richPathDrawable?.getTouchedPath(event)?.let { richPath ->
            richPath.onPathClickListener?.onClick(richPath)
            this.onPathClickListener?.onClick(richPath)
        }
        return true
    }

    fun setOnPatchClickListener(action: (RichPath) -> Unit) {
        onPathClickListener = object : RichPath.OnPathClickListener {
            override fun onClick(richPath: RichPath) {
                action.invoke(richPath)
            }
        }
    }
}
