/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2020/13/05 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.extension

import com.lunabeestudio.error.AppError
import com.lunabeestudio.error.LocalError
import com.lunabeestudio.error.RemoteError
import com.lunabeestudio.error.TACError

fun TACError.getString(strings: Map<String, String>): String = when (this) {
    is AppError -> getString(strings)
    is LocalError -> getString(strings)
    is RemoteError -> getString(strings)
}

private fun AppError.getString(strings: Map<String, String>): String = when (this.code) {
    AppError.Code.UNKNOWN -> listOfNotNull(strings["common.error.unknown"], message).joinToString("\n")
    AppError.Code.WALLET_CERTIFICATE_MALFORMED -> strings["wallet.proof.error.1.message"] ?: message
    AppError.Code.WALLET_CERTIFICATE_INVALID_SIGNATURE -> strings["wallet.proof.error.2.message"] ?: message
    AppError.Code.WALLET_CERTIFICATE_UNKNOWN_ERROR -> message
    AppError.Code.KEY_FIGURES_NOT_AVAILABLE -> strings["keyFiguresController.fetchError.message"] ?: message
}

private fun LocalError.getString(strings: Map<String, String>): String = when (this.code) {
    LocalError.Code.SECRET_KEY_ALREADY_GENERATED -> strings["common.error.secretKey"] ?: message
}

private fun RemoteError.getString(strings: Map<String, String>): String = when (this.code) {
    RemoteError.Code.NO_INTERNET -> strings["common.error.internet"] ?: message
    RemoteError.Code.UNAUTHORIZED,
    RemoteError.Code.FORBIDDEN,
    -> listOfNotNull(strings["common.error.unauthorized"], message).joinToString("\n")
    RemoteError.Code.UNKNOWN -> listOfNotNull(strings["common.error.unknown"], message).joinToString("\n")
    RemoteError.Code.BACKEND -> strings["common.error.server"] ?: message
}
