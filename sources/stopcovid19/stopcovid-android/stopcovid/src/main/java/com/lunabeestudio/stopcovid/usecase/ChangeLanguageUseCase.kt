/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2022/3/9 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.usecase

import android.content.Context
import android.content.SharedPreferences
import com.lunabeestudio.common.extension.userLanguage
import com.lunabeestudio.stopcovid.extension.lastInfoCenterRefresh
import com.lunabeestudio.stopcovid.worker.InfoCenterWorker
import java.util.Date
import java.util.Locale

class ChangeLanguageUseCase(
    private val context: Context,
    private val sharedPrefs: SharedPreferences,
) {
    operator fun invoke(locale: Locale) {
        sharedPrefs.userLanguage = locale.language
        forceRefreshInfoCenter()
    }

    private fun forceRefreshInfoCenter() {
        sharedPrefs.lastInfoCenterRefresh = Date(0)
        InfoCenterWorker.runNow(context)
    }
}
