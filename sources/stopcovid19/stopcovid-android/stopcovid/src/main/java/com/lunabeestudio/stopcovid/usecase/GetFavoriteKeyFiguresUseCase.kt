/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2022/3/9 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.usecase

import com.lunabeestudio.domain.model.KeyFigure
import com.lunabeestudio.domain.repository.KeyFigureRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

class GetFavoriteKeyFiguresUseCase(
    private val keyFigureRepository: KeyFigureRepository,
) {
    operator fun invoke(): Flow<List<KeyFigure>> {
        return keyFigureRepository.keyFiguresWithDepartmentFlow.map { result ->
            result.data?.let { keyFigures ->
                keyFigures
                    .filter { keyFigure ->
                        keyFigure.isFavorite
                    }
                    .sortedBy { keyFigure ->
                        keyFigure.favoriteOrder
                    }
            } ?: emptyList()
        }
    }
}
