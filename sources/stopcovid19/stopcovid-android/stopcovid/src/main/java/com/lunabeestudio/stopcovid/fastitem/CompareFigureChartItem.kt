/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2022/3/18 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.fastitem

import android.content.res.ColorStateList
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.core.widget.TextViewCompat
import com.github.mikephil.charting.data.CombinedData
import com.github.mikephil.charting.renderer.CombinedChartRenderer
import com.lunabeestudio.domain.model.KeyFigure
import com.lunabeestudio.stopcovid.Constants
import com.lunabeestudio.stopcovid.R
import com.lunabeestudio.stopcovid.core.manager.LocalizedStrings
import com.lunabeestudio.stopcovid.databinding.ItemKeyFigureChartCardBinding
import com.lunabeestudio.stopcovid.extension.generateCombinedData
import com.lunabeestudio.stopcovid.extension.setupStyle
import com.mikepenz.fastadapter.binding.AbstractBindingItem
import kotlin.time.Duration

class CompareFigureChartItem : AbstractBindingItem<ItemKeyFigureChartCardBinding>() {
    override val type: Int = R.id.item_compare_key_figure_chart_card

    var chartExplanationLabel: String? = null
    var shareContentDescription: String? = null
    var onShareCard: ((binding: ItemKeyFigureChartCardBinding) -> Unit)? = null
    var onClickListener: View.OnClickListener? = null

    var keyFigurePair: Pair<KeyFigure, KeyFigure>? = null
    var isChartAnimated: Boolean = true
    var localizedStrings: LocalizedStrings? = null
    var durationToShow: Duration? = null

    override fun createBinding(inflater: LayoutInflater, parent: ViewGroup?): ItemKeyFigureChartCardBinding {
        return ItemKeyFigureChartCardBinding.inflate(inflater, parent, false).apply {
            chartContainer.removeView(keyFigureBarChart)
            chartContainer.removeView(keyFigureLineChart)
            keyFigureCombinedChart.isVisible = true
        }
    }

    override fun bindView(binding: ItemKeyFigureChartCardBinding, payloads: List<Any>) {
        super.bindView(binding, payloads)

        val strings = localizedStrings
        val durationToShow = durationToShow

        val combinedData = if (strings != null && durationToShow != null) {
            keyFigurePair?.generateCombinedData(binding.root.context, strings, durationToShow)
        } else {
            null
        }

        val areMagnitudeTheSame = keyFigurePair?.first?.magnitude == keyFigurePair?.second?.magnitude
        val chartData = ChartCompareFiguresData(combinedData, areMagnitudeTheSame)

        binding.apply {
            chartDescriptionTextView.text = chartExplanationLabel
            shareButton.contentDescription = shareContentDescription
            shareButton.setOnClickListener { onShareCard?.invoke(binding) }

            // Reset left axis to allow axis re-calculation when moving from data with different magnitude
            keyFigureCombinedChart.axisLeft.resetAxisMaximum()
            // Hack remove renderers before setting the new data to avoid crash when moving from bar chart to line chart only
            (keyFigureCombinedChart.renderer as? CombinedChartRenderer)?.subRenderers?.clear()

            keyFigureCombinedChart.data = chartData.combinedData

            keyFigureCombinedChart.setupStyle(isAxisRightEnabled = !chartData.areMagnitudeTheSame)
            if (isChartAnimated) {
                keyFigureCombinedChart.animateX(Constants.Chart.X_ANIMATION_DURATION_MILLIS)
            }

            // Set legend
            chartSerie1LegendTextView.text = chartData.combinedData?.dataSets?.get(0)?.label
            chartSerie2LegendTextView.text = chartData.combinedData?.dataSets?.get(1)?.label
            chartData.combinedData?.dataSets?.get(0)?.color?.let {
                chartSerie1LegendTextView.setTextColor(it)
                TextViewCompat.setCompoundDrawableTintList(chartSerie1LegendTextView, ColorStateList.valueOf(it))
            }
            chartData.combinedData?.dataSets?.get(1)?.color?.let {
                chartSerie2LegendTextView.setTextColor(it)
                TextViewCompat.setCompoundDrawableTintList(chartSerie2LegendTextView, ColorStateList.valueOf(it))
            }

            keyFigureCombinedChart.invalidate()

            root.setOnClickListener(onClickListener)
        }
    }

    override fun unbindView(binding: ItemKeyFigureChartCardBinding) {
        super.unbindView(binding)
        binding.root.setOnClickListener(null)
        unbindYAxis(binding)
        unbindXAxis(binding)
    }

    private fun unbindYAxis(binding: ItemKeyFigureChartCardBinding) {
        binding.keyFigureCombinedChart.axisLeft.resetAxisMinimum()
    }

    private fun unbindXAxis(binding: ItemKeyFigureChartCardBinding) {
        binding.keyFigureCombinedChart.xAxis.removeAllLimitLines()
        binding.keyFigureCombinedChart.xAxis.resetAxisMinimum()
        binding.keyFigureCombinedChart.xAxis.resetAxisMaximum()
    }

    data class ChartCompareFiguresData(
        val combinedData: CombinedData?,
        val areMagnitudeTheSame: Boolean,
    )
}

fun compareFigureCardChartItem(block: (CompareFigureChartItem.() -> Unit)): CompareFigureChartItem = CompareFigureChartItem().apply(
    block,
)
