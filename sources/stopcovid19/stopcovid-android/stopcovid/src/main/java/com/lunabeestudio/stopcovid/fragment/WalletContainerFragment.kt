/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2020/29/10 - for the STOP-COVID project
 */

package com.lunabeestudio.stopcovid.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.URLUtil
import androidx.appcompat.app.AlertDialog
import androidx.core.os.bundleOf
import androidx.core.view.doOnLayout
import androidx.fragment.app.FragmentContainerView
import androidx.fragment.app.setFragmentResultListener
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.navArgs
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.lunabeestudio.domain.model.TacResult
import com.lunabeestudio.domain.model.WalletCertificateError
import com.lunabeestudio.domain.model.WalletCertificateType
import com.lunabeestudio.error.AppError
import com.lunabeestudio.stopcovid.core.extension.appCompatActivity
import com.lunabeestudio.stopcovid.core.extension.findNavControllerOrNull
import com.lunabeestudio.stopcovid.core.fragment.BaseFragment
import com.lunabeestudio.stopcovid.databinding.FragmentWalletContainerBinding
import com.lunabeestudio.stopcovid.extension.findNavControllerOrNull
import com.lunabeestudio.stopcovid.extension.getParcelableCompat
import com.lunabeestudio.stopcovid.extension.injectionContainer
import com.lunabeestudio.stopcovid.extension.safeNavigate
import com.lunabeestudio.stopcovid.extension.showDbFailure
import com.lunabeestudio.stopcovid.extension.splitUrlFragment
import com.lunabeestudio.stopcovid.extension.toRaw
import com.lunabeestudio.stopcovid.model.EuropeanCertificate
import com.lunabeestudio.stopcovid.model.RawCodeData
import com.lunabeestudio.stopcovid.model.WalletCertificate
import com.lunabeestudio.stopcovid.viewmodel.WalletContainerViewModel
import com.lunabeestudio.stopcovid.viewmodel.WalletContainerViewModelFactory
import kotlinx.coroutines.launch

class WalletContainerFragment : BaseFragment(), DeeplinkFragment {

    private val args: WalletContainerFragmentArgs by navArgs()

    var binding: FragmentWalletContainerBinding? = null

    private var dbFailureDialog: AlertDialog? = null

    private val viewModel by viewModels<WalletContainerViewModel> {
        WalletContainerViewModelFactory(
            injectionContainer.walletRepository,
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (savedInstanceState == null) { // do not replay navigation on config change
            lifecycleScope.launch {
                injectionContainer.debugManager.logOpenWalletContainer(viewModel.certificates.value.toRaw())

                val rawCodeData = RawCodeData(
                    args.code,
                    args.certificateFormat?.let { WalletCertificateType.Format.fromValue(it) },
                    args.deeplinkOrigin,
                )
                val rawCodeHandled = handleRawCode(rawCodeData)
                if (!rawCodeHandled) {
                    showDbFailureIfNeeded()
                }
            }
        }

        setScanResultListener()
        setQuantityWarningResultListener()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return FragmentWalletContainerBinding.inflate(inflater, container, false).also {
            binding = it

            if (savedInstanceState == null && args.scrollCertificateId != null) {
                it.walletNavHostFragment.doOnLayout { navHostFragment ->
                    (navHostFragment as? FragmentContainerView)?.findNavControllerOrNull()?.currentBackStackEntry?.savedStateHandle?.set(
                        WalletPagerFragment.SAVED_STATE_HANDLE_SCROLL_TO_CERTIFICATE_KEY,
                        args.scrollCertificateId,
                    )
                }
            }
        }.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }

    private suspend fun handleRawCode(
        rawCodeData: RawCodeData,
        skipMaxWarning: Boolean = false,
    ): Boolean {
        val code = rawCodeData.code?.splitUrlFragment()
        val certificateValue = code?.getOrNull(0)

        val shouldAddCertificate =
            injectionContainer.configurationManager.configuration.displaySanitaryCertificatesWallet && certificateValue != null
        val shouldShowMaxCertificatesWarning =
            shouldAddCertificate &&
                !skipMaxWarning &&
                viewModel.getCertificatesCount() >= injectionContainer.configurationManager.configuration.maxCertBeforeWarning

        when {
            shouldShowMaxCertificatesWarning -> {
                val passThroughData = bundleOf(BUNDLE_PASSTHROUGH_RAW_CODE_DATA_KEY to rawCodeData)
                findNavControllerOrNull()?.safeNavigate(
                    WalletContainerFragmentDirections.actionWalletContainerFragmentToWalletQuantityWarningFragment(
                        passThroughData,
                    ),
                )
            }
            shouldAddCertificate -> {
                findNavControllerOrNull()?.safeNavigate(
                    WalletContainerFragmentDirections.actionWalletContainerFragmentToConfirmAddWalletCertificateFragment(
                        certificateValue!!,
                        rawCodeData.format?.name,
                    ),
                )
            }
            !injectionContainer.configurationManager.configuration.displaySanitaryCertificatesWallet ->
                findNavControllerOrNull()?.navigateUp()
            else -> return false
        }

        return true
    }

    private fun setScanResultListener() {
        setFragmentResultListener(WalletQRCodeFragment.SCANNED_CODE_RESULT_KEY) { _, bundle ->
            val scannedData = bundle.getString(WalletQRCodeFragment.SCANNED_CODE_BUNDLE_KEY)
            scannedData?.let { data ->
                if (URLUtil.isValidUrl(data)) {
                    try {
                        val certificateData = injectionContainer.walletRepository.extractCertificateDataFromUrl(data)
                        lifecycleScope.launch {
                            handleRawCode(
                                RawCodeData(
                                    certificateData.first,
                                    certificateData.second,
                                    null,
                                ),
                            )
                        }
                    } catch (e: AppError) {
                        if (e.code == AppError.Code.WALLET_CERTIFICATE_MALFORMED) {
                            findNavControllerOrNull()?.safeNavigate(
                                WalletContainerFragmentDirections.actionWalletContainerFragmentToWalletCertificateErrorFragment(
                                    WalletCertificateType.VACCINATION_EUROPE,
                                    WalletCertificateError.MALFORMED_CERTIFICATE,
                                ),
                            )
                        } else {
                            throw e
                        }
                    }
                } else {
                    lifecycleScope.launch {
                        handleRawCode(RawCodeData(data, null, null))
                    }
                }
            }
        }
    }

    private fun setQuantityWarningResultListener() {
        setFragmentResultListener(WalletQuantityWarningFragment.WALLET_QUANTITY_WARNING_RESULT_KEY) { _, bundle ->
            val confirmAdd = bundle.getBoolean(
                WalletQuantityWarningFragment.WALLET_QUANTITY_WARNING_BUNDLE_CONFIRM_KEY,
                false,
            )
            if (confirmAdd) {
                val passthroughBundle = bundle.getBundle(
                    WalletQuantityWarningFragment.WALLET_QUANTITY_WARNING_BUNDLE_PASSTHROUGH_KEY,
                )
                val rawCodeData = passthroughBundle?.getParcelableCompat<RawCodeData>(
                    BUNDLE_PASSTHROUGH_RAW_CODE_DATA_KEY,
                )
                rawCodeData?.let { safeRawCodeData ->
                    lifecycleScope.launch {
                        handleRawCode(safeRawCodeData, true)
                    }
                }
            }
        }
    }

    override fun refreshScreen() {
        appCompatActivity?.supportActionBar?.title = strings["walletController.title"]
    }

    fun navigateToFullscreenCertificate(certificate: WalletCertificate) {
        if (certificate is EuropeanCertificate) {
            findNavControllerOrNull()?.safeNavigate(
                WalletContainerFragmentDirections.actionWalletContainerFragmentToLegacyFullscreenDccFragment(
                    id = certificate.id,
                ),
            )
        } else {
            findNavControllerOrNull()?.safeNavigate(
                WalletContainerFragmentDirections.actionWalletContainerFragmentToFullscreen2DdocFragment(certificate.id),
            )
        }
    }

    override fun onNewIntent(bundleArgs: Bundle?) {
        bundleArgs?.runCatching { WalletContainerFragmentArgs.fromBundle(this) }?.getOrNull()?.let { fragmentArgs ->
            lifecycleScope.launch {
                val rawCodeData = RawCodeData(
                    fragmentArgs.code,
                    fragmentArgs.certificateFormat?.let { WalletCertificateType.Format.fromValue(it) },
                    fragmentArgs.deeplinkOrigin,
                )
                handleRawCode(rawCodeData)
            }
        }
    }

    private fun showDbFailureIfNeeded() {
        if (dbFailureDialog == null) {
            lifecycleScope.launch {
                val result = viewModel.certificates.value

                if (result is TacResult.Failure) {
                    context?.let { ctx ->
                        dbFailureDialog = MaterialAlertDialogBuilder(ctx)
                            .setOnDismissListener {
                                if (it == dbFailureDialog) { // In case of concurrent dismiss, do not nullify another dialog instance
                                    dbFailureDialog = null
                                }
                            }
                            .showDbFailure(
                                strings,
                                onRetry = {
                                    dbFailureDialog?.dismiss()
                                    dbFailureDialog = null
                                    lifecycleScope.launch {
                                        viewModel.forceRefreshCertificates()
                                        showDbFailureIfNeeded()
                                    }
                                },
                                onClear = "android.db.error.clearWallet" to {
                                    dbFailureDialog?.dismiss()
                                    dbFailureDialog = null
                                    viewModel.deleteLostCertificates()
                                    injectionContainer.debugManager.logReinitializeWallet()
                                },
                            )
                    }
                }
            }
        }
    }

    fun setupBottomAction(text: String?, onClick: (() -> Unit)?) {
        binding?.let { binding ->
            if (onClick != null) {
                binding.walletBottomSheetButton.text = text
                binding.walletBottomSheetButton.setOnClickListener {
                    onClick()
                }
                binding.root.transitionToStart()
            } else {
                binding.root.transitionToEnd()
            }
        }
    }

    companion object {
        private const val BUNDLE_PASSTHROUGH_RAW_CODE_DATA_KEY: String = "BUNDLE_PASSTHROUGH_RAW_CODE_DATA_KEY"
    }
}
