/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2020/04/05 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid

object Constants {

    object SharedPrefs {
        const val ON_BOARDING_DONE: String = "On.Boarding.Done"
        const val LAST_PRIVACY_REFRESH: String = "Last.Privacy.Refresh"
        const val LAST_LINKS_REFRESH: String = "Last.Links.Refresh"
        const val LAST_MORE_KEY_FIGURES_REFRESH: String = "Last.MoreKeyFigures.Refresh"
        const val LAST_MAINTENANCE_REFRESH: String = "Last.Maintenance.Refresh"
        const val LAST_INFO_CENTER_REFRESH: String = "Last.Info.Center.Refresh"
        const val HAS_NEWS: String = "Has.News"
        const val ARE_INFO_NOTIFICATIONS_ENABLED: String = "Are.Info.Notifications.Enabled"
        const val LAST_VERSION_CODE: String = "Last.Version.Code"
        const val LAST_VERSION_NAME: String = "Last.Version.Name"
        const val LAST_VERSION_MIGRATION: String = "Last.Version.Migration"
        const val SHOW_SMART_WALLET: String = "showSmartWallet"
        const val RATINGS_KEY_FIGURES_OPENING: String = "ratingsKeyFiguresOpening"
        const val RATINGS_SHOWN: String = "ratingsShown"
        const val GOOGLE_REVIEW_SHOWN: String = "Google.Review.Shown"
        const val NOTIFICATION_VERSION_CLOSED: String = "Notification.Version.Closed"
        const val LOW_STORAGE_ALERT_SHOWN: String = "Low.Storage.Alert.Shown"
        const val ENABLE_AUTO_FULLSCREEN_BRIGHTNESS: String = "Enable.Auto.Fullscreen.Brightness"
        const val SMART_WALLET_SENT_NOTIFICATIONS: String = "Smart.Wallet.Sent.Notifications"
        const val BLACKLIST_REFRESHED_AT: String = "Blacklist.Refreshed.At"

        // Compare Key Figure
        const val COMPARE_KEY_FIGURE_LABEL_1: String = "Compare.Key.Figures.1"
        const val COMPARE_KEY_FIGURE_LABEL_2: String = "Compare.Key.Figures.2"
        const val MAP_KEY_FIGURE_LABEL: String = "Map.Key.Figures"
        const val MAP_KEY_FIGURE_STATE: String = "Map.Key.Figures.State"
        const val MAP_KEY_FIGURE_VALUE: String = "Map.Key.Figures.Value"
    }

    object Notification {
        const val APP_IN_MAINTENANCE: String = "App.In.Maintenance"
    }

    object WorkerNames {
        const val UPGRADE_MAINTENANCE: String = "StopCovid.UpgradeMaintenance.Worker"
        const val DCC_VALID_REMINDER: String = "StopCovid.VaccinationCompleted.Reminder.Worker"
        const val SMART_WALLET_NOTIFICATION: String = "StopCovid.SmartWalletNotification.Worker"
        const val SMART_WALLET_ELIGIBLE_NOTIFICATION: String = "StopCovid.SmartWalleEligibletNotification.Worker"
        const val BLACKLIST: String = "StopCovid.Blacklist.Worker"
        const val INFO_CENTER: String = "StopCovid.InfoCenter.Worker"
        const val CONFIG: String = "StopCovid.Config.Worker"
    }

    object WorkerTags {
        const val BLACKLIST: String = "BLACKLIST_WORKERS"
        const val SMART_WALLET: String = "SMART_WALLET_WORKERS"
    }

    object WorkerPeriodicTime {
        const val SMART_WALLET_NOTIFICATION_HOURS: Long = 12L
        const val SMART_WALLET_ELIGIBLE_NOTIFICATION_DAY: Long = 1L
    }

    object Url {
        const val FIGURES_FRAGMENT_URI: String = "tousanticovid://allFigures/"
        const val UNIVERSAL_QRCODE_SHORTCUT_URI: String = "tousanticovid://universalQRCode/"
        const val WALLET_CERTIFICATE_SHORTCUT_URI: String = "tousanticovid://walletCertificate/list"
        const val DCC_FULLSCREEN_SHORTCUT_URI: String = "tousanticovid://dccFullScreen/"
    }

    object Android {
        const val FORCE_LOADING_DELAY: Long = 2000L
        const val STORAGE_THRESHOLD_MB: Int = 300
    }

    object Chart {
        const val LIMIT_LINE_TEXT_SIZE: Float = 15f
        const val SHARE_CHART_FILENAME: String = "chart.jpg"
        const val MIN_CIRCLE_RADIUS_SIZE: Float = 1.75f
        const val RESIZE_START_CIRCLE_COUNT: Float = 25f
        const val DEFAULT_CIRCLE_SIZE: Float = 4f
        const val CIRCLE_LINE_RATIO: Float = 2f
        const val Y_AXIS_LABEL_COUNT: Int = 3
        const val X_AXIS_LABEL_COUNT: Int = 2
        const val X_ANIMATION_DURATION_MILLIS: Int = 1000
        const val AXIS_LABEL_TEXT_SIZE: Float = 15f
        const val EXTRA_BOTTOM_OFFSET: Float = 16f
        const val WIDGET_CIRCLE_SIZE: Float = 2f
        const val WIDGET_LINE_WIDTH: Float = 1f
        const val WIDGET_MARGIN_SIZE: Float = 6f
        const val ZOOM_MIN_THRESHOLD: Float = 1.25f
        const val SIGNIFICANT_DIGIT_MAX: Int = 3
    }

    object QrCode {
        const val FORMAT_2D_DOC: String = "2D-DOC"
    }

    object Certificate {
        const val MANUFACTURER_AUTOTEST: String = "AUTOTEST"
        const val TEST_TYPE_ANTIGEN: String = "LP217198-3"
        const val DCC_EXEMPTION_PREFIX: String = "EX1:"
        const val BLACKLIST_HASH_LENGTH: Int = 32
    }

    object HomeScreenWidget {
        const val NUMBER_VALUES_GRAPH_FIGURE: Int = 8
        const val WORKER_UPDATE_FIGURES_NAME: String = "updateFiguresWorker"
        const val WORKER_UPDATE_FIGURES_PERIODIC_REFRESH_HOURS: Long = 5
    }

    object Build {
        const val NB_DIGIT_MAJOR_RELEASE: Int = 2
    }

    object Map {
        const val ANIMATION_DURATION_AUTO_PLAY: Long = 100L
        const val MAX_LINE_FIGURE_DEF: Int = 3
        const val MAX_LINE_FIGURE_SELECT: Int = 3

        private const val MIN_PERCENTAGE_ALPHA: Float = 0.10f
        const val MIN_ALPHA_COLOR: Int = (255 * MIN_PERCENTAGE_ALPHA).toInt()
        const val RANGE_ALPHA_COLOR: Int = 255 - MIN_ALPHA_COLOR
        const val PADDING_MARKER_MAP_VIEW: Int = 20
    }

    object FeaturedInfo {
        const val RATIO_HOME_ITEM: Float = 0.66f
    }
}
