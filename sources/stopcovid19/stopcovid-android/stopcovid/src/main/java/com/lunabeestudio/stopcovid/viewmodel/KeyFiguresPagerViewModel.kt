/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2022/3/18 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import com.lunabeestudio.domain.model.KeyFigure
import com.lunabeestudio.stopcovid.usecase.GetFavoriteKeyFiguresUseCase

class KeyFiguresPagerViewModel(
    private val getFavoriteKeyFiguresUseCase: GetFavoriteKeyFiguresUseCase,
) : ViewModel() {

    val favFigures: LiveData<List<KeyFigure>?> by lazy {
        getFavoriteKeyFiguresUseCase().asLiveData(viewModelScope.coroutineContext)
    }
}

class KeyFiguresPagerViewModelFactory(
    private val getFavoriteKeyFiguresUseCase: GetFavoriteKeyFiguresUseCase,
) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        @Suppress("UNCHECKED_CAST")
        return KeyFiguresPagerViewModel(
            getFavoriteKeyFiguresUseCase,
        ) as T
    }
}
