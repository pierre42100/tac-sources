/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2020/10/29 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.extension

import android.content.Context
import android.content.SharedPreferences
import android.graphics.Color
import com.github.mikephil.charting.data.BarData
import com.github.mikephil.charting.data.BarDataSet
import com.github.mikephil.charting.data.BarEntry
import com.github.mikephil.charting.data.CombinedData
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.lunabeestudio.common.extension.hasChosenPostalCode
import com.lunabeestudio.domain.model.DepartmentKeyFigure
import com.lunabeestudio.domain.model.KeyFigure
import com.lunabeestudio.domain.model.KeyFigureChartType
import com.lunabeestudio.stopcovid.core.extension.isNightMode
import com.lunabeestudio.stopcovid.core.extension.stringsFormat
import com.lunabeestudio.stopcovid.core.manager.LocalizedStrings
import com.lunabeestudio.stopcovid.fastitem.KeyFigureCardItem
import com.lunabeestudio.stopcovid.fastitem.keyFigureCardItem
import com.lunabeestudio.stopcovid.fragment.ChartDataType
import com.lunabeestudio.stopcovid.manager.ConfigurationManager
import com.lunabeestudio.stopcovid.model.ChartInformation
import java.text.DateFormat
import java.text.NumberFormat
import java.util.Date
import kotlin.time.Duration
import kotlin.time.Duration.Companion.days
import kotlin.time.Duration.Companion.seconds

val KeyFigure.labelStringKey: String
    get() = "$labelKey.label"

val KeyFigure.labelShortStringKey: String
    get() = "$labelKey.shortLabel"

val KeyFigure.descriptionStringKey: String
    get() = "$labelKey.description"

val KeyFigure.learnMoreStringKey: String
    get() = "$labelKey.learnMore"

val KeyFigure.limitLineStringKey: String
    get() = "$labelKey.limitLine"

fun KeyFigure.colorStringKey(dark: Boolean?): String = if (dark == true) {
    "$labelKey.colorCode.dark"
} else {
    "$labelKey.colorCode.light"
}

val KeyFigure.shortUnitStringKey: String
    get() = "$labelKey.shortUnit"

val KeyFigure.longUnitStringKey: String
    get() = "$labelKey.longUnit"

fun KeyFigure.hasAverageChart(): Boolean = !avgSeries.isNullOrEmpty()

fun KeyFigure.getKeyFigureForPostalCode(postalCode: String?, configurationManager: ConfigurationManager): DepartmentKeyFigure? {
    val key = postalCode
        ?.takeIf { configurationManager.configuration.displayDepartmentLevel }
        ?.let { KeyFigure.getDepartmentKeyFromPostalCode(it) }
    return valuesDepartments?.firstOrNull { it.dptNb == key }
}

fun KeyFigure.itemForFigure(
    context: Context,
    sharedPrefs: SharedPreferences,
    departmentKeyFigure: DepartmentKeyFigure?,
    numberFormat: NumberFormat,
    dateFormat: DateFormat,
    strings: LocalizedStrings,
    block: KeyFigureCardItem.() -> Unit,
): KeyFigureCardItem? {
    return keyFigureCardItem {
        val extractDateS: Long
        if (departmentKeyFigure != null) {
            rightLocation = strings["common.country.france"]
            leftLocation = departmentKeyFigure.dptLabel
            leftValue = departmentKeyFigure.valueToDisplay?.formatNumberIfNeeded(numberFormat)
            rightValue = valueGlobalToDisplay.formatNumberIfNeeded(numberFormat)
            extractDateS = departmentKeyFigure.extractDateS
        } else {
            if (sharedPrefs.hasChosenPostalCode) {
                leftLocation = strings["common.country.france"]
            }
            leftValue = valueGlobalToDisplay.formatNumberIfNeeded(numberFormat)
            extractDateS = this@itemForFigure.extractDateS
        }

        val extractDateMs = extractDateS.seconds.inWholeMilliseconds
        val dayInMs = 1.days.inWholeMilliseconds

        val nowDaysSinceEpoch = System.currentTimeMillis() / dayInMs
        val extractDaysSinceEpoch = extractDateMs / dayInMs
        updatedAt = when (nowDaysSinceEpoch) {
            extractDaysSinceEpoch -> strings.stringsFormat(
                "keyFigures.update.today",
                strings["common.today"]?.lowercase(),
            )
            extractDaysSinceEpoch + 1 -> strings.stringsFormat(
                "keyFigures.update.today",
                strings["common.yesterday"]?.lowercase(),
            )
            else -> strings.stringsFormat("keyFigures.update", dateFormat.format(Date(extractDateMs)))
        }

        label = strings[labelStringKey]
        description = strings[descriptionStringKey]
        identifier = labelKey.hashCode().toLong()

        strings[colorStringKey(context.isNightMode())]?.let {
            color = Color.parseColor(it)
        }

        favoriteState = if (isFavorite) {
            KeyFigureCardItem.FavoriteState.CHECKED
        } else {
            KeyFigureCardItem.FavoriteState.NOT_CHECKED
        }
    }
        .apply(block)
        .takeIf {
            it.label != null
        }
}

private fun KeyFigure.generateBarData(
    figureNumber: Int,
    context: Context,
    strings: LocalizedStrings,
    durationToShow: Duration,
): BarDataSet {
    val chartInfo = ChartInformation(
        context = context,
        strings = strings,
        keyFigure = this,
        chartDataType = ChartDataType.GLOBAL,
        departmentKeyFigure = null,
        durationToShow = durationToShow,
    )
    val dataSet = BarDataSet(
        chartInfo.chartData[0].entries.map {
            BarEntry(it.x, it.y)
        },
        this.getLegend(strings),
    )
    getColorFigureFromConfig(context, figureNumber)?.let { dataSet.setupStyle(it) }
    return dataSet
}

private fun KeyFigure.generateLineData(
    figureNumber: Int,
    context: Context,
    strings: LocalizedStrings,
    durationToShow: Duration,
): LineDataSet {
    val chartInfo = ChartInformation(
        context = context,
        strings = strings,
        keyFigure = this,
        chartDataType = ChartDataType.GLOBAL,
        departmentKeyFigure = null,
        durationToShow = durationToShow,
    )
    val dataSet = LineDataSet(chartInfo.chartData[0].entries, this.getLegend(strings))
    getColorFigureFromConfig(context, figureNumber)?.let { dataSet.setupStyle(it) }
    return dataSet
}

fun Pair<KeyFigure, KeyFigure>.generateCombinedData(context: Context, strings: LocalizedStrings, durationToShow: Duration): CombinedData {
    val keyFigure1 = first.clearSeries(second)
    val keyFigure2 = second.clearSeries(first)

    val lineData = LineData()
    val barData = BarData()
    when (keyFigure1.chartType) {
        KeyFigureChartType.LINES -> lineData.addDataSet(
            keyFigure1.generateLineData(0, context, strings, durationToShow),
        )
        KeyFigureChartType.BARS -> barData.addDataSet(keyFigure1.generateBarData(0, context, strings, durationToShow))
    }
    when (keyFigure2.chartType) {
        KeyFigureChartType.LINES -> lineData.addDataSet(
            keyFigure2.generateLineData(1, context, strings, durationToShow),
        )
        KeyFigureChartType.BARS -> barData.addDataSet(keyFigure2.generateBarData(1, context, strings, durationToShow))
    }

    val combinedData = CombinedData()

    if (barData.dataSetCount > 0) {
        val xValueDiff = barData.xMax - barData.xMin
        barData.barWidth = xValueDiff / barData.entryCount
        if (barData.dataSetCount > 1) {
            barData.groupBars(barData.xMin, 0f, 0f)
        }

        combinedData.setData(barData)
    }

    if (lineData.dataSetCount > 0) {
        combinedData.setData(lineData)
    }

    return combinedData
}

private fun KeyFigure.clearSeries(keyFigure2: KeyFigure): KeyFigure {
    val newSeries = keyFigure2.series?.let { serie2 -> this.series?.filter { entry -> serie2.any { it.date == entry.date } } }
    return KeyFigure(
        index = this.index,
        category = this.category,
        labelKey = this.labelKey,
        valueGlobalToDisplay = this.valueGlobalToDisplay,
        valueGlobal = this.valueGlobal,
        isFeatured = false,
        isHighlighted = false,
        extractDateS = this.extractDateS,
        valuesDepartments = null,
        displayOnSameChart = false,
        limitLine = null,
        chartType = this.chartType,
        series = newSeries,
        avgSeries = null,
        magnitude = this.magnitude,
        isFavorite = this.isFavorite,
        favoriteOrder = this.favoriteOrder,
        figureType = this.figureType,
    )
}

private fun KeyFigure.getLegend(strings: LocalizedStrings): String {
    val unit = strings[this.longUnitStringKey]?.let { "($it)" } ?: ""
    return "${strings[this.labelStringKey]} $unit"
}

private fun getColorFigureFromConfig(context: Context, figureNumber: Int): Int? {
    val keyFigureColor = if (figureNumber == 1) {
        context.configurationDataSource().configuration.colorsCompareKeyFigures?.colorKeyFigure1
    } else {
        context.configurationDataSource().configuration.colorsCompareKeyFigures?.colorKeyFigure2
    }
    val color = if (context.isNightMode()) keyFigureColor?.darkColor else keyFigureColor?.lightColor
    return color?.safeParseColor()
}
