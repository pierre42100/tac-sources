/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2020/04/05 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.fragment

import android.content.SharedPreferences
import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.preference.PreferenceManager
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.lunabeestudio.common.CommonConstant
import com.lunabeestudio.common.extension.getApplicationLanguage
import com.lunabeestudio.stopcovid.R
import com.lunabeestudio.stopcovid.activity.MainActivity
import com.lunabeestudio.stopcovid.core.fastitem.captionItem
import com.lunabeestudio.stopcovid.core.fastitem.dividerItem
import com.lunabeestudio.stopcovid.core.fastitem.lightButtonItem
import com.lunabeestudio.stopcovid.core.fastitem.spaceItem
import com.lunabeestudio.stopcovid.core.fastitem.switchItem
import com.lunabeestudio.stopcovid.core.fastitem.titleItem
import com.lunabeestudio.stopcovid.extension.areInfoNotificationsEnabled
import com.lunabeestudio.stopcovid.extension.enableAutoFullscreenBrightness
import com.lunabeestudio.stopcovid.extension.flaggedCountry
import com.lunabeestudio.stopcovid.extension.injectionContainer
import com.lunabeestudio.stopcovid.extension.listLogFiles
import com.lunabeestudio.stopcovid.extension.showSmartWallet
import com.lunabeestudio.stopcovid.fastitem.selectionItem
import com.lunabeestudio.stopcovid.viewmodel.SettingsViewModel
import com.lunabeestudio.stopcovid.viewmodel.SettingsViewModelFactory
import com.mikepenz.fastadapter.GenericItem

class SettingsFragment : MainFragment() {

    private val sharedPreferences: SharedPreferences by lazy {
        PreferenceManager.getDefaultSharedPreferences(requireContext())
    }

    private val viewModel: SettingsViewModel by viewModels {
        SettingsViewModelFactory(
            walletRepository,
            debugManager,
            injectionContainer.changeLanguageUseCase,
            injectionContainer.logsDir,
        )
    }

    override fun getTitleKey(): String = "manageDataController.title"

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViewModelObserver()
    }

    private fun initViewModelObserver() {
        viewModel.loadingInProgress.observe(viewLifecycleOwner) { loadingInProgress ->
            (activity as? MainActivity)?.showProgress(loadingInProgress)
        }
        viewModel.eraseLocalSuccess.observe(viewLifecycleOwner) {
            showSnackBar(strings["manageDataController.eraseLocalHistory.success"] ?: "")
        }
        viewModel.eraseRemoteSuccess.observe(viewLifecycleOwner) {
            showSnackBar(strings["manageDataController.eraseRemoteContact.success"] ?: "")
        }
    }

    override suspend fun getItems(): List<GenericItem> {
        val items = ArrayList<GenericItem>()

        manageNotificationsItems(items)
        spaceDividerItems(items)
        userLanguageItems(items)
        spaceDividerItems(items)
        if (configurationManager.configuration.displaySanitaryCertificatesWallet && configurationManager.configuration.isSmartWalletOn) {
            smartWalletItems(items)
            spaceDividerItems(items)
        }
        manageCertificatesItems(items)
        spaceDividerItems(items)
        brightnessItems(items)
        spaceDividerItems(items)
        logsItems(items)

        return items
    }

    private fun spaceDividerItems(items: MutableList<GenericItem>) {
        items += dividerItem {
            identifier = items.count().toLong()
        }
        items += spaceItem {
            spaceRes = R.dimen.spacing_medium
            identifier = items.count().toLong()
        }
    }

    private fun manageNotificationsItems(items: MutableList<GenericItem>) {
        items += titleItem {
            text = strings["manageDataController.showInfoNotifications.title"]
            identifier = items.count().toLong()
        }
        items += spaceItem {
            spaceRes = R.dimen.spacing_medium
        }
        items += captionItem {
            text = strings["manageDataController.showInfoNotifications.subtitle"]
            identifier = items.count().toLong()
        }
        items += switchItem {
            title = strings["manageDataController.showInfoNotifications.button"]
            isChecked = sharedPreferences.areInfoNotificationsEnabled
            onCheckChange = { isChecked ->
                sharedPreferences.areInfoNotificationsEnabled = isChecked
            }
            identifier = items.count().toLong()
        }
    }

    private fun manageCertificatesItems(items: MutableList<GenericItem>) {
        items += titleItem {
            text = strings["manageDataController.walletData.title"]
            identifier = items.count().toLong()
        }
        items += spaceItem {
            spaceRes = R.dimen.spacing_medium
        }
        items += captionItem {
            text = strings["manageDataController.walletData.subtitle"]
            identifier = items.count().toLong()
        }
        items += lightButtonItem {
            text = strings["manageDataController.walletData.button"]
            onClickListener = View.OnClickListener {
                MaterialAlertDialogBuilder(requireContext())
                    .setTitle(strings["manageDataController.walletData.confirmationDialog.title"])
                    .setMessage(strings["manageDataController.walletData.confirmationDialog.message"])
                    .setNegativeButton(strings["common.cancel"], null)
                    .setPositiveButton(strings["common.confirm"]) { _, _ ->
                        viewModel.eraseCertificates()
                    }
                    .show()
            }
            identifier = items.count().toLong()
        }
    }

    private fun userLanguageItems(items: MutableList<GenericItem>) {
        items += titleItem {
            text = strings["manageDataController.userLanguage.title"]
            identifier = items.count().toLong()
        }
        items += spaceItem {
            spaceRes = R.dimen.spacing_medium
        }
        items += captionItem {
            text = strings["manageDataController.userLanguage.subtitle"]
            identifier = items.count().toLong()
        }
        CommonConstant.SUPPORTED_LOCALES.mapTo(items) { locale ->
            selectionItem {
                caption = locale.flaggedCountry
                showSelection = requireContext().getApplicationLanguage().equals(locale.language, false)
                onClick = {
                    viewModel.changeLanguage(locale)
                }
                identifier = locale.language.hashCode().toLong()
            }
        }
    }

    private fun smartWalletItems(items: MutableList<GenericItem>) {
        items += titleItem {
            text = strings["manageDataController.smartWalletActivation.title"]
            identifier = items.count().toLong()
        }
        items += spaceItem {
            spaceRes = R.dimen.spacing_medium
        }
        items += captionItem {
            text = strings["manageDataController.smartWalletActivation.subtitle"]
            identifier = items.count().toLong()
        }
        items += switchItem {
            title = if (sharedPreferences.showSmartWallet) {
                strings["manageDataController.smartWalletActivation.switch.on"]
            } else {
                strings["manageDataController.smartWalletActivation.switch.off"]
            }
            isChecked = sharedPreferences.showSmartWallet
            onCheckChange = { isChecked ->
                sharedPreferences.showSmartWallet = isChecked
                refreshScreen()
            }
            identifier = items.count().toLong()
        }
    }

    private fun brightnessItems(items: MutableList<GenericItem>) {
        items += titleItem {
            text = strings["common.settings.fullBrightnessSwitch.title"]
            identifier = items.count().toLong()
        }
        items += spaceItem {
            spaceRes = R.dimen.spacing_medium
        }
        items += captionItem {
            text = strings["common.settings.fullBrightnessSwitch.subtitle"]
            identifier = items.count().toLong()
        }
        items += switchItem {
            isChecked = sharedPreferences.enableAutoFullscreenBrightness
            title = if (isChecked) {
                strings["common.settings.fullBrightnessSwitch.switch.on"]
            } else {
                strings["common.settings.fullBrightnessSwitch.switch.off"]
            }
            onCheckChange = { isChecked ->
                sharedPreferences.enableAutoFullscreenBrightness = isChecked
                refreshScreen()
            }
            identifier = items.count().toLong()
        }
    }

    private fun logsItems(items: MutableList<GenericItem>) {
        items += titleItem {
            text = strings["manageDataController.logFiles.title"]
            identifier = "manageDataController.logFiles.title".hashCode().toLong()
        }
        items += spaceItem {
            spaceRes = R.dimen.spacing_medium
        }
        items += captionItem {
            text = strings["manageDataController.logFiles.subtitle"]
            identifier = "manageDataController.logFiles.subtitle".hashCode().toLong()
        }
        if (!injectionContainer.logsDir.listLogFiles().isNullOrEmpty()) {
            items += captionItem {
                text = stringsFormat(
                    "manageDataController.logFiles.logsFilesCount",
                    injectionContainer.logsDir.listLogFiles()?.count() ?: 0,
                )
                identifier = "manageDataController.logFiles.noLogs".hashCode().toLong()
            }
            items += lightButtonItem {
                text = strings["manageDataController.logFiles.share.button"]
                onClickListener = View.OnClickListener {
                    viewModel.exportLogs(requireContext(), strings)
                }
                identifier = "manageDataController.logs.button".hashCode().toLong()
            }
            items += lightButtonItem {
                text = strings["manageDataController.logFiles.delete.button"]
                onClickListener = View.OnClickListener {
                    MaterialAlertDialogBuilder(requireContext())
                        .setTitle(strings["manageDataController.logFiles.delete.confirmationDialog.title"])
                        .setMessage(strings["manageDataController.logFiles.delete.confirmationDialog.message"])
                        .setNegativeButton(strings["common.cancel"], null)
                        .setPositiveButton(strings["common.confirm"]) { _, _ ->
                            context?.let {
                                injectionContainer.logsDir.deleteRecursively()
                                injectionContainer.logsDir.mkdir()
                                refreshScreen()
                            }
                        }
                        .show()
                }
                identifier = "manageDataController.logFiles.delete.button".hashCode().toLong()
            }
        } else {
            items += captionItem {
                text = strings["manageDataController.logFiles.noLogs"]
                identifier = "manageDataController.logFiles.noLogs".hashCode().toLong()
            }
        }
    }
}
