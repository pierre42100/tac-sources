/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2021/05/12 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.model

import com.lunabeestudio.domain.model.smartwallet.SmartWalletValidity

sealed class SmartWalletState(val smartWalletValidity: SmartWalletValidity?, val eligibility: Eligibility?) {

    class Valid(smartWalletValidity: SmartWalletValidity?, eligibility: Eligibility?) : SmartWalletState(
        smartWalletValidity,
        eligibility,
    )
    class EligibleSoon(smartWalletValidity: SmartWalletValidity?, eligibility: Eligibility?) : SmartWalletState(
        smartWalletValidity,
        eligibility,
    )

    class Eligible(smartWalletValidity: SmartWalletValidity?, eligibility: Eligibility?) : SmartWalletState(
        smartWalletValidity,
        eligibility,
    )

    class ExpireSoon(smartWalletValidity: SmartWalletValidity?, eligibility: Eligibility?) : SmartWalletState(
        smartWalletValidity,
        eligibility,
    )

    class Expired(smartWalletValidity: SmartWalletValidity?, eligibility: Eligibility?) : SmartWalletState(
        smartWalletValidity,
        eligibility,
    )
}
