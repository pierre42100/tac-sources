/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2020/04/05 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.asLiveData
import com.lunabeestudio.stopcovid.model.FrenchCertificate
import com.lunabeestudio.stopcovid.repository.WalletRepository
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.map

class WalletFullscreen2DdocViewModel(
    private val walletRepository: WalletRepository,
) : ViewModel() {
    fun getCertificate(id: String): LiveData<FrenchCertificate> {
        return walletRepository.walletCertificateFlow.map { result ->
            result.data
                ?.filterIsInstance<FrenchCertificate>()
                ?.firstOrNull { certificate -> certificate.id == id }
        }.filterNotNull().asLiveData()
    }
}

class WalletFullscreen2DdocViewModelFactory(
    private val walletRepository: WalletRepository,
) :
    ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        @Suppress("UNCHECKED_CAST")
        return WalletFullscreen2DdocViewModel(
            walletRepository,
        ) as T
    }
}
