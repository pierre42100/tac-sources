/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2021/6/10 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.usecase

import com.lunabeestudio.domain.model.TacResult
import com.lunabeestudio.error.AppError
import com.lunabeestudio.stopcovid.model.EuropeanCertificate
import com.lunabeestudio.stopcovid.model.WalletCertificate
import com.lunabeestudio.stopcovid.repository.WalletRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.onStart

class GenerateMultipassUseCase(
    private val walletRepository: WalletRepository,
    private val verifyCertificateUseCase: VerifyCertificateUseCase,
) {
    operator fun invoke(certificateList: List<EuropeanCertificate>): Flow<TacResult<EuropeanCertificate>> {
        return flow {
            val dccResult = walletRepository.generateMultipass(certificateList.map(WalletCertificate::value))

            when (dccResult) {
                is TacResult.Failure -> {
                    emit(TacResult.Failure(dccResult.throwable))
                }
                is TacResult.Success -> {
                    val dccValue = dccResult.successData

                    val multipass = try {
                        EuropeanCertificate.getCertificate(dccValue)?.also { dcc ->
                            dcc.parse()
                            verifyCertificateUseCase(dcc)
                        }
                    } catch (e: Exception) {
                        emit(TacResult.Failure(e))
                        return@flow
                    }

                    if (multipass != null) {
                        walletRepository.saveCertificate(multipass)
                        emit(TacResult.Success(multipass))
                    } else {
                        emit(TacResult.Failure(AppError(AppError.Code.UNKNOWN, "Generated multipass is null")))
                    }
                }
                is TacResult.Loading -> {
                    // No-op
                }
            }
        }.onStart { emit(TacResult.Loading()) }
    }
}
