/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2021/4/10 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.lunabeestudio.stopcovid.core.LocalizedApplication
import com.lunabeestudio.stopcovid.core.manager.LocalizedStrings
import com.lunabeestudio.stopcovid.databinding.BottomSheetFragmentInfoSectionWarningBinding

class InfoSectionWarningBottomSheetFragment : BottomSheetDialogFragment() {

    private lateinit var binding: BottomSheetFragmentInfoSectionWarningBinding

    private val strings: LocalizedStrings
        get() = (activity?.application as? LocalizedApplication)?.localizedStrings ?: emptyMap()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = BottomSheetFragmentInfoSectionWarningBinding.inflate(inflater, container, false)
        binding.setTexts()
        binding.setClickListeners()

        return binding.root
    }

    private fun BottomSheetFragmentInfoSectionWarningBinding.setTexts() {
        explanationTextView.text = strings["home.infoSection.warning"]
        okButton.text = strings["common.ok"]
    }

    private fun BottomSheetFragmentInfoSectionWarningBinding.setClickListeners() {
        okButton.setOnClickListener {
            dismiss()
        }
    }
}
