/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2022/3/18 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.fragment

import android.os.Bundle
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.fragment.app.setFragmentResult
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import com.lunabeestudio.domain.model.KeyFigure
import com.lunabeestudio.stopcovid.R
import com.lunabeestudio.stopcovid.core.extension.findNavControllerOrNull
import com.lunabeestudio.stopcovid.core.fastitem.spaceItem
import com.lunabeestudio.stopcovid.extension.descriptionStringKey
import com.lunabeestudio.stopcovid.extension.injectionContainer
import com.lunabeestudio.stopcovid.extension.labelStringKey
import com.lunabeestudio.stopcovid.fastitem.SelectionItem
import com.lunabeestudio.stopcovid.fastitem.bigTitleItem
import com.lunabeestudio.stopcovid.fastitem.selectionItem
import com.lunabeestudio.stopcovid.viewmodel.SelectKeyFiguresFragmentViewModel
import com.lunabeestudio.stopcovid.viewmodel.SelectKeyFiguresFragmentViewModelFactory
import com.mikepenz.fastadapter.GenericItem

class SelectKeyFiguresFragment : MainFragment() {

    private val args: SelectKeyFiguresFragmentArgs by navArgs()

    private val viewModel: SelectKeyFiguresFragmentViewModel by viewModels {
        SelectKeyFiguresFragmentViewModelFactory(injectionContainer.keyFigureRepository)
    }

    private var labelKey1: String? = null
    private var labelKey2: String? = null

    override fun getTitleKey(): String = "keyfigures.comparison.keyfiguresList.screen.title${args.figureNumber}"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        labelKey1 = args.labelKeyFigure1
        labelKey2 = args.labelKeyFigure2
    }

    override suspend fun getItems(): List<GenericItem> {
        val items = arrayListOf<GenericItem>()
        getKeyFiguresList(items)
        return items
    }

    private suspend fun getKeyFiguresList(items: ArrayList<GenericItem>) {
        viewModel.getKeyFigures().data?.groupBy { it.category }?.forEach {
            categorySelectionItem(items, it.value, it.key)
        }
    }

    private fun categorySelectionItem(items: ArrayList<GenericItem>, keyFigures: List<KeyFigure>, category: String) {
        strings["keyFiguresController.category.$category"]?.let { nameCategory ->
            items += spaceItem {
                spaceRes = R.dimen.spacing_medium
                identifier = items.count().toLong()
            }
            items += bigTitleItem {
                text = nameCategory
                identifier = "keyFiguresController.category.$category".hashCode().toLong()
            }
            keyFigures.mapTo(items) {
                selectionItem {
                    title = strings[it.labelStringKey]
                    caption = strings[it.descriptionStringKey]
                    maxLineCaption = 3
                    identifier = it.labelStringKey.hashCode().toLong()
                    if (args.figureNumber == 1 && labelKey2 != it.labelKey) {
                        onClick = {
                            labelKey1 = it.labelKey
                            backToChoiceFragment()
                        }
                    } else if (args.figureNumber == 2 && labelKey1 != it.labelKey) {
                        onClick = {
                            labelKey2 = it.labelKey
                            backToChoiceFragment()
                        }
                    }

                    when (it.labelKey) {
                        labelKey1 -> applyColorSelectItem(this, 1)
                        labelKey2 -> applyColorSelectItem(this, 2)
                    }
                }
            }
        }
    }

    private fun applyColorSelectItem(selectionItem: SelectionItem, labelKeyNumber: Int) {
        selectionItem.apply {
            showSelection = true
            iconSelectionOn = if (labelKeyNumber == 1) R.drawable.ic_one else R.drawable.ic_two
            if (args.figureNumber != labelKeyNumber) {
                context?.let {
                    val color = ContextCompat.getColor(it, R.color.color_gray)
                    iconTint = color
                    textColor = color
                }
            }
        }
    }

    private fun backToChoiceFragment() {
        setFragmentResult(
            RESULT_LISTENER_KEY,
            bundleOf(
                RESULT_LISTENER_BUNDLE_KEY1 to labelKey1,
                RESULT_LISTENER_BUNDLE_KEY2 to labelKey2,
            ),
        )
        findNavControllerOrNull()?.popBackStack()
    }

    companion object {
        const val RESULT_LISTENER_KEY: String = "resultKeyFigurePicker"
        const val RESULT_LISTENER_BUNDLE_KEY1: String = "resultKeyFigure1"
        const val RESULT_LISTENER_BUNDLE_KEY2: String = "resultKeyFigure2"
    }
}
