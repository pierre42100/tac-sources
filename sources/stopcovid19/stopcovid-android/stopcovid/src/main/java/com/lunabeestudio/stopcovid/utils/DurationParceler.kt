/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2022/3/31 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.utils

import android.os.Parcel
import kotlinx.parcelize.Parceler
import kotlin.time.Duration
import kotlin.time.Duration.Companion.milliseconds

object DurationParceler : Parceler<Duration> {
    override fun Duration.write(parcel: Parcel, flags: Int): Unit = parcel.writeLong(inWholeMilliseconds)
    override fun create(parcel: Parcel): Duration = parcel.readLong().milliseconds
}
