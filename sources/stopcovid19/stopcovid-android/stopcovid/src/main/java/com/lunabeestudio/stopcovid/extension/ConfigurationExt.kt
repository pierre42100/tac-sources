/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2022/3/18 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.extension

import com.lunabeestudio.domain.model.Configuration

fun Configuration.getDefaultFigureLabel1(): String? {
    return keyFiguresCombination?.firstOrNull()?.keyFigureLabel1?.getLabelKeyFigureFromConfig()
}

fun Configuration.getDefaultFigureLabel2(): String? {
    return keyFiguresCombination?.firstOrNull()?.keyFigureLabel2?.getLabelKeyFigureFromConfig()
}
