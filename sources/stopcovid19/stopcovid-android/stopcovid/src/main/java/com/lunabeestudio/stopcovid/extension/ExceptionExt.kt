/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2020/04/05 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.extension

import com.lunabeestudio.domain.model.WalletCertificateError
import com.lunabeestudio.error.AppError
import timber.log.Timber

internal fun Exception.walletCertificateError(): WalletCertificateError? {
    Timber.e(this)
    return when {
        this is AppError && code == AppError.Code.WALLET_CERTIFICATE_INVALID_SIGNATURE ->
            WalletCertificateError.INVALID_CERTIFICATE_SIGNATURE
        this is AppError && code == AppError.Code.WALLET_CERTIFICATE_MALFORMED ->
            WalletCertificateError.MALFORMED_CERTIFICATE
        this is AppError && code == AppError.Code.WALLET_CERTIFICATE_UNKNOWN_ERROR ->
            WalletCertificateError.INVALID_CERTIFICATE_SIGNATURE
        else -> null
    }
}
