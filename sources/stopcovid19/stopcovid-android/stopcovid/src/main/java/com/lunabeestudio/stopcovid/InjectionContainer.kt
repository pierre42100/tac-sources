/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2022/3/18 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid

import android.content.Context
import android.content.SharedPreferences
import com.lunabeestudio.domain.di.LogsDir
import com.lunabeestudio.domain.manager.DebugManager
import com.lunabeestudio.domain.repository.BlacklistRepository
import com.lunabeestudio.domain.repository.CertificateDocumentRepository
import com.lunabeestudio.domain.repository.CertificateRepository
import com.lunabeestudio.domain.repository.DccCertificateRepository
import com.lunabeestudio.domain.repository.EUTagsRepository
import com.lunabeestudio.domain.repository.FeaturedInfoRepository
import com.lunabeestudio.domain.repository.InfoCenterRepository
import com.lunabeestudio.domain.repository.KeyFigureMapRepository
import com.lunabeestudio.domain.repository.KeyFigureRepository
import com.lunabeestudio.domain.repository.RisksLevelRepository
import com.lunabeestudio.domain.repository.SmartWalletEligibilityRepository
import com.lunabeestudio.domain.repository.SmartWalletValidityRepository
import com.lunabeestudio.domain.syncmanager.BlacklistSyncManager
import com.lunabeestudio.stopcovid.core.extension.getApplicationLocale
import com.lunabeestudio.stopcovid.core.manager.ConfigurationDatasource
import com.lunabeestudio.stopcovid.core.manager.StringsManager
import com.lunabeestudio.stopcovid.manager.ConfigurationManager
import com.lunabeestudio.stopcovid.manager.LinksManager
import com.lunabeestudio.stopcovid.manager.MoreKeyFiguresManager
import com.lunabeestudio.stopcovid.manager.PrivacyManager
import com.lunabeestudio.stopcovid.repository.WalletRepository
import com.lunabeestudio.stopcovid.usecase.ChangeLanguageUseCase
import com.lunabeestudio.stopcovid.usecase.ComputeDccEligibilityUseCase
import com.lunabeestudio.stopcovid.usecase.ComputeDccValidityUseCase
import com.lunabeestudio.stopcovid.usecase.DeletePostalCodeUseCase
import com.lunabeestudio.stopcovid.usecase.GenerateMultipassUseCase
import com.lunabeestudio.stopcovid.usecase.GetCloseMultipassProfilesUseCase
import com.lunabeestudio.stopcovid.usecase.GetFavoriteKeyFiguresUseCase
import com.lunabeestudio.stopcovid.usecase.GetFilteredMultipassProfileFromIdUseCase
import com.lunabeestudio.stopcovid.usecase.GetKeyFiguresWithCompareUseCase
import com.lunabeestudio.stopcovid.usecase.GetMultipassProfilesUseCase
import com.lunabeestudio.stopcovid.usecase.GetSmartWalletMapUseCase
import com.lunabeestudio.stopcovid.usecase.GetSmartWalletStateUseCase
import com.lunabeestudio.stopcovid.usecase.HasCloseMultipassProfileUseCase
import com.lunabeestudio.stopcovid.usecase.IsCertificateEUValidUseCase
import com.lunabeestudio.stopcovid.usecase.MigrateAppUseCase
import com.lunabeestudio.stopcovid.usecase.RefreshInfoCenterUseCase
import com.lunabeestudio.stopcovid.usecase.ReorderFavoriteKeyFiguresUseCase
import com.lunabeestudio.stopcovid.usecase.SendInfoCenterNotificationUseCase
import com.lunabeestudio.stopcovid.usecase.SmartWalletNotificationUseCase
import com.lunabeestudio.stopcovid.usecase.UpdateBlacklistUseCase
import com.lunabeestudio.stopcovid.usecase.VerifyAndGetCertificateCodeValueUseCase
import com.lunabeestudio.stopcovid.usecase.VerifyCertificateUseCase
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.CoroutineScope
import okhttp3.OkHttpClient
import java.io.File
import java.text.SimpleDateFormat
import javax.inject.Inject

class InjectionContainer @Inject constructor(
    @ApplicationContext private val context: Context,
    private val coroutineScope: CoroutineScope,
    private val sharedPrefs: SharedPreferences,
    val okHttpClient: OkHttpClient,
    val featuredInfoRepository: FeaturedInfoRepository,
    private val blacklistSyncManager: BlacklistSyncManager,
    private val blacklistRepository: BlacklistRepository,
    val infoCenterRepository: InfoCenterRepository,
    val keyFigureRepository: KeyFigureRepository,
    val keyFigureMapRepository: KeyFigureMapRepository,
    val certificateRepository: CertificateRepository,
    @LogsDir val logsDir: File,
    val debugManager: DebugManager,
    val certificateDocumentRepository: CertificateDocumentRepository,
    val risksLevelRepository: RisksLevelRepository,
    val euTagsRepository: EUTagsRepository,
    val dccCertificateRepository: DccCertificateRepository,
    val smartWalletEligibilityRepository: SmartWalletEligibilityRepository,
    val smartWalletValidityRepository: SmartWalletValidityRepository,
) {

    val stringsManager: StringsManager by lazy { StringsManager(okHttpClient) }
    val privacyManager: PrivacyManager by lazy { PrivacyManager(okHttpClient) }
    val linksManager: LinksManager by lazy { LinksManager(okHttpClient) }
    val moreKeyFiguresManager: MoreKeyFiguresManager by lazy { MoreKeyFiguresManager(okHttpClient) }
    private val configurationDataSource: ConfigurationDatasource by lazy { ConfigurationDatasource(okHttpClient) }
    val configurationManager: ConfigurationManager by lazy { ConfigurationManager(context, configurationDataSource) }

    private val verifyCertificateUseCase: VerifyCertificateUseCase
        get() = VerifyCertificateUseCase(
            configurationManager = configurationManager,
            dccCertificateRepository = dccCertificateRepository,
        )

    val verifyAndGetCertificateCodeValueUseCase: VerifyAndGetCertificateCodeValueUseCase
        get() = VerifyAndGetCertificateCodeValueUseCase(
            verifyCertificateUseCase = verifyCertificateUseCase,
        )

    val getSmartWalletMapUseCase: GetSmartWalletMapUseCase
        get() = GetSmartWalletMapUseCase(
            walletRepository = walletRepository,
            getSmartWalletStateUseCase = getSmartWalletStateUseCase,
        )

    val smartWalletNotificationUseCase: SmartWalletNotificationUseCase
        get() = SmartWalletNotificationUseCase(
            configurationManager = configurationManager,
            sharedPreferences = sharedPrefs,
            getSmartWalletMapUseCase = getSmartWalletMapUseCase,
            getSmartWalletStateUseCase = getSmartWalletStateUseCase,
        )

    val getFilteredMultipassProfileFromIdUseCase: GetFilteredMultipassProfileFromIdUseCase
        get() = GetFilteredMultipassProfileFromIdUseCase(
            configurationManager = configurationManager,
            walletRepository = walletRepository,
            dateFormat = SimpleDateFormat.getDateInstance(SimpleDateFormat.SHORT, context.getApplicationLocale()),
        )

    val getMultipassProfilesUseCase: GetMultipassProfilesUseCase
        get() = GetMultipassProfilesUseCase(
            walletRepository = walletRepository,
            dateFormat = SimpleDateFormat.getDateInstance(SimpleDateFormat.SHORT, context.getApplicationLocale()),
        )

    val hasCloseMultipassProfileUseCase: HasCloseMultipassProfileUseCase
        get() = HasCloseMultipassProfileUseCase(
            getMultipassProfilesUseCase = getMultipassProfilesUseCase,
            getCloseMultipassProfilesUseCase = getCloseMultipassProfilesUseCase,
            dateFormat = SimpleDateFormat.getDateInstance(SimpleDateFormat.SHORT, context.getApplicationLocale()),
        )

    val generateMultipassUseCase: GenerateMultipassUseCase
        get() = GenerateMultipassUseCase(
            walletRepository = walletRepository,
            verifyCertificateUseCase = verifyCertificateUseCase,
        )

    val getCloseMultipassProfilesUseCase: GetCloseMultipassProfilesUseCase
        get() = GetCloseMultipassProfilesUseCase()

    private val computeDccEligibilityUseCase: ComputeDccEligibilityUseCase
        get() = ComputeDccEligibilityUseCase(
            configurationManager = configurationManager,
            smartWalletEligibilityRepository = smartWalletEligibilityRepository,
        )

    val computeDccValidityUseCase: ComputeDccValidityUseCase
        get() = ComputeDccValidityUseCase(
            configurationManager = configurationManager,
            smartWalletValidityRepository = smartWalletValidityRepository,
        )

    val getSmartWalletStateUseCase: GetSmartWalletStateUseCase
        get() = GetSmartWalletStateUseCase(
            configurationManager = configurationManager,
            computeDccValidityUseCase = computeDccValidityUseCase,
            computeDccEligibilityUseCase = computeDccEligibilityUseCase,
        )

    val updateBlacklistUseCase: UpdateBlacklistUseCase
        get() = UpdateBlacklistUseCase(
            configurationManager = configurationManager,
            walletRepository = walletRepository,
            blacklistRepository = blacklistRepository,
            blacklistSyncManager = blacklistSyncManager,
            sharedPreferences = sharedPrefs,
            computeDccValidityUseCase = computeDccValidityUseCase,
        )

    val migrateAppUseCase: MigrateAppUseCase
        get() = MigrateAppUseCase(
            context = context,
            sharedPrefs = sharedPrefs,
        )

    val sendInfoCenterNotificationUseCase: SendInfoCenterNotificationUseCase
        get() = SendInfoCenterNotificationUseCase(
            stringsManager = stringsManager,
            sharedPreferences = sharedPrefs,
            tousAntiCovid = context as TousAntiCovid,
        )

    @Inject lateinit var refreshInfoCenterUseCase: RefreshInfoCenterUseCase

    val changeLanguageUseCase: ChangeLanguageUseCase
        get() = ChangeLanguageUseCase(
            context = context,
            sharedPrefs = sharedPrefs,
        )

    val getCompareKeyFiguresUseCase: GetKeyFiguresWithCompareUseCase
        get() = GetKeyFiguresWithCompareUseCase(
            sharedPrefs = sharedPrefs,
            configurationManager = configurationManager,
        )

    val isCertificateEUValidUseCase: IsCertificateEUValidUseCase
        get() = IsCertificateEUValidUseCase(
            euTagsRepository = euTagsRepository,
        )

    val deletePostalCodeUseCase: DeletePostalCodeUseCase
        get() = DeletePostalCodeUseCase(
            sharedPrefs = sharedPrefs,
            keyFiguresRepository = keyFigureRepository,
        )

    val getFavoriteKeyFiguresUseCase: GetFavoriteKeyFiguresUseCase
        get() = GetFavoriteKeyFiguresUseCase(
            keyFigureRepository = keyFigureRepository,
        )

    val reorderFavoriteKeyFiguresUseCase: ReorderFavoriteKeyFiguresUseCase
        get() = ReorderFavoriteKeyFiguresUseCase(
            keyFiguresRepository = keyFigureRepository,
        )

    val walletRepository: WalletRepository by lazy {
        WalletRepository(
            context = context,
            certificateRepository = certificateRepository,
            configurationManager = configurationManager,
            coroutineScope = coroutineScope,
        )
    }
}
