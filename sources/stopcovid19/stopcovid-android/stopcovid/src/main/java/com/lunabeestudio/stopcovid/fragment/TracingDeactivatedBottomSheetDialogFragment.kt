/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2023/02/14 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.fragment

import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import com.lunabeestudio.stopcovid.R
import com.lunabeestudio.stopcovid.core.fastitem.captionItem
import com.lunabeestudio.stopcovid.core.fastitem.lightButtonItem
import com.lunabeestudio.stopcovid.core.fastitem.spaceItem
import com.lunabeestudio.stopcovid.core.fragment.FastAdapterBottomSheetDialogFragment
import com.lunabeestudio.stopcovid.fastitem.bigTitleItem
import com.mikepenz.fastadapter.GenericItem

class TracingDeactivatedBottomSheetDialogFragment : FastAdapterBottomSheetDialogFragment() {

    override fun refreshScreen() {
        val items = arrayListOf<GenericItem>()

        items += spaceItem {
            spaceRes = R.dimen.spacing_xlarge
        }
        items += bigTitleItem {
            text = strings["noTracingExplanations.title"]
            gravity = Gravity.CENTER
            identifier = "noTracingExplanations.title".hashCode().toLong()
        }
        items += spaceItem {
            spaceRes = R.dimen.spacing_medium
        }
        items += captionItem {
            text = strings["noTracingExplanations.message"]
            identifier = "noTracingExplanations.message".hashCode().toLong()
        }
        items += spaceItem {
            spaceRes = R.dimen.spacing_medium
        }
        items += lightButtonItem {
            text = strings["noTracingExplanations.button"]
            onClickListener = View.OnClickListener {
                dismiss()
            }
            width = ViewGroup.LayoutParams.MATCH_PARENT
            identifier = "noTracingExplanations.button".hashCode().toLong()
        }
        items += spaceItem {
            spaceRes = R.dimen.spacing_xlarge
        }

        adapter.setNewList(items)
    }
}
