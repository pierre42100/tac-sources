/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2023/03/08 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.repository

import android.content.Context
import android.net.Uri
import android.net.UrlQuerySanitizer
import com.lunabeestudio.domain.model.RawWalletCertificate
import com.lunabeestudio.domain.model.TacResult
import com.lunabeestudio.domain.model.WalletCertificateType
import com.lunabeestudio.domain.repository.CertificateRepository
import com.lunabeestudio.error.AppError
import com.lunabeestudio.stopcovid.extension.raw
import com.lunabeestudio.stopcovid.manager.ConfigurationManager
import com.lunabeestudio.stopcovid.model.EuropeanCertificate
import com.lunabeestudio.stopcovid.model.WalletCertificate
import com.lunabeestudio.stopcovid.widgetshomescreen.DccWidget
import com.lunabeestudio.stopcovid.worker.SmartWalletNotificationWorker
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.firstOrNull
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.launch
import timber.log.Timber

class WalletRepository(
    context: Context,
    private val certificateRepository: CertificateRepository,
    private val configurationManager: ConfigurationManager,
    coroutineScope: CoroutineScope,
) {
    val walletCertificateFlow: StateFlow<TacResult<List<WalletCertificate>>>

    val certificateCountFlow: Flow<Int>
        get() = certificateRepository.certificateCountFlow.combine(walletCertificateFlow) { encryptedCount, certificates ->
            certificates.data?.size?.let { encryptedCount.coerceAtMost(it) } ?: encryptedCount
        }

    init {
        // SharingStarted.Eagerly to use the state flow as cache
        walletCertificateFlow = certificateRepository.rawWalletCertificatesFlow.map { rawWalletCertificatesResult ->
            rawWalletCertificatesResult.toWalletCertificatesResult()
        }.stateIn(coroutineScope, SharingStarted.Eagerly, TacResult.Loading())

        coroutineScope.launch(Dispatchers.Main) {
            walletCertificateFlow.collect {
                DccWidget.updateWidget(context)
            }
        }

        SmartWalletNotificationWorker.start(context)
    }

    fun extractCertificateDataFromUrl(urlValue: String): Pair<String, WalletCertificateType.Format?> {
        val uri = Uri.parse(urlValue)
        var code = uri.fragment

        if (code == null) { // Try the old way
            val sanitizer = UrlQuerySanitizer()
            sanitizer.registerParameter("v") {
                it // Do nothing since there are plenty of non legal characters in this value
            }
            sanitizer.parseUrl(sanitizer.unescape(urlValue))
            code = sanitizer.getValue("v")
        }

        val certificateFormat = uri.lastPathSegment?.let { WalletCertificateType.Format.fromValue(it) }

        return (code ?: throw AppError(AppError.Code.WALLET_CERTIFICATE_MALFORMED)) to certificateFormat
    }

    suspend fun saveCertificate(walletCertificate: WalletCertificate) {
        try {
            certificateRepository.insertAllRawWalletCertificates(listOf(walletCertificate.raw))
        } catch (e: Exception) {
            throw e
        }
    }

    suspend fun toggleFavorite(dcc: EuropeanCertificate) {
        val walletCertificates = certificateRepository.rawWalletCertificatesFlow.firstOrNull()?.data.orEmpty()
        var currentRawFavorite: RawWalletCertificate? = null
        val newRawFavorite: RawWalletCertificate? = walletCertificates.firstOrNull { it.id == dcc.id }

        newRawFavorite?.let {
            if (!dcc.isFavorite) {
                // Remove current favorite if there is one
                currentRawFavorite = walletCertificates.firstOrNull { it.isFavorite }
                currentRawFavorite?.let { currentFavorite ->
                    currentFavorite.isFavorite = false
                }
            }

            newRawFavorite.isFavorite = !newRawFavorite.isFavorite
        }

        certificateRepository.updateAllRawWalletCertificates(listOfNotNull(currentRawFavorite, newRawFavorite))
    }

    suspend fun deleteCertificate(walletCertificate: WalletCertificate) {
        certificateRepository.deleteRawWalletCertificate(walletCertificate.id)
    }

    suspend fun deleteAllCertificates() {
        certificateRepository.deleteAllRawWalletCertificates()
    }

    suspend fun getCertificateCount(): Int {
        return certificateRepository.getCertificateCount()
    }

    fun certificateExists(certificate: WalletCertificate): Boolean {
        return walletCertificateFlow.value.data?.any { certificate.value == it.value } == true
    }

    suspend fun getCertificateById(certificateId: String): WalletCertificate? =
        certificateRepository.getCertificateById(certificateId)?.toWalletCertificate()

    private suspend fun TacResult<List<RawWalletCertificate>>.toWalletCertificatesResult(): TacResult<List<WalletCertificate>> {
        val walletCertificates = data?.mapNotNull { rawWallet ->
            rawWallet.toWalletCertificate()
        }.orEmpty()
        return if (this is TacResult.Success) {
            TacResult.Success(walletCertificates)
        } else {
            TacResult.Failure((this as? TacResult.Failure)?.throwable, walletCertificates)
        }
    }

    private suspend fun RawWalletCertificate.toWalletCertificate() = try {
        WalletCertificate.createCertificateFromRaw(this)?.apply {
            parse()
        }
    } catch (e: Exception) {
        Timber.e(e)
        null
    }

    suspend fun updateCertificate(certificate: RawWalletCertificate) {
        certificateRepository.updateAllRawWalletCertificates(listOf(certificate))
    }

    suspend fun updateCertificate(certificates: List<RawWalletCertificate>) {
        certificateRepository.updateAllRawWalletCertificates(certificates)
    }

    suspend fun generateMultipass(certificateValueList: List<String>): TacResult<String> {
        return certificateRepository.generateMultipass(
            configurationManager.configuration.generationServerPublicKey,
            certificateValueList,
        )
    }

    suspend fun forceRefreshCertificatesFlow() {
        certificateRepository.forceRefreshCertificatesFlow()
    }

    suspend fun deleteLostCertificates() {
        certificateRepository.deleteLostCertificates()
    }

    fun resetKeyCryptoGeneratedFlag() {
        certificateRepository.resetKeyGeneratedFlag()
    }
}
