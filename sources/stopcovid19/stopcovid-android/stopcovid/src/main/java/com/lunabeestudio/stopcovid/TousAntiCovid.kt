/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2020/04/05 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid

import android.app.Application
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Build
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.emoji.bundled.BundledEmojiCompatConfig
import androidx.emoji.text.EmojiCompat
import androidx.lifecycle.DefaultLifecycleObserver
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.ProcessLifecycleOwner
import androidx.preference.PreferenceManager
import androidx.work.Constraints
import androidx.work.ExistingPeriodicWorkPolicy
import androidx.work.NetworkType
import androidx.work.PeriodicWorkRequestBuilder
import androidx.work.WorkManager
import com.lunabeestudio.common.CommonConstant
import com.lunabeestudio.common.ConfigConstant
import com.lunabeestudio.common.OkHttpConstants
import com.lunabeestudio.domain.utils.JCEUtils
import com.lunabeestudio.stopcovid.activity.MainActivity
import com.lunabeestudio.stopcovid.core.LocalizedApplication
import com.lunabeestudio.stopcovid.core.UiConstants
import com.lunabeestudio.stopcovid.core.manager.LocalizedStrings
import com.lunabeestudio.stopcovid.core.utils.Event
import com.lunabeestudio.stopcovid.core.utils.ImmutablePendingIntentCompat
import com.lunabeestudio.stopcovid.extension.googleReviewShown
import com.lunabeestudio.stopcovid.extension.lastVersionCode
import com.lunabeestudio.stopcovid.extension.lastVersionMigration
import com.lunabeestudio.stopcovid.extension.lastVersionName
import com.lunabeestudio.stopcovid.extension.lowStorageAlertShown
import com.lunabeestudio.stopcovid.extension.ratingPopInShown
import com.lunabeestudio.stopcovid.extension.ratingsKeyFiguresOpening
import com.lunabeestudio.stopcovid.manager.AppMaintenanceManager
import com.lunabeestudio.stopcovid.usecase.MigrateAppUseCase
import com.lunabeestudio.stopcovid.worker.ConfigWorker
import com.lunabeestudio.stopcovid.worker.InfoCenterWorker
import com.lunabeestudio.stopcovid.worker.MaintenanceWorker
import dagger.hilt.android.HiltAndroidApp
import fr.bipi.tressence.file.FileLoggerTree
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import okhttp3.Cache
import timber.log.Timber
import java.io.File
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import kotlin.time.Duration.Companion.seconds

@HiltAndroidApp
class TousAntiCovid : Application(), LocalizedApplication {

    @Inject lateinit var appCoroutineScope: CoroutineScope

    @Inject lateinit var injectionContainer: InjectionContainer

    @Inject lateinit var jceUtils: JCEUtils

    override val localizedStrings: LocalizedStrings
        get() = injectionContainer.stringsManager.strings
    override val liveLocalizedStrings: LiveData<Event<LocalizedStrings>>
        get() = injectionContainer.stringsManager.liveStrings

    override suspend fun initializeStrings(): Unit = injectionContainer.stringsManager.initialize(this)

    var isAppInForeground: Boolean = false

    private val sharedPrefs: SharedPreferences by lazy {
        PreferenceManager.getDefaultSharedPreferences(this)
    }

    private var firstResume = false

    private val sharedPreferenceChangeListener = SharedPreferences.OnSharedPreferenceChangeListener { _, key ->
        if (key == CommonConstant.SharedPrefs.USER_LANGUAGE) {
            appCoroutineScope.launch {
                injectionContainer.stringsManager.onAppForeground(this@TousAntiCovid)
                injectionContainer.privacyManager.onAppForeground(this@TousAntiCovid)
                injectionContainer.linksManager.onAppForeground(this@TousAntiCovid)
                injectionContainer.moreKeyFiguresManager.onAppForeground(this@TousAntiCovid)
                injectionContainer.featuredInfoRepository.initialize()
            }
        }
    }

    private val appObserver = object : DefaultLifecycleObserver {
        override fun onResume(owner: LifecycleOwner) {
            super.onResume(owner)
            isAppInForeground = true
            refreshData()
        }

        override fun onPause(owner: LifecycleOwner) {
            super.onPause(owner)
            isAppInForeground = false
        }
    }

    init {
        System.setProperty("kotlinx.coroutines.debug", if (BuildConfig.DEBUG) "on" else "off")
    }

    override fun onCreate() {
        super.onCreate()

        ProcessLifecycleOwner.get().lifecycle.addObserver(appObserver)
        firstResume = true

        setupTimber()

        jceUtils.updateBouncyCastle()

        // Make sure data is cleaned before initializeData
        val lastVersionCode = sharedPrefs.lastVersionCode
        if (lastVersionCode < BuildConfig.VERSION_CODE) {
            if (lastVersionCode == 0) {
                sharedPrefs.lastVersionMigration = MigrateAppUseCase.CURRENT_MIGRATION_VERSION
            }
            injectionContainer.migrateAppUseCase()
            clearData()
            appCoroutineScope.launch {
                injectionContainer.keyFigureMapRepository.reload()
            }
        }

        if (sharedPrefs.lastVersionName != BuildConfig.VERSION_NAME) {
            sharedPrefs.ratingsKeyFiguresOpening = 0
            sharedPrefs.googleReviewShown = false
            sharedPrefs.lowStorageAlertShown = false
        }
        if (!isSameMajorRelease()) {
            sharedPrefs.ratingPopInShown = false
        }

        initializeData()

        val config = BundledEmojiCompatConfig(this)
        EmojiCompat.init(config)

        setupAppMaintenance()
        startConfigWorker()

        sharedPrefs.registerOnSharedPreferenceChangeListener(sharedPreferenceChangeListener)

        sharedPrefs.lastVersionCode = BuildConfig.VERSION_CODE
        sharedPrefs.lastVersionName = BuildConfig.VERSION_NAME
    }

    private fun setupTimber() {
        Timber.plant(WarnTree())
        injectionContainer.logsDir.mkdir()
        Timber.plant(
            FileLoggerTree.Builder()
                .withFileName("logs_%g.log")
                .withDir(injectionContainer.logsDir)
                .withSizeLimit(1024 * 1024 * 2)
                .withFileLimit(2)
                .withMinPriority(Log.VERBOSE)
                .appendToFile(true)
                .build(),
        )
        Timber.plant(
            FileLoggerTree.Builder()
                .withFileName("error_logs_%g.log")
                .withDir(injectionContainer.logsDir)
                .withSizeLimit(1024 * 1024 * 2)
                .withFileLimit(2)
                .withMinPriority(Log.WARN)
                .appendToFile(true)
                .build(),
        )
    }

    private fun clearData() {
        injectionContainer.moreKeyFiguresManager.clearLocal(this)
        injectionContainer.linksManager.clearLocal(this)
        injectionContainer.privacyManager.clearLocal(this)
        injectionContainer.stringsManager.clearLocal(this)
        injectionContainer.configurationManager.clearLocal(this)
        injectionContainer.risksLevelRepository.clearLocal()
        val okHttpCacheDir = File(cacheDir, OkHttpConstants.OKHTTP_CACHE_FILENAME)
        if (okHttpCacheDir.exists()) {
            try {
                @Suppress("BlockingMethodInNonBlockingContext")
                Cache(okHttpCacheDir, OkHttpConstants.OKHTTP_MAX_CACHE_SIZE_BYTES).delete()
            } catch (e: Exception) {
                Timber.e(e)
            }
        }
    }

    private fun initializeData() {
        appCoroutineScope.launch {
            injectionContainer.moreKeyFiguresManager.initialize(this@TousAntiCovid)
        }
        appCoroutineScope.launch {
            injectionContainer.linksManager.initialize(this@TousAntiCovid)
        }
        appCoroutineScope.launch {
            injectionContainer.privacyManager.initialize(this@TousAntiCovid)
        }
        appCoroutineScope.launch {
            injectionContainer.stringsManager.initialize(this@TousAntiCovid)
        }
        appCoroutineScope.launch {
            injectionContainer.refreshInfoCenterUseCase()
            InfoCenterWorker.schedule(this@TousAntiCovid)
        }
        appCoroutineScope.launch {
            injectionContainer.euTagsRepository.initialize()
        }
        appCoroutineScope.launch {
            injectionContainer.smartWalletEligibilityRepository.initialize()
        }
        appCoroutineScope.launch {
            injectionContainer.smartWalletValidityRepository.initialize()
        }
        appCoroutineScope.launch {
            injectionContainer.keyFigureRepository.onAppForeground()
        }
        appCoroutineScope.launch {
            injectionContainer.featuredInfoRepository.initialize()
        }
        appCoroutineScope.launch {
            injectionContainer.risksLevelRepository.initialize()
        }
        appCoroutineScope.launch {
            injectionContainer.keyFigureMapRepository.initialize()
        }

        runBlocking {
            injectionContainer.dccCertificateRepository.initialize()
        }
    }

    private fun setupAppMaintenance() {
        AppMaintenanceManager.initialize(
            this@TousAntiCovid,
            R.drawable.home,
            R.drawable.home,
            ConfigConstant.Maintenance.URL,
        )
        startAppMaintenanceWorker()
    }

    private fun refreshData() {
        appCoroutineScope.launch(Dispatchers.IO) {
            if (firstResume) {
                delay(1.seconds) // Add some delay to let the main activity start
            }
            firstResume = false

            AppMaintenanceManager.checkForMaintenanceUpgrade(this@TousAntiCovid, injectionContainer.okHttpClient)
            injectionContainer.moreKeyFiguresManager.onAppForeground(this@TousAntiCovid)
            injectionContainer.linksManager.onAppForeground(this@TousAntiCovid)
            injectionContainer.privacyManager.onAppForeground(this@TousAntiCovid)
            injectionContainer.stringsManager.onAppForeground(this@TousAntiCovid)
            injectionContainer.euTagsRepository.onAppForeground()
            injectionContainer.risksLevelRepository.onAppForeground()
            injectionContainer.dccCertificateRepository.onAppForeground()
            injectionContainer.smartWalletEligibilityRepository.onAppForeground()
            injectionContainer.smartWalletValidityRepository.onAppForeground()
            injectionContainer.keyFigureRepository.onAppForeground()
            injectionContainer.featuredInfoRepository.initialize()
            injectionContainer.updateBlacklistUseCase()
            injectionContainer.refreshInfoCenterUseCase()

            injectionContainer.certificateDocumentRepository.onAppForeground(
                injectionContainer.configurationManager.configuration.updateFilesAfterDays,
            )

            injectionContainer.okHttpClient.connectionPool.evictAll()
        }
    }

    suspend fun sendUpgradeNotification() {
        val notificationManager = applicationContext.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        if (injectionContainer.stringsManager.strings.isEmpty()) {
            injectionContainer.stringsManager.initialize(applicationContext)
        }
        val strings = injectionContainer.stringsManager.strings

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(
                UiConstants.Notification.UPGRADE.channelId,
                strings["notification.channel.upgrade.title"] ?: "Upgrade",
                NotificationManager.IMPORTANCE_HIGH,
            )
            notificationManager.createNotificationChannel(channel)
        }

        val notificationIntent = Intent(this, MainActivity::class.java)
        val pendingIntent = ImmutablePendingIntentCompat.getActivity(
            this,
            0,
            notificationIntent,
        )
        val notification = NotificationCompat.Builder(
            this,
            UiConstants.Notification.UPGRADE.channelId,
        )
            .setContentTitle(strings["notification.upgrade.title"])
            .setPriority(NotificationCompat.PRIORITY_HIGH)
            .setVisibility(NotificationCompat.VISIBILITY_SECRET)
            .setAutoCancel(true)
            .setOnlyAlertOnce(true)
            .setSmallIcon(com.lunabeestudio.stopcovid.core.R.drawable.ic_notification_bar)
            .setStyle(
                NotificationCompat.BigTextStyle()
                    .bigText(strings["notification.upgrade.message"]),
            )
            .setContentIntent(pendingIntent)
            .build()
        notificationManager.notify(UiConstants.Notification.UPGRADE.notificationId, notification)
    }

    private fun startAppMaintenanceWorker() {
        val constraints = Constraints.Builder()
            .setRequiredNetworkType(NetworkType.CONNECTED)
            .build()
        val appMaintenanceWorkRequest = PeriodicWorkRequestBuilder<MaintenanceWorker>(6L, TimeUnit.HOURS)
            .setConstraints(constraints)
            .setInitialDelay(6L, TimeUnit.HOURS)
            .build()
        WorkManager.getInstance(this)
            .enqueueUniquePeriodicWork(
                Constants.WorkerNames.UPGRADE_MAINTENANCE,
                ExistingPeriodicWorkPolicy.KEEP,
                appMaintenanceWorkRequest,
            )
    }

    private fun startConfigWorker() {
        val constraints = Constraints.Builder()
            .setRequiredNetworkType(NetworkType.CONNECTED)
            .build()
        val configWorkRequest = PeriodicWorkRequestBuilder<ConfigWorker>(6L, TimeUnit.HOURS)
            .setConstraints(constraints)
            .setInitialDelay(6L, TimeUnit.HOURS)
            .build()
        WorkManager.getInstance(this)
            .enqueueUniquePeriodicWork(Constants.WorkerNames.CONFIG, ExistingPeriodicWorkPolicy.KEEP, configWorkRequest)
    }

    private fun isSameMajorRelease(): Boolean {
        val lastImportantRelease = sharedPrefs.lastVersionName
            ?.split(".")
            ?.take(Constants.Build.NB_DIGIT_MAJOR_RELEASE)
            ?.joinToString(".")
        val currentImportantRelease = BuildConfig.VERSION_NAME
            .split(".")
            .take(Constants.Build.NB_DIGIT_MAJOR_RELEASE)
            .joinToString(".")
        return lastImportantRelease == currentImportantRelease
    }
}
