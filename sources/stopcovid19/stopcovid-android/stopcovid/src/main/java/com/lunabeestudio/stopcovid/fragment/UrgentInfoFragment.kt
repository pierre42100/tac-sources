/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2021/10/04 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.fragment

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.lunabeestudio.stopcovid.R
import com.lunabeestudio.stopcovid.core.fastitem.cardWithActionItem
import com.lunabeestudio.stopcovid.core.fastitem.spaceItem
import com.lunabeestudio.stopcovid.core.model.Action
import com.lunabeestudio.stopcovid.extension.openInExternalBrowser
import com.lunabeestudio.stopcovid.fastitem.videoPlayerItem
import com.mikepenz.fastadapter.GenericItem

class UrgentInfoFragment : MainFragment() {
    override fun getTitleKey(): String = "dgsUrgentController.title"

    private var hideMediaController: (() -> Unit)? = null

    private var onScrollListener: RecyclerView.OnScrollListener = object : RecyclerView.OnScrollListener() {
        override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
            hideMediaController?.invoke()
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        // To avoid media controller glitch on scroll
        binding?.recyclerView?.addOnScrollListener(onScrollListener)
    }

    override suspend fun getItems(): List<GenericItem> {
        val items = ArrayList<GenericItem>()

        strings["dgsUrgentController.videoUrl"]?.let {
            if (it.isNotBlank()) {
                items += videoPlayerItem {
                    url = it
                    hideMediaController = this::hideMediaController
                    autoPlay = true
                    retryContentDescription = strings["common.tryAgain"]
                    identifier = "dgsUrgentController.videoUrl".hashCode().toLong()
                }
            }
        }

        items += spaceItem {
            spaceRes = R.dimen.spacing_large
        }

        if (!strings["dgsUrgentController.section1.title"].isNullOrBlank()) {
            items += cardWithActionItem {
                mainTitle = strings["dgsUrgentController.section1.title"]
                mainBody = strings["dgsUrgentController.section1.desc"]
                actions = strings["dgsUrgentController.section1.url"]?.let { url ->
                    listOf(
                        Action(label = strings["dgsUrgentController.section1.labelUrl"]) {
                            context?.let(url::openInExternalBrowser)
                        },
                    )
                }
                identifier = "dgsUrgentController.section1.title".hashCode().toLong()
            }

            items += spaceItem {
                spaceRes = R.dimen.spacing_large
            }
        }

        if (!strings["dgsUrgentController.section2.title"].isNullOrBlank()) {
            items += cardWithActionItem {
                mainTitle = strings["dgsUrgentController.section2.title"]
                mainBody = strings["dgsUrgentController.section2.desc"]
                actions = strings["dgsUrgentController.section2.url"]?.let { url ->
                    listOf(
                        Action(label = strings["dgsUrgentController.section2.labelUrl"]) {
                            context?.let(url::openInExternalBrowser)
                        },
                    )
                }
                identifier = "dgsUrgentController.section2.title".hashCode().toLong()
            }

            items += spaceItem {
                spaceRes = R.dimen.spacing_large
            }
        }

        return items
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding?.recyclerView?.removeOnScrollListener(onScrollListener)
    }
}
