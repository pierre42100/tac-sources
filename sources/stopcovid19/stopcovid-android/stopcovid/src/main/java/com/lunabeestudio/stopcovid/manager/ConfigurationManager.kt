/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2020/28/05 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.manager

import android.content.Context
import com.lunabeestudio.domain.model.Configuration
import com.lunabeestudio.domain.model.TacResult
import com.lunabeestudio.stopcovid.core.manager.ConfigurationDatasource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import timber.log.Timber

class ConfigurationManager(context: Context, private val configManager: ConfigurationDatasource) {

    var configuration: Configuration = loadConfig(context)
        private set

    fun clearLocal(context: Context) {
        configManager.clearLocal(context)
        configuration = loadConfig(context)
    }

    suspend fun onAppForeground(context: Context) {
        @Suppress("BlockingMethodInNonBlockingContext")
        return withContext(Dispatchers.IO) {
            try {
                configuration = configManager.fetchOrLoad(context)
            } catch (e: Exception) {
                Timber.e(e)
            }
        }
    }

    suspend fun refreshConfig(context: Context): TacResult<Configuration> {
        return withContext(Dispatchers.IO) {
            try {
                configuration = configManager.fetch(context)!!
                TacResult.Success(configuration)
            } catch (e: Exception) {
                Timber.e(e)
                TacResult.Failure(e)
            }
        }
    }

    private fun loadConfig(context: Context): Configuration {
        return configManager.load(context)
    }

    // TODO remove from prod build + use somewhere else (used only for debug build)
    fun debugReplaceConfig(configuration: Configuration) {
        this.configuration = configuration
    }
}
