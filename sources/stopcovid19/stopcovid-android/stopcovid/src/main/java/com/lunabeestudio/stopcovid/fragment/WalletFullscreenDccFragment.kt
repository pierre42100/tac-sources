/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2020/10/05 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.fragment

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.core.view.MenuHost
import androidx.core.view.MenuProvider
import androidx.core.view.isVisible
import androidx.fragment.app.setFragmentResultListener
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.navigation.NavController
import androidx.navigation.NavDestination
import androidx.navigation.fragment.navArgs
import com.google.zxing.BarcodeFormat
import com.journeyapps.barcodescanner.BarcodeEncoder
import com.lunabeestudio.domain.model.TacResult
import com.lunabeestudio.domain.model.WalletCertificateType
import com.lunabeestudio.stopcovid.R
import com.lunabeestudio.stopcovid.activity.MainActivity
import com.lunabeestudio.stopcovid.core.extension.appCompatActivity
import com.lunabeestudio.stopcovid.core.extension.findNavControllerOrNull
import com.lunabeestudio.stopcovid.core.extension.setTextOrHide
import com.lunabeestudio.stopcovid.core.extension.toDimensSize
import com.lunabeestudio.stopcovid.core.fragment.BaseFragment
import com.lunabeestudio.stopcovid.databinding.FragmentWalletFullscreenDccBinding
import com.lunabeestudio.stopcovid.extension.collectDataWithLifecycle
import com.lunabeestudio.stopcovid.extension.fullDescription
import com.lunabeestudio.stopcovid.extension.fullScreenBorderDescription
import com.lunabeestudio.stopcovid.extension.fullScreenBorderFooter
import com.lunabeestudio.stopcovid.extension.fullScreenDescription
import com.lunabeestudio.stopcovid.extension.injectionContainer
import com.lunabeestudio.stopcovid.extension.isFrench
import com.lunabeestudio.stopcovid.extension.safeNavigate
import com.lunabeestudio.stopcovid.extension.stringKey
import com.lunabeestudio.stopcovid.manager.ShareManager
import com.lunabeestudio.stopcovid.model.EuropeanCertificate
import com.lunabeestudio.stopcovid.viewmodel.WalletFullscreenDccViewModel
import com.lunabeestudio.stopcovid.viewmodel.WalletFullscreenDccViewModelFactory

class WalletFullscreenDccFragment : BaseFragment() {

    private val args: WalletFullscreenDccFragmentArgs by navArgs()

    private val viewModel: WalletFullscreenDccViewModel by viewModels {
        WalletFullscreenDccViewModelFactory(
            injectionContainer.walletRepository,
            injectionContainer.isCertificateEUValidUseCase,
        )
    }

    private val barcodeEncoder = BarcodeEncoder()
    private val qrCodeSize by lazy {
        R.dimen.qr_code_fullscreen_size.toDimensSize(requireContext()).toInt()
    }

    private var europeanCertificate: EuropeanCertificate? = null

    private lateinit var binding: FragmentWalletFullscreenDccBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentWalletFullscreenDccBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        appCompatActivity?.supportActionBar?.title = strings["walletController.title"]
        viewModel.getCertificate(args.id).collectDataWithLifecycle(viewLifecycleOwner) { result ->
            val mainActivity = activity as? MainActivity
            when (result) {
                is TacResult.Failure -> {
                    mainActivity?.showProgress(false)
                    strings["walletFullscreenController.error.certificateNotFound"]?.let {
                        mainActivity?.showErrorSnackBar(
                            it,
                        )
                    }
                    findNavControllerOrNull()?.popBackStack()
                }
                is TacResult.Loading -> mainActivity?.showProgress(true)
                is TacResult.Success -> {
                    mainActivity?.showProgress(false)
                    this@WalletFullscreenDccFragment.europeanCertificate = result.successData
                    refreshScreen()
                }
            }
        }
        binding.detailsTextSwitcher.setInAnimation(view.context, R.anim.fade_in)
        binding.detailsTextSwitcher.setOutAnimation(view.context, R.anim.fade_out)
        binding.explanationTextSwitcher.setInAnimation(view.context, R.anim.fade_in)
        binding.explanationTextSwitcher.setOutAnimation(view.context, R.anim.fade_out)
        addMenuProvider()
    }

    private fun addMenuProvider() {
        (activity as? MenuHost)?.addMenuProvider(
            object : MenuProvider {
                override fun onCreateMenu(menu: Menu, menuInflater: MenuInflater) {
                    menuInflater.inflate(R.menu.fullscreen_qr_code_menu, menu)
                }

                override fun onPrepareMenu(menu: Menu) {
                    super.onPrepareMenu(menu)
                    menu.findItem(R.id.qr_code_menu_share).title = strings["walletController.menu.share"]
                    menu.findItem(R.id.qr_code_menu_more).title = strings["accessibility.hint.otherActions"]
                }

                override fun onMenuItemSelected(menuItem: MenuItem): Boolean {
                    return when (menuItem.itemId) {
                        R.id.qr_code_menu_share -> {
                            showCertificateSharingBottomSheet()
                            true
                        }
                        R.id.qr_code_menu_more -> {
                            showQrCodeMoreActionBottomSheet()
                            true
                        }
                        else -> false
                    }
                }
            },
            viewLifecycleOwner,
            Lifecycle.State.RESUMED,
        )
    }

    private fun showCertificateSharingBottomSheet() {
        val text = europeanCertificate?.fullDescription(
            strings,
            injectionContainer.configurationManager.configuration,
            context,
            null,
        )
        ShareManager.setupCertificateSharingBottomSheet(this, text) {
            binding.barcodeSecuredView.runUnsecured {
                binding.let { ShareManager.getShareCaptureUri(it, ShareManager.certificateScreenshotFilename) }
            }
        }
        findNavControllerOrNull()?.safeNavigate(
            WalletFullscreenDccFragmentDirections.actionWalletFullscreenDccFragmentToCertificateSharingBottomSheetFragment(),
        )
    }

    private fun showQrCodeMoreActionBottomSheet() {
        setFragmentResultListener(QrCodeMoreActionBottomSheetFragment.MORE_ACTION_RESULT_KEY) { _, bundle ->
            if (bundle.getBoolean(QrCodeMoreActionBottomSheetFragment.MORE_ACTION_BUNDLE_KEY_SHARE_REQUESTED, false)) {
                findNavControllerOrNull()?.addOnDestinationChangedListener(
                    object : NavController.OnDestinationChangedListener {
                        override fun onDestinationChanged(
                            controller: NavController,
                            destination: NavDestination,
                            arguments: Bundle?,
                        ) {
                            if (controller.currentDestination?.id == R.id.walletFullscreenDccFragment) {
                                showCertificateSharingBottomSheet()
                                controller.removeOnDestinationChangedListener(this)
                            }
                        }
                    },
                )
            }
        }

        findNavControllerOrNull()?.safeNavigate(
            WalletFullscreenDccFragmentDirections.actionWalletFullscreenDccFragmentToQrCodeMoreActionBottomSheetFragment(
                showShare = true,
                showBrightness = true,
            ),
        )
    }

    override fun refreshScreen() {
        val europeanCertificate = europeanCertificate ?: return

        binding.apply {
            logosImageView.isVisible =
                europeanCertificate.greenCertificate.isFrench && europeanCertificate.type != WalletCertificateType.MULTI_PASS
            binding.showMoreSwitch.isVisible = europeanCertificate.type != WalletCertificateType.MULTI_PASS
            binding.explanationTextSwitcher.isVisible = europeanCertificate.type != WalletCertificateType.MULTI_PASS

            showMoreSwitch.text = strings["europeanCertificate.fullscreen.type.border.switch"]
            showMoreSwitch.setOnCheckedChangeListener { _, isChecked ->
                refreshDetails(isChecked, europeanCertificate)
            }
            refreshDetails(showMoreSwitch.isChecked, europeanCertificate)
            barcodeSecuredView.bitmap =
                barcodeEncoder.encodeBitmap(
                    europeanCertificate.value,
                    BarcodeFormat.QR_CODE,
                    qrCodeSize,
                    qrCodeSize,
                )
        }
    }

    @SuppressLint("SimpleDateFormat")
    private fun FragmentWalletFullscreenDccBinding.refreshDetails(
        isBorder: Boolean,
        europeanCertificate: EuropeanCertificate,
    ) {
        val smartWalletState = injectionContainer.getSmartWalletStateUseCase(europeanCertificate)
        if (isBorder) {
            detailsTextSwitcher.setCurrentText("")
            detailsTextSwitcher.setText(
                europeanCertificate.fullScreenBorderDescription(
                    strings = strings,
                    configuration = injectionContainer.configurationManager.configuration,
                    smartWalletState = smartWalletState,
                ),
            )
            explanationTextSwitcher.setCurrentText("")
            explanationTextSwitcher.setText(europeanCertificate.fullScreenBorderFooter(strings, context))
            if (!viewModel.isCertificateEUValid(europeanCertificate)) {
                headerTextView.setTextOrHide(strings["europeanCertificate.fullscreen.tagNotUe.border.warning"])
            } else {
                headerTextView.setTextOrHide(
                    strings["europeanCertificate.fullscreen.${europeanCertificate.type.stringKey}.border.warning"],
                )
            }
        } else {
            detailsTextSwitcher.setCurrentText("")
            detailsTextSwitcher.setText(
                context?.let { ctx ->
                    europeanCertificate.fullScreenDescription(
                        strings = strings,
                        context = ctx,
                        smartWalletState = smartWalletState,
                    )
                },
            )
            explanationTextSwitcher.setCurrentText("")
            explanationTextSwitcher.setText(strings["europeanCertificate.fullscreen.type.minimum.footer"])
            headerTextView.isVisible = false
        }
    }
}
