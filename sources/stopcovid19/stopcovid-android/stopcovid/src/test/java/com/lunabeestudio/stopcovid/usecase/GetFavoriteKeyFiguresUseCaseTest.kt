/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2021/7/10 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.usecase

import com.lunabeestudio.domain.model.KeyFigure
import com.lunabeestudio.domain.model.TacResult
import com.lunabeestudio.domain.repository.KeyFigureRepository
import com.lunabeestudio.stopcovid.utils.KeyFiguresHelper
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class GetFavoriteKeyFiguresUseCaseTest {
    private lateinit var useCase: GetFavoriteKeyFiguresUseCase

    private val keyFiguresRepository = mockk<KeyFigureRepository>(relaxed = true)

    @Before
    fun init() {
        useCase = GetFavoriteKeyFiguresUseCase(
            keyFiguresRepository,
        )
    }

    @Test
    fun only_favorites() {
        val list = listOf(
            KeyFiguresHelper.defaultKeyFigureForFav(0, true, 0f),
            KeyFiguresHelper.defaultKeyFigureForFav(1, true, 2f),
            KeyFiguresHelper.defaultKeyFigureForFav(2, true, 1f),
        )
        coEvery { keyFiguresRepository.keyFiguresWithDepartmentFlow } returns flowOf(TacResult.Success(list))

        val result = runBlocking {
            useCase.invoke().toList().first()
        }

        coVerify {
            keyFiguresRepository.keyFiguresWithDepartmentFlow
        }
        assertEquals(0, result[0].index)
        assertEquals(2, result[1].index)
        assertEquals(1, result[2].index)
    }

    @Test
    fun only_non_favorites() {
        val list = listOf(
            KeyFiguresHelper.defaultKeyFigureForFav(0, false, 0f),
            KeyFiguresHelper.defaultKeyFigureForFav(1, false, 2f),
            KeyFiguresHelper.defaultKeyFigureForFav(2, false, 1f),
        )
        coEvery { keyFiguresRepository.keyFiguresWithDepartmentFlow } returns flowOf(TacResult.Success(list))

        val result = runBlocking {
            useCase.invoke().toList().first()
        }

        coVerify {
            keyFiguresRepository.keyFiguresWithDepartmentFlow
        }
        assertTrue(result.isEmpty())
    }

    @Test
    fun mixed() {
        val list = listOf(
            KeyFiguresHelper.defaultKeyFigureForFav(0, true, 0f),
            KeyFiguresHelper.defaultKeyFigureForFav(1, true, 2f),
            KeyFiguresHelper.defaultKeyFigureForFav(2, false, 0.5f),
            KeyFiguresHelper.defaultKeyFigureForFav(3, true, 1f),
        )
        coEvery { keyFiguresRepository.keyFiguresWithDepartmentFlow } returns flowOf(TacResult.Success(list))

        val result = runBlocking {
            useCase.invoke().toList().first()
        }

        coVerify {
            keyFiguresRepository.keyFiguresWithDepartmentFlow
        }
        assertEquals(0, result[0].index)
        assertEquals(3, result[1].index)
        assertEquals(1, result[2].index)
    }

    @Test
    fun empty() {
        val list = emptyList<KeyFigure>()
        coEvery { keyFiguresRepository.keyFiguresWithDepartmentFlow } returns flowOf(TacResult.Success(list))

        val result = runBlocking {
            useCase.invoke().toList().first()
        }

        coVerify {
            keyFiguresRepository.keyFiguresWithDepartmentFlow
        }
        assertTrue(result.isEmpty())
    }

    @Test
    fun error() {
        coEvery { keyFiguresRepository.keyFiguresWithDepartmentFlow } returns flowOf(TacResult.Failure(null))

        val result = runBlocking {
            useCase.invoke().toList().first()
        }

        coVerify {
            keyFiguresRepository.keyFiguresWithDepartmentFlow
        }
        assertTrue(result.isEmpty())
    }
}
