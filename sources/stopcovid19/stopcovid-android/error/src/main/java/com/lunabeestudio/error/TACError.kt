/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2023/03/19 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.error

sealed class TACError(
    override val message: String,
    override val cause: Throwable?,
) : Exception(message, cause)
