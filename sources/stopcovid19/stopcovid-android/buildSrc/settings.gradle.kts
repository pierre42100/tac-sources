/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2022/5/17 - for the TOUS-ANTI-COVID project
 */

pluginManagement {
    plugins {
        id("de.fayard.refreshVersions") version "0.40.1"
    }
}

plugins {
    id("de.fayard.refreshVersions")
}
