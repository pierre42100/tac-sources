/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2023/03/15 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.common

enum class EnvConstant {
    Prod {
        override val configFilename: String = "config.json"
        override val dccCertificatesFilename: String = "dcc-certs.json"
        override val dccBaseUrl: String = "https://dcclight.tousanticovid.gouv.fr"
    }, ;

    abstract val configFilename: String
    abstract val dccCertificatesFilename: String
    abstract val dccBaseUrl: String
}
