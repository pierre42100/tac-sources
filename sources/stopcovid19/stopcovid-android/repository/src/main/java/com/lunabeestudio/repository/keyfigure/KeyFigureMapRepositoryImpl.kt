/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2023/03/08 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.repository.keyfigure

import com.lunabeestudio.domain.model.KeyFigureMap
import com.lunabeestudio.domain.model.TacResult
import com.lunabeestudio.domain.repository.KeyFigureMapRepository
import com.lunabeestudio.error.AppError
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class KeyFigureMapRepositoryImpl @Inject constructor(
    private val keyFigureMapRemoteDatasource: KeyFigureMapRemoteDataSource,
    private val keyFigureMapLocalDatasource: KeyFigureMapLocalDataSource,
) : KeyFigureMapRepository {
    override suspend fun initialize() {
        if (keyFigureMapLocalDatasource.count() == 0) {
            keyFigureMapLocalDatasource.loadFromAsset()
        }
    }

    override suspend fun reload() {
        keyFigureMapLocalDatasource.loadFromAsset()
    }

    // Restore the favorites, remove the old keyfigures and update data
    override suspend fun fetchNewData() {
        val result = keyFigureMapRemoteDatasource.fetchKeyFigureMap()

        if (result is TacResult.Success && result.successData != null) {
            result.successData?.let {
                keyFigureMapLocalDatasource.insertNewAndDeleteOld(it)
            }
        }
    }

    override fun availableKeyFiguresFlow(): Flow<List<String>> = keyFigureMapLocalDatasource.availableKeyFigureMap()

    override suspend fun keyFigureMapByLabel(label: String): TacResult<KeyFigureMap> {
        val result = keyFigureMapLocalDatasource.keyFigureMapByLabel(label)
        return if (result != null) {
            TacResult.Success(result)
        } else {
            TacResult.Failure(AppError(AppError.Code.KEY_FIGURES_NOT_AVAILABLE))
        }
    }
}
