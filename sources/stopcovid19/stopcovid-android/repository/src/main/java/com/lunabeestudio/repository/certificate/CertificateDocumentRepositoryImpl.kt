/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2023/03/15 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.repository.certificate

import com.lunabeestudio.domain.model.DownloadStatus
import com.lunabeestudio.domain.repository.CertificateDocumentRepository
import com.lunabeestudio.repository.file.FileLocalDataSource
import com.lunabeestudio.repository.file.FileRemoteToLocalDataSource
import com.lunabeestudio.repository.file.FileRemoteToLocalRepositoryImpl
import java.io.File
import javax.inject.Inject
import kotlin.time.Duration

abstract class ImageDocumentRepositoryImpl(
    private val imageDocumentLocalDataSource: ImageDocumentLocalDataSource,
    fileLocalDataSource: FileLocalDataSource,
    fileRemoteToLocalDataSource: FileRemoteToLocalDataSource,
) : FileRemoteToLocalRepositoryImpl(
    fileLocalDataSource,
    fileRemoteToLocalDataSource,
) {
    open val remoteFileUrlTemplate: String = ""
    override val mimeType: String = "image/png"

    override fun getRemoteFileUrl(): String = remoteFileUrlTemplate.format(
        imageDocumentLocalDataSource.getApplicationLanguage(),
    )
    final override fun getAssetFilePath(): String? = null

    final override suspend fun fileNotCorrupted(file: File): Boolean {
        return imageDocumentLocalDataSource.fileNotCorrupted(file)
    }

    suspend fun fetchLastImage(): DownloadStatus {
        return super.fetchLast()
    }
}

class CertificateDocumentRepositoryImpl @Inject constructor(
    private val certificateDocumentLocalDataSource: CertificateDocumentLocalDataSource,
) : CertificateDocumentRepository {
    override suspend fun onAppForeground(delay: Duration) {
        certificateDocumentLocalDataSource.fetchLastImages(delay)
    }
}
