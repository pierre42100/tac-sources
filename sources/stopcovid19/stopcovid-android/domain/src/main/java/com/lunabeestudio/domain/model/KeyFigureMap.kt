/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2022/12/21 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.domain.model

data class KeyFigureMap(
    val labelKey: String,
    val version: String,
    val valuesDepartments: List<DepartmentKeyFigureMap>?,
)

data class DepartmentKeyFigureMap(
    val dptNb: String,
    val dptLabel: String,
    val series: List<KeyFigureSeriesItem>?,
)
