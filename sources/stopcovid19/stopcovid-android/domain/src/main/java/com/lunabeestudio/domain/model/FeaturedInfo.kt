/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2023/01/03 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.domain.model

data class FeaturedInfo(
    val index: Int,
    val thumbnail: String,
    val hd: String,
    val hint: String?,
    val url: String?,
    val title: String,
    val hdHeight: Int? = null,
    val hdWidth: Int? = null,
)
