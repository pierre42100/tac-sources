/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2022/3/2 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.local.blacklist

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Transaction

@Dao
interface BlacklistPrefixDao {
    @Transaction
    fun updatePrefixes(prefixes: List<BlacklistPrefixRoom>) {
        deleteAll()
        insert(prefixes)
    }

    @Query("DELETE FROM BlacklistPrefixRoom")
    fun deleteAll()

    @Insert
    fun insert(prefixes: List<BlacklistPrefixRoom>)

    @Query("SELECT prefix FROM BlacklistPrefixRoom WHERE :prefix LIKE prefix || '%' LIMIT 1")
    fun getMatchPrefix(
        prefix: String,
    ): String?
}
