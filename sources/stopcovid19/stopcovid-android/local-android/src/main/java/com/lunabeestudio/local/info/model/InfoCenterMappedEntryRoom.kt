/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2023/03/07 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.local.info.model

import com.lunabeestudio.domain.model.InfoCenterData

data class InfoCenterMappedEntryRoom(
    val id: String,
    val title: String,
    val description: String,
    val buttonLabel: String?,
    val url: String?,
    val timestamp: Long,
) {
    fun toInfoCenterEntry(): InfoCenterData.InfoCenterEntry {
        return InfoCenterData.InfoCenterEntry(
            id = this.id.hashCode().toLong(),
            title = this.title,
            description = this.description,
            buttonLabel = this.buttonLabel,
            url = this.url,
            timestamp = this.timestamp,
            tags = null,
        )
    }
}
