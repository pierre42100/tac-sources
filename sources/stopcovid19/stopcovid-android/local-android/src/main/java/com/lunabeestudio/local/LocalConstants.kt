/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2023/03/07 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.local

object LocalConstants {
    object RoomColumnName {
        const val LabelAndDptNb: String = "labelAndDptNb"
        const val LabelKey: String = "labelKey"
        const val LabelKeyFigure: String = "labelKeyFigure"
    }

    object SharedPrefs {
        const val ZIP_GEOLOC_VERSION: String = "Zip.Geoloc.Version"
        const val CURRENT_VACCINATION_REFERENCE_LATITUDE: String =
            "currentVaccinationReferenceLatitude"
        const val CURRENT_VACCINATION_REFERENCE_LONGITUDE: String =
            "currentVaccinationReferenceLongitude"
        const val CURRENT_VACCINATION_REFERENCE_DEPARTMENT_CODE: String =
            "currentVaccinationReferenceDepartmentCode"
        const val LastCertificatesDocumentsManagerSuccessSync: String = "Last.Certificates.Documents.Manager.Success.Sync"
    }
}
