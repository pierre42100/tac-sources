/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2023/03/07 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.local.extension

import com.lunabeestudio.domain.model.FeaturedInfo
import com.lunabeestudio.local.featuredinfo.FeaturedInfoRoom

internal fun FeaturedInfoRoom.toFeaturedInfo(): FeaturedInfo = FeaturedInfo(
    index,
    thumbnail,
    hd,
    hint,
    url,
    title,
    hdHeight,
    hdWidth,
)

internal fun FeaturedInfo.toFeaturedInfoRoom(): FeaturedInfoRoom = FeaturedInfoRoom(
    index,
    thumbnail,
    hd,
    hint,
    url,
    title,
    hdHeight,
    hdWidth,
)
