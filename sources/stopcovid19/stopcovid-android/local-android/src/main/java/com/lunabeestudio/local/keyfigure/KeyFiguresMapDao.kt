/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2023/03/08 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.local.keyfigure

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Transaction
import com.lunabeestudio.local.keyfigure.model.DepartmentKeyFigureMapAndSerieRoom
import com.lunabeestudio.local.keyfigure.model.DepartmentKeyFigureMapRoom
import com.lunabeestudio.local.keyfigure.model.KeyFigureMapSerieItemRoom
import kotlinx.coroutines.flow.Flow
import timber.log.Timber

@Dao
interface KeyFiguresMapDao {

    @Query("DELETE FROM DepartmentKeyFigureMapRoom")
    fun deleteDepartmentKeyFigure()

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertDepartmentKeyFigureMapRoom(departmentKeyFigureMapRoom: DepartmentKeyFigureMapRoom)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertSeries(series: List<KeyFigureMapSerieItemRoom>)

    @Transaction
    fun insertNewAndDeleteOld(
        new: List<DepartmentKeyFigureMapRoom>,
    ) {
        Timber.d(new.first().toString())
        deleteDepartmentKeyFigure()
        insertKeyFiguresMap(new)
    }

    @Transaction
    private fun insertKeyFiguresMap(keyFigures: List<DepartmentKeyFigureMapRoom>) {
        keyFigures.forEach {
            insertDepartmentKeyFigureMapRoom(it)
            it.series?.let { series -> insertSeries(series) }
        }
    }

    @Query("SELECT * FROM DepartmentKeyFigureMapRoom WHERE labelKey = :label")
    suspend fun getDepartmentKeyFigureMapRoomByLabel(label: String): List<DepartmentKeyFigureMapAndSerieRoom>

    @Query("SELECT * FROM DepartmentKeyFigureMapRoom")
    fun getFlowDepartmentKeyFigureMapRoom(): Flow<List<DepartmentKeyFigureMapRoom>>

    @Query("SELECT COUNT(labelAndDptNb) FROM DepartmentKeyFigureMapRoom")
    fun getDepartmentKeyFigureMapCount(): Int
}
