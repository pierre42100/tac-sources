/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2023/03/15 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.local.eutags

import com.google.gson.reflect.TypeToken
import com.lunabeestudio.common.ConfigConstant
import com.lunabeestudio.repository.eutags.EUTagsLocalDataSource
import java.lang.reflect.Type
import javax.inject.Inject

class EUTagsLocalDataSourceImpl @Inject constructor() : EUTagsLocalDataSource {
    override val type: Type = object : TypeToken<List<String>>() {}.type

    override fun getLocalFileName(): String {
        return ConfigConstant.EUTags.FILENAME
    }

    override fun getRemoteFileUrl(): String {
        return ConfigConstant.EUTags.URL
    }

    override fun getAssetFilePath(): String {
        return ConfigConstant.EUTags.ASSET_FILE_PATH
    }
}
