/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2023/03/07 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.local.keyfigure.model

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey
import com.lunabeestudio.local.LocalConstants

@Entity(
    foreignKeys = [
        ForeignKey(
            entity = KeyFigureRoom::class,
            parentColumns = [LocalConstants.RoomColumnName.LabelKey],
            childColumns = [LocalConstants.RoomColumnName.LabelKeyFigure],
            onDelete = ForeignKey.CASCADE,
        ),
        ForeignKey(
            entity = DepartmentKeyFigureRoom::class,
            parentColumns = [LocalConstants.RoomColumnName.LabelAndDptNb],
            childColumns = [LocalConstants.RoomColumnName.LabelAndDptNb],
            onDelete = ForeignKey.CASCADE,
        ),
    ],
)
data class KeyFigureSeriesItemRoom(
    val date: Long,
    val value: Double,
    var serieType: KeyFigureSeriesTypeRoom,
    var labelKeyFigure: String? = null,
    var labelAndDptNb: String? = null,
    @PrimaryKey(autoGenerate = true) val id: Int = 0,
)
