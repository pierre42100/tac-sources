/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2023/03/07 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.local.extension

import com.lunabeestudio.domain.model.DepartmentKeyFigure
import com.lunabeestudio.local.keyfigure.model.DepartmentKeyFigureRoom
import com.lunabeestudio.local.keyfigure.model.KeyFigureSeriesItemRoom
import com.lunabeestudio.local.keyfigure.model.KeyFigureSeriesTypeRoom

internal fun DepartmentKeyFigure.toDepartmentKeyFigureRoom(labelKeyFigure: String) = DepartmentKeyFigureRoom(
    labelKeyFigure,
    dptNb,
    dptLabel,
    extractDateS,
    value.toDouble(),
    valueToDisplay,
    dptLabel + labelKeyFigure,
    series?.map {
        it.toKeyFigureSeriesItemRoom(
            null,
            dptLabel + labelKeyFigure,
            KeyFigureSeriesTypeRoom.DEPARTMENT,
        )
    },
)

internal fun DepartmentKeyFigureRoom.toDepartmentKeyFigure(series: List<KeyFigureSeriesItemRoom>?) = DepartmentKeyFigure(
    dptNb,
    dptLabel,
    extractDateS,
    value,
    valueToDisplay,
    series?.map(KeyFigureSeriesItemRoom::toKeyFigureSeriesItem),
)
