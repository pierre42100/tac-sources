/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2023/03/07 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.local.keyfigure

import android.content.SharedPreferences
import com.lunabeestudio.common.ConfigConstant
import com.lunabeestudio.common.extension.chosenPostalCode
import com.lunabeestudio.domain.model.DepartmentKeyFigure
import com.lunabeestudio.domain.model.KeyFigure
import com.lunabeestudio.local.extension.toDepartmentKeyFigure
import com.lunabeestudio.local.extension.toDepartmentKeyFigureRoom
import com.lunabeestudio.local.extension.toKeyFigure
import com.lunabeestudio.local.extension.toKeyFigureRoom
import com.lunabeestudio.local.keyfigure.model.DepartmentKeyFigureRoom
import com.lunabeestudio.local.keyfigure.model.KeyFigureFavoriteRoom
import com.lunabeestudio.repository.keyfigure.KeyFigureLocalDataSource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.withContext
import javax.inject.Inject

class KeyFigureLocalDataSourceImpl @Inject constructor(
    private val keyFiguresDao: KeyFiguresDao,
    private val sharedPreferences: SharedPreferences,
) : KeyFigureLocalDataSource {
    override fun flowAllKeyFiguresWithFavorites(): Flow<List<KeyFigure>> {
        return keyFiguresDao.getAllKeyFigureAndFavoriteFlow().map {
            it.map { figureRoom ->
                figureRoom.keyFigure.toKeyFigure(
                    figureRoom.favoriteRoom?.isFavorite,
                    figureRoom.favoriteRoom?.favoriteOrder,
                    figureRoom.series,
                )
            }
        }
    }

    override fun flowKeyFigureByLabel(label: String): Flow<KeyFigure?> {
        return keyFiguresDao.getFlowKeyFigureAndFavoritesByLabel(label).map {
            it?.keyFigure?.toKeyFigure(
                it.favoriteRoom?.isFavorite,
                it.favoriteRoom?.favoriteOrder,
                it.series,
            )
        }
    }

    override suspend fun keyFigureByLabel(label: String): KeyFigure? {
        return withContext(Dispatchers.IO) {
            keyFiguresDao.getKeyFiguresAndFavoriteByLabel(label).let {
                it?.keyFigure?.toKeyFigure(
                    it.favoriteRoom?.isFavorite,
                    it.favoriteRoom?.favoriteOrder,
                    it.series,
                )
            }
        }
    }

    override suspend fun allKeyFigures(): List<KeyFigure> {
        return withContext(Dispatchers.IO) {
            keyFiguresDao.getAllKeyFiguresAndFavorite().let {
                it.map { figureRoom ->
                    figureRoom.keyFigure.toKeyFigure(
                        figureRoom.favoriteRoom?.isFavorite,
                        figureRoom.favoriteRoom?.favoriteOrder,
                        figureRoom.series,
                    )
                }
            }
        }
    }

    override suspend fun setFavorite(labelKey: String, isFavorite: Boolean, favoriteOrder: Float) {
        withContext(Dispatchers.IO) {
            keyFiguresDao.insertFavorite(KeyFigureFavoriteRoom(labelKey, isFavorite, favoriteOrder))
        }
    }

    override suspend fun deleteFavorites() {
        withContext(Dispatchers.IO) {
            keyFiguresDao.deleteAllFavorites()
        }
    }

    override suspend fun insertNewAndDeleteOld(old: List<KeyFigure>, new: List<KeyFigure>) {
        withContext(Dispatchers.IO) {
            val departmentsList = mutableListOf<DepartmentKeyFigureRoom>()
            new.forEach { figure ->
                figure.valuesDepartments?.map {
                    it.toDepartmentKeyFigureRoom(figure.labelKey)
                }?.let { departmentsList.addAll(it) }
            }
            keyFiguresDao.insertNewAndDeleteOld(
                old.map(KeyFigure::toKeyFigureRoom),
                new.map(KeyFigure::toKeyFigureRoom),
                departmentsList,
            )
        }
    }

    override suspend fun getDepartment(label: String, dptNb: String): DepartmentKeyFigure? {
        return withContext(Dispatchers.IO) {
            keyFiguresDao.getDepartmentByKeyFigureAndNumber(label, dptNb)?.let {
                it.department.toDepartmentKeyFigure(it.series)
            }
        }
    }

    override val chosenPostalCode: String?
        get() = sharedPreferences.chosenPostalCode

    override val nationalSuffix: String = ConfigConstant.KeyFigures.NATIONAL_SUFFIX
}
