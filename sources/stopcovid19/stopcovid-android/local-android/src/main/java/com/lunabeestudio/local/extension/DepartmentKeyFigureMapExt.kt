/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2023/03/07 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.local.extension

import com.lunabeestudio.domain.model.DepartmentKeyFigureMap
import com.lunabeestudio.domain.model.KeyFigureMap
import com.lunabeestudio.local.keyfigure.model.DepartmentKeyFigureMapRoom
import com.lunabeestudio.local.keyfigure.model.KeyFigureMapSerieItemRoom

internal fun KeyFigureMap.toDepartmentKeyFigureMapRoomList(): List<DepartmentKeyFigureMapRoom> {
    return this.valuesDepartments?.map { (dptNb, dptLabel, series) ->
        DepartmentKeyFigureMapRoom(
            labelKey = this.labelKey,
            dptLabel = dptLabel,
            dptNb = dptNb,
            labelAndDptNb = "$labelKey$dptNb",
            series = series?.map {
                it.toKeyFigureMapSeriesItemRoom(
                    labelAndDptNb = "$labelKey$dptNb",
                )
            },
        )
    } ?: listOf()
}

internal fun DepartmentKeyFigureMapRoom.toDepartmentKeyFigureMap(series: List<KeyFigureMapSerieItemRoom>) = DepartmentKeyFigureMap(
    dptNb = dptNb,
    dptLabel = dptLabel,
    series = series.map(com.lunabeestudio.local.keyfigure.model.KeyFigureMapSerieItemRoom::toKeyFigureSeriesItem),
)
