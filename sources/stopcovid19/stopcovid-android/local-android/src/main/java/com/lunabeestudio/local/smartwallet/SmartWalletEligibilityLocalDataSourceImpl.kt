/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2023/03/15 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.local.smartwallet

import com.google.gson.reflect.TypeToken
import com.lunabeestudio.common.ConfigConstant
import com.lunabeestudio.repository.smartwallet.SmartWalletEligibilityLocalDataSource
import com.lunabeestudio.repository.smartwallet.model.ApiSmartWalletEligibilityPivot
import java.lang.reflect.Type
import javax.inject.Inject

class SmartWalletEligibilityLocalDataSourceImpl @Inject constructor() : SmartWalletEligibilityLocalDataSource {
    override val type: Type
        get() = object : TypeToken<List<ApiSmartWalletEligibilityPivot>>() {}.type

    override fun getLocalFileName(): String {
        return ConfigConstant.SmartWallet.ELIGIBILITY_FILENAME
    }

    override fun getRemoteFileUrl(): String {
        return ConfigConstant.SmartWallet.URL + getLocalFileName()
    }

    override fun getAssetFilePath(): String {
        return ConfigConstant.SmartWallet.FOLDER + getLocalFileName()
    }
}
