/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2023/03/07 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.local.keyfigure.model

import androidx.room.Embedded
import androidx.room.Relation
import com.lunabeestudio.local.LocalConstants

data class DepartmentKeyFigureAndSeriesRoom(
    @Embedded val department: DepartmentKeyFigureRoom,
    @Relation(
        parentColumn = LocalConstants.RoomColumnName.LabelAndDptNb,
        entityColumn = LocalConstants.RoomColumnName.LabelAndDptNb,
    )
    val series: List<KeyFigureSeriesItemRoom>?,
)
