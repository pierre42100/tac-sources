# Personal data

Version française: https://gitlab.inria.fr/stopcovid19/accueil/-/blob/master/CGU.md#application-tousanticovid

## TousAntiCovid application
The TousAntiCovid application is part of a comprehensive strategy to fight the Covid-19 epidemic. This application is not mandatory, its use is on a voluntary basis.

## Data controller and purpose
The development coordination of which has been initially given to Inria (Institut national de recherche en sciences et technologies du numérique), which is under the responsibility of the Direction générale de la santé du ministère de la Santé et de la Prévention.

The data will be used in order to:

1° Raising awareness of application users on the symptoms of this virus, barrier gestures and what to do to fight against the spread of the virus;

2° Obtaining health information related to Covid-19 with the possibility of filtering by place of interest;

3° Keeping certificates of vaccination and negative/positive tests for Covid-19, of recovery from Covid-19 and exemption to vaccination;

4° To inform users of their eligibility for the vaccination booster and the expiry of their health certificate by sending them targeted messages on the basis of the certificates present in the Wallet;

5° To notify users in the event of invalidation of their health certificate following the observation of fraudulent use of the health certificate by a third party, without the user's knowledge or not;

## Legal basis and regulatory nature of the processing
This processing and its statistics are part of the public interest missions (article 6.1.e of the GDPR) entrusted to the Direction générale de la santé in the fight against health threats.

It also benefits from a regulatory framework – decree of July 30, 2022 modifying decree n ° 2020-650 of May 29, 2020 relating to the processing of data called “StopCovid” (renamed since TousAntiCovid) taken after advice from the CNIL.

## Personal data processed in the TousAntiCovid application
To obtain health information related to Covid-19 relating to a place of interest, the information entered is processed locally, on the user's phone, and is not subject to any transfer.

For the storage of the vaccination certificate, negative / positive tests for Covid-19, recovery from Covid-19, exemption to vaccination, the information is processed locally, on the user's phone, and are not subject to any transfer.

For the notification of eligibility for the vaccine booster or the expiration of the health certificate, the information is processed locally, on the user's phone, and does not make the subject to no transfer.

To fight against fraudulent use of health certificates, information is processed locally, on the user's phone, and is not subject to any transfer. This local processing is based on the comparison of a hash of the certificate with those of a reference database hosted by Orange Business Services, previously downloaded in the mobile application.

### Using the camera
The application may ask the user to authorize the use of his/her mobile phone's camera, when the user wishes to scan a QR Code within the TousAntiCovid application. This authorization will materialize when the user wants to use the QR Code scanning functionality for the first time within the TousAntiCovid application by displaying a dialog box asking the user for authorization to "take photos or videos". No personal data (photo or video belonging to the user) is stored or transmitted by TousAntiCovid, only the content of the QR Code is used as part of the application.

## Data recipient
For obtaining health information on news related to Covid-19 relating to a place of interest:
* Application users

For the storage of vaccination certificates, negative / positive tests for Covid-19, recovery from Covid-19 or exemption to vaccination:
* Application users

When displaying the certificates in the Wallet module:
* Public authorities and persons empowered, in accordance with the decree, to carry out health certificate checks

To fight against the fraudulent use of health certificates:
* IN Groupe as a subcontractor of the Direction générale de la santé du ministère de la Santé et de la Prévention and responsible for providing the TousAntiCovid application, TAC Verif or third party device, the reference base of validated fraudulent health certificates by the Direction générale de la santé

## Retention period
For obtaining health information related to Covid-19 relating to a place of interest: the postal code is not recorded on the central server, but only in the user's application. It is retained until modified or deleted by the user.

For the storage of vaccination certificates, recovery certificates or negative / positive tests for Covid-19: certificates can be deleted by the user at any time in the application.

## Legal notices
Directeur de la publication
Professeur Jérôme SALOMON
Ministère de la Santé et de la Prévention
Direction Générale de la Santé (DGS)
14 avenue Duquesne 75350 Paris 07 SP

## Publisher
Ministère de la Santé et de la Prévention
Direction Générale de la Santé (DGS)
14 avenue Duquesne 75350 Paris 07 SP

## Hosting
* Orange Business Services
* 1 Place des Droits de l'Homme
93210 Saint-Denis
FRANCE

## Exercising your rights
Each user can, at any time, delete data on the mobile application himself, in the application settings.

The user can declare, at any time, the fraudulent use of his health certificate by e-mail by writing to revocation-passe-sanitaire@sante.gouv.fr and recover, after confirmation of his/her revocation, his/her new certificate on the site https://sidep.gouv.fr or https://attestation-vaccin.ameli.fr

The user can at any time via the "settings" of the application deactivate the Smart Wallet allowing them to receive targeted health recommendations, in particular on the eligibility for the vaccination booster and the expiration of certificates.

For any questions about the processing of your personal data, you can consult either the website https://sante.gouv.fr/soins-et-maladies/maladies/maladies-infectieuses/coronavirus/tout-savoir-sur-le-covid-19/article/tousanticovid or by email: tousanticovid-rgpd@sante.gouv.fr or by mail: ministère de la Santé et de la Prévention - Data Protection Officer - Direction générale de la santé - 14 Avenue Duquesne 75350 Paris 07 SP.

If you believe that your data is being processed in a way that does not comply with data protection law, you may submit a complaint to the CNIL (France’s data protection authority) https://www.cnil.fr/fr/plaintes/ CNIL - Service des plaintes - 3 place Fontenoy - TSA 80715 - 75334 PARIS CEDEX 07