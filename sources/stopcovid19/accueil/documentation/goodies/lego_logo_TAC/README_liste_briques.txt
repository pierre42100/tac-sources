https://www.lego.com/fr-fr/page/static/pick-a-brick?query=PLATE&page=1

LEGO Baseplate 48 x 48
ID: 4176640 ou 6030674 (4186)
Qté: 1
(à chercher chez d'autres revendeurs que Lego)

PLATE 1X6 bleue
ID: 366623/3666
Qté: 9

PLATE 1X4 bleue
ID: 371023/3710
Qté: 2

PLATE 2X3 bleue
ID: 302123/3021
Qté: 3

PLATE 1X3 bleue
ID: 362323/3623
Qté: 17

PLATE 2X2 bleue
ID: 302223/3022
Qté: 4

CORNER PLATE 1X2X2 bleue
ID: 242023/2420
Qté: 37

PLATE 1X2 bleue
ID: 302323/3023
Qté: 35

PLATE 1X1 bleue
ID: 302423/3024
Qté: 28

PLATE 1X6 rouge
ID: 366621/3666
Qté: 4

PLATE 1X4 rouge
ID: 371021/3710
Qté: 5

PLATE 2X3 rouge
ID: 302121/3021
Qté: 2 

PLATE 1X3 rouge
ID: 362321/3623
Qté: 18

PLATE 2X2 rouge
ID: 302221/3022
Qté: 3

CORNER PLATE 1X2X2 rouge
ID: 242021/2420
Qté: 45

PLATE 1X2 rouge
ID: 302321/3023
Qté: 35

PLATE 1X1 rouge
ID: 302421/3024
Qté: 13