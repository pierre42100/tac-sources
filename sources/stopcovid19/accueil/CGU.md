# Données personnelles

English version: https://gitlab.inria.fr/stopcovid19/accueil/-/blob/master/CGU-en.md#personal-data

## Application "TousAntiCovid"
L’application TousAntiCovid s’inscrit dans le cadre d’une stratégie globale de lutte contre l’épidémie de Covid-19. Cette application n’a aucun caractère obligatoire, son utilisation s’effectue sur la base du volontariat.

## Finalités et responsable de traitement
Cette application dont la coordination du développement a été initialement confiée à Inria (Institut national de recherche en sciences et technologies du numérique) est placée sous la responsabilité de la Direction générale de la santé du ministère de la Santé et de la Prévention.

Ce traitement a pour objets :

1° La sensibilisation des utilisateurs de l’application sur les symptômes de ce virus, les gestes barrières et la conduite à tenir pour lutter contre la propagation du virus ;

2° L’obtention d’informations sanitaires en lien avec la Covid-19 avec possibilité de filtrage par lieu d’intérêt ;

3° La conservation des certificats de vaccination et de tests négatifs/positifs à la Covid-19, de rétablissement à la Covid-19 et de contre-indication à la vaccination ;

4° D’informer les utilisateurs de l’application de leur éligibilité au rappel vaccinal et de l’expiration de leur certificat sanitaire en leur adressant des messages ciblés sur la base des certificats présents dans le Carnet ;

5° De notifier les utilisateurs en cas d’invalidation de leur certificat sanitaire à la suite de la constatation d’un usage frauduleux du certificat sanitaire par une tierce personne, à l’insu ou non, de l’utilisateur ;

## Base légale et caractère réglementaire du traitement
Ce traitement et ses statistiques s’inscrivent dans le cadre des missions d’intérêt public (article 6.1.e du RGPD) confiées à la Direction générale de la santé en matière de lutte contre les menaces sanitaires.

Il bénéficie également d’un encadrement réglementaire – décret du 30 juillet 2022 modifiant le décret n° 2020-650 du 29 mai 2020 relatif au traitement de données dénommé « StopCovid » (renommé depuis TousAntiCovid) pris après avis de la CNIL.

## Données personnelles traitées dans l’application TousAntiCovid
Pour l’obtention des informations sanitaires en lien avec la Covid-19 relativement à un lieu d’intérêt, l’information saisie est traitée localement, sur le téléphone de l’utilisateur, et ne fait l’objet d’aucun transfert.

Pour la conservation du certificat de vaccination, de tests négatifs/positifs à la Covid-19, de rétablissement à la Covid-19, de contre-indication à la vaccination, les informations sont traitées localement, sur le téléphone de l’utilisateur, et ne font l’objet d’aucun transfert.

Pour la notification à l’éligibilité au rappel vaccinal ou à l’expiration du certificat sanitaire (dans le cadre du certificat sanitaire de voyage), les informations sont traitées localement, sur le téléphone de l’utilisateur, et ne font l’objet d’aucun transfert.

Pour la lutte contre l’utilisation frauduleuse des certificats sanitaires, les informations sont traitées localement, sur le téléphone de l’utilisateur, et ne font l’objet d’aucun transfert. Ce traitement local repose sur la confrontation d'une empreinte (hash) du certificat à ceux d’une base de référence hébergée par Orange Business Services, préalablement téléchargée dans l’application mobile.

### Utilisation de la caméra
L'application peut être amenée à demander à l'utilisateur d’autoriser l’usage de la caméra de son téléphone mobile, dès lors que l'utilisateur souhaite scanner un QR Code au sein de l’application TousAntiCovid. Cette autorisation se matérialisera lorsque l'utilisateur souhaitera utiliser pour la première fois cette fonctionnalité de lecture de QR Codes au sein de l’application TousAntiCovid en affichant une boîte de dialogue lui demandant l’autorisation de « prendre des photos ou des vidéos ». Aucune donnée à caractère personnel (photo ou vidéo lui appartenant) n’est stockée ou transmise par TousAntiCovid, seul le contenu du QR Code est utilisé dans le cadre de l’application.

## Destinataire des données
Pour l’obtention des informations sanitaires sur les actualités en lien avec la Covid-19 relativement à un lieu d’intérêt :
* Les utilisateurs de l’application

Pour la conservation des certificats de vaccination, de tests négatifs/positifs à la Covid-19, de rétablissement à la Covid-19 ou de contre-indication à la vaccination :
* Les utilisateurs de l’application

Lors de l’affichage des certificats figurant dans le module Carnet :
* Les autorités publiques et les personnes habilitées, conformément au décret, à procéder à des contrôles du certificat sanitaire

Pour la lutte contre l’utilisation frauduleuse des certificats sanitaires :
* IN Groupe en tant que sous-traitant de la Direction générale de la santé du ministère de la Santé et de la Prévention et chargé de mettre à disposition de l’application TousAntiCovid, de TAC Verif ou dispositif tiers, la base de référence des certificats sanitaires frauduleux validée par la Direction générale de la santé

## Durée de conservation
Pour l’obtention des informations sanitaires en lien avec la Covid-19 relativement à un lieu d’intérêt : le code postal n’est pas enregistré sur le serveur central, mais uniquement dans l'application de l’utilisateur. Il est conservé jusqu’à modification ou effacement par l’utilisateur.

Pour la conservation des certificats de vaccination et de tests négatifs/positifs ou de rétablissement à la Covid-19 : les certificats peuvent être supprimés par l’utilisateur à tout moment dans l’application.

## Mentions légales
Directeur de la publication
Professeur Jérôme SALOMON
Ministère de la Santé et de la Prévention
Direction Générale de la Santé (DGS)
14 avenue Duquesne 75350 Paris 07 SP

## Éditeur
Ministère de la Santé et de la Prévention
Direction Générale de la Santé (DGS)
14 avenue Duquesne 75350 Paris 07 SP

## Hébergement
* Orange Business Services
* 1 Place des Droits de l'Homme
93210 Saint-Denis
FRANCE

## Exercice des droits
Chaque utilisateur peut, à tout moment, exercer son droit d’effacement des données présentes sur l’application mobile via les Paramètres de l’application TousAntiCovid.

L’utilisateur peut déclarer, à tout moment, l’usage frauduleux de son certificat sanitaire par e-mail en écrivant à revocation-passe-sanitaire@sante.gouv.fr et récupérer, après confirmation de sa révocation, son nouveau certificat sur le site https://sidep.gouv.fr ou https://attestation-vaccin.ameli.fr

L’utilisateur peut à tout moment via les « paramètres » de l’application désactiver le Carnet intelligent permettant de recevoir des recommandations sanitaires ciblées notamment sur l’éligibilité au rappel vaccinal et l’expiration des certificats.

Pour toute question sur le traitement de vos données à caractère personnel dans ce dispositif, vous pouvez consulter le site https://sante.gouv.fr/soins-et-maladies/maladies/maladies-infectieuses/coronavirus/tout-savoir-sur-le-covid-19/article/tousanticovid ou vous adresser au ministère de la santé par e-mail : tousanticovid-rgpd@sante.gouv.fr ou par courrier postal : ministère de la Santé et de la Prévention – Référent en protection des données - Direction générale de la santé - 14, avenue Duquesne 75350 PARIS 07 SP.

Si vous estimez que le traitement n’est pas conforme aux règles de protection des données, vous pouvez adresser une réclamation à la CNIL https://www.cnil.fr/fr/plaintes/ CNIL - Service des plaintes - 3 place Fontenoy - TSA 80715 - 75334 PARIS CEDEX 07
