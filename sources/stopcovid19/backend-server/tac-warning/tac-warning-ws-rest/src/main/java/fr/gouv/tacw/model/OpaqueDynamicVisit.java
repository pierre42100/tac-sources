package fr.gouv.tacw.model;

public class OpaqueDynamicVisit extends OpaqueVisit {
	public OpaqueDynamicVisit(String payload, long visitTime) {
		super(payload, visitTime);
	}
}
