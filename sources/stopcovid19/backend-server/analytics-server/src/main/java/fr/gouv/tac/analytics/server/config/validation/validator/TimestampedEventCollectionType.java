package fr.gouv.tac.analytics.server.config.validation.validator;

public enum TimestampedEventCollectionType {
    EVENT,
    ERROR
}
