package fr.gouv.tac.analytics.server.utils;

public interface UriConstants {

	String ANALYTICS = "/analytics";

	String API_V1 = "/v1";
}
