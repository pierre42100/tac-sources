package fr.gouv.clea.ws.utils;

public class UriConstants {

    public static final String REPORT = "/wreport";

    public static final String API_V1 = "/v1";
}
