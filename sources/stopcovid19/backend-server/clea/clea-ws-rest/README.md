Run the Clea WS ReST application:

```bash
mvn spring-boot:run
```

Display the Clea API and use it: http://localhost:8088/swagger-ui/

Display the Clea OpenAPI generated from the code: http://localhost:8088/v3/api-docs?group=clea