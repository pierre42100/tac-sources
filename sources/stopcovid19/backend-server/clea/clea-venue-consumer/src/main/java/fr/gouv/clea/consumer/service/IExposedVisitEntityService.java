package fr.gouv.clea.consumer.service;

public interface IExposedVisitEntityService {

    void deleteOutdatedExposedVisits();
}
