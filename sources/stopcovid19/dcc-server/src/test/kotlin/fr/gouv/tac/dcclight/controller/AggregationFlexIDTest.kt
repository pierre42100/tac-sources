package fr.gouv.tac.dcclight.controller

import com.spotify.hamcrest.jackson.IsJsonObject.jsonObject
import fr.gouv.tac.dcclight.api.model.DccAggregationRequest
import fr.gouv.tac.dcclight.test.DccSigningKeysManager.Companion.DCC_SERVER_KEYS
import fr.gouv.tac.dcclight.test.IntegrationTest
import fr.gouv.tac.dcclight.test.RestAssuredManager.Companion.When
import fr.gouv.tac.dcclight.test.RestAssuredManager.Companion.bodyEncrypted
import fr.gouv.tac.dcclight.test.RestAssuredManager.Companion.givenBaseHeaders
import fr.gouv.tac.dcclight.test.TransportEncryptionKeysManager.Companion.CLIENT_PUBLIC_KEY_BASE64
import fr.gouv.tac.dcclight.test.eu.DccCryptographer.Companion.encodeToDccAndEncrypt
import fr.gouv.tac.dcclight.test.extension.withFamilyName
import fr.gouv.tac.dcclight.test.extension.withFamilyNameTransliterated
import fr.gouv.tac.dcclight.test.extension.withGivenName
import fr.gouv.tac.dcclight.test.extension.withGivenNameTransliterated
import fr.gouv.tac.dcclight.test.matchers.isEuDcc
import fr.gouv.tac.dcclight.test.matchers.isHcertEncoded
import fr.gouv.tac.dcclight.test.matchers.matchesUvciPattern
import fr.gouv.tac.dcclight.test.matchers.where
import fr.gouv.tac.dcclight.test.pfizer1_1VaccinationCertificate
import fr.gouv.tac.dcclight.test.pfizer2_2VaccinationCertificate
import fr.gouv.tac.dcclight.test.today
import org.hamcrest.Matchers.equalTo
import org.junit.jupiter.api.Test
import org.springframework.http.HttpStatus
import org.springframework.http.HttpStatus.OK
import kotlin.time.Duration.Companion.days

@IntegrationTest
class AggregationFlexIDTest {

    @Test
    fun matches_when_familyName_does_not_match_but_flexId_lastNameTransliterated_does() {
        // target rule name: "Double vaccination"

        val vaccinationDcc = pfizer2_2VaccinationCertificate(today(minus = 100.days))
            .withFamilyName("Dupont Dos Santos")
            .withFamilyNameTransliterated("DUPONT<DOS<SANTOS")
            .withGivenName("Jérôme")
            .withGivenNameTransliterated("JEROME")

        val vaccination2Dcc = pfizer1_1VaccinationCertificate(today(minus = 39.days))
            .withFamilyName("Dupont")
            .withFamilyNameTransliterated("DUPONT")
            .withGivenName("Jérôme")
            .withGivenNameTransliterated("JEROME")

        givenBaseHeaders()
            .body(
                DccAggregationRequest(
                    publicKey = CLIENT_PUBLIC_KEY_BASE64,
                    certificates = listOf(
                        encodeToDccAndEncrypt(vaccinationDcc),
                        encodeToDccAndEncrypt(vaccination2Dcc)
                    ),
                )
            )

            .When()
            .post("/api/v1/aggregate")

            .then()
            .statusCode(OK.value())
            .bodyEncrypted(
                "response",
                jsonObject()
                    .where(
                        "certificate",
                        isHcertEncoded(
                            issuedBy = DCC_SERVER_KEYS,
                            euDcc = isEuDcc()
                                .forJeromeDupont()
                                .withProofType("v")
                                .where("dn", 3)
                                .where("ma", "ORG-100030215")
                                .where("vp", "1119349007")
                                .where("dt", today(minus = 39.days).toString())
                                .where("co", "FR")
                                .where("ci", matchesUvciPattern())
                                .where("mp", "EU/1/20/1528")
                                .where("is", "CNAM")
                                .where("sd", 3)
                                .where("tg", "840539006")
                        )
                    )
            )
    }

    @Test
    fun matches_when_givenName_does_not_match_but_flexId_givenNameTransliterated_does() {
        // target rule name: "Double vaccination"

        val vaccinationDcc = pfizer2_2VaccinationCertificate(today(minus = 100.days))
            .withFamilyName("Dupont")
            .withFamilyNameTransliterated("DUPONT")
            .withGivenName("Jean-Jérôme")
            .withGivenNameTransliterated("JEAN<JEROME")

        val vaccination2Dcc = pfizer1_1VaccinationCertificate(today(minus = 39.days))
            .withFamilyName("Dupont")
            .withFamilyNameTransliterated("DUPONT")
            .withGivenName("Jean")
            .withGivenNameTransliterated("JEAN")

        givenBaseHeaders()
            .body(
                DccAggregationRequest(
                    publicKey = CLIENT_PUBLIC_KEY_BASE64,
                    certificates = listOf(
                        encodeToDccAndEncrypt(vaccinationDcc),
                        encodeToDccAndEncrypt(vaccination2Dcc)
                    ),
                )
            )

            .When()
            .post("/api/v1/aggregate")

            .then()
            .statusCode(OK.value())
            .bodyEncrypted(
                "response",
                jsonObject()
                    .where(
                        "certificate",
                        isHcertEncoded(
                            issuedBy = DCC_SERVER_KEYS,
                            euDcc = isEuDcc()
                                .forJeromeDupontWith(
                                    gn = "Jean",
                                    gnt = "JEAN"
                                )
                                .withProofType("v")
                                .where("dn", 3)
                                .where("ma", "ORG-100030215")
                                .where("vp", "1119349007")
                                .where("dt", today(minus = 39.days).toString())
                                .where("co", "FR")
                                .where("ci", matchesUvciPattern())
                                .where("mp", "EU/1/20/1528")
                                .where("is", "CNAM")
                                .where("sd", 3)
                                .where("tg", "840539006")
                        )
                    )
            )
    }

    @Test
    fun matches_when_givenNameTransliterated_does_not_match_but_givenName_matches_exactly() {
        // target rule name: "Double vaccination"

        val vaccinationDcc = pfizer2_2VaccinationCertificate(today(minus = 100.days))
            .withFamilyName("Dupont")
            .withFamilyNameTransliterated("DUPONT")
            .withGivenName("Jean")
            .withGivenNameTransliterated("JEAN<JEROME")

        val vaccination2Dcc = pfizer1_1VaccinationCertificate(today(minus = 39.days))
            .withFamilyName("Dupont")
            .withFamilyNameTransliterated("DUPONT")
            .withGivenName("Jean")
            .withGivenNameTransliterated("JEAN<MARC")

        givenBaseHeaders()
            .body(
                DccAggregationRequest(
                    publicKey = CLIENT_PUBLIC_KEY_BASE64,
                    certificates = listOf(
                        encodeToDccAndEncrypt(vaccinationDcc),
                        encodeToDccAndEncrypt(vaccination2Dcc)
                    ),
                )
            )

            .When()
            .post("/api/v1/aggregate")

            .then()
            .statusCode(OK.value())
            .bodyEncrypted(
                "response",
                jsonObject()
                    .where(
                        "certificate",
                        isHcertEncoded(
                            issuedBy = DCC_SERVER_KEYS,
                            euDcc = isEuDcc()
                                .forJeromeDupontWith(
                                    gn = "Jean",
                                    gnt = "JEAN<MARC"
                                )
                                .withProofType("v")
                                .where("dn", 3)
                                .where("ma", "ORG-100030215")
                                .where("vp", "1119349007")
                                .where("dt", today(minus = 39.days).toString())
                                .where("co", "FR")
                                .where("ci", matchesUvciPattern())
                                .where("mp", "EU/1/20/1528")
                                .where("is", "CNAM")
                                .where("sd", 3)
                                .where("tg", "840539006")
                        )
                    )
            )
    }

    @Test
    fun matches_when_familyNameTransliterated_does_not_match_but_familyName_matches_exactly() {
        // target rule name: "Double vaccination"
        val vaccinationDcc = pfizer2_2VaccinationCertificate(today(minus = 100.days))
            .withFamilyName("Dupont")
            .withFamilyNameTransliterated("DU<PONT")
            .withGivenName("Jean")
            .withGivenNameTransliterated("JEAN")

        val vaccination2Dcc = pfizer1_1VaccinationCertificate(today(minus = 39.days))
            .withFamilyName("Dupont")
            .withFamilyNameTransliterated("DUPONT<DULAC")
            .withGivenName("Jean")
            .withGivenNameTransliterated("JEAN")

        givenBaseHeaders()
            .body(
                DccAggregationRequest(
                    publicKey = CLIENT_PUBLIC_KEY_BASE64,
                    certificates = listOf(
                        encodeToDccAndEncrypt(vaccinationDcc),
                        encodeToDccAndEncrypt(vaccination2Dcc)
                    ),
                )
            )

            .When()
            .post("/api/v1/aggregate")

            .then()
            .statusCode(OK.value())
            .bodyEncrypted(
                "response",
                jsonObject()
                    .where(
                        "certificate",
                        isHcertEncoded(
                            issuedBy = DCC_SERVER_KEYS,
                            euDcc = isEuDcc()
                                .forJeromeDupontWith(
                                    gn = "Jean",
                                    gnt = "JEAN",
                                    fn = "Dupont",
                                    fnt = "DUPONT<DULAC"
                                )
                                .withProofType("v")
                                .where("dn", 3)
                                .where("ma", "ORG-100030215")
                                .where("vp", "1119349007")
                                .where("dt", today(minus = 39.days).toString())
                                .where("co", "FR")
                                .where("ci", matchesUvciPattern())
                                .where("mp", "EU/1/20/1528")
                                .where("is", "CNAM")
                                .where("sd", 3)
                                .where("tg", "840539006")
                        )
                    )
            )
    }

    @Test
    fun does_not_match_when_givenName_does_not_match_and_givenNameTransliterated_does_not_match_flexid() {

        val vaccinationDcc = pfizer2_2VaccinationCertificate(today(minus = 100.days))
            .withFamilyName("Dupont")
            .withFamilyNameTransliterated("DUPONT")
            .withGivenName("Jean-Claude")
            .withGivenNameTransliterated("JEAN<CLAUDE")

        val vaccination2Dcc = pfizer1_1VaccinationCertificate(today(minus = 39.days))
            .withFamilyName("Dupont")
            .withFamilyNameTransliterated("DUPONT")
            .withGivenName("Jean-Marc")
            .withGivenNameTransliterated("JEAN<MARC")

        givenBaseHeaders()
            .body(
                DccAggregationRequest(
                    publicKey = CLIENT_PUBLIC_KEY_BASE64,
                    certificates = listOf(
                        encodeToDccAndEncrypt(vaccinationDcc),
                        encodeToDccAndEncrypt(vaccination2Dcc)
                    ),
                )
            )

            .When()
            .post("/api/v1/aggregate")

            .then()
            .statusCode(HttpStatus.BAD_REQUEST.value())
            .body("error", equalTo(HttpStatus.BAD_REQUEST.reasonPhrase))
            .body("message", equalTo("Request body contains invalid attributes"))
            .body("errors[0].field", equalTo("certificates[1]"))
            .body("errors[0].code", equalTo("INCONSISTENT_GIVEN_NAME"))
            .body("errors[0].message", equalTo("Given name does not match reference certificate given name"))
            .body("errors[1].field", equalTo("certificates"))
            .body("errors[1].code", equalTo("FLEX_ID_MISMATCH"))
            .body("errors[1].message", equalTo("Flex id condition did not match"))
    }

    @Test
    fun does_not_match_when_familyName_does_not_match_and_familyNameTransliterated_does_not_match_flexid() {

        val vaccinationDcc = pfizer2_2VaccinationCertificate(today(minus = 100.days))
            .withFamilyName("Dupont Dos Santos")
            .withFamilyNameTransliterated("DUPONT<DOS<SANTOS")
            .withGivenName("Jean")
            .withGivenNameTransliterated("JEAN")

        val vaccination2Dcc = pfizer1_1VaccinationCertificate(today(minus = 39.days))
            .withFamilyName("Dupont Du Lac")
            .withFamilyNameTransliterated("DUPONT<DULAC")
            .withGivenName("Jean")
            .withGivenNameTransliterated("JEAN")

        givenBaseHeaders()
            .body(
                DccAggregationRequest(
                    publicKey = CLIENT_PUBLIC_KEY_BASE64,
                    certificates = listOf(
                        encodeToDccAndEncrypt(vaccinationDcc),
                        encodeToDccAndEncrypt(vaccination2Dcc)
                    ),
                )
            )

            .When()
            .post("/api/v1/aggregate")

            .then()
            .statusCode(HttpStatus.BAD_REQUEST.value())
            .body("error", equalTo(HttpStatus.BAD_REQUEST.reasonPhrase))
            .body("message", equalTo("Request body contains invalid attributes"))
            .body("errors[0].field", equalTo("certificates[1]"))
            .body("errors[0].code", equalTo("INCONSISTENT_FAMILY_NAME"))
            .body("errors[0].message", equalTo("Family name does not match reference certificate family name"))
            .body("errors[1].field", equalTo("certificates"))
            .body("errors[1].code", equalTo("FLEX_ID_MISMATCH"))
            .body("errors[1].message", equalTo("Flex id condition did not match"))
    }
}
