package fr.gouv.tac.dcclight.e2e

import ehn.techiop.hcert.kotlin.data.ValueSetEntryAdapter
import fr.gouv.tac.dcclight.rules.SanitaryProofType.RECOVERY_STATEMENT
import fr.gouv.tac.dcclight.rules.SanitaryProofType.TEST
import fr.gouv.tac.dcclight.rules.SanitaryProofType.VACCINATION
import fr.gouv.tac.dcclight.rules.TestResult
import fr.gouv.tac.dcclight.test.entryOf
import io.cucumber.core.exception.CucumberException
import io.cucumber.java.ParameterType
import kotlinx.datetime.toKotlinInstant
import kotlinx.datetime.toLocalDate
import java.text.SimpleDateFormat
import java.time.Duration
import java.time.Instant
import java.time.ZoneId
import java.time.format.DateTimeFormatter
import java.time.temporal.ChronoUnit

class ParameterTypes {

    private val dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd").withZone(ZoneId.systemDefault())

    private val matriceVaccins: List<Proof> = listOf(
        Proof("Pfizer", "EU/1/20/1528", "ORG-100030215"),
        Proof("Moderna", "EU/1/20/1507", "ORG-100031184"),
        Proof("Astra-zeneca", "EU/1/21/1529", "ORG-100001699"),
        Proof("Novavax", "EU/1/21/1618", "ORG-100032020"),
        Proof("Novavax combinaison", "NVX-CoV2373", "ORG-100032020"),
        Proof("Jensen", "EU/1/20/1525", "ORG-100001417"),
        Proof("Covid-19-recombinant", "Covid-19-recombinant", "Fiocruz"),
        Proof("Sinovac", "CoronaVac", "Sinovac-Biotech"),
        Proof("Covovax", "Covovax", "ORG-100001981"),
        Proof("Bharat", "Covaxin", "Bharat-Biotech"),
        Proof("mauvais medicinal product", "000000", "ORG-100030215"),
        Proof("mauvais authorization holder", "EU/1/20/1528", "000000"),
    )

    @ParameterType(".*")
    fun proof(vaccine: String): Proof {
        return matriceVaccins
            .filter { it.name == vaccine }
            .ifEmpty {
                throw IllegalArgumentException("$vaccine is unsupported")
            }
            .first()
    }

    @ParameterType("\\d{2}\\/\\d{2}\\/\\d{4}")
    fun frenchDateFormat(dateString: String?) =
        dateTimeFormatter.format(SimpleDateFormat("dd/MM/yyyy").parse(dateString).toInstant()).toLocalDate()

    @ParameterType("(dans|il y a|depuis) (\\d+) (jour|jours|heures|minutes)")
    fun naturalTime(sens: String, amountExpression: String, unitExpression: String): kotlinx.datetime.Instant {
        val unit = when (unitExpression) {
            "jour" -> ChronoUnit.DAYS
            "jours" -> ChronoUnit.DAYS
            "heures" -> ChronoUnit.HOURS
            "minutes" -> ChronoUnit.MINUTES
            else -> throw IllegalArgumentException("$unitExpression is unsupported")
        }
        val duration = Duration.of(amountExpression.toLong(), unit)
        return if (sens == "dans") {
            Instant.now().plus(duration).toKotlinInstant()
        } else {
            Instant.now().minus(duration).toKotlinInstant()
        }
    }

    @ParameterType("([0-9]{1,2}\\/[0-9]{1,2})")
    fun completion(completion: String): String = completion

    @ParameterType("pcr|antigénique")
    fun typeTest(typeTest: String): ValueSetEntryAdapter {
        return when (typeTest) {
            "pcr" -> entryOf("LP6464-4")
            "antigénique" -> entryOf("LP217198-3")
            else -> throw IllegalArgumentException("$typeTest must be either 'pcr' or 'antigénique'")
        }
    }

    @ParameterType("(AUTRE|Autre|sans fabricant|[0-9]{1,5})")
    fun manufacturerTest(manufacturerTest: String): ValueSetEntryAdapter {
        return when (manufacturerTest) {
            "0000" -> entryOf("not-allowed-test-manufacturer")
            "sans fabricant" -> entryOf("")
            else -> entryOf(manufacturerTest)
        }
    }

    @ParameterType("négative|positive")
    fun statusTest(status: String): ValueSetEntryAdapter {
        return when (status) {
            "négative" -> entryOf(TestResult.NOT_DETECTED.EUValue)
            "positive" -> entryOf(TestResult.DETECTED.EUValue)
            else -> throw IllegalArgumentException("$status must be either 'négative' or 'positive'")
        }
    }

    @ParameterType(".*")
    fun words(words: String): String = words

    @ParameterType("vaccination|test|rétablissement")
    fun proofType(proofType: String) = when (proofType) {
        "vaccination" -> VACCINATION
        "test" -> TEST
        "rétablissement" -> RECOVERY_STATEMENT
        else -> throw CucumberException("Unsupported proof type")
    }
}
