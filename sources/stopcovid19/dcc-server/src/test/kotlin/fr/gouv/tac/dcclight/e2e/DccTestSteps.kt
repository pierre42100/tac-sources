package fr.gouv.tac.dcclight.e2e

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.spotify.hamcrest.jackson.IsJsonObject.jsonObject
import ehn.techiop.hcert.kotlin.data.GreenCertificate
import ehn.techiop.hcert.kotlin.data.Person
import ehn.techiop.hcert.kotlin.data.RecoveryStatement
import ehn.techiop.hcert.kotlin.data.Test
import ehn.techiop.hcert.kotlin.data.Vaccination
import ehn.techiop.hcert.kotlin.data.ValueSetEntryAdapter
import fr.gouv.tac.dcclight.api.model.CertificatesAggregate
import fr.gouv.tac.dcclight.api.model.DccAggregationRequest
import fr.gouv.tac.dcclight.api.model.EncryptedApiResponse
import fr.gouv.tac.dcclight.extension.toLocalDate
import fr.gouv.tac.dcclight.rules.CertificateTarget
import fr.gouv.tac.dcclight.rules.SanitaryProofType
import fr.gouv.tac.dcclight.test.DccSigningKeysManager.Companion.DCCLIGHT_SERVER_KEYS
import fr.gouv.tac.dcclight.test.DccSigningKeysManager.Companion.DCC_SERVER_KEYS
import fr.gouv.tac.dcclight.test.DccSigningKeysManager.Companion.DCC_TEST_INPUT_KEYS
import fr.gouv.tac.dcclight.test.NettyManager.Companion.givenCountryUvciBlockListContains
import fr.gouv.tac.dcclight.test.RestAssuredManager.Companion.bodyEncrypted
import fr.gouv.tac.dcclight.test.RestAssuredManager.Companion.givenBaseHeaders
import fr.gouv.tac.dcclight.test.TransportEncryptionKeysManager.Companion.CLIENT_PUBLIC_KEY_BASE64
import fr.gouv.tac.dcclight.test.TransportEncryptionKeysManager.Companion.TEST_TRANSPORT_KEYS
import fr.gouv.tac.dcclight.test.TransportEncryptionKeysManager.Companion.decryptServerResponse
import fr.gouv.tac.dcclight.test.entryOf
import fr.gouv.tac.dcclight.test.extension.base64Decode
import fr.gouv.tac.dcclight.test.matchers.isEuDcc
import fr.gouv.tac.dcclight.test.matchers.isHcertEncoded
import fr.gouv.tac.dcclight.test.matchers.isNear
import fr.gouv.tac.dcclight.test.matchers.matchesUvciPattern
import fr.gouv.tac.dcclight.test.matchers.where
import io.cucumber.java.fr.Alors
import io.cucumber.java.fr.Etantdonné
import io.cucumber.java.fr.Etque
import io.cucumber.java.fr.Lorsque
import io.cucumber.java.fr.Quand
import io.restassured.http.ContentType.JSON
import io.restassured.response.Response
import kotlinx.datetime.Clock.System.now
import kotlinx.datetime.Instant
import kotlinx.datetime.LocalDate
import kotlinx.datetime.TimeZone
import kotlinx.datetime.toLocalDateTime
import org.apache.commons.lang3.RandomStringUtils
import org.hamcrest.Matchers.allOf
import org.hamcrest.Matchers.startsWith
import org.springframework.http.HttpStatus.OK
import kotlin.time.Duration.Companion.days
import kotlin.time.Duration.Companion.hours
import kotlin.time.Duration.Companion.minutes

class DccTestSteps {

    private val certificates: MutableList<Certificate> = mutableListOf()
    private var response: Response? = null
    private var publicKey: String = CLIENT_PUBLIC_KEY_BASE64
    private var dateTimeSample: Instant = now()

    @Etque("la preuve est sur liste d'exclusion")
    fun excludeDerogationCertificate() {
        val certificate = certificates.last() as ModifiableCertificate
        givenCountryUvciBlockListContains(certificate.greenCertificate)
    }

    @Etantdonné("la clé publique utilisée pour le chiffrement est {string}")
    fun changePublicKey(key: String) {
        publicKey = key
    }

    @Etantdonné("une preuve de vaccin {proof} {completion} injecté {naturalTime} en {word} au nom de {words}, {words} - {words}, {words} né le {frenchDateFormat}")
    fun generateVaccineCertificateWithSameEmissionAndInjectionDate(
        proof: Proof,
        completion: String,
        dateTimeSample: Instant,
        countryCode: String,
        givenNameTransliterated: String,
        familyNameTransliterated: String,
        givenName: String,
        familyName: String,
        dateOfBirth: LocalDate
    ) {
        generateVaccine(
            proof = proof,
            completion = completion,
            dateTimeSample = dateTimeSample,
            dateEmission = now(),
            countryCode = countryCode,
            certificateIssuer = RandomStringUtils.randomAlphanumeric(20),
            givenNameTransliterated = givenNameTransliterated,
            familyNameTransliterated = familyNameTransliterated,
            givenName = givenName,
            familyName = familyName,
            dateOfBirth = dateOfBirth.toString()
        )
    }

    @Etantdonné("une preuve de vaccin {proof} {completion} injecté {naturalTime} en {word} au nom de {words}, {words} - {words}, {words} né une mauvaise date le {word}")
    fun generateVaccineCertificateWithSameEmissionAndInjectionDateWithWrongDateFormat(
        proof: Proof,
        completion: String,
        dateTimeSample: Instant,
        countryCode: String,
        givenNameTransliterated: String,
        familyNameTransliterated: String,
        givenName: String,
        familyName: String,
        dateOfBirth: String
    ) {
        generateVaccine(
            proof = proof,
            completion = completion,
            dateTimeSample = dateTimeSample,
            dateEmission = now(),
            countryCode = countryCode,
            certificateIssuer = RandomStringUtils.randomAlphanumeric(20),
            givenNameTransliterated = givenNameTransliterated,
            familyNameTransliterated = familyNameTransliterated,
            givenName = givenName,
            familyName = familyName,
            dateOfBirth = dateOfBirth
        )
    }

    @Etantdonné("une preuve de vaccin {proof} {completion} injecté {naturalTime} en {word} sans émetteur au nom de {words}, {words} - {words}, {words} né le {frenchDateFormat}")
    fun generateVaccineWithoutIssuerCertificate(
        proof: Proof,
        completion: String,
        dateTimeSample: Instant,
        countryCode: String,
        givenNameTransliterated: String,
        familyNameTransliterated: String,
        givenName: String,
        familyName: String,
        dateOfBirth: LocalDate
    ) {
        generateVaccine(
            proof,
            completion,
            dateTimeSample,
            dateTimeSample,
            countryCode,
            "",
            givenNameTransliterated,
            familyNameTransliterated,
            givenName,
            familyName,
            dateOfBirth.toString()
        )
    }

    @Etantdonné("une preuve de vaccin {proof} {completion} injecté {naturalTime} en {word} avec l'émetteur {word} au nom de {words} {words}, {words} {words} né le {frenchDateFormat}")
    fun generateVaccineWithIssuerCertificate(
        proof: Proof,
        completion: String,
        dateTimeSample: Instant,
        countryCode: String,
        certificateIssuer: String,
        givenNameTransliterated: String,
        familyNameTransliterated: String,
        givenName: String,
        familyName: String,
        dateOfBirth: LocalDate
    ) {
        generateVaccine(
            proof,
            completion,
            dateTimeSample,
            dateTimeSample,
            countryCode,
            certificateIssuer,
            givenNameTransliterated,
            familyNameTransliterated,
            givenName,
            familyName,
            dateOfBirth.toString()
        )
    }

    @Etantdonné("une preuve de vaccin {proof} {completion} injecté {naturalTime} émis {naturalTime} en {word} au nom de {word}, {word} - {word}, {word} né le {frenchDateFormat}")
    fun generateVaccineWithEmissionCertificate(
        proof: Proof,
        completion: String,
        dateTimeSample: Instant,
        dateEmission: Instant,
        countryCode: String,
        givenNameTransliterated: String,
        familyNameTransliterated: String,
        givenName: String,
        familyName: String,
        dateOfBirth: LocalDate
    ) {
        generateVaccine(
            proof,
            completion,
            dateTimeSample,
            dateEmission,
            countryCode,
            RandomStringUtils.randomAlphanumeric(20),
            givenNameTransliterated,
            familyNameTransliterated,
            givenName,
            familyName,
            dateOfBirth.toString()
        )
    }

    private fun generateVaccine(
        proof: Proof,
        completion: String,
        dateTimeSample: Instant,
        dateEmission: Instant,
        countryCode: String,
        certificateIssuer: String,
        givenNameTransliterated: String,
        familyNameTransliterated: String,
        givenName: String,
        familyName: String,
        dateOfBirth: String
    ) {

        val dosesCompletion = completion.split("/")

        val vaccination = Vaccination(
            target = entryOf("840539006"),
            vaccine = entryOf("J07BX03"),
            medicinalProduct = entryOf(proof.medicinalProduct),
            authorizationHolder = entryOf(proof.authorizationHolder),
            doseNumber = dosesCompletion.first().toInt(),
            doseTotalNumber = dosesCompletion.last().toInt(),
            date = dateTimeSample.toLocalDateTime(TimeZone.currentSystemDefault()).date,
            country = countryCode,
            certificateIssuer = certificateIssuer,
            certificateIdentifier = RandomStringUtils.randomAlphanumeric(20),
        )

        val certificate = GreenCertificate(
            schemaVersion = "1.3.0",
            subject = Person(
                familyName = familyName,
                familyNameTransliterated = familyNameTransliterated,
                givenName = givenName,
                givenNameTransliterated = givenNameTransliterated
            ),
            dateOfBirthString = dateOfBirth,
            vaccinations = arrayOf(vaccination)
        )

        certificates.add(ModifiableCertificate(certificate, DCC_TEST_INPUT_KEYS, dateEmission))
    }

    @Etantdonné("une preuve de rétablissement valable {naturalTime} en {word} au nom de {words}, {words} - {words}, {words} né une mauvaise date le {word}")
    @Etantdonné("une preuve de rétablissement valable {naturalTime} en {word} au nom de {words}, {words} - {words}, {words} né le {frenchDateFormat}")
    fun generateRecoveryCertificate(
        dateOfFirstPositiveTestResult: Instant,
        countryCode: String,
        givenNameTransliterated: String,
        familyNameTransliterated: String,
        givenName: String,
        familyName: String,
        dateOfBirth: LocalDate
    ) {

        val recovery = RecoveryStatement(
            target = entryOf(CertificateTarget.COVID_19.EUValue),
            country = countryCode,
            dateOfFirstPositiveTestResult = dateOfFirstPositiveTestResult.toLocalDateTime(TimeZone.currentSystemDefault()).date,
            certificateValidFrom = dateOfFirstPositiveTestResult.toLocalDateTime(TimeZone.currentSystemDefault()).date,
            certificateValidUntil = now().plus(300.days).toLocalDateTime(TimeZone.currentSystemDefault()).date,
            certificateIssuer = RandomStringUtils.randomAlphanumeric(20),
            certificateIdentifier = RandomStringUtils.randomAlphanumeric(20)
        )

        val certificate = GreenCertificate(
            schemaVersion = "1.3.0",
            subject = Person(
                familyName = familyName,
                familyNameTransliterated = familyNameTransliterated,
                givenName = givenName,
                givenNameTransliterated = givenNameTransliterated
            ),
            dateOfBirthString = dateOfBirth.toString(),
            recoveryStatements = arrayOf(recovery)
        )

        certificates.add(ModifiableCertificate(certificate, DCC_TEST_INPUT_KEYS, dateOfFirstPositiveTestResult))
    }

    @Etantdonné("une preuve {statusTest} de test {typeTest} {manufacturerTest} effectué {naturalTime} en {word} par {word} au nom de {words}, {words} - {words}, {words} né une mauvaise date le {word}")
    @Etantdonné("une preuve {statusTest} de test {typeTest} {manufacturerTest} effectué {naturalTime} en {word} par {word} au nom de {words}, {words} - {words}, {words} né le {frenchDateFormat}")
    fun generateTestCertificate(
        statusTest: ValueSetEntryAdapter,
        typeTest: ValueSetEntryAdapter,
        manufacturer: ValueSetEntryAdapter,
        dateTimeSample: Instant,
        countryCode: String,
        certificateIssuer: String,
        givenNameTransliterated: String,
        familyNameTransliterated: String,
        givenName: String,
        familyName: String,
        dateOfBirth: LocalDate
    ) {
        this.dateTimeSample = dateTimeSample
        val test = Test(
            target = entryOf("840539006"),
            type = typeTest,
            dateTimeSample = dateTimeSample,
            resultPositive = statusTest,
            country = countryCode,
            certificateIssuer = certificateIssuer,
            certificateIdentifier = "identifier",
            nameRat = manufacturer
        )

        val certificate = GreenCertificate(
            schemaVersion = "1.3.0",
            subject = Person(
                familyName = familyName,
                familyNameTransliterated = familyNameTransliterated,
                givenName = givenName,
                givenNameTransliterated = givenNameTransliterated
            ),
            dateOfBirthString = dateOfBirth.toString(),
            tests = arrayOf(test)
        )

        certificates.add(ModifiableCertificate(certificate, DCC_TEST_INPUT_KEYS, dateTimeSample))
    }

    @Quand("l'utilisateur commence une nouvelle procédure d'agrégation")
    fun emptyCertificatesList() {
        certificates.clear()
    }

    @Etque("l'utilisateur renvoie la dernière preuve réceptionnée")
    fun sendLastRetrievedCertificate() {
        val encryptedResponse = response!!
            .then()
            .extract().`as`(EncryptedApiResponse::class.java)
        val responseBody =
            decryptServerResponse(encryptedResponse.response.base64Decode(), transportKeys = TEST_TRANSPORT_KEYS)
        val lastCertificate = jacksonObjectMapper()
            .readValue(responseBody, CertificatesAggregate::class.java)
            .certificate
        certificates.add(RawCertificate(lastCertificate))
    }

    @Quand("une demande de conversion de preuve est réalisée")
    @Quand("une demande d'agrégation de preuves est réalisée")
    @Quand("une demande de combinaison de preuves pour avoir un passlight est réalisée")
    fun generateAndAggregate() {
        val dccAggRequest = DccAggregationRequest(
            publicKey = publicKey,
            certificates = certificates.map { it.encrypt(TEST_TRANSPORT_KEYS) }
        )
        response = givenBaseHeaders()
            .contentType(JSON)
            .body(dccAggRequest)
            .post("/api/v1/aggregate")
    }

    @Alors("réception d'une preuve de type {proofType} valide à partir de maintenant au nom de {words}, {words} - {words}, {words} né le {frenchDateFormat}")
    fun isAggregationProofWellGenerated(
        proofType: SanitaryProofType,
        givenNameTransliterated: String,
        familyNameTransliterated: String,
        givenName: String,
        familyName: String,
        dateOfBirth: LocalDate
    ) {
        response!!
            .then()
            .statusCode(OK.value())
            .bodyEncrypted(
                "response",
                jsonObject()
                    .where(
                        "certificate",
                        isHcertEncoded(
                            issuedBy = DCC_SERVER_KEYS,
                            euDcc = isEuDcc()
                                .forJeromeDupontWith(
                                    dob = dateOfBirth.toString(),
                                    gn = givenName,
                                    gnt = givenNameTransliterated,
                                    fn = familyName,
                                    fnt = familyNameTransliterated,
                                )
                                .withProofType(proofType.type)
                                .where("ci", matchesUvciPattern())
                        )
                    )
            )
    }

    @Alors("la preuve de test a été réalisée dans le centre {word}")
    fun checkTestCenter(
        certificateIssuer: String
    ) {
        response!!
            .then()
            .statusCode(OK.value())
            .bodyEncrypted(
                "response",
                jsonObject()
                    .where(
                        "certificate",
                        isHcertEncoded(
                            issuedBy = DCC_SERVER_KEYS,
                            euDcc = isEuDcc()
                                .withProofType(SanitaryProofType.TEST.type)
                                .where("tc", certificateIssuer)
                        )
                    )
            )
    }

    @Alors("la preuve de {proofType} est émise par {word}")
    fun checkCertificatIssuer(
        proofType: SanitaryProofType,
        certificateIssuer: String
    ) {
        response!!
            .then()
            .statusCode(OK.value())
            .bodyEncrypted(
                "response",
                jsonObject()
                    .where(
                        "certificate",
                        isHcertEncoded(
                            issuedBy = DCC_SERVER_KEYS,
                            euDcc = isEuDcc()
                                .withProofType(proofType.type)
                                .where("is", certificateIssuer)
                        )
                    )
            )
    }

    @Alors("la preuve de vaccination a un nombre de doses total de {completion}")
    fun checkInjectionCompletion(
        completion: String
    ) {
        val dosesCompletion = completion.split("/")
        response!!
            .then()
            .statusCode(OK.value())
            .bodyEncrypted(
                "response",
                jsonObject()
                    .where(
                        "certificate",
                        isHcertEncoded(
                            issuedBy = DCC_SERVER_KEYS,
                            euDcc = isEuDcc()
                                .withProofType(SanitaryProofType.VACCINATION.type)
                                .where("dn", dosesCompletion.first().toInt())
                                .where("sd", dosesCompletion.last().toInt())
                        )
                    )
            )
    }

    @Alors("le début de validité du certificat est la date de collection du test + {int} jours et la fin de validité est la date de collection du test + {int} jours")
    fun checkProofValidity(
        start: Int,
        end: Int
    ) {
        response!!
            .then()
            .statusCode(OK.value())
            .bodyEncrypted(
                "response",
                jsonObject()
                    .where(
                        "certificate",
                        isHcertEncoded(
                            issuedBy = DCC_SERVER_KEYS,
                            euDcc = isEuDcc()
                                .withProofType("r")
                                .where("df", dateTimeSample.plus(start.days).toLocalDate())
                                .where("du", dateTimeSample.plus(end.days).toLocalDate())
                        )
                    )
            )
    }

    @Alors("réception d'un passlight valide pendant {int} heures au nom de {words}, {words} - {words}, {words} né le {frenchDateFormat}")
    fun isPasslightWellGenerated(
        duration: Int,
        givenNameTransliterated: String,
        familyNameTransliterated: String,
        givenName: String,
        familyName: String,
        dateOfBirth: LocalDate
    ) {
        response!!
            .then()
            .statusCode(OK.value())
            .bodyEncrypted(
                "response",
                jsonObject()
                    .where(
                        "certificate",
                        allOf(
                            startsWith("HCFR1:"),
                            isHcertEncoded(
                                issuedBy = DCCLIGHT_SERVER_KEYS,
                                issuedAt = isNear(dateTimeSample, 1.minutes),
                                expirationTime = isNear(dateTimeSample.plus(duration.hours), 1.minutes),
                                euDcc = isEuDcc()
                                    .forJeromeDupontWith(
                                        ver = "1.0.0",
                                        dob = dateOfBirth.toString(),
                                        gn = givenName,
                                        gnt = givenNameTransliterated,
                                        fn = familyName,
                                        fnt = familyNameTransliterated,
                                    )
                            )
                        )
                    )
            )
    }

    @Alors("l'utilisateur {words} preuve")
    @Alors("l'utilisateur {words} passlight")
    fun noProof(accepted: String) {
        when (accepted) {
            "reçoit un", "reçoit une" -> response!!.then().statusCode(200)
            "ne reçoit pas de" -> response!!.then().statusCode(400)
            else -> throw IllegalStateException("Unexpected sentence definition in method noProof")
        }
    }

    @Lorsque("le {words} de la preuve {int} est {words}")
    fun updateInputDccsField(fieldToUpdate: String, proofIndex: Int, value: String) {
        val listIndex = proofIndex - 1
        val certificateToUpdate = certificates[listIndex] as ModifiableCertificate
        val dccToUpdate = certificateToUpdate.greenCertificate
        val subjectToUpdate = dccToUpdate.subject
        val updatedSubject = when (fieldToUpdate) {
            "prénom" -> subjectToUpdate.copy(givenName = value)
            "prénom translitéré" -> subjectToUpdate.copy(givenNameTransliterated = value)
            "nom de famille" -> subjectToUpdate.copy(familyName = value)
            "nom de famille translitéré" -> subjectToUpdate.copy(familyNameTransliterated = value)
            else -> throw IllegalStateException("Updating field '$fieldToUpdate' is not yet supported")
        }
        certificates[listIndex] = certificateToUpdate.copy(
            greenCertificate = dccToUpdate.copy(
                subject = updatedSubject
            )
        )
    }
}
