package fr.gouv.tac.dcclight.e2e

import fr.gouv.tac.dcclight.test.DccSigningKeysManager.Companion.DCC_TEST_INPUT_KEYS
import fr.gouv.tac.dcclight.test.IntegrationTest
import fr.gouv.tac.dcclight.test.NettyManager
import fr.gouv.tac.dcclight.test.RestAssuredManager
import io.cucumber.java.Before
import io.cucumber.spring.CucumberContextConfiguration
import org.junit.platform.suite.api.SelectClasspathResource
import org.junit.platform.suite.api.Suite
import org.springframework.boot.test.web.server.LocalServerPort

@Suite
@SelectClasspathResource("/features")
@CucumberContextConfiguration
@IntegrationTest
class CucumberTest(@LocalServerPort private val port: Int) {

    @Before
    fun before() {
        RestAssuredManager.configureRestAssured("http://localhost:$port")
        NettyManager.givenCertificatesTrustListContains(DCC_TEST_INPUT_KEYS)
        NettyManager.givenRealAggregationRulesAreExposed()
        NettyManager.givenBlockListIsEmpty()
    }
}
