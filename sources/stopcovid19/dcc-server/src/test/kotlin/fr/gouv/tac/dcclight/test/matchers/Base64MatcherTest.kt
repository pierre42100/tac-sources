package fr.gouv.tac.dcclight.test.matchers

import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.equalTo
import org.hamcrest.Matchers.not
import org.junit.jupiter.api.DisplayNameGeneration
import org.junit.jupiter.api.DisplayNameGenerator.ReplaceUnderscores
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows

@DisplayNameGeneration(ReplaceUnderscores::class)
class Base64MatcherTest {

    // echo -n 'DCC Server' | base64
    // outputs 'RENDIFNlcnZlcg=='

    @Test
    fun can_unwrap_base64_encoded_data() {
        assertThat("RENDIFNlcnZlcg==", isBase64Encoded(equalTo("DCC Server".toByteArray())))
        assertThat("RENDIFNlcnZlcg==", isBase64Encoded(not(equalTo("other server".toByteArray()))))
    }

    @Test
    fun can_detect_invalid_base64_input() {
        val error: IllegalArgumentException = assertThrows {
            assertThat("!!!", isBase64Encoded(equalTo("DCC Server".toByteArray())))
        }
        assertThat(error.message, equalTo("Illegal base64 character 21"))
    }
}
