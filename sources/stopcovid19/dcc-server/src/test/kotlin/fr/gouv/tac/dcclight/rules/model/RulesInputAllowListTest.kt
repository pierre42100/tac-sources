package fr.gouv.tac.dcclight.rules.model

import fr.gouv.tac.dcclight.api.model.DccAggregationRequest
import fr.gouv.tac.dcclight.test.IntegrationTest
import fr.gouv.tac.dcclight.test.NettyManager.Companion.givenExposedRules
import fr.gouv.tac.dcclight.test.RestAssuredManager.Companion.When
import fr.gouv.tac.dcclight.test.RestAssuredManager.Companion.givenBaseHeaders
import fr.gouv.tac.dcclight.test.TransportEncryptionKeysManager.Companion.CLIENT_PUBLIC_KEY_BASE64
import fr.gouv.tac.dcclight.test.antigenicTestYamlInputRule
import fr.gouv.tac.dcclight.test.defaultEmptyRuleList
import fr.gouv.tac.dcclight.test.eu.DccCryptographer.Companion.encodeToDccAndEncrypt
import fr.gouv.tac.dcclight.test.extension.addInputRule
import fr.gouv.tac.dcclight.test.extension.withOutputRule
import fr.gouv.tac.dcclight.test.extension.withRecoveryCountry
import fr.gouv.tac.dcclight.test.extension.withTestFacility
import fr.gouv.tac.dcclight.test.extension.withTestIssuer
import fr.gouv.tac.dcclight.test.extension.withTestNameNaa
import fr.gouv.tac.dcclight.test.extension.withTestNameRat
import fr.gouv.tac.dcclight.test.extension.withTestTarget
import fr.gouv.tac.dcclight.test.extension.withVaccinationTarget
import fr.gouv.tac.dcclight.test.extension.withVaccinationVaccine
import fr.gouv.tac.dcclight.test.pfizer1_1VaccinationCertificate
import fr.gouv.tac.dcclight.test.pfizer1_1VaccinationInputRule
import fr.gouv.tac.dcclight.test.positiveAntigenicTestCertificate
import fr.gouv.tac.dcclight.test.recoveryInputRule
import fr.gouv.tac.dcclight.test.recoveryStatementCertificate
import fr.gouv.tac.dcclight.test.recoveryToTestOutputRule
import fr.gouv.tac.dcclight.test.testOutputRule
import fr.gouv.tac.dcclight.test.today
import fr.gouv.tac.dcclight.test.vaccine2_2OutputRule
import kotlinx.datetime.Instant
import org.hamcrest.Matchers
import org.junit.jupiter.api.Test
import org.springframework.http.HttpStatus
import kotlin.time.Duration.Companion.days

@IntegrationTest
class RulesInputAllowListTest {

    private fun getVaccine1_1ToVaccine2_2AllowListRule(allowElement: Map<String, List<String>>): YamlRules {
        val vaccineInputRule = pfizer1_1VaccinationInputRule().copy(
            allowLists = allowElement
        )
        return defaultEmptyRuleList()
            .addInputRule(vaccineInputRule)
            .withOutputRule(
                vaccine2_2OutputRule().copy(
                    referentialDccRanking = 1
                )
            )
    }

    private fun getTestRule(allowElement: Map<String, List<String>>): YamlRules {
        val inputRule = antigenicTestYamlInputRule().copy(
            allowLists = allowElement
        )
        return defaultEmptyRuleList()
            .addInputRule(inputRule)
            .withOutputRule(testOutputRule())
    }

    private fun getRecoveryToTestAllowListRule(allowElement: Map<String, List<String>>): YamlRules {
        val inputRule = recoveryInputRule().copy(
            allowLists = allowElement
        )
        return defaultEmptyRuleList()
            .addInputRule(inputRule)
            .withOutputRule(recoveryToTestOutputRule())
    }

    @Test
    fun vaccine_with_an_attribute_not_in_rule_input_allow_lists_must_return_an_error() {
        givenExposedRules(getVaccine1_1ToVaccine2_2AllowListRule(mapOf("tg" to listOf("IN_ALLOW_LIST"))))

        val vaccinationDcc = pfizer1_1VaccinationCertificate(today(minus = 50.days))
            .withVaccinationTarget("NOT_IN_ALLOW_LIST")
        givenBaseHeaders()
            .body(
                DccAggregationRequest(
                    publicKey = CLIENT_PUBLIC_KEY_BASE64,
                    certificates = listOf(
                        encodeToDccAndEncrypt(vaccinationDcc),
                    ),
                )
            )

            .When()
            .post("/api/v1/aggregate")

            .then()
            .statusCode(HttpStatus.BAD_REQUEST.value())
            .body("error", Matchers.equalTo(HttpStatus.BAD_REQUEST.reasonPhrase))
            .body("message", Matchers.equalTo("Request body contains invalid attributes"))
            .body("errors[0].code", Matchers.equalTo("INCOMPATIBLE_DCC_COMBINATION"))
            .body("errors[0].message", Matchers.equalTo("Provided certificates are not compatible with any operation"))
    }

    @Test
    fun vaccine_with_one_attribute_in_rule_input_allow_lists_must_return_a_new_certificate() {
        // Note : some allow-list, "vp" by exemple must not be in contradiction with vaccineProphylaxisList
        givenExposedRules(getVaccine1_1ToVaccine2_2AllowListRule(mapOf("vp" to listOf("IN_ALLOW_LIST"))))

        val vaccination = pfizer1_1VaccinationCertificate(today(minus = 50.days))
            .withVaccinationVaccine("IN_ALLOW_LIST")

        givenBaseHeaders()
            .body(
                DccAggregationRequest(
                    publicKey = CLIENT_PUBLIC_KEY_BASE64,
                    certificates = listOf(
                        encodeToDccAndEncrypt(vaccination),
                    ),
                )
            )

            .When()
            .post("/api/v1/aggregate")

            .then()
            .statusCode(HttpStatus.OK.value())
    }

    @Test
    fun test_with_one_attribute_in_rule_input_allow_lists_must_return_a_new_certificate() {
        givenExposedRules(getTestRule(mapOf("nm" to listOf("IN_ALLOW_LIST"))))

        val testDate = Instant.parse("1899-10-10T10:10:10Z")

        val testDcc = positiveAntigenicTestCertificate(testDate)
            .withTestTarget("840539006")
            .withTestIssuer("APHP")
            .withTestNameRat("1833")
            .withTestFacility("facility")
            .withTestNameNaa("IN_ALLOW_LIST")

        givenBaseHeaders()
            .body(
                DccAggregationRequest(
                    publicKey = CLIENT_PUBLIC_KEY_BASE64,
                    certificates = listOf(
                        encodeToDccAndEncrypt(testDcc)
                    ),
                )
            )

            .When()
            .post("/api/v1/aggregate")

            .then()
            .statusCode(HttpStatus.OK.value())
    }

    @Test
    fun test_with_an_attribute_not_in_rule_input_allow_lists_must_return_an_error() {
        givenExposedRules(getTestRule(mapOf("tg" to listOf("IN_ALLOW_LIST"))))

        val testDate = Instant.parse("1899-10-10T10:10:10Z")

        val testDcc = positiveAntigenicTestCertificate(testDate)
            .withTestTarget("840539006")
            .withTestIssuer("APHP")
            .withTestNameRat("1833")
            .withTestFacility("facility")

        givenBaseHeaders()
            .body(
                DccAggregationRequest(
                    publicKey = CLIENT_PUBLIC_KEY_BASE64,
                    certificates = listOf(
                        encodeToDccAndEncrypt(testDcc)
                    ),
                )
            )

            .When()
            .post("/api/v1/aggregate")

            .then()
            .statusCode(HttpStatus.BAD_REQUEST.value())
            .body("error", Matchers.equalTo(HttpStatus.BAD_REQUEST.reasonPhrase))
            .body("message", Matchers.equalTo("Request body contains invalid attributes"))
            .body("errors[0].code", Matchers.equalTo("INCOMPATIBLE_DCC_COMBINATION"))
            .body("errors[0].message", Matchers.equalTo("Provided certificates are not compatible with any operation"))
    }

    @Test
    fun recovery_with_one_attribute_in_rule_input_allow_lists_must_return_a_new_certificate() {
        givenExposedRules(getRecoveryToTestAllowListRule(mapOf("co" to listOf("IN_ALLOW_LIST"))))

        val testDate = Instant.parse("1899-10-10T10:10:10Z")
        val recoveryDcc = recoveryStatementCertificate(testDate)
            .withRecoveryCountry("IN_ALLOW_LIST")

        givenBaseHeaders()
            .body(
                DccAggregationRequest(
                    publicKey = CLIENT_PUBLIC_KEY_BASE64,
                    certificates = listOf(
                        encodeToDccAndEncrypt(recoveryDcc),
                    ),
                )
            )

            .When()
            .post("/api/v1/aggregate")

            .then()
            .statusCode(HttpStatus.OK.value())
    }

    @Test
    fun recovery_with_no_attribute_in_rule_input_block_lists_must_return_a_new_certificate() {
        givenExposedRules(getRecoveryToTestAllowListRule(mapOf("co" to listOf("NOT_IN_ALLOW_LIST"))))

        val testDate = Instant.parse("1899-10-10T10:10:10Z")

        val recoveryDcc = recoveryStatementCertificate(testDate)

        givenBaseHeaders()
            .body(
                DccAggregationRequest(
                    publicKey = CLIENT_PUBLIC_KEY_BASE64,
                    certificates = listOf(
                        encodeToDccAndEncrypt(recoveryDcc),
                    ),
                )
            )

            .When()
            .post("/api/v1/aggregate")

            .then()
            .statusCode(HttpStatus.BAD_REQUEST.value())
            .body("error", Matchers.equalTo(HttpStatus.BAD_REQUEST.reasonPhrase))
            .body("message", Matchers.equalTo("Request body contains invalid attributes"))
            .body("errors[0].code", Matchers.equalTo("INCOMPATIBLE_DCC_COMBINATION"))
            .body("errors[0].message", Matchers.equalTo("Provided certificates are not compatible with any operation"))
    }
}
