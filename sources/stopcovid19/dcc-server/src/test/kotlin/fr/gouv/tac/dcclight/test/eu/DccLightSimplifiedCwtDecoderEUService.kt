package fr.gouv.tac.dcclight.test.eu

import ehn.techiop.hcert.kotlin.chain.CwtService
import ehn.techiop.hcert.kotlin.chain.Error
import ehn.techiop.hcert.kotlin.chain.Error.CBOR_DESERIALIZATION_FAILED
import ehn.techiop.hcert.kotlin.chain.VerificationException
import ehn.techiop.hcert.kotlin.chain.VerificationResult
import ehn.techiop.hcert.kotlin.crypto.CwtHeaderKeys
import ehn.techiop.hcert.kotlin.data.CborObject
import ehn.techiop.hcert.kotlin.trust.CwtAdapter
import ehn.techiop.hcert.kotlin.trust.CwtHelper
import kotlinx.datetime.Instant

class DccLightSimplifiedCwtDecoderEUService : CwtService {

    override fun decode(input: ByteArray, verificationResult: VerificationResult): CborObject {
        val map = CwtHelper.fromCbor(input)
        // issuer is truly optional
        map.getString(CwtHeaderKeys.ISSUER.intVal)?.let {
            verificationResult.issuer = it
        }

        val issuedAtSeconds = map.getNumber(CwtHeaderKeys.ISSUED_AT.intVal)
            ?: throw VerificationException(
                Error.CWT_EXPIRED,
                details = mapOf("issuedAt" to "null")
            )
        val issuedAt = Instant.fromEpochSeconds(issuedAtSeconds.toLong())
        verificationResult.issuedAt = issuedAt

        val expirationSeconds = map.getNumber(CwtHeaderKeys.EXPIRATION.intVal)
            ?: throw VerificationException(Error.CWT_EXPIRED, details = mapOf("expirationTime" to "null"))
        val expirationTime = Instant.fromEpochSeconds(expirationSeconds.toLong())
        verificationResult.expirationTime = expirationTime

        val hcert: CwtAdapter = map.getMap(CwtHeaderKeys.HCERT.intVal)
            ?: throw VerificationException(CBOR_DESERIALIZATION_FAILED, "CWT contains no HCERT")

        val dgc = hcert.getMap(CwtHeaderKeys.EUDGC_IN_HCERT.intVal)
            ?: throw VerificationException(CBOR_DESERIALIZATION_FAILED, "CWT contains no EUDGC")

        return dgc.toCborObject()
    }

    // NEVER USED
    override fun encode(input: ByteArray): ByteArray {
        return ByteArray(0)
    }
}
