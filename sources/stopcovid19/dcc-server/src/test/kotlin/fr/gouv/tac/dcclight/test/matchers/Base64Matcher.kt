package fr.gouv.tac.dcclight.test.matchers

import fr.gouv.tac.dcclight.test.extension.base64Decode
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.TypeSafeDiagnosingMatcher

/**
 * Creates a matcher that matches if the examined String is Base64 encoded and matches the specified matcher when it is Base64 decoded.
 * For example:
 *
 *     assertThat("RENDIFNlcnZlcg==", isBase64Encoded(equalTo("DCC Server"))
 */
fun isBase64Encoded(matcher: Matcher<ByteArray>): TypeSafeDiagnosingMatcher<String> = Base64Matcher(matcher)

private class Base64Matcher(private val nextMatcher: Matcher<ByteArray>) : TypeSafeDiagnosingMatcher<String>() {
    override fun describeTo(description: Description) {
        description.appendText("a Base64 encoded String that contains ")
        nextMatcher.describeTo(description)
    }

    override fun matchesSafely(item: String, mismatchDescription: Description): Boolean {
        val certificate = item.base64Decode()
        if (!nextMatcher.matches(certificate)) {
            mismatchDescription.appendText("a Base64 encoded string that contains ")
            nextMatcher.describeMismatch(certificate, mismatchDescription)
            return false
        }
        return true
    }
}
