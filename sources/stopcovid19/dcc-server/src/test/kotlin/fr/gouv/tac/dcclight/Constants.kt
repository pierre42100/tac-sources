package fr.gouv.tac.dcclight

internal const val CONVERSION_STRING_INPUT: String = "conversion"
internal const val HASH_HMACSHA256 = "HmacSHA256"
internal const val ALGORITHM_ECDH = "ECDH"
internal const val ALGORITHM_AES = "AES"
internal const val NAMED_CURVE_SPEC = "secp256r1"

internal const val AES_GCM_CIPHER_TYPE = "AES/GCM/NoPadding"
internal const val AES_GCM_IV_LENGTH = 12
internal const val AES_GCM_TAG_LENGTH_IN_BITS = 128
