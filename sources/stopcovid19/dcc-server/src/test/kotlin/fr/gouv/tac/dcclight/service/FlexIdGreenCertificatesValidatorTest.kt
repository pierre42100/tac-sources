package fr.gouv.tac.dcclight.service

import fr.gouv.tac.dcclight.validation.FlexIdValidator
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.DisplayNameGeneration
import org.junit.jupiter.api.DisplayNameGenerator
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test

@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores::class)
class FlexIdGreenCertificatesValidatorTest {

    val flexIdValidator = object : FlexIdValidator() {}

    @Nested
    inner class FlexIdTest {
        @Test
        fun matches_when_smallest_names_list_is_included_in_all_others() {

            assertThat(
                flexIdValidator.matchFlexId(
                    listOf(
                        "DUPONT<DOS<SANTOS",
                        "DUPONT<AIGNAN",
                        "DUPONT"
                    )
                )
            )
                .`as`("Given smallest string list (DUPONT) is contained in all other lists (DUPONT, AIGNAN) (DUPONT, DOS, SANTOS)")
                .isTrue
        }

        @Test
        fun does_not_match_when_smallest_names_list_contains_accented_characters() {

            assertThat(
                flexIdValidator.matchFlexId(
                    listOf(
                        "DUPONT<DOS<SANTOS",
                        "DUPONT<AIGNAN",
                        "DÚPONT"
                    )
                )
            )
                .`as`("Given smallest string list (DÚPÖNT) is not included other lists (DUPONT, AIGNAN) (DUPONT, DOS, SANTOS) because of accented characters")
                .isFalse
        }

        @Test
        fun does_not_match_when_same_size_names_lists_do_not_match() {

            assertThat(flexIdValidator.matchFlexId(listOf("DUPONT<JACQUELIN<CHEVALLIER", "DUPONT<JACQUELIN<METAYER")))
                .`as`("Given string list (DUPONT, JACQUELIN, METAYER) does not match (DUPONT, JACQUELIN, CHEVALLIER) while they have same size")
                .isFalse
        }

        @Test
        fun does_not_match_when_smallest_names_list_is_not_included_in_others() {

            assertThat(flexIdValidator.matchFlexId(listOf("DUPONT<DOS<SANTOS", "DUPONT<AIGNAN")))
                .`as`("Given smallest string list (DUPONT, AIGNAN) is not included in other list (DUPONT, DOS, SANTOS)")
                .isFalse
        }
    }
}
