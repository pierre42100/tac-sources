package fr.gouv.tac.dcclight.e2e

data class Proof(
    val name: String,
    val medicinalProduct: String,
    val authorizationHolder: String
)
