package fr.gouv.tac.dcclight.test.matchers

import fr.gouv.tac.dcclight.test.TransportEncryptionKeysManager.Companion.TEST_TRANSPORT_KEYS
import fr.gouv.tac.dcclight.test.TransportEncryptionKeysManager.Companion.decryptServerResponse
import fr.gouv.tac.dcclight.test.TransportEncryptionKeysManager.TransportKeys
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.TypeSafeDiagnosingMatcher
/**
 * Creates a matcher that matches if the examined ByteArray is well encrypted by the server and matches the given matcher.
 * For example:
 *
 *     assertThat("PC_�:�]�A�kߌ���7y^�]�ydG��[utg�".toByteArray(), isEncrypted(equalTo("DCC Server")
 */
fun isEncrypted(matcher: Matcher<ByteArray>, transportKeys: TransportKeys = TEST_TRANSPORT_KEYS): TypeSafeDiagnosingMatcher<ByteArray> = EncryptedMatcher(matcher, transportKeys)

private class EncryptedMatcher(
    private val nextMatcher: Matcher<ByteArray>,
    private val transportKeys: TransportKeys
) : TypeSafeDiagnosingMatcher<ByteArray>() {

    override fun describeTo(description: Description) {
        description.appendText("a software-encrypted bytearray which contains ")
        nextMatcher.describeTo(description)
    }

    override fun matchesSafely(item: ByteArray, mismatchDescription: Description): Boolean {
        val certificate = decryptServerResponse(item, transportKeys)
        if (!nextMatcher.matches(certificate)) {
            mismatchDescription.appendText("a software-encrypted bytearray which contains ")
            nextMatcher.describeMismatch(certificate, mismatchDescription)
            return false
        }
        return true
    }
}
