package fr.gouv.tac.dcclight.service.dcc

import ehn.techiop.hcert.kotlin.chain.VerificationResult
import ehn.techiop.hcert.kotlin.chain.impl.DefaultBase45Service
import ehn.techiop.hcert.kotlin.chain.impl.DefaultCompressorService
import ehn.techiop.hcert.kotlin.chain.impl.DefaultContextIdentifierService
import fr.gouv.tac.dcclight.extension.countryUvciHash
import fr.gouv.tac.dcclight.extension.uvciHash
import fr.gouv.tac.dcclight.test.extension.withVaccinationIdentifier
import fr.gouv.tac.dcclight.test.pfizer1_1VaccinationCertificate
import fr.gouv.tac.dcclight.test.today
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.CsvSource
import kotlin.time.Duration.Companion.days

class DccHashTest {

    @ParameterizedTest
    @CsvSource(
        "URN:UVCI:01:FR:DSQLGF8Y43FOKJR#1,a2d5132cc36a5183afb30026ee792fe5",
        "URN:UVCI:01:FR:NIFA7VW835EA84E#2,99bcb8b1e770649664b21cecc3df4658",
        "URN:UVCI:01:FR:HM2LWPJE3DRRVF1#3,fb2dbd2d67705d9f901ab87e4251a5bc",
        "URN:UVCI:01:FR:E8IZWUK8K0YPRTK#4,aaa1ea360a2e109c2f1389810041000d",
        "URN:UVCI:01:FR:AZIPDJAZEIFHB23#5,a5ec64c3dda9bda95dfd65dffb7b3e92",
        "URN:UVCI:01:FR:I6KFPIT0IBBDH26#6,debd882a91e6b829fe76d98891a615c9",
        "urn:uvci:01:FR:LIFPYVGBHDOQ#B,64027d2f1d6fdb211422398d3c01dacf",
        "URN:UVCI:01:FR:Y4996KBJ4Q3Q#:,afe98db8c94a83ec77e973acc7be55a9"
    )
    fun can_build_countryCode_uvci_hash(identifier: String, expectedEncodedIdentifier: String) {
        val dcc = pfizer1_1VaccinationCertificate(today(minus = 39.days))
            .withVaccinationIdentifier(identifier)

        assertThat(dcc.countryUvciHash())
            .isEqualTo(expectedEncodedIdentifier)
    }

    @ParameterizedTest
    @CsvSource(
        "URN:UVCI:01:FR:DSQLGF8Y43FOKJR#1,b1231e6058eefb1157ff522762abbb5e",
        "URN:UVCI:01:FR:NIFA7VW835EA84E#2,174b5cc3dc875d1b410407560314f8a4",
        "URN:UVCI:01:FR:HM2LWPJE3DRRVF1#3,65c40e25968f137bd5eeb3e13c31ac01",
        "URN:UVCI:01:FR:E8IZWUK8K0YPRTK#4,4793441b009942bdd5b42c7c943c4e47",
        "URN:UVCI:01:FR:AZIPDJAZEIFHB23#5,c0886dc3083da0d810eeccaef91ad7e3",
        "URN:UVCI:01:FR:I6KFPIT0IBBDH26#6,a0dfdf9ffe307944ccd769e5d6ad258b",
        "urn:uvci:01:FR:LIFPYVGBHDOQ#B,43f60fdb4e15aca4633ede08c96f8080",
        "URN:UVCI:01:FR:Y4996KBJ4Q3Q#:,98e40b68a22f8b2cbd86a0382b1da0fd"
    )
    fun can_build_uvci_hash(identifier: String, expectedEncodedIdentifier: String) {
        val dcc = pfizer1_1VaccinationCertificate(today(minus = 39.days))
            .withVaccinationIdentifier(identifier)

        assertThat(dcc.uvciHash())
            .isEqualTo(expectedEncodedIdentifier)
    }

    @ParameterizedTest
    @CsvSource(
        "HC1:6BFOXN%TSMAHN-H6SKJPT.-7G2TZ97KW8SFBZEJN*2XG4W45BZU:X9NPS +OAN9I6T5XH-G2%E3EV4*2D5GPB+2SEB7:I/2DY735LBJU09OVBNN\$T8YINKHF0GVBF5EV499TL EX/KR96/-KKTCY73JC3KD3LWTPAJ3ZCD6L8YV3SVR/S09T./0LWTKD33238J3HKB+T4A+2XEN QT QTHC31M3DG3F.456L X4CZKHKB-43.E3KD3OAJ5%IWZKRA38M7323Q05.\$SBA0+Q6A\$Q*P6UQK*%NH\$RSC9EGF:HOK4M33KU2M NN9:H*+7 A8ND19NTNC32IH8OT4IJGG11FD:W4UU2Q5AU-69NT2V457U9LKJJK0/FWKP/HLIJLKNF8JF172HCW9/46G1F4NE\$I6 AK69\$IOABSM39LOEJBM3%PCPSHHMDUR-3KN%VZ1IH2J5JH1SS8H6B0OO3QP*4RZ5 82T/M:LL%KL9BO31J+VT\$JO:WE,6a6d8b5880637b551ee6b29ae17dc9df",
        "HC1:NCF:603A0T9WTWGSLKC 4F79\$7T/2FCF6B90WAB0XK3JCSGA2F35OPUKI2F3S1QUD2Y50.FK8ZKO/EZKEZ967L6C56..DV%DZJC0/D5UA QELPCG/DYUCHY83UAGVC*JCNF6F463W5KF6VF6IECSHG4KCD3DX47B46IL6646H*6MWEWJDA6A:961A6Q47EM6B\$DFOC0R63KCZPCNF6OF63W5\$Q6+96/SA5R6NF61G73564KC*KETF6A46.96646B565WEC.D1\$CKWEDZC6VCS446\$C4WEUPC3JCUIA+ED\$.EF\$DMWE8\$CBJEMVCB445\$CBWER.CGPC4WEOPCE8FHZA1+9LZAZM81G72A62+8.G74R6LA76N96H9.BAQBA-G7498S*5HD7:A1ZL32SL1LL1BF4NFGSCGNOT65SC6M5FDEC5HECC6V7I\$CE%AM099J*PN:7MN2V.LY-P5\$0F8RQ-U9O2II4ESHMMIBSSI0,95453462b5b4902bc96b8b781028742d",
        "HC1:NCFOXN%TSMAHN-H*UKXRD NR07OXPG1V8ELBZEJN*2EF4:8TXRQ:X9-MNWIJCV4*XUA2PSGH.+H\$NI4L6BP2%UG/YL WO*Z7ON13:LHNGA:V6%H8SOAT1Z0W:J01/RL-JL/N:PI0EGLS47%S7Y48YIZ734234LTZABFG3J%4*%2797\$E7C-4D-4HRVUMNMD3323623P131X45K1*TB3:U-1VVS1UU15%HTNIPPAAMI PQVW5/O16%HAT1Z%PHOP+MMBT16Y5+Z9XV7G+SB.V Q5NN9BOAJH0PK99Q9E\$BDZI69JMIAXCI83BLZI19JC%EA*C0LS9LN KP8EFGDD.FKZ7W9C4.LTW2L-QDLAOBQDUDBQEAJJKKKMWC8:P8ETOECWR:DLV5JZDON9ZBFGIKKVQVWN5E9/Z4OSDLWHD49V/CG6EDCE%QV%/OS%B9LH-3MH+R+2AZ-JF8JXCN35F*0TWPVIXUK*SFB0*0JF5, fb69d922cfbacf33e514dbfd429b7391"
    )
    fun can_build_signature_hash(dcc: String, expectedHash: String) {
        val verificationResult = VerificationResult()
        val encoded = DefaultContextIdentifierService("HC1:").decode(dcc, verificationResult)
        val compressed = DefaultBase45Service().decode(encoded, verificationResult)
        val cose = DefaultCompressorService().decode(compressed, verificationResult)
        val hashFromSignature = DccSignatureHash(cose).buildSignatureHash()

        assertThat(hashFromSignature).isEqualTo(expectedHash)
    }
}
