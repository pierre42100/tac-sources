package fr.gouv.tac.dcclight.test.matchers

import com.spotify.hamcrest.jackson.IsJsonObject.jsonObject
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.equalTo
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows

class JsonObjectMatcherTest {

    @Test
    fun can_transform_a_ByteArray_and_match_successfuly() {
        val jsonByteArray = """
            {
              "ver": "1.3.0"
            }
        """.trimIndent()
        assertThat(jsonByteArray.toByteArray(), isJson(jsonObject()))
    }

    @Test
    fun can_detect_a_ByteArray_doesnt_contain_a_json_string() {
        val error: AssertionError = assertThrows {
            assertThat("{ ver: INVALID JSON }".toByteArray(), isJson(jsonObject()))
        }
        assertThat(
            error.message?.replace("\r", ""),
            equalTo(
                """

            Expected: a json object containing {
            }
                 but: was not a valid json "{ ver: INVALID JSON }" because of parse error Unexpected character ('v' (code 118)): was expecting double-quote to start field name
             at [Source: (byte[])"{ ver: INVALID JSON }"; line: 1, column: 4]
                """.trimIndent()
            )
        )
    }
}
