package fr.gouv.tac.dcclight.test.extension

import java.util.Base64.getDecoder
import java.util.Base64.getEncoder

fun String.base64Decode(): ByteArray = getDecoder().decode(this)
fun ByteArray.base64Encode(): String = getEncoder().encodeToString(this)
