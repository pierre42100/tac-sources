package fr.gouv.tac.dcclight.test.extension

import ehn.techiop.hcert.kotlin.data.GreenCertificate
import fr.gouv.tac.dcclight.extension.getRecoveryStatement
import fr.gouv.tac.dcclight.extension.getTest
import fr.gouv.tac.dcclight.extension.getVaccination
import fr.gouv.tac.dcclight.test.entryOf
import kotlinx.datetime.Instant
import kotlinx.datetime.LocalDate

fun GreenCertificate.withVaccinationVaccine(vaccine: String) = this.copy(
    vaccinations = arrayOf(
        this.getVaccination().copy(vaccine = entryOf(vaccine))
    )
)

fun GreenCertificate.withVaccinationMedicinalProduct(medicinalProduct: String) = this.copy(
    vaccinations = arrayOf(
        this.getVaccination().copy(medicinalProduct = entryOf(medicinalProduct))
    )
)

fun GreenCertificate.withVaccinationManufacturer(manufacturer: String) = this.copy(
    vaccinations = arrayOf(
        this.getVaccination().copy(authorizationHolder = entryOf(manufacturer))
    )
)

fun GreenCertificate.withVaccinationDoseTotalNumber(doseTotalNumber: Int) = this.copy(
    vaccinations = arrayOf(
        this.getVaccination().copy(doseTotalNumber = doseTotalNumber)
    )
)

fun GreenCertificate.withVaccinationDoseNumber(doseNumber: Int) = this.copy(
    vaccinations = arrayOf(
        this.getVaccination().copy(doseNumber = doseNumber)
    )
)

fun GreenCertificate.withVaccinationCountry(country: String) = this.copy(
    vaccinations = arrayOf(
        this.getVaccination().copy(country = country)
    )
)

fun GreenCertificate.withVaccinationDate(date: String) = this.copy(
    vaccinations = arrayOf(
        this.getVaccination().copy(date = LocalDate.parse(date))
    )
)

fun GreenCertificate.withVaccinationTarget(target: String) = this.copy(
    vaccinations = arrayOf(
        this.getVaccination().copy(target = entryOf(target))
    )
)

fun GreenCertificate.withVaccinationIssuer(issuer: String) = this.copy(
    vaccinations = arrayOf(
        this.getVaccination().copy(certificateIssuer = issuer)
    )
)

fun GreenCertificate.withVaccinationIdentifier(identifier: String) = this.copy(
    vaccinations = arrayOf(
        this.getVaccination().copy(certificateIdentifier = identifier)
    )
)

fun GreenCertificate.withTestTarget(target: String) = this.copy(
    tests = arrayOf(
        this.getTest().copy(target = entryOf(target))
    )
)

fun GreenCertificate.withTestType(type: String) = this.copy(
    tests = arrayOf(
        this.getTest().copy(type = entryOf(type))
    )
)

fun GreenCertificate.withTestResult(testResult: String) = this.copy(
    tests = arrayOf(
        this.getTest().copy(resultPositive = entryOf(testResult))
    )
)

fun GreenCertificate.withTestNameNaa(nameNaa: String?) = this.copy(
    tests = arrayOf(
        this.getTest().copy(nameNaa = nameNaa?.let { nameNaa })
    )
)

fun GreenCertificate.withTestDateTimeSample(dateTime: Instant) = this.copy(
    tests = arrayOf(
        this.getTest().copy(dateTimeSample = dateTime)
    )
)

fun GreenCertificate.withTestDateTimeResult(dateTime: Instant) = this.copy(
    tests = arrayOf(
        this.getTest().copy(dateTimeResult = dateTime)
    )
)

fun GreenCertificate.withTestNameRat(nameRat: String?) = this.copy(
    tests = arrayOf(
        this.getTest().copy(nameRat = nameRat?.let { entryOf(nameRat) })
    )
)

fun GreenCertificate.withTestFacility(facility: String?) = this.copy(
    tests = arrayOf(
        this.getTest().copy(testFacility = facility)
    )
)

fun GreenCertificate.withTestIssuer(issuer: String) = this.copy(
    tests = arrayOf(
        this.getTest().copy(certificateIssuer = issuer)
    )
)

fun GreenCertificate.withTestIdentifier(identifier: String) = this.copy(
    tests = arrayOf(
        this.getTest().copy(certificateIdentifier = identifier)
    )
)

fun GreenCertificate.withRecoveryCountry(country: String) = this.copy(
    recoveryStatements = arrayOf(
        this.getRecoveryStatement().copy(country = country)
    )
)

fun GreenCertificate.withRecoveryTarget(target: String) = this.copy(
    recoveryStatements = arrayOf(
        this.getRecoveryStatement().copy(target = entryOf(target))
    )
)

fun GreenCertificate.withTestCountry(country: String) = this.copy(
    tests = arrayOf(
        this.getTest().copy(country = country)
    )
)

fun GreenCertificate.withGivenName(name: String?) = this.copy(
    subject = this.subject.copy(givenName = name)
)

fun GreenCertificate.withGivenNameTransliterated(name: String?) = this.copy(
    subject = this.subject.copy(givenNameTransliterated = name)
)

fun GreenCertificate.withFamilyName(name: String?) = this.copy(
    subject = this.subject.copy(familyName = name)
)

fun GreenCertificate.withFamilyNameTransliterated(name: String) = this.copy(
    subject = this.subject.copy(familyNameTransliterated = name)
)

fun GreenCertificate.withRecoveryValidUntil(date: LocalDate) = this.copy(
    recoveryStatements = arrayOf(
        this.getRecoveryStatement().copy(certificateValidUntil = date)
    )
)

fun GreenCertificate.withRecoveryValidFrom(date: LocalDate) = this.copy(
    recoveryStatements = arrayOf(
        this.getRecoveryStatement().copy(certificateValidFrom = date)
    )
)

fun GreenCertificate.withRecoveryIdentifier(identifier: String) = this.copy(
    recoveryStatements = arrayOf(
        this.getRecoveryStatement().copy(certificateIdentifier = identifier)
    )
)

fun GreenCertificate.withRecoveryDateOfFirstPositiveTestResult(date: LocalDate) = this.copy(
    recoveryStatements = arrayOf(
        this.getRecoveryStatement().copy(dateOfFirstPositiveTestResult = date)
    )
)

fun GreenCertificate.withRecoveryIssuer(issuer: String) = this.copy(
    recoveryStatements = arrayOf(
        this.getRecoveryStatement().copy(certificateIssuer = issuer)
    )
)

fun GreenCertificate.withDateOfBirth(date: LocalDate) = this.copy(
    dateOfBirthString = date.toString()
)
