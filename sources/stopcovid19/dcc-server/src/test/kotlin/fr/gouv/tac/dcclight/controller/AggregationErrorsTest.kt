package fr.gouv.tac.dcclight.controller

import fr.gouv.tac.dcclight.api.model.DccAggregationRequest
import fr.gouv.tac.dcclight.test.DccSigningKeysManager.Companion.DCCLIGHT_SERVER_KEYS
import fr.gouv.tac.dcclight.test.DccSigningKeysManager.Companion.DCC_TEST_INPUT_KEYS
import fr.gouv.tac.dcclight.test.IntegrationTest
import fr.gouv.tac.dcclight.test.NettyManager.Companion.givenCertificatesTrustListContains
import fr.gouv.tac.dcclight.test.NettyManager.Companion.givenCountryUvciBlockListContains
import fr.gouv.tac.dcclight.test.NettyManager.Companion.givenSignatureBlocklistContains
import fr.gouv.tac.dcclight.test.NettyManager.Companion.givenUvciBlocklistContains
import fr.gouv.tac.dcclight.test.RestAssuredManager.Companion.When
import fr.gouv.tac.dcclight.test.RestAssuredManager.Companion.givenBaseHeaders
import fr.gouv.tac.dcclight.test.TransportEncryptionKeysManager.Companion.CLIENT_PUBLIC_KEY_BASE64
import fr.gouv.tac.dcclight.test.eu.DccCryptographer.Companion.encodeToDcc
import fr.gouv.tac.dcclight.test.eu.DccCryptographer.Companion.encodeToDccAndEncrypt
import fr.gouv.tac.dcclight.test.eu.DccCryptographer.Companion.encryptDcc
import fr.gouv.tac.dcclight.test.extension.withRecoveryCountry
import fr.gouv.tac.dcclight.test.extension.withRecoveryTarget
import fr.gouv.tac.dcclight.test.extension.withTestCountry
import fr.gouv.tac.dcclight.test.extension.withTestIdentifier
import fr.gouv.tac.dcclight.test.extension.withTestIssuer
import fr.gouv.tac.dcclight.test.extension.withTestNameRat
import fr.gouv.tac.dcclight.test.extension.withVaccinationIdentifier
import fr.gouv.tac.dcclight.test.extension.withVaccinationIssuer
import fr.gouv.tac.dcclight.test.extension.withVaccinationManufacturer
import fr.gouv.tac.dcclight.test.extension.withVaccinationMedicinalProduct
import fr.gouv.tac.dcclight.test.extension.withVaccinationVaccine
import fr.gouv.tac.dcclight.test.negativeAntigenicTestCertificate
import fr.gouv.tac.dcclight.test.pfizer1_1VaccinationCertificate
import fr.gouv.tac.dcclight.test.pfizer2_2VaccinationCertificate
import fr.gouv.tac.dcclight.test.positiveAntigenicTestCertificate
import fr.gouv.tac.dcclight.test.positivePcrTestCertificate
import fr.gouv.tac.dcclight.test.recoveryStatementCertificate
import fr.gouv.tac.dcclight.test.today
import kotlinx.datetime.Clock.System.now
import org.hamcrest.Matchers.equalTo
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.ValueSource
import org.springframework.http.HttpStatus.BAD_REQUEST
import kotlin.time.Duration.Companion.days

@IntegrationTest
class AggregationErrorsTest {

    private val now = now()

    @Test
    fun double_vaccination_input_does_not_match_when_second_vaccination_date_is_too_close_to_first_according_to_rule() {

        val vaccinationDcc = pfizer2_2VaccinationCertificate(today(minus = 50.days))

        val vaccination2Dcc = pfizer1_1VaccinationCertificate(today(minus = 25.days))

        givenBaseHeaders()
            .body(
                DccAggregationRequest(
                    publicKey = CLIENT_PUBLIC_KEY_BASE64,
                    certificates = listOf(
                        encodeToDccAndEncrypt(vaccinationDcc),
                        encodeToDccAndEncrypt(vaccination2Dcc)
                    ),
                )
            )

            .When()
            .post("/api/v1/aggregate")

            .then()
            .statusCode(BAD_REQUEST.value())
            .body("error", equalTo(BAD_REQUEST.reasonPhrase))
            .body("message", equalTo("Request body contains invalid attributes"))
            .body("errors[0].field", equalTo("certificates"))
            .body("errors[0].code", equalTo("INCOMPATIBLE_DCC_COMBINATION"))
            .body("errors[0].message", equalTo("Provided certificates are not compatible with any operation"))
            .body("errors.size()", equalTo(1))
    }

    @Test
    fun double_vaccination_input_does_not_match_when_vaccination_contains_forbidden_vaccine_type() {

        val vaccinationDcc = pfizer2_2VaccinationCertificate(today(minus = 50.days))
            .withVaccinationVaccine("FORBIDDEN")

        val vaccination2Dcc = pfizer1_1VaccinationCertificate(today(minus = 20.days))

        givenBaseHeaders()
            .body(
                DccAggregationRequest(
                    publicKey = CLIENT_PUBLIC_KEY_BASE64,
                    certificates = listOf(
                        encodeToDccAndEncrypt(vaccinationDcc),
                        encodeToDccAndEncrypt(vaccination2Dcc)
                    ),
                )
            )

            .When()
            .post("/api/v1/aggregate")

            .then()
            .statusCode(BAD_REQUEST.value())
            .body("error", equalTo(BAD_REQUEST.reasonPhrase))
            .body("message", equalTo("Request body contains invalid attributes"))
            .body("errors[0].field", equalTo("certificates"))
            .body("errors[0].code", equalTo("INCOMPATIBLE_DCC_COMBINATION"))
            .body("errors[0].message", equalTo("Provided certificates are not compatible with any operation"))
            .body("errors.size()", equalTo(1))
    }

    @Test
    fun double_vaccination_input_does_not_match_when_second_entry_dose_number_does_not_match_rule() {

        val vaccinationDcc = pfizer2_2VaccinationCertificate(today(minus = 50.days))
        val vaccination2Dcc = pfizer2_2VaccinationCertificate(today(minus = 20.days))

        givenBaseHeaders()
            .body(
                DccAggregationRequest(
                    publicKey = CLIENT_PUBLIC_KEY_BASE64,
                    certificates = listOf(
                        encodeToDccAndEncrypt(vaccinationDcc),
                        encodeToDccAndEncrypt(vaccination2Dcc)
                    ),
                )
            )

            .When()
            .post("/api/v1/aggregate")

            .then()
            .statusCode(BAD_REQUEST.value())
            .body("error", equalTo(BAD_REQUEST.reasonPhrase))
            .body("message", equalTo("Request body contains invalid attributes"))
            .body("errors[0].field", equalTo("certificates"))
            .body("errors[0].code", equalTo("INCOMPATIBLE_DCC_COMBINATION"))
            .body("errors[0].message", equalTo("Provided certificates are not compatible with any operation"))
            .body("errors.size()", equalTo(1))
    }

    @Test
    fun double_vaccination_input_does_not_match_when_one_entry_has_forbidden_medicinal_product() {

        val vaccinationDcc = pfizer2_2VaccinationCertificate(today(minus = 50.days))

        val vaccination2Dcc = pfizer2_2VaccinationCertificate(today(minus = 20.days))
            .withVaccinationMedicinalProduct("FORBIDDEN")

        givenBaseHeaders()
            .body(
                DccAggregationRequest(
                    publicKey = CLIENT_PUBLIC_KEY_BASE64,
                    certificates = listOf(
                        encodeToDccAndEncrypt(vaccinationDcc),
                        encodeToDccAndEncrypt(vaccination2Dcc)
                    ),
                )
            )

            .When()
            .post("/api/v1/aggregate")

            .then()
            .statusCode(BAD_REQUEST.value())
            .body("error", equalTo(BAD_REQUEST.reasonPhrase))
            .body("message", equalTo("Request body contains invalid attributes"))
            .body("errors[0].field", equalTo("certificates"))
            .body("errors[0].code", equalTo("INCOMPATIBLE_DCC_COMBINATION"))
            .body("errors[0].message", equalTo("Provided certificates are not compatible with any operation"))
            .body("errors.size()", equalTo(1))
    }

    @Test
    fun double_vaccination_input_does_not_match_when_one_entry_has_forbidden_authorization_holder() {

        val vaccinationDcc = pfizer2_2VaccinationCertificate(today(minus = 50.days))

        val forbiddenManufacturerVaccinationDcc = pfizer2_2VaccinationCertificate(today(minus = 20.days))
            .withVaccinationManufacturer("FORBIDDEN")

        givenBaseHeaders()
            .body(
                DccAggregationRequest(
                    publicKey = CLIENT_PUBLIC_KEY_BASE64,
                    certificates = listOf(
                        encodeToDccAndEncrypt(vaccinationDcc),
                        encodeToDccAndEncrypt(forbiddenManufacturerVaccinationDcc)
                    ),
                )
            )

            .When()
            .post("/api/v1/aggregate")

            .then()
            .statusCode(BAD_REQUEST.value())
            .body("error", equalTo(BAD_REQUEST.reasonPhrase))
            .body("message", equalTo("Request body contains invalid attributes"))
            .body("errors[0].field", equalTo("certificates"))
            .body("errors[0].code", equalTo("INCOMPATIBLE_DCC_COMBINATION"))
            .body("errors[0].message", equalTo("Provided certificates are not compatible with any operation"))
            .body("errors.size()", equalTo(1))
    }

    @Test
    fun vaccination_and_test_input_does_not_match_when_vaccination_date_is_too_close_to_test_date() {
        // target rule name: "Positive antigenic test + vaccination"

        val testDcc = positiveAntigenicTestCertificate(now().minus(60.days))
        val vaccinationDcc = pfizer1_1VaccinationCertificate(today(minus = 30.days))

        givenBaseHeaders()
            .body(
                DccAggregationRequest(
                    publicKey = CLIENT_PUBLIC_KEY_BASE64,
                    certificates = listOf(
                        encodeToDccAndEncrypt(vaccinationDcc),
                        encodeToDccAndEncrypt(testDcc)
                    ),
                )
            )

            .When()
            .post("/api/v1/aggregate")

            .then()
            .statusCode(BAD_REQUEST.value())
            .body("error", equalTo(BAD_REQUEST.reasonPhrase))
            .body("message", equalTo("Request body contains invalid attributes"))
            .body("errors[0].field", equalTo("certificates"))
            .body("errors[0].code", equalTo("INCOMPATIBLE_DCC_COMBINATION"))
            .body("errors[0].message", equalTo("Provided certificates are not compatible with any operation"))
            .body("errors.size()", equalTo(1))
    }

    @Test
    fun vaccination_and_test_input_does_not_match_when_negative_test() {
        // target rule name: "Positive antigenic test + vaccination"

        val testDcc = negativeAntigenicTestCertificate(now().minus(60.days))
        val vaccinationDcc = pfizer1_1VaccinationCertificate(today(minus = 7.days))

        givenBaseHeaders()
            .body(
                DccAggregationRequest(
                    publicKey = CLIENT_PUBLIC_KEY_BASE64,
                    certificates = listOf(
                        encodeToDccAndEncrypt(vaccinationDcc),
                        encodeToDccAndEncrypt(testDcc)
                    ),
                )
            )

            .When()
            .post("/api/v1/aggregate")

            .then()
            .statusCode(BAD_REQUEST.value())
            .body("error", equalTo(BAD_REQUEST.reasonPhrase))
            .body("message", equalTo("Request body contains invalid attributes"))
            .body("errors[0].field", equalTo("certificates"))
            .body("errors[0].code", equalTo("INCOMPATIBLE_DCC_COMBINATION"))
            .body("errors[0].message", equalTo("Provided certificates are not compatible with any operation"))
            .body("errors.size()", equalTo(1))
    }

    @Test
    fun vaccination_and_test_input_do_not_match_when_antigenic_without_nameRat_manufacturer_name() {
        // target rule name: "Positive antigenic test + vaccination"

        val testDcc = positiveAntigenicTestCertificate(now().minus(60.days)).withTestNameRat(null)

        val vaccinationDcc = pfizer1_1VaccinationCertificate(today(minus = 7.days))

        givenBaseHeaders()
            .body(
                DccAggregationRequest(
                    publicKey = CLIENT_PUBLIC_KEY_BASE64,
                    certificates = listOf(
                        encodeToDccAndEncrypt(vaccinationDcc),
                        encodeToDccAndEncrypt(testDcc)
                    ),
                )
            )

            .When()
            .post("/api/v1/aggregate")

            .then()
            .statusCode(BAD_REQUEST.value())
            .body("error", equalTo(BAD_REQUEST.reasonPhrase))
            .body("message", equalTo("Request body contains invalid attributes"))
            .body("errors[0].field", equalTo("certificates"))
            .body("errors[0].code", equalTo("INCOMPATIBLE_DCC_COMBINATION"))
            .body("errors[0].message", equalTo("Provided certificates are not compatible with any operation"))
            .body("errors.size()", equalTo(1))
    }

    @Test
    fun vaccination_and_test_input_do_not_match_when_test_with_empty_certificateIssuer() {
        // target rule name: "Positive antigenic test + vaccination"

        val testDcc = positiveAntigenicTestCertificate(now().minus(60.days)).withTestIssuer("")
        val vaccinationDcc = pfizer1_1VaccinationCertificate(today(minus = 7.days))

        givenBaseHeaders()
            .body(
                DccAggregationRequest(
                    publicKey = CLIENT_PUBLIC_KEY_BASE64,
                    certificates = listOf(
                        encodeToDccAndEncrypt(vaccinationDcc),
                        encodeToDccAndEncrypt(testDcc)
                    ),
                )
            )

            .When()
            .post("/api/v1/aggregate")

            .then()
            .statusCode(BAD_REQUEST.value())
            .body("error", equalTo(BAD_REQUEST.reasonPhrase))
            .body("message", equalTo("Request body contains invalid attributes"))
            .body("errors[0].field", equalTo("certificates[1]"))
            .body("errors[0].code", equalTo("MISSING_CERTIFICATE_ISSUER"))
            .body("errors[0].message", equalTo("Missing certificate issuer"))
            .body("errors.size()", equalTo(1))
    }

    @Test
    fun vaccination_and_test_input_do_not_match_when_test_with_empty_certificateIdentifier() {
        // target rule name: "Positive antigenic test + vaccination"

        val testDcc = positiveAntigenicTestCertificate(now().minus(60.days)).withTestIdentifier("")

        val vaccinationDcc = pfizer1_1VaccinationCertificate(today(minus = 7.days))

        givenBaseHeaders()
            .body(
                DccAggregationRequest(
                    publicKey = CLIENT_PUBLIC_KEY_BASE64,
                    certificates = listOf(
                        encodeToDccAndEncrypt(vaccinationDcc),
                        encodeToDccAndEncrypt(testDcc)
                    ),
                )
            )

            .When()
            .post("/api/v1/aggregate")

            .then()
            .statusCode(BAD_REQUEST.value())
            .body("error", equalTo(BAD_REQUEST.reasonPhrase))
            .body("message", equalTo("Request body contains invalid attributes"))
            .body("errors[0].field", equalTo("certificates[1]"))
            .body("errors[0].code", equalTo("MISSING_CERTIFICATE_IDENTIFIER"))
            .body("errors[0].message", equalTo("Missing certificate identifier"))
            .body("errors.size()", equalTo(1))
    }

    @Test
    fun vaccination_and_test_input_do_not_match_when_vaccination_with_empty_certificateIdentifier() {
        // target rule name: "Positive antigenic test + vaccination"

        val testDcc = positiveAntigenicTestCertificate(now().minus(60.days))

        val vaccinationDcc = pfizer1_1VaccinationCertificate(today(minus = 7.days))
            .withVaccinationIdentifier("")

        givenBaseHeaders()
            .body(
                DccAggregationRequest(
                    publicKey = CLIENT_PUBLIC_KEY_BASE64,
                    certificates = listOf(
                        encodeToDccAndEncrypt(vaccinationDcc),
                        encodeToDccAndEncrypt(testDcc)
                    ),
                )
            )

            .When()
            .post("/api/v1/aggregate")

            .then()
            .statusCode(BAD_REQUEST.value())
            .body("error", equalTo(BAD_REQUEST.reasonPhrase))
            .body("message", equalTo("Request body contains invalid attributes"))
            .body("errors[0].field", equalTo("certificates[0]"))
            .body("errors[0].code", equalTo("MISSING_CERTIFICATE_IDENTIFIER"))
            .body("errors[0].message", equalTo("Missing certificate identifier"))
            .body("errors.size()", equalTo(1))
    }

    @Test
    fun double_test_and_vaccination_does_not_match_because_isRuleActive_set_to_false() {
        // target rule name: "Positive test + Positive antigenic test + vaccination"

        val testDcc = positiveAntigenicTestCertificate(now.minus(60.days))
        val testDcc2 = positiveAntigenicTestCertificate(now.minus(56.days))
        val vaccinationDcc = pfizer1_1VaccinationCertificate(today(minus = 7.days))

        givenBaseHeaders()
            .body(
                DccAggregationRequest(
                    publicKey = CLIENT_PUBLIC_KEY_BASE64,
                    certificates = listOf(
                        encodeToDccAndEncrypt(vaccinationDcc),
                        encodeToDccAndEncrypt(testDcc),
                        encodeToDccAndEncrypt(testDcc2)
                    ),
                )
            )

            .When()
            .post("/api/v1/aggregate")

            .then()
            .statusCode(BAD_REQUEST.value())
            .body("error", equalTo(BAD_REQUEST.reasonPhrase))
            .body("message", equalTo("Request body contains invalid attributes"))
            .body("errors[0].field", equalTo("certificates"))
            .body("errors[0].code", equalTo("INCOMPATIBLE_DCC_COMBINATION"))
            .body("errors[0].message", equalTo("Provided certificates are not compatible with any operation"))
            .body("errors.size()", equalTo(1))
    }

    @Test
    fun vaccination_and_test_input_do_not_match_when_vaccination_with_empty_certificateIssuer() {
        // target rule name: "Positive antigenic test + vaccination"

        val testDcc = positiveAntigenicTestCertificate(now().minus(60.days))

        val vaccinationDcc = pfizer1_1VaccinationCertificate(today(minus = 53.days))
            .withVaccinationIssuer("")

        givenBaseHeaders()
            .body(
                DccAggregationRequest(
                    publicKey = CLIENT_PUBLIC_KEY_BASE64,
                    certificates = listOf(
                        encodeToDccAndEncrypt(vaccinationDcc),
                        encodeToDccAndEncrypt(testDcc)
                    ),
                )
            )

            .When()
            .post("/api/v1/aggregate")

            .then()
            .statusCode(BAD_REQUEST.value())
            .body("error", equalTo(BAD_REQUEST.reasonPhrase))
            .body("message", equalTo("Request body contains invalid attributes"))
            .body("errors[0].field", equalTo("certificates[0]"))
            .body("errors[0].code", equalTo("MISSING_CERTIFICATE_ISSUER"))
            .body("errors[0].message", equalTo("Missing certificate issuer"))
            .body("errors.size()", equalTo(1))
    }

    @Test
    fun vaccination_and_test_input_does_not_match_when_test_type_PCR() {
        // target rule name: "Positive antigenic test + vaccination"

        val vaccinationDcc = pfizer1_1VaccinationCertificate(today(minus = 53.days))

        val testDcc = positivePcrTestCertificate(now.minus(60.days))

        givenBaseHeaders()
            .body(
                DccAggregationRequest(
                    publicKey = CLIENT_PUBLIC_KEY_BASE64,
                    certificates = listOf(
                        encodeToDccAndEncrypt(vaccinationDcc),
                        encodeToDccAndEncrypt(testDcc)
                    ),
                )
            )

            .When()
            .post("/api/v1/aggregate")

            .then()
            .statusCode(BAD_REQUEST.value())
            .body("error", equalTo(BAD_REQUEST.reasonPhrase))
            .body("message", equalTo("Request body contains invalid attributes"))
            .body("errors[0].field", equalTo("certificates"))
            .body("errors[0].code", equalTo("INCOMPATIBLE_DCC_COMBINATION"))
            .body("errors[0].message", equalTo("Provided certificates are not compatible with any operation"))
            .body("errors.size()", equalTo(1))
    }

    @Test
    fun double_vaccination_input_does_not_match_when_one_entry_is_in_the_future_more_than_1_day_or_24_hours() {

        val vaccinationDcc = pfizer2_2VaccinationCertificate(today(minus = 20.days))
        val vaccination2Dcc = pfizer1_1VaccinationCertificate(today(plus = 10.days))

        givenBaseHeaders()
            .body(
                DccAggregationRequest(
                    publicKey = CLIENT_PUBLIC_KEY_BASE64,
                    certificates = listOf(
                        encodeToDccAndEncrypt(vaccinationDcc),
                        encodeToDccAndEncrypt(vaccination2Dcc)
                    ),
                )
            )

            .When()
            .post("/api/v1/aggregate")

            .then()
            .statusCode(BAD_REQUEST.value())
            .body("error", equalTo(BAD_REQUEST.reasonPhrase))
            .body("message", equalTo("Request body contains invalid attributes"))
            .body("errors[0].field", equalTo("certificates[1]"))
            .body("errors[0].code", equalTo("CERTIFICATE_PROOF_DATE_TOO_FAR_IN_FUTURE"))
            .body("errors[0].message", equalTo("Certificate proof date is in more than 24 hours"))
            .body("errors.size()", equalTo(1))
    }

    @Test
    fun vaccination_and_test_combination_input_does_not_match_when_test_entry_is_in_the_future_more_than_1_day_or_24_hours() {
        // target rule name: "Vaccination + positive test"

        val vaccinationDcc = pfizer1_1VaccinationCertificate(today(minus = 47.days))

        val testDcc = positiveAntigenicTestCertificate(now.plus(6.days))

        givenBaseHeaders()
            .body(
                DccAggregationRequest(
                    publicKey = CLIENT_PUBLIC_KEY_BASE64,
                    certificates = listOf(
                        encodeToDccAndEncrypt(vaccinationDcc),
                        encodeToDccAndEncrypt(testDcc)
                    ),
                )
            )

            .When()
            .post("/api/v1/aggregate")

            .then()
            .statusCode(BAD_REQUEST.value())
            .body("error", equalTo(BAD_REQUEST.reasonPhrase))
            .body("message", equalTo("Request body contains invalid attributes"))
            .body("errors[0].field", equalTo("certificates[1]"))
            .body("errors[0].code", equalTo("CERTIFICATE_PROOF_DATE_TOO_FAR_IN_FUTURE"))
            .body("errors[0].message", equalTo("Certificate proof date is in more than 24 hours"))
            .body("errors.size()", equalTo(1))
    }

    @Test
    fun recovery_test_vaccination_input_does_not_match_when_recovery_statement_does_not_respect_rule_delays_between_dccs() {
        // target rule name: "recovery test and vaccination"

        val vaccinationDcc = pfizer1_1VaccinationCertificate(today(minus = 12.days))

        val recoveryDcc = recoveryStatementCertificate(now.minus(82.days))

        val testDcc = positiveAntigenicTestCertificate(now.minus(100.days))

        givenBaseHeaders()
            .body(
                DccAggregationRequest(
                    publicKey = CLIENT_PUBLIC_KEY_BASE64,
                    certificates = listOf(
                        encodeToDccAndEncrypt(vaccinationDcc),
                        encodeToDccAndEncrypt(recoveryDcc),
                        encodeToDccAndEncrypt(testDcc),
                    ),
                )
            )

            .When()
            .post("/api/v1/aggregate")

            .then()
            .statusCode(BAD_REQUEST.value())
            .body("error", equalTo(BAD_REQUEST.reasonPhrase))
            .body("message", equalTo("Request body contains invalid attributes"))
            .body("errors[0].field", equalTo("certificates"))
            .body("errors[0].code", equalTo("INCOMPATIBLE_DCC_COMBINATION"))
            .body("errors[0].message", equalTo("Provided certificates are not compatible with any operation"))
            .body("errors.size()", equalTo(1))
    }

    @Test
    fun recovery_test_vaccination_input_does_not_match_when_vaccination_does_not_respect_rule_delays_between_dccs() {
        // target rule name: "recovery test and vaccination"

        val testDcc = positiveAntigenicTestCertificate(now.minus(100.days))

        val recoveryDcc = recoveryStatementCertificate(now.minus(75.days))

        val vaccinationDcc = pfizer1_1VaccinationCertificate(today(minus = 25.days))

        givenBaseHeaders()
            .body(
                DccAggregationRequest(
                    publicKey = CLIENT_PUBLIC_KEY_BASE64,
                    certificates = listOf(
                        encodeToDccAndEncrypt(vaccinationDcc),
                        encodeToDccAndEncrypt(recoveryDcc),
                        encodeToDccAndEncrypt(testDcc),
                    ),
                )
            )

            .When()
            .post("/api/v1/aggregate")

            .then()
            .statusCode(BAD_REQUEST.value())
            .body("error", equalTo(BAD_REQUEST.reasonPhrase))
            .body("message", equalTo("Request body contains invalid attributes"))
            .body("errors[0].field", equalTo("certificates"))
            .body("errors[0].code", equalTo("INCOMPATIBLE_DCC_COMBINATION"))
            .body("errors[0].message", equalTo("Provided certificates are not compatible with any operation"))
            .body("errors.size()", equalTo(1))
    }

    @Test
    fun recovery_test_vaccination_input_does_not_match_when_recovery_statement_has_wrong_target() {
        // target rule name: "recovery test and vaccination"

        val vaccinationDcc = pfizer1_1VaccinationCertificate(today(minus = 5.days))

        val recoveryDcc = recoveryStatementCertificate(now.minus(75.days)).withRecoveryTarget("random_target")

        val testDcc = positiveAntigenicTestCertificate(now.minus(100.days))

        givenBaseHeaders()
            .body(
                DccAggregationRequest(
                    publicKey = CLIENT_PUBLIC_KEY_BASE64,
                    certificates = listOf(
                        encodeToDccAndEncrypt(vaccinationDcc),
                        encodeToDccAndEncrypt(recoveryDcc),
                        encodeToDccAndEncrypt(testDcc),
                    ),
                )
            )

            .When()
            .post("/api/v1/aggregate")

            .then()
            .statusCode(BAD_REQUEST.value())
            .body("error", equalTo(BAD_REQUEST.reasonPhrase))
            .body("message", equalTo("Request body contains invalid attributes"))
            .body("errors[0].field", equalTo("certificates"))
            .body("errors[0].code", equalTo("INCOMPATIBLE_DCC_COMBINATION"))
            .body("errors[0].message", equalTo("Provided certificates are not compatible with any operation"))
            .body("errors.size()", equalTo(1))
    }

    @Test
    fun recovery_test_vaccination_input_does_not_match_when_recovery_statement_does_not_have_allowlisted_country() {
        // target rule name: "recovery test and vaccination"

        val now = now()

        val vaccinationDcc = pfizer1_1VaccinationCertificate(today(minus = 30.days))

        val recoveryDcc = recoveryStatementCertificate(now.minus(75.days)).withRecoveryCountry("EN")

        val testDcc = positiveAntigenicTestCertificate(now.minus(100.days))

        givenBaseHeaders()
            .body(
                DccAggregationRequest(
                    publicKey = CLIENT_PUBLIC_KEY_BASE64,
                    certificates = listOf(
                        encodeToDccAndEncrypt(vaccinationDcc),
                        encodeToDccAndEncrypt(recoveryDcc),
                        encodeToDccAndEncrypt(testDcc),
                    ),
                )
            )

            .When()
            .post("/api/v1/aggregate")

            .then()
            .statusCode(BAD_REQUEST.value())
            .body("error", equalTo(BAD_REQUEST.reasonPhrase))
            .body("message", equalTo("Request body contains invalid attributes"))
            .body("errors[0].field", equalTo("certificates"))
            .body("errors[0].code", equalTo("INCOMPATIBLE_DCC_COMBINATION"))
            .body("errors[0].message", equalTo("Provided certificates are not compatible with any operation"))
            .body("errors.size()", equalTo(1))
    }

    @Test
    fun recovery_test_vaccination_input_does_not_match_when_test_has_blocklisted_country() {
        // target rule name: "recovery test and vaccination"

        val now = now()

        val testDcc = positiveAntigenicTestCertificate(now.minus(100.days)).withTestCountry("SP")
        val recoveryDcc = recoveryStatementCertificate(now.minus(75.days)).withRecoveryCountry("UK")
        val vaccinationDcc = pfizer1_1VaccinationCertificate(today(minus = 5.days))

        givenBaseHeaders()
            .body(
                DccAggregationRequest(
                    publicKey = CLIENT_PUBLIC_KEY_BASE64,
                    certificates = listOf(
                        encodeToDccAndEncrypt(testDcc),
                        encodeToDccAndEncrypt(recoveryDcc),
                        encodeToDccAndEncrypt(vaccinationDcc),
                    ),
                )
            )

            .When()
            .post("/api/v1/aggregate")

            .then()
            .statusCode(BAD_REQUEST.value())
            .body("error", equalTo(BAD_REQUEST.reasonPhrase))
            .body("message", equalTo("Request body contains invalid attributes"))
            .body("errors[0].field", equalTo("certificates"))
            .body("errors[0].code", equalTo("INCOMPATIBLE_DCC_COMBINATION"))
            .body("errors[0].message", equalTo("Provided certificates are not compatible with any operation"))
            .body("errors.size()", equalTo(1))
    }

    @Test
    fun double_vaccination_input_does_not_match_when_one_vaccination_contains_unsupported_certificateIdentifier_prefix() {
        // target rule name: "Double vaccination"

        val vaccinationDcc = pfizer2_2VaccinationCertificate(today(minus = 100.days))

        val vaccination2Dcc =
            pfizer1_1VaccinationCertificate(today(minus = 39.days)).withVaccinationIdentifier("URN:UVCI:01:FR:DGSAG/chose")

        givenBaseHeaders()
            .body(
                DccAggregationRequest(
                    publicKey = CLIENT_PUBLIC_KEY_BASE64,
                    certificates = listOf(
                        encodeToDccAndEncrypt(vaccinationDcc),
                        encodeToDccAndEncrypt(vaccination2Dcc)
                    ),
                )
            )

            .When()
            .post("/api/v1/aggregate")

            .then()
            .statusCode(BAD_REQUEST.value())
            .body("error", equalTo(BAD_REQUEST.reasonPhrase))
            .body("message", equalTo("Request body contains invalid attributes"))
            .body("errors[0].field", equalTo("certificates[1]"))
            .body("errors[0].code", equalTo("AGGREGATE_CERTIFICATE_NOT_ALLOWED"))
            .body("errors[0].message", equalTo("Certificate comes from a previous aggregation"))
            .body("errors.size()", equalTo(1))
    }

    @Test
    fun double_vaccination_input_does_not_match_when_one_vaccination_dcc_has_blocklisted_country_identifier_hash() {
        // target rule name: "Double vaccination"

        val vaccinationDcc = pfizer1_1VaccinationCertificate(today(minus = 100.days))

        val vaccination2Dcc = pfizer1_1VaccinationCertificate(today(minus = 39.days))
            .withVaccinationIdentifier("URN:UVCI:01:FR:RE4AFH9LS8ANCTW#Z")

        givenCountryUvciBlockListContains(vaccination2Dcc)

        givenBaseHeaders()
            .body(
                DccAggregationRequest(
                    publicKey = CLIENT_PUBLIC_KEY_BASE64,
                    certificates = listOf(
                        encodeToDccAndEncrypt(vaccinationDcc),
                        encodeToDccAndEncrypt(vaccination2Dcc)
                    ),
                )
            )

            .When()
            .post("/api/v1/aggregate")

            .then()
            .statusCode(BAD_REQUEST.value())
            .body("error", equalTo(BAD_REQUEST.reasonPhrase))
            .body("message", equalTo("Request body contains invalid attributes"))
            .body("errors[0].field", equalTo("certificates[1]"))
            .body("errors[0].code", equalTo("BLOCKLISTED_CERTIFICATE"))
            .body("errors[0].message", equalTo("Certificate is blocklisted"))
            .body("errors.size()", equalTo(1))
    }

    @Test
    fun double_vaccination_input_does_not_match_when_one_vaccination_dcc_has_blocklisted_identifier_hash() {
        // target rule name: "Double vaccination"

        val vaccinationDcc = pfizer1_1VaccinationCertificate(today(minus = 100.days))

        val vaccination2Dcc = pfizer1_1VaccinationCertificate(today(minus = 39.days))
            .withVaccinationIdentifier("URN:UVCI:01:FR:RE4AFH9LS8ANCTW#Z")

        givenUvciBlocklistContains(vaccination2Dcc)

        givenBaseHeaders()
            .body(
                DccAggregationRequest(
                    publicKey = CLIENT_PUBLIC_KEY_BASE64,
                    certificates = listOf(
                        encodeToDccAndEncrypt(vaccinationDcc),
                        encodeToDccAndEncrypt(vaccination2Dcc)
                    ),
                )
            )

            .When()
            .post("/api/v1/aggregate")

            .then()
            .statusCode(BAD_REQUEST.value())
            .body("error", equalTo(BAD_REQUEST.reasonPhrase))
            .body("message", equalTo("Request body contains invalid attributes"))
            .body("errors[0].field", equalTo("certificates[1]"))
            .body("errors[0].code", equalTo("BLOCKLISTED_CERTIFICATE"))
            .body("errors[0].message", equalTo("Certificate is blocklisted"))
            .body("errors.size()", equalTo(1))
    }

    @Test
    fun double_vaccination_input_does_not_match_when_one_vaccination_dcc_has_blocklisted_signature_hash() {
        // target rule name: "Double vaccination"

        val vaccinationDcc = pfizer1_1VaccinationCertificate(today(minus = 100.days))

        val vaccination2Dcc = pfizer1_1VaccinationCertificate(today(minus = 39.days))
            .withVaccinationIdentifier("URN:UVCI:01:FR:RE4AFH9LS8ANCTW#Z")

        val encodedVaccination2Dcc = encodeToDcc(vaccination2Dcc)

        givenSignatureBlocklistContains(encodedVaccination2Dcc.step2Cose)

        givenBaseHeaders()
            .body(
                DccAggregationRequest(
                    publicKey = CLIENT_PUBLIC_KEY_BASE64,
                    certificates = listOf(
                        encodeToDccAndEncrypt(vaccinationDcc),
                        encryptDcc(encodedVaccination2Dcc.step5Prefixed)
                    ),
                )
            )

            .When()
            .post("/api/v1/aggregate")

            .then()
            .statusCode(BAD_REQUEST.value())
            .body("error", equalTo(BAD_REQUEST.reasonPhrase))
            .body("message", equalTo("Request body contains invalid attributes"))
            .body("errors[0].field", equalTo("certificates[1]"))
            .body("errors[0].code", equalTo("BLOCKLISTED_CERTIFICATE"))
            .body("errors[0].message", equalTo("Certificate is blocklisted"))
            .body("errors.size()", equalTo(1))
    }

    @ParameterizedTest
    @ValueSource(
        strings = [
            "APFC_BAD_KEY",
            "dGVzdHRlc3R0ZXN0dGVzdHRlc3R0ZXN0dGVzdHRlc3R0ZXN0dGVzdHRlc3R0ZXN0dGVzdHRlc3R0ZXN0dGVzdHRlc3R0ZXN0dGVzdHRlc3R0ZXN0dGVzdHRlcw==",
            "MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAE1E4zIcpuQMsQ0HD3JAwGmsH91ucFDanXp9ucZmw6Lrjc3o1Sua/shTsyz+Ruyml1iCoIEEUwn+wuK+48RijhCA=="
        ]
    )
    fun bad_request_when_bad_publickey_format(publicKey: String) {
        // target rule name: "Positive antigenic test + vaccination"

        val vaccinationDcc = pfizer1_1VaccinationCertificate(today(minus = 7.days))

        val testDcc = positiveAntigenicTestCertificate(now.minus(60.days))

        givenBaseHeaders()
            .body(
                DccAggregationRequest(
                    publicKey = publicKey,
                    certificates = listOf(
                        encodeToDccAndEncrypt(vaccinationDcc),
                        encodeToDccAndEncrypt(testDcc)
                    ),
                )
            )

            .When()
            .post("/api/v1/aggregate")

            .then()
            .statusCode(BAD_REQUEST.value())
            .body("errors[0].field", equalTo("publicKey"))
            .body("errors[0].code", equalTo("INVALID_PUBLIC_KEY"))
            .body("errors[0].message", equalTo("Request publicKey attribute is invalid"))
            .body("errors.size()", equalTo(1))
    }

    @Test
    fun null_certificate_in_body_is_ignored() {
        // target rule name: "Double vaccination"

        val vaccinationDcc = pfizer2_2VaccinationCertificate(today(minus = 100.days))

        givenBaseHeaders()
            .body(
                """
                    {
                        "publicKey": "$CLIENT_PUBLIC_KEY_BASE64",
                        "certificates": [
                            "${encodeToDccAndEncrypt(vaccinationDcc)}",
                            null
                        ]
                    }
                """.trimIndent()
            )
            .When()
            .post("/api/v1/aggregate")
            .then()
            .statusCode(BAD_REQUEST.value())
            .body("errors[0].field", equalTo("certificates"))
            .body("errors[0].code", equalTo("INCOMPATIBLE_DCC_COMBINATION"))
            .body("errors[0].message", equalTo("Provided certificates are not compatible with any operation"))
            .body("errors.size()", equalTo(1))
    }

    @Test
    fun certificate_not_matching_validIfDatedWithin_rule_gets_rejected() {
        // target rule name: "Double vaccination"

        val vaccinationDcc = pfizer2_2VaccinationCertificate(today(minus = 201.days))

        val vaccination2Dcc = pfizer1_1VaccinationCertificate(today(minus = 39.days))

        givenBaseHeaders()
            .body(
                DccAggregationRequest(
                    publicKey = CLIENT_PUBLIC_KEY_BASE64,
                    certificates = listOf(
                        encodeToDccAndEncrypt(vaccinationDcc),
                        encodeToDccAndEncrypt(vaccination2Dcc)
                    ),
                )
            )
            .When()
            .post("/api/v1/aggregate")
            .then()
            .statusCode(BAD_REQUEST.value())
            .body("errors[0].field", equalTo("certificates"))
            .body("errors[0].code", equalTo("INCOMPATIBLE_DCC_COMBINATION"))
            .body("errors[0].message", equalTo("Provided certificates are not compatible with any operation"))
            .body("errors.size()", equalTo(1))
    }

    @Test
    fun dcc_signed_with_untrusted_certificate_is_rejected() {
        // target rule name: "Double vaccination"
        givenCertificatesTrustListContains(DCC_TEST_INPUT_KEYS)

        val vaccinationDcc = pfizer2_2VaccinationCertificate(today(minus = 201.days))

        val vaccination2Dcc = pfizer1_1VaccinationCertificate(today(minus = 39.days))

        givenBaseHeaders()
            .body(
                DccAggregationRequest(
                    publicKey = CLIENT_PUBLIC_KEY_BASE64,
                    certificates = listOf(
                        encodeToDccAndEncrypt(vaccinationDcc),
                        encodeToDccAndEncrypt(vaccination2Dcc, dccSigningKeys = DCCLIGHT_SERVER_KEYS)
                    ),
                )
            )
            .When()
            .post("/api/v1/aggregate")
            .then()
            .statusCode(BAD_REQUEST.value())
            .body("errors[0].field", equalTo("certificates[1]"))
            .body("errors[0].code", equalTo("DCC_DECRYPTION_ERROR"))
            .body("errors[0].message", equalTo("Error during DCC decryption"))
            .body("errors.size()", equalTo(1))
    }

    @Test
    fun too_many_certificates_sent_must_return_an_error() {
        givenBaseHeaders()
            .body(
                DccAggregationRequest(
                    publicKey = CLIENT_PUBLIC_KEY_BASE64,
                    certificates = listOf(
                        encodeToDccAndEncrypt(pfizer2_2VaccinationCertificate(today(minus = 50.days))),
                        encodeToDccAndEncrypt(pfizer2_2VaccinationCertificate(today(minus = 50.days))),
                        encodeToDccAndEncrypt(pfizer2_2VaccinationCertificate(today(minus = 50.days))),
                        encodeToDccAndEncrypt(pfizer2_2VaccinationCertificate(today(minus = 50.days))),
                        encodeToDccAndEncrypt(pfizer2_2VaccinationCertificate(today(minus = 50.days))),
                        encodeToDccAndEncrypt(pfizer2_2VaccinationCertificate(today(minus = 50.days))),
                        encodeToDccAndEncrypt(pfizer2_2VaccinationCertificate(today(minus = 50.days))),
                        encodeToDccAndEncrypt(pfizer2_2VaccinationCertificate(today(minus = 50.days))),
                        encodeToDccAndEncrypt(pfizer2_2VaccinationCertificate(today(minus = 50.days)))
                    ),
                )
            )

            .When()
            .post("/api/v1/aggregate")

            .then()
            .statusCode(BAD_REQUEST.value())
            .body("error", equalTo(BAD_REQUEST.reasonPhrase))
            .body("message", equalTo("Request body contains invalid attributes"))
            .body("errors[0].field", equalTo("certificates"))
            .body("errors[0].code", equalTo("MAX_NUMBER_OF_CERTIFICATES_EXCEEDED"))
            .body("errors[0].message", equalTo("max number of certificates exceeded"))
            .body("errors.size()", equalTo(1))
    }
}
