package fr.gouv.tac.dcclight.rules.model

import com.spotify.hamcrest.jackson.IsJsonObject.jsonObject
import fr.gouv.tac.dcclight.api.model.DccAggregationRequest
import fr.gouv.tac.dcclight.extension.toLocalDate
import fr.gouv.tac.dcclight.test.DccSigningKeysManager.Companion.DCC_SERVER_KEYS
import fr.gouv.tac.dcclight.test.IntegrationTest
import fr.gouv.tac.dcclight.test.NettyManager.Companion.givenExposedRules
import fr.gouv.tac.dcclight.test.RestAssuredManager.Companion.When
import fr.gouv.tac.dcclight.test.RestAssuredManager.Companion.bodyEncrypted
import fr.gouv.tac.dcclight.test.RestAssuredManager.Companion.givenBaseHeaders
import fr.gouv.tac.dcclight.test.TransportEncryptionKeysManager.Companion.CLIENT_PUBLIC_KEY_BASE64
import fr.gouv.tac.dcclight.test.antigenicTestYamlInputRule
import fr.gouv.tac.dcclight.test.defaultEmptyRuleList
import fr.gouv.tac.dcclight.test.eu.DccCryptographer.Companion.encodeToDccAndEncrypt
import fr.gouv.tac.dcclight.test.extension.addInputRule
import fr.gouv.tac.dcclight.test.extension.withAllowListItem
import fr.gouv.tac.dcclight.test.extension.withOutputRule
import fr.gouv.tac.dcclight.test.extension.withTestFacility
import fr.gouv.tac.dcclight.test.extension.withTestIssuer
import fr.gouv.tac.dcclight.test.extension.withTestNameRat
import fr.gouv.tac.dcclight.test.extension.withTestTarget
import fr.gouv.tac.dcclight.test.getRuleWithInputVaccine1_1ValidAfterDate
import fr.gouv.tac.dcclight.test.getRuleWithInputVaccine1_1ValidBeforeDate
import fr.gouv.tac.dcclight.test.matchers.isEuDcc
import fr.gouv.tac.dcclight.test.matchers.isHcertEncoded
import fr.gouv.tac.dcclight.test.matchers.where
import fr.gouv.tac.dcclight.test.pfizer1_1VaccinationCertificate
import fr.gouv.tac.dcclight.test.pfizer1_1VaccinationInputRule
import fr.gouv.tac.dcclight.test.pfizer2_2VaccinationCertificate
import fr.gouv.tac.dcclight.test.positiveAntigenicTestCertificate
import fr.gouv.tac.dcclight.test.testToRecoveryOutputRule
import fr.gouv.tac.dcclight.test.today
import fr.gouv.tac.dcclight.test.vaccine2_2OutputRule
import kotlinx.datetime.Clock.System.now
import kotlinx.datetime.LocalDate
import org.hamcrest.Matchers.equalTo
import org.junit.jupiter.api.Test
import org.springframework.http.HttpStatus.BAD_REQUEST
import org.springframework.http.HttpStatus.OK
import kotlin.time.Duration.Companion.days

@IntegrationTest
class OutputValuesTest {

    private fun getOverrideRule(): YamlRules {
        val vaccine1InputRule = pfizer1_1VaccinationInputRule()
        val vaccine2InputRule = pfizer1_1VaccinationInputRule()
            .withAllowListItem("dn", listOf("2"))
            .withAllowListItem("sd", listOf("2"))

        val outputRule = vaccine2_2OutputRule()
            .copy(
                referentialDccRanking = 1,
                overrideValues = mapOf(
                    "tg" to "copied",
                    "vp" to "copied",
                    "mp" to "copied",
                    "ma" to "copied",
                    "dt" to "2000-01-12",
                    "dn" to "6",
                    "sd" to "6",
                    "co" to "copied",
                )
            )
        return defaultEmptyRuleList()
            .addInputRule(vaccine1InputRule)
            .addInputRule(vaccine2InputRule)
            .withOutputRule(outputRule)
    }

    private fun getCopyRule(): YamlRules {
        return defaultEmptyRuleList()
            .addInputRule(antigenicTestYamlInputRule())
            .withOutputRule(testToRecoveryOutputRule())
    }

    @Test
    fun override_values_from_rule_are_in_final_dcc() {
        givenExposedRules(getOverrideRule())

        val vaccinationDcc = pfizer1_1VaccinationCertificate(today(minus = 200.days))
        val vaccination2Dcc = pfizer2_2VaccinationCertificate(today(minus = 140.days))

        givenBaseHeaders()
            .body(
                DccAggregationRequest(
                    publicKey = CLIENT_PUBLIC_KEY_BASE64,
                    certificates = listOf(
                        encodeToDccAndEncrypt(vaccination2Dcc),
                        encodeToDccAndEncrypt(vaccinationDcc),
                    ),
                )
            )

            .When()
            .post("/api/v1/aggregate")

            .then()
            .statusCode(OK.value())
            .bodyEncrypted(
                "response",
                jsonObject()
                    .where(
                        "certificate",
                        isHcertEncoded(
                            issuedBy = DCC_SERVER_KEYS,
                            euDcc = isEuDcc()
                                .forJeromeDupont()
                                .withProofType("v")
                                .where("tg", "copied")
                                .where("vp", "copied")
                                .where("mp", "copied")
                                .where("ma", "copied")
                                .where("dt", "2000-01-12")
                                .where("dn", 6)
                                .where("sd", 6)
                                .where("co", "copied")
                        )
                    )
            )
    }

    @Test
    fun copy_values_from_reference_certificate_in_rule_are_in_final_dcc() {
        givenExposedRules(getCopyRule())

        val testDate = now().minus(60.days)

        val testDcc = positiveAntigenicTestCertificate(testDate)
            .withTestTarget("840539006")
            .withTestIssuer("APHP")
            .withTestNameRat("1833")
            .withTestFacility("facility")

        givenBaseHeaders()
            .body(
                DccAggregationRequest(
                    publicKey = CLIENT_PUBLIC_KEY_BASE64,
                    certificates = listOf(
                        encodeToDccAndEncrypt(testDcc)
                    ),
                )
            )

            .When()
            .post("/api/v1/aggregate")

            .then()
            .statusCode(OK.value())
            .bodyEncrypted(
                "response",
                jsonObject()
                    .where(
                        "certificate",
                        isHcertEncoded(
                            issuedBy = DCC_SERVER_KEYS,
                            euDcc = isEuDcc()
                                .forJeromeDupont()
                                .withProofType("r")
                                .where("tg", "840539006")
                                .where("co", "FR")
                        )
                    )
            )
    }

    @Test
    fun incremented_days_desired_in_rule_are_well_calculated_in_final_dcc() {
        givenExposedRules(getCopyRule())

        val testDate = now().minus(60.days)

        val testDcc = positiveAntigenicTestCertificate(testDate)
            .withTestTarget("840539006")
            .withTestIssuer("APHP")
            .withTestNameRat("1833")
            .withTestFacility("facility")

        givenBaseHeaders()
            .body(
                DccAggregationRequest(
                    publicKey = CLIENT_PUBLIC_KEY_BASE64,
                    certificates = listOf(
                        encodeToDccAndEncrypt(testDcc)
                    ),
                )
            )

            .When()
            .post("/api/v1/aggregate")

            .then()
            .statusCode(OK.value())
            .bodyEncrypted(
                "response",
                jsonObject()
                    .where(
                        "certificate",
                        isHcertEncoded(
                            issuedBy = DCC_SERVER_KEYS,
                            euDcc = isEuDcc()
                                .forJeromeDupont()
                                .withProofType("r")
                                .where("fr", testDate.toLocalDate())
                                .where("df", testDate.plus(11.days).toLocalDate())
                                .where("du", testDate.plus(180.days).toLocalDate())
                        )
                    )
            )
    }

    @Test
    fun an_error_is_reported_if_date_before_does_not_match_boundaries() {
        givenExposedRules(getRuleWithInputVaccine1_1ValidBeforeDate(LocalDate(2010, 1, 1)))

        val vaccinationDcc = pfizer1_1VaccinationCertificate(LocalDate(2011, 1, 1))

        givenBaseHeaders()
            .body(
                DccAggregationRequest(
                    publicKey = CLIENT_PUBLIC_KEY_BASE64,
                    certificates = listOf(
                        encodeToDccAndEncrypt(vaccinationDcc),
                    ),
                )
            )

            .When()
            .post("/api/v1/aggregate")

            .then()
            .statusCode(BAD_REQUEST.value())
            .body("error", equalTo(BAD_REQUEST.reasonPhrase))
            .body("message", equalTo("Request body contains invalid attributes"))
            .body("errors[0].field", equalTo("certificates"))
            .body("errors[0].code", equalTo("INCOMPATIBLE_DCC_COMBINATION"))
            .body("errors[0].message", equalTo("Provided certificates are not compatible with any operation"))
            .body("errors.size()", equalTo(1))
    }

    @Test
    fun date_before_match_boundaries_a_new_certificate_is_received() {
        givenExposedRules(getRuleWithInputVaccine1_1ValidBeforeDate(LocalDate(2010, 1, 1)))

        val vaccinationDcc = pfizer1_1VaccinationCertificate(LocalDate(2008, 1, 1))

        givenBaseHeaders()
            .body(
                DccAggregationRequest(
                    publicKey = CLIENT_PUBLIC_KEY_BASE64,
                    certificates = listOf(
                        encodeToDccAndEncrypt(vaccinationDcc),
                    ),
                )
            )

            .When()
            .post("/api/v1/aggregate")

            .then()
            .statusCode(OK.value())
    }

    @Test
    fun an_error_is_reported_if_date_after_does_not_matche_boundaries() {
        givenExposedRules(getRuleWithInputVaccine1_1ValidAfterDate(LocalDate(2010, 1, 1)))

        val vaccinationDcc = pfizer1_1VaccinationCertificate(LocalDate(1999, 1, 1))

        givenBaseHeaders()
            .body(
                DccAggregationRequest(
                    publicKey = CLIENT_PUBLIC_KEY_BASE64,
                    certificates = listOf(
                        encodeToDccAndEncrypt(vaccinationDcc),
                    ),
                )
            )

            .When()
            .post("/api/v1/aggregate")

            .then()
            .statusCode(BAD_REQUEST.value())
            .body("error", equalTo(BAD_REQUEST.reasonPhrase))
            .body("message", equalTo("Request body contains invalid attributes"))
            .body("errors[0].field", equalTo("certificates"))
            .body("errors[0].code", equalTo("INCOMPATIBLE_DCC_COMBINATION"))
            .body("errors[0].message", equalTo("Provided certificates are not compatible with any operation"))
            .body("errors.size()", equalTo(1))
    }

    @Test
    fun date_after_match_boundaries_a_new_certificate_is_received() {
        givenExposedRules(getRuleWithInputVaccine1_1ValidAfterDate(LocalDate(2010, 1, 1)))

        val vaccinationDcc = pfizer1_1VaccinationCertificate(LocalDate(2022, 1, 1))

        givenBaseHeaders()
            .body(
                DccAggregationRequest(
                    publicKey = CLIENT_PUBLIC_KEY_BASE64,
                    certificates = listOf(
                        encodeToDccAndEncrypt(vaccinationDcc),
                    ),
                )
            )

            .When()
            .post("/api/v1/aggregate")

            .then()
            .statusCode(OK.value())
    }
}
