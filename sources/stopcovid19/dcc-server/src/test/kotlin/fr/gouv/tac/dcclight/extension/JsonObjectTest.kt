package fr.gouv.tac.dcclight.extension

import kotlinx.serialization.json.JsonObject
import kotlinx.serialization.json.JsonPrimitive
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.entry
import org.junit.jupiter.api.DisplayNameGeneration
import org.junit.jupiter.api.DisplayNameGenerator
import org.junit.jupiter.api.Test

@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores::class)
class JsonObjectTest {

    @Test
    fun plus_pair_adds_values_to_json_object() {
        val input = JsonObject(mapOf("item1" to JsonPrimitive(1)))

        assertThat(input.plus(Pair("item2", "foo")))
            .containsExactly(
                entry("item1", JsonPrimitive(1)),
                entry("item2", JsonPrimitive("foo"))
            )
    }

    @Test
    fun plus_map_adds_entries_to_json_object() {
        val input = JsonObject(mapOf("item1" to JsonPrimitive(1)))

        assertThat(input.plus(mapOf("item2" to "foo")))
            .containsExactly(
                entry("item1", JsonPrimitive(1)),
                entry("item2", JsonPrimitive("foo"))
            )
    }

    @Test
    fun plus_map_overrides_value_when_key_already_exists() {
        val input = JsonObject(mapOf("item1" to JsonPrimitive(1)))

        assertThat(input.plus(mapOf("item1" to "foo")))
            .containsExactly(
                entry("item1", JsonPrimitive("foo"))
            )
    }

    @Test
    fun plus_pair_overrides_value_when_key_already_exists() {
        val input = JsonObject(mapOf("item1" to JsonPrimitive(1)))

        assertThat(input.plus(Pair("item1", "foo")))
            .containsExactly(
                entry("item1", JsonPrimitive("foo"))
            )
    }

    @Test
    fun plusOptional_overrides_value_when_key_already_exists() {
        val input = JsonObject(mapOf("item1" to JsonPrimitive(1)))

        assertThat(input.plusOptional(Pair("item1", "foo")))
            .containsExactly(
                entry("item1", JsonPrimitive("foo"))
            )
    }

    @Test
    fun plusOptional_adds_entry_when_value_not_null() {
        val input = JsonObject(mapOf("item1" to JsonPrimitive(1)))
        assertThat(input.plusOptional(Pair("item2", "foo")))
            .containsExactly(
                entry("item1", JsonPrimitive(1)),
                entry("item2", JsonPrimitive("foo"))
            )
    }

    @Test
    fun plusOptional_does_not_add_entry_when_value_is_null() {
        val input = JsonObject(mapOf("item1" to JsonPrimitive(1)))
        assertThat(input.plusOptional(Pair("item2", null)))
            .containsExactly(
                entry("item1", JsonPrimitive(1))
            )
    }

    @Test
    fun mapJsonPrimitiveValues_applies_transformation_to_values() {
        val input = JsonObject(mapOf("item1" to JsonPrimitive(1), "item2" to JsonPrimitive(2)))

        assertThat(
            input.mapJsonPrimitiveValues { JsonPrimitive(it.value.content.toInt() * 2) }
        )
            .containsExactly(
                entry("item1", JsonPrimitive(2)),
                entry("item2", JsonPrimitive(4))
            )
    }
}
