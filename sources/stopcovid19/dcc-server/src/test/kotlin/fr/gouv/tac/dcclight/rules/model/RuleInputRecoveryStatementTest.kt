package fr.gouv.tac.dcclight.rules.model

import fr.gouv.tac.dcclight.extension.getVaccination
import fr.gouv.tac.dcclight.rules.SanitaryProofType.VACCINATION
import fr.gouv.tac.dcclight.test.pfizer1_1VaccinationCertificate
import kotlinx.datetime.LocalDate
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.DisplayNameGeneration
import org.junit.jupiter.api.DisplayNameGenerator.ReplaceUnderscores
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test

@DisplayNameGeneration(ReplaceUnderscores::class)
class RuleInputRecoveryStatementTest {

    private val minimalInputVaccination = RuleInput(
        type = VACCINATION,
        dateConstraints = DateConstraints(),
        allowLists = mapOf(
            "dn" to AllowList("dn", listOf("1")),
            "sd" to AllowList("sd", listOf("1")),
            "vp" to AllowList("sd", emptyList()),
            "mp" to AllowList("sd", emptyList()),
            "ma" to AllowList("sd", emptyList()),
        ),
        blockLists = emptyMap()
    )

    private val pfizer1_1 = pfizer1_1VaccinationCertificate(LocalDate(2020, 1, 1))

    @Nested
    inner class MinimalConstraints {

        @Test
        fun should_accept_a_certificate() {
            assertThat(minimalInputVaccination.matches(pfizer1_1))
                .describedAs("input rule should match")
                .isTrue
        }

        @Test
        fun should_reject_a_certificate_with_wrong_dose_number() {
            assertThat(
                minimalInputVaccination.matches(
                    pfizer1_1.copy(
                        vaccinations = arrayOf(
                            pfizer1_1.getVaccination().copy(doseNumber = 2)
                        )
                    )
                )
            )
                .describedAs("input rule shouldn't match")
                .isFalse
        }

        @Test
        fun should_reject_a_certificate_with_wrong_total_dose_number() {
            assertThat(
                minimalInputVaccination.matches(
                    pfizer1_1.copy(
                        vaccinations = arrayOf(
                            pfizer1_1.getVaccination().copy(doseTotalNumber = 2)
                        )
                    )
                )
            )
                .describedAs("input rule shouldn't match")
                .isFalse
        }
    }
}
