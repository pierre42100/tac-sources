package fr.gouv.tac.dcclight.extension

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.DisplayNameGeneration
import org.junit.jupiter.api.DisplayNameGenerator
import org.junit.jupiter.api.Test

@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores::class)
class StringTest {

    @Test
    fun accented_characters_are_removed() {
        assertThat("àÀâÂÄäçÇÈÉÊËèéêëíÍîïÎÏÑñÓóÖöÙùÚúü".removeAccents()).isEqualTo("aAaAAacCEEEEeeeeiIiiIINnOoOoUuUuu")
    }
}
