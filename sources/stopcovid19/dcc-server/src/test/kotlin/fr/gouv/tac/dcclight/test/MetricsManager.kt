package fr.gouv.tac.dcclight.test

import io.micrometer.core.instrument.Counter
import io.micrometer.core.instrument.Meter
import io.micrometer.core.instrument.MeterRegistry
import io.micrometer.core.instrument.Tag
import org.assertj.core.api.AbstractDoubleAssert
import org.assertj.core.api.Assertions.assertThat
import org.springframework.test.context.TestContext
import org.springframework.test.context.TestExecutionListener

class MetricsManager : TestExecutionListener {

    override fun beforeTestMethod(testContext: TestContext) {
        meterRegistry = testContext.applicationContext.getBean(MeterRegistry::class.java)

        countersSnapshot = meterRegistry
            .meters
            .filterIsInstance(Counter::class.java)
            .associateBy(Counter::getId, Counter::count)
    }

    companion object {

        private lateinit var meterRegistry: MeterRegistry

        private lateinit var countersSnapshot: Map<Meter.Id, Double>

        fun assertThatCounter(name: String, vararg tags: Tag): AbstractDoubleAssert<*> {
            val counter = meterRegistry.counter(name, listOf(*tags))
            return assertThat(counter.count() - countersSnapshot.getOrDefault(counter.id, 0.0))
        }
    }
}
