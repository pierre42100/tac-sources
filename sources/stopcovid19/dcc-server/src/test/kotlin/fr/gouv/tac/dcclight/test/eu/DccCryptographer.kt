package fr.gouv.tac.dcclight.test.eu

import ehn.techiop.hcert.kotlin.chain.Chain
import ehn.techiop.hcert.kotlin.chain.ChainResult
import ehn.techiop.hcert.kotlin.chain.DecodeResult
import ehn.techiop.hcert.kotlin.chain.impl.DefaultBase45Service
import ehn.techiop.hcert.kotlin.chain.impl.DefaultCborService
import ehn.techiop.hcert.kotlin.chain.impl.DefaultCompressorService
import ehn.techiop.hcert.kotlin.chain.impl.DefaultContextIdentifierService
import ehn.techiop.hcert.kotlin.chain.impl.DefaultCoseService
import ehn.techiop.hcert.kotlin.chain.impl.DefaultCwtService
import ehn.techiop.hcert.kotlin.chain.impl.DefaultHigherOrderValidationService
import ehn.techiop.hcert.kotlin.chain.impl.DefaultSchemaValidationService
import ehn.techiop.hcert.kotlin.chain.impl.FileBasedCryptoService
import ehn.techiop.hcert.kotlin.chain.impl.PrefilledCertificateRepository
import ehn.techiop.hcert.kotlin.data.GreenCertificate
import fr.gouv.tac.dcclight.service.dcc.chain.OptimizedBase45EUService
import fr.gouv.tac.dcclight.test.DccSigningKeysManager.Companion.DCC_TEST_INPUT_KEYS
import fr.gouv.tac.dcclight.test.DccSigningKeysManager.DccSigningKeys
import fr.gouv.tac.dcclight.test.TransportEncryptionKeysManager
import fr.gouv.tac.dcclight.test.TransportEncryptionKeysManager.Companion.TEST_TRANSPORT_KEYS
import fr.gouv.tac.dcclight.test.TransportEncryptionKeysManager.TransportKeys
import kotlinx.datetime.Clock
import java.util.Base64
import kotlin.text.Charsets.UTF_8
import kotlin.time.Duration.Companion.days

class DccCryptographer {

    companion object {
        private val higherOrderValidationService = DefaultHigherOrderValidationService()
        private val schemaValidationService = DefaultSchemaValidationService()
        private val cborService = DefaultCborService()

        private val compressorService = DefaultCompressorService()
        private val base45Service = OptimizedBase45EUService()
        private val contextIdentifierService = DefaultContextIdentifierService()

        fun encodeToDcc(
            greenCertificate: GreenCertificate,
            clock: Clock = Clock.System,
            keys: DccSigningKeys = DCC_TEST_INPUT_KEYS
        ): ChainResult {
            val coseService = FileBasedCryptoService(
                pemEncodedPrivateKey = keys.privateKeyAsPem(),
                pemEncodedCertificate = keys.certificateAsPem()
            )
            val encodingChain = Chain(
                higherOrderValidationService,
                schemaValidationService,
                cborService,
                DefaultCwtService(countryCode = "FR", validity = 3650.days, clock = clock),
                DefaultCoseService(coseService),
                compressorService,
                base45Service,
                contextIdentifierService
            )
            return encodingChain.encode(greenCertificate)
        }

        fun encodeToDccAndEncrypt(
            greenCertificate: GreenCertificate,
            clock: Clock = Clock.System,
            transportKeys: TransportKeys = TEST_TRANSPORT_KEYS,
            dccSigningKeys: DccSigningKeys = DCC_TEST_INPUT_KEYS
        ): String {
            val encodedCertificate = encodeToDcc(greenCertificate, clock, dccSigningKeys).step5Prefixed
            val encryptedCertificate = TransportEncryptionKeysManager.encryptForServer(
                encodedCertificate.toByteArray(UTF_8),
                transportKeys
            )
            return Base64.getEncoder()
                .encodeToString(encryptedCertificate)
        }

        fun encryptDcc(
            encodedCertificate: String,
            transportKeys: TransportKeys = TEST_TRANSPORT_KEYS
        ): String {
            val encryptedCertificate = TransportEncryptionKeysManager.encryptForServer(
                encodedCertificate.toByteArray(UTF_8),
                transportKeys
            )
            return Base64.getEncoder()
                .encodeToString(encryptedCertificate)
        }

        fun decodeCertificate(certificate: String, signingKeys: DccSigningKeys): DecodeResult {
            return if (certificate.startsWith("HCFR1:")) {
                decodeLightCertificate(certificate, signingKeys)
            } else if (certificate.startsWith("HC1:")) {
                decodeAggregationCertificate(certificate, signingKeys)
            } else {
                throw IllegalArgumentException("Unknown certificate prefix: $certificate")
            }
        }

        private fun decodeLightCertificate(certificate: String, signingKeys: DccSigningKeys): DecodeResult {
            val trustedCertificatesRepository = PrefilledCertificateRepository(signingKeys.certificateAsPem())

            val verificationChain = Chain(
                DccLightHigherOrderValidationService(),
                DCCLightSchemaValidationService(),
                DefaultCborService(),
                DccLightSimplifiedCwtDecoderEUService(),
                DefaultCoseService(trustedCertificatesRepository),
                DefaultCompressorService(),
                DefaultBase45Service(),
                DefaultContextIdentifierService("HCFR1:")
            )

            return verificationChain.decode(certificate)
        }

        private fun decodeAggregationCertificate(certificate: String, signingKeys: DccSigningKeys): DecodeResult {
            val trustedCertificatesRepository = PrefilledCertificateRepository(signingKeys.certificateAsPem())

            val verificationChain = Chain(
                DefaultHigherOrderValidationService(),
                DefaultSchemaValidationService(),
                DefaultCborService(),
                DefaultCwtService(),
                DefaultCoseService(trustedCertificatesRepository),
                DefaultCompressorService(),
                DefaultBase45Service(),
                DefaultContextIdentifierService("HC1:")
            )

            return verificationChain.decode(certificate)
        }
    }
}
