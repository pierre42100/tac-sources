package fr.gouv.tac.dcclight.test.matchers

import kotlinx.datetime.Instant
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.equalTo
import org.junit.jupiter.api.DisplayNameGeneration
import org.junit.jupiter.api.DisplayNameGenerator.ReplaceUnderscores
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import kotlin.time.Duration.Companion.minutes
import kotlin.time.Duration.Companion.seconds

@DisplayNameGeneration(ReplaceUnderscores::class)
class DateTimeMatchersTest {

    private val instant = Instant.parse("2012-07-15T13:34:44Z")

    @Test
    fun can_detect_instant_is_one_minute_around_another() {
        assertThat(instant.minus(1.minutes), isNear(instant, 1.minutes))
        assertThat(instant, isNear(instant, 1.minutes))
        assertThat(instant.minus(1.minutes), isNear(instant, 1.minutes))
    }

    @Test
    fun can_detect_timestamp_is_too_old() {
        val error: AssertionError = assertThrows {
            assertThat(instant.plus(1.minutes).plus(1.seconds), isNear(instant, 1.minutes))
        }
        assertThat(
            error.message?.replace("\r", ""),
            equalTo(
                """

                    Expected: an instant near <2012-07-15T13:34:44Z> +/- 1m
                         but: was <2012-07-15T13:35:45Z> which is after upper bound 2012-07-15T13:35:44Z
                """.trimIndent()
            )
        )
    }

    @Test
    fun can_detect_instant_is_too_fresh() {
        val error: AssertionError = assertThrows {
            assertThat(instant.minus(1.minutes).minus(1.seconds), isNear(instant, 1.minutes))
        }
        assertThat(
            error.message?.replace("\r", ""),
            equalTo(
                """

                    Expected: an instant near <2012-07-15T13:34:44Z> +/- 1m
                         but: was <2012-07-15T13:33:43Z> which is before lower bound 2012-07-15T13:33:44Z
                """.trimIndent()
            )
        )
    }
}
