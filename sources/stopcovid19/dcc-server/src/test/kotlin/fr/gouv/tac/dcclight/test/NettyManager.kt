package fr.gouv.tac.dcclight.test

import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import ehn.techiop.hcert.kotlin.data.GreenCertificate
import fr.gouv.tac.dcclight.configuration.ExternalResource
import fr.gouv.tac.dcclight.extension.countryUvciHash
import fr.gouv.tac.dcclight.extension.uvciHash
import fr.gouv.tac.dcclight.rules.model.YamlRules
import fr.gouv.tac.dcclight.rules.model.yamlRulesObjectMapper
import fr.gouv.tac.dcclight.service.dcc.DccSignatureHash
import fr.gouv.tac.dcclight.test.DccSigningKeysManager.Companion.DCC_TEST_INPUT_KEYS
import org.springframework.test.context.TestContext
import org.springframework.test.context.TestExecutionListener
import reactor.core.publisher.Flux
import reactor.netty.http.server.HttpServer

class NettyManager : TestExecutionListener {

    override fun beforeTestClass(testContext: TestContext) {
        externalResources = testContext.applicationContext
            .getBeansOfType(ExternalResource::class.java)
            .values.toList()
    }

    override fun beforeTestMethod(testContext: TestContext) {
        givenFakeAggregationRulesAreExposed()
        givenCertificatesTrustListContains(DCC_TEST_INPUT_KEYS)
        givenBlockListIsEmpty()
    }

    companion object {

        private lateinit var externalResources: List<ExternalResource<*>>

        private val mapper = jacksonObjectMapper()

        private var dccCerts: String = "{}"
        private var yamlRules: String = "rules: []"
        private val dccBlocklist: DccBlocklist = DccBlocklist(mutableSetOf(), mutableSetOf(), mutableSetOf())

        private val server = HttpServer.create()
            .host("localhost")
            .route { route ->
                route.get("/json/dsc/dcc-certs.json") { _, res -> res.sendString(Flux.just(dccCerts)) }
                route.get("/json/aggregation-rules.yml") { _, res -> res.sendString(Flux.just(yamlRules)) }
                route.get("/json/blocklist/certlist.json") { _, res -> res.sendString(Flux.just(jacksonObjectMapper().writeValueAsString(dccBlocklist))) }
            }
            .bindNow()
        private val serverAddress = "http://${server.host()}:${server.port()}"

        init {
            System.setProperty("dcclight.signing-certificates-endpoint.url", "$serverAddress/json/dsc/dcc-certs.json")
            System.setProperty("dcclight.aggregation-rules-endpoint.url", "$serverAddress/json/aggregation-rules.yml")
            System.setProperty("dcclight.blocklist-endpoint.url", "$serverAddress/json/blocklist/certlist.json")
        }

        private fun fetchExternalResources() {
            // Force app beans to fetch new external configurations
            externalResources.forEach { it.fetch() }
        }

        fun givenExposedRules(rules: YamlRules) {
            yamlRules = yamlRulesObjectMapper().dump(rules)
            fetchExternalResources()
        }

        fun givenFakeAggregationRulesAreExposed() {
            yamlRules = this::class.java.getResource("/aggregation/rules-test.yml")!!.readText()
            fetchExternalResources()
        }

        fun givenRealAggregationRulesAreExposed() {
            yamlRules = this::class.java.getResource("/aggregation/rules.yml")!!.readText()
            fetchExternalResources()
        }

        fun givenCertificatesTrustListContains(vararg certificates: DccSigningKeysManager.DccSigningKeys) {
            dccCerts = mapper.writeValueAsString(
                certificates.associate {
                    it.kid() to listOf(it.certificateAsBase64())
                }
            )
            fetchExternalResources()
        }

        fun givenCountryUvciBlockListContains(greenCertificate: GreenCertificate) {
            dccBlocklist.countryUvciHashes.add(greenCertificate.countryUvciHash())
            fetchExternalResources()
        }

        fun givenUvciBlocklistContains(greenCertificate: GreenCertificate) {
            dccBlocklist.uvciHashes.add(greenCertificate.uvciHash())
            fetchExternalResources()
        }

        fun givenSignatureBlocklistContains(cose: ByteArray) {
            dccBlocklist.signatureHashes.add(DccSignatureHash(cose).buildSignatureHash())
            fetchExternalResources()
        }

        fun givenBlockListIsEmpty() {
            dccBlocklist.countryUvciHashes.clear()
            dccBlocklist.uvciHashes.clear()
            dccBlocklist.signatureHashes.clear()
            fetchExternalResources()
        }
    }

    data class DccBlocklist(
        @JsonProperty("signature_hashes")
        val signatureHashes: MutableSet<String>,
        @JsonProperty("uvci_hashes")
        val uvciHashes: MutableSet<String>,
        @JsonProperty("country_uvci_hashes")
        val countryUvciHashes: MutableSet<String>
    )
}
