package fr.gouv.tac.dcclight.controller

import fr.gouv.tac.dcclight.api.model.DccAggregationRequest
import fr.gouv.tac.dcclight.test.IntegrationTest
import fr.gouv.tac.dcclight.test.RestAssuredManager.Companion.When
import fr.gouv.tac.dcclight.test.RestAssuredManager.Companion.givenBaseHeaders
import fr.gouv.tac.dcclight.test.TransportEncryptionKeysManager.Companion.CLIENT_PUBLIC_KEY_BASE64
import fr.gouv.tac.dcclight.test.eu.DccCryptographer.Companion.encodeToDccAndEncrypt
import fr.gouv.tac.dcclight.test.extension.withDateOfBirth
import fr.gouv.tac.dcclight.test.extension.withGivenNameTransliterated
import fr.gouv.tac.dcclight.test.extension.withVaccinationManufacturer
import fr.gouv.tac.dcclight.test.extension.withVaccinationMedicinalProduct
import fr.gouv.tac.dcclight.test.extension.withVaccinationVaccine
import fr.gouv.tac.dcclight.test.negativeAntigenicTestCertificate
import fr.gouv.tac.dcclight.test.negativePcrTestCertificate
import fr.gouv.tac.dcclight.test.pfizer1_1VaccinationCertificate
import fr.gouv.tac.dcclight.test.pfizer1_2VaccinationCertificate
import fr.gouv.tac.dcclight.test.today
import kotlinx.datetime.Clock.System.now
import kotlinx.datetime.LocalDate
import org.hamcrest.Matchers.equalTo
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.TestInstance.Lifecycle.PER_CLASS
import org.springframework.http.HttpStatus.BAD_REQUEST
import kotlin.time.Duration.Companion.days
import kotlin.time.Duration.Companion.hours

const val ALLOWED_VACCINE_MEDICINAL_PRODUCT = "EU/1/20/1528"
const val ALLOWED_VACCINE_MANUFACTURER = "ORG-100032020"
const val ALLOWED_VACCINE_TYPE = "1119349007"

@TestInstance(PER_CLASS)
@IntegrationTest
class TestAndVaccinationCombinationErrorsTest {

    @Test
    fun returns_BAD_REQUEST_when_pcr_test_wth_invalid_date_of_birth() {

        val vaccinationDcc = pfizer1_2VaccinationCertificate(today(minus = 25.days))

        val testDcc = negativePcrTestCertificate(now().minus(3.hours))
            .withDateOfBirth(LocalDate(1970, 1, 1))

        givenBaseHeaders()
            .body(
                DccAggregationRequest(
                    publicKey = CLIENT_PUBLIC_KEY_BASE64,
                    certificates = listOf(
                        encodeToDccAndEncrypt(vaccinationDcc),
                        encodeToDccAndEncrypt(testDcc)
                    ),
                )
            )

            .When()
            .post("/api/v1/aggregate")

            .then()
            .statusCode(BAD_REQUEST.value())
            .body("error", equalTo(BAD_REQUEST.reasonPhrase))
            .body("message", equalTo("Request body contains invalid attributes"))
            .body("errors[0].field", equalTo("certificates[1]"))
            .body("errors[0].code", equalTo("INVALID_DATE_OF_BIRTH"))
            .body("errors[0].message", equalTo("Invalid date of birth"))
            .body("errors.size()", equalTo(1))
    }

    @Test
    fun returns_BAD_REQUEST_when_antigenic_test_wth_invalid_date_of_birth() {

        val vaccinationDcc = pfizer1_2VaccinationCertificate(today(minus = 25.days))

        val testDcc = negativeAntigenicTestCertificate(now().minus(3.hours))
            .withDateOfBirth(LocalDate(1970, 1, 1))

        givenBaseHeaders()
            .body(
                DccAggregationRequest(
                    publicKey = CLIENT_PUBLIC_KEY_BASE64,
                    certificates = listOf(
                        encodeToDccAndEncrypt(vaccinationDcc),
                        encodeToDccAndEncrypt(testDcc)
                    ),
                )
            )

            .When()
            .post("/api/v1/aggregate")

            .then()
            .statusCode(BAD_REQUEST.value())
            .body("error", equalTo(BAD_REQUEST.reasonPhrase))
            .body("message", equalTo("Request body contains invalid attributes"))
            .body("errors[0].field", equalTo("certificates[1]"))
            .body("errors[0].code", equalTo("INVALID_DATE_OF_BIRTH"))
            .body("errors[0].message", equalTo("Invalid date of birth"))
            .body("errors.size()", equalTo(1))
    }

    @Test
    fun returns_BAD_REQUEST_when_insufficient_number_of_total_doses() {

        val vaccinationDcc = pfizer1_1VaccinationCertificate(today(minus = 25.days))
            .withVaccinationVaccine(ALLOWED_VACCINE_TYPE)
            .withVaccinationMedicinalProduct(ALLOWED_VACCINE_MEDICINAL_PRODUCT)
            .withVaccinationManufacturer(ALLOWED_VACCINE_MANUFACTURER)

        val testDcc = negativeAntigenicTestCertificate(now().minus(3.hours))

        givenBaseHeaders()
            .body(
                DccAggregationRequest(
                    publicKey = CLIENT_PUBLIC_KEY_BASE64,
                    certificates = listOf(
                        encodeToDccAndEncrypt(vaccinationDcc),
                        encodeToDccAndEncrypt(testDcc)
                    ),
                )
            )

            .When()
            .post("/api/v1/aggregate")

            .then()
            .statusCode(BAD_REQUEST.value())
            .body("error", equalTo(BAD_REQUEST.reasonPhrase))
            .body("message", equalTo("Request body contains invalid attributes"))
            .body("errors[0].field", equalTo("certificates"))
            .body("errors[0].code", equalTo("INCOMPATIBLE_DCC_COMBINATION"))
            .body("errors[0].message", equalTo("Provided certificates are not compatible with any operation"))
            .body("errors.size()", equalTo(1))
    }

    @Test
    fun returns_BAD_REQUEST_when_provided_with_pcr_test_of_more_than_24h_and_valid_vaccination_dcc() {

        val vaccinationDcc = pfizer1_2VaccinationCertificate(today(minus = 4.days))

        val testDcc = negativePcrTestCertificate(now().plus(25.hours))

        givenBaseHeaders()
            .body(
                DccAggregationRequest(
                    publicKey = CLIENT_PUBLIC_KEY_BASE64,
                    certificates = listOf(
                        encodeToDccAndEncrypt(vaccinationDcc),
                        encodeToDccAndEncrypt(testDcc)
                    ),
                )
            )

            .When()
            .post("/api/v1/aggregate")

            .then()
            .statusCode(BAD_REQUEST.value())
            .body("error", equalTo(BAD_REQUEST.reasonPhrase))
            .body("message", equalTo("Request body contains invalid attributes"))
            .body("errors[0].field", equalTo("certificates[1]"))
            .body("errors[0].code", equalTo("CERTIFICATE_PROOF_DATE_TOO_FAR_IN_FUTURE"))
            .body("errors[0].message", equalTo("Certificate proof date is in more than 24 hours"))
            .body("errors.size()", equalTo(1))
    }

    @Test
    fun returns_BAD_REQUEST_when_provided_with_antigenic_test_of_more_than_24h_and_valid_vaccination_dcc() {

        val vaccinationDcc = pfizer1_2VaccinationCertificate(today(minus = 4.days))

        val testDcc = negativeAntigenicTestCertificate(now().plus(25.hours))

        givenBaseHeaders()
            .body(
                DccAggregationRequest(
                    publicKey = CLIENT_PUBLIC_KEY_BASE64,
                    certificates = listOf(
                        encodeToDccAndEncrypt(vaccinationDcc),
                        encodeToDccAndEncrypt(testDcc)
                    ),
                )
            )

            .When()
            .post("/api/v1/aggregate")

            .then()
            .statusCode(BAD_REQUEST.value())
            .body("error", equalTo(BAD_REQUEST.reasonPhrase))
            .body("message", equalTo("Request body contains invalid attributes"))
            .body("errors[0].field", equalTo("certificates[1]"))
            .body("errors[0].code", equalTo("CERTIFICATE_PROOF_DATE_TOO_FAR_IN_FUTURE"))
            .body("errors[0].message", equalTo("Certificate proof date is in more than 24 hours"))
            .body("errors.size()", equalTo(1))
    }

    @Test
    fun returns_BAD_REQUEST_when_one_certificate_is_missing_givennametransliterated() {

        val vaccinationDcc = pfizer1_2VaccinationCertificate(today(minus = 25.days))
            .withVaccinationVaccine(ALLOWED_VACCINE_TYPE)
            .withVaccinationMedicinalProduct(ALLOWED_VACCINE_MEDICINAL_PRODUCT)
            .withVaccinationManufacturer(ALLOWED_VACCINE_MANUFACTURER)

        val testDcc = negativeAntigenicTestCertificate(now().minus(3.hours))
            .withGivenNameTransliterated(null)

        givenBaseHeaders()
            .body(
                DccAggregationRequest(
                    publicKey = CLIENT_PUBLIC_KEY_BASE64,
                    certificates = listOf(
                        encodeToDccAndEncrypt(vaccinationDcc),
                        encodeToDccAndEncrypt(testDcc)
                    ),
                )
            )

            .When()
            .post("/api/v1/aggregate")

            .then()
            .statusCode(BAD_REQUEST.value())
            .body("error", equalTo(BAD_REQUEST.reasonPhrase))
            .body("message", equalTo("Request body contains invalid attributes"))
            .body("errors[0].field", equalTo("certificates[1]"))
            .body("errors[0].code", equalTo("MISSING_GIVEN_NAME_TRANSLITERATED"))
            .body("errors[0].message", equalTo("Missing transliterated given name"))
            .body("errors.size()", equalTo(1))
    }

    @Test
    fun returns_BAD_REQUEST_when_two_test_certificates_are_provided() {

        val now = now()

        val vaccinationDcc = pfizer1_2VaccinationCertificate(today(minus = 25.days))
            .withVaccinationVaccine(ALLOWED_VACCINE_TYPE)
            .withVaccinationMedicinalProduct(ALLOWED_VACCINE_MEDICINAL_PRODUCT)
            .withVaccinationManufacturer(ALLOWED_VACCINE_MANUFACTURER)

        val testDcc = negativeAntigenicTestCertificate(now.minus(3.hours))

        val testDcc2 = negativePcrTestCertificate(now.minus(3.hours))

        givenBaseHeaders()
            .body(
                DccAggregationRequest(
                    publicKey = CLIENT_PUBLIC_KEY_BASE64,
                    certificates = listOf(
                        encodeToDccAndEncrypt(vaccinationDcc),
                        encodeToDccAndEncrypt(testDcc),
                        encodeToDccAndEncrypt(testDcc2)
                    ),
                )
            )

            .When()
            .post("/api/v1/aggregate")

            .then()
            .statusCode(BAD_REQUEST.value())
            .body("error", equalTo(BAD_REQUEST.reasonPhrase))
            .body("message", equalTo("Request body contains invalid attributes"))
            .body("errors[0].field", equalTo("certificates"))
            .body("errors[0].code", equalTo("INCOMPATIBLE_DCC_COMBINATION"))
            .body("errors[0].message", equalTo("Provided certificates are not compatible with any operation"))
            .body("errors.size()", equalTo(1))
    }

    @Test
    fun returns_BAD_REQUEST_when_two_vaccination_certificates_are_provided() {

        val vaccinationDcc = pfizer1_2VaccinationCertificate(today(minus = 25.days))
            .withVaccinationVaccine(ALLOWED_VACCINE_TYPE)
            .withVaccinationMedicinalProduct(ALLOWED_VACCINE_MEDICINAL_PRODUCT)
            .withVaccinationManufacturer(ALLOWED_VACCINE_MANUFACTURER)

        val vaccinationDcc2 = pfizer1_2VaccinationCertificate(today(minus = 20.days))
            .withVaccinationVaccine(ALLOWED_VACCINE_TYPE)
            .withVaccinationMedicinalProduct(ALLOWED_VACCINE_MEDICINAL_PRODUCT)
            .withVaccinationManufacturer(ALLOWED_VACCINE_MANUFACTURER)

        val testDcc = negativeAntigenicTestCertificate(now().minus(3.hours))

        givenBaseHeaders()
            .body(
                DccAggregationRequest(
                    publicKey = CLIENT_PUBLIC_KEY_BASE64,
                    certificates = listOf(
                        encodeToDccAndEncrypt(vaccinationDcc),
                        encodeToDccAndEncrypt(vaccinationDcc2),
                        encodeToDccAndEncrypt(testDcc),
                    ),
                )
            )

            .When()
            .post("/api/v1/aggregate")

            .then()
            .statusCode(BAD_REQUEST.value())
            .body("error", equalTo(BAD_REQUEST.reasonPhrase))
            .body("message", equalTo("Request body contains invalid attributes"))
            .body("errors[0].field", equalTo("certificates"))
            .body("errors[0].code", equalTo("INCOMPATIBLE_DCC_COMBINATION"))
            .body("errors[0].message", equalTo("Provided certificates are not compatible with any operation"))
            .body("errors.size()", equalTo(1))
    }
}
