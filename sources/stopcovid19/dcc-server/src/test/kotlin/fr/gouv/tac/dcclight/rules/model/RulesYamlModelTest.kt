package fr.gouv.tac.dcclight.rules.model

import org.junit.jupiter.api.Test
import org.springframework.core.io.ClassPathResource

class RulesYamlModelTest {

    private val yaml = ClassPathResource("/aggregation/rules-test.yml")
        .inputStream.use { it.bufferedReader().readText() }

    private lateinit var rules: List<Rule>

    @Test
    fun can_be_parsed() {
        yamlRulesObjectMapper().loadAs(yaml, YamlRules::class.java)
    }

    @Test
    fun can_be_converted_to_business_model() {
        rules = yamlRulesObjectMapper().loadAs(yaml, YamlRules::class.java)
            .toRules()
            .rules
    }
}
