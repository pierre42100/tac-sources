package fr.gouv.tac.dcclight.test.matchers

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.spotify.hamcrest.jackson.JsonMatchers.jsonObject
import ehn.techiop.hcert.kotlin.chain.Error
import fr.gouv.tac.dcclight.test.DccSigningKeysManager.DccSigningKeys
import fr.gouv.tac.dcclight.test.eu.DccCryptographer.Companion.decodeCertificate
import kotlinx.datetime.Clock
import kotlinx.datetime.Instant
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.TypeSafeDiagnosingMatcher
import kotlin.time.Duration.Companion.days
import kotlin.time.Duration.Companion.hours
import kotlin.time.Duration.Companion.minutes

private val dscValidityEndInstant = Clock.System.now().plus(30.days)

fun isHcertEncoded(
    issuedBy: DccSigningKeys,
    issuedAt: Matcher<Instant> = isNear(Clock.System.now(), 1.minutes),
    expirationTime: Matcher<Instant> = isNear(dscValidityEndInstant, 12.hours),
    euDcc: Matcher<JsonNode> = jsonObject()
) =
    HcertEncodedMatcher(issuedBy, issuedAt, expirationTime, euDcc)

class HcertEncodedMatcher(
    private val signingKeys: DccSigningKeys,
    private val issuedAtMatcher: Matcher<Instant>,
    private val expirationTimeMatcher: Matcher<Instant>,
    private val euDccMatcher: Matcher<JsonNode>,
) : TypeSafeDiagnosingMatcher<String>() {

    override fun describeTo(description: Description) {
        description
            .appendText("an HCERT encoded string issued by certificate ")
            .appendValue(signingKeys)
            .appendText(" at ")
        issuedAtMatcher.describeTo(description)
        description.appendText(" and expiring on ")
        expirationTimeMatcher.describeTo(description)
        description.appendText(" and euDcc ")
        euDccMatcher.describeTo(description)
    }

    override fun matchesSafely(item: String, mismatchDescription: Description): Boolean {
        val decodedResult = decodeCertificate(item, signingKeys)
        if (decodedResult.verificationResult.error == Error.KEY_NOT_IN_TRUST_LIST) {
            mismatchDescription.appendText("was not signed by ").appendValue(signingKeys)
            return false
        }
        val rawEuDcc: JsonNode = jacksonObjectMapper()
            .readTree(decodedResult.chainDecodeResult.step0rawEuGcc)

        mismatchDescription
            .appendText("issuedAt was ")
            .appendValue(decodedResult.verificationResult.issuedAt)
            .appendText(" and expirationTime was ")
            .appendValue(decodedResult.verificationResult.expirationTime)
            .appendText(" and euDcc ")
        euDccMatcher.describeMismatch(rawEuDcc, mismatchDescription)

        return issuedAtMatcher.matches(decodedResult.verificationResult.issuedAt) &&
            expirationTimeMatcher.matches(decodedResult.verificationResult.expirationTime) &&
            euDccMatcher.matches(rawEuDcc)
    }
}
