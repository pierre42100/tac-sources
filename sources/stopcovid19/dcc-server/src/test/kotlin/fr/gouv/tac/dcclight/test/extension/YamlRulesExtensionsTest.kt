package fr.gouv.tac.dcclight.test.extension

import fr.gouv.tac.dcclight.rules.model.YamlInputRule
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.DisplayNameGeneration
import org.junit.jupiter.api.DisplayNameGenerator
import org.junit.jupiter.api.Test
import java.util.Map.entry

@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores::class)
class YamlRulesExtensionsTest {

    @Test
    fun withAllowListItem_updates_map_entry() {

        val input = YamlInputRule(
            type = "t",
            allowLists = mapOf(
                "truc" to listOf("machin")
            )
        )

        val updatedInput = input.withAllowListItem("truc", listOf("chose"))
        Assertions.assertThat(updatedInput.allowLists)
            .contains(entry("truc", listOf("chose")))
            .doesNotContain(
                entry("truc", listOf("machin"))
            )
    }

    @Test
    fun withAllowListItem_adds_map_entry() {

        val input = YamlInputRule(
            type = "t",
            allowLists = mapOf(
                "truc" to listOf("machin")
            )
        )

        val updatedInput = input.withAllowListItem("bidule", listOf("chose"))
        Assertions.assertThat(updatedInput.allowLists)
            .contains(
                entry("truc", listOf("machin")),
                entry(
                    "bidule", listOf("chose")
                )
            )
    }

    @Test
    fun withBlockListItem_updates_map_entry() {

        val input = YamlInputRule(
            type = "t",
            blockLists = mapOf(
                "truc" to listOf("machin")
            )
        )

        val updatedInput = input.withBlockListItem("truc", listOf("chose"))
        Assertions.assertThat(updatedInput.blockLists)
            .contains(entry("truc", listOf("chose")))
            .doesNotContain(
                entry("truc", listOf("machin"))
            )
    }

    @Test
    fun withBlockListItem_adds_map_entry() {

        val input = YamlInputRule(
            type = "t",
            blockLists = mapOf(
                "truc" to listOf("machin")
            )
        )

        val updatedInput = input.withBlockListItem("bidule", listOf("chose"))
        Assertions.assertThat(updatedInput.blockLists)
            .contains(
                entry("truc", listOf("machin")),
                entry(
                    "bidule", listOf("chose")
                )
            )
    }
}
