package fr.gouv.tac.dcclight.test

import fr.gouv.tac.dcclight.rules.model.YamlDateConstraints
import fr.gouv.tac.dcclight.rules.model.YamlInputRule
import fr.gouv.tac.dcclight.rules.model.YamlOutputRule
import fr.gouv.tac.dcclight.rules.model.YamlRule
import fr.gouv.tac.dcclight.rules.model.YamlRules
import fr.gouv.tac.dcclight.test.extension.addInputRule
import fr.gouv.tac.dcclight.test.extension.toJavaDate
import fr.gouv.tac.dcclight.test.extension.withOutputRule
import kotlinx.datetime.LocalDate

fun vaccine2_2OutputRule() = YamlOutputRule(
    schema = "DCC",
    type = "v",
    overrideValues = mapOf(
        "dn" to "2",
        "sd" to "2"
    ),
    copyValues = mapOf(),
    incDateByDays = mapOf()
)

fun testOutputRule() = YamlOutputRule(
    schema = "DCC",
    type = "t",
    referentialDccRanking = 1,
    overrideValues = mapOf(),
    copyValues = mapOf(),
    incDateByDays = mapOf()
)

fun recoveryToTestOutputRule() = YamlOutputRule(
    schema = "DCC",
    type = "t",
    referentialDccRanking = 1,
    overrideValues = mapOf(),
    copyValues = mapOf(
        "tg" to "tg",
        "tt" to "tg",
        "sc" to "fr",
        "tr" to "tg",
        "co" to "co",
        "is" to "is"
    ),
    incDateByDays = mapOf()
)

fun testToRecoveryOutputRule() = YamlOutputRule(
    schema = "DCC",
    referentialDccRanking = 1,
    type = "r",
    overrideValues = mapOf(
        "is" to "DGSAG"
    ),
    copyValues = mapOf(
        "tg" to "tg",
        "fr" to "sc",
        "df" to "sc",
        "du" to "sc",
        "co" to "co"
    ),
    incDateByDays = mapOf(
        "df" to 11,
        "du" to 180
    )
)

fun defaultEmptyRuleList() = YamlRules(
    listOf(
        YamlRule(
            "Generated rule",
            "Generated rule",
            true,
            listOf(),
            YamlOutputRule()
        )
    )
)

fun antigenicTestYamlInputRule() = YamlInputRule(
    type = "t",
    proofDateConstraints = YamlDateConstraints(),
    allowLists = mapOf(
        "tr" to listOf("260373001"),
        "tt" to listOf("LP217198-3"),
        "ma" to listOf("1833", "1232")
    ),
    blockLists = mapOf()
)

fun recoveryInputRule() = YamlInputRule(type = "r")

fun pfizer1_1VaccinationInputRule() = YamlInputRule(
    type = "v",
    proofDateConstraints = YamlDateConstraints(minDaysAfterPreviousDcc = 0),
    allowLists = mapOf(
        "dn" to listOf("1"),
        "sd" to listOf("1"),
        "vp" to listOf("1119305005", "1119349007", "J07BX03", "IN_ALLOW_LIST"),
        "mp" to listOf(
            "EU/1/20/1528",
            "EU/1/20/1507",
            "EU/1/21/1529",
            "NVX-CoV2373",
            "Covid-19-recombinant",
            "IN_ALLOW_LIST"
        ),
        "ma" to listOf(
            "ORG-100001699",
            "ORG-100030215",
            "ORG-100031184",
            "ORG-100032020",
            "ORG-100001699",
            "Fiocruz",
            "IN_ALLOW_LIST"
        )
    ),
    blockLists = mapOf()
)

fun getRuleWithInputVaccine1_1ValidBeforeDate(date: LocalDate): YamlRules {
    val vaccine1on1InputRule = pfizer1_1VaccinationInputRule()
        .copy(proofDateConstraints = YamlDateConstraints(validIfBefore = date.toJavaDate()))
    return defaultEmptyRuleList()
        .addInputRule(vaccine1on1InputRule)
        .withOutputRule(vaccine2_2OutputRule().copy(referentialDccRanking = 1))
}

fun getRuleWithInputVaccine1_1ValidAfterDate(date: LocalDate): YamlRules {
    val vaccine1on1InputRule = pfizer1_1VaccinationInputRule()
        .copy(proofDateConstraints = YamlDateConstraints(validIfAfter = date.toJavaDate()))
    return defaultEmptyRuleList()
        .addInputRule(vaccine1on1InputRule)
        .withOutputRule(vaccine2_2OutputRule().copy(referentialDccRanking = 1))
}
