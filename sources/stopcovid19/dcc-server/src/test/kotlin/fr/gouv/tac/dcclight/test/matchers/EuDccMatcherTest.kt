package fr.gouv.tac.dcclight.test.matchers

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.equalTo
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows

class EuDccMatcherTest {

    private val euDccForJeromeDupont = jacksonObjectMapper().readTree(
        """
            {
              "ver": "1.3.0",
              "dob": "1955-05-05",
              "nam": {
                "fnt": "DUPONT",
                "fn": "Dupont",
                "gnt": "JEROME",
                "gn": "Jérôme"
              }
            }
            """
    )

    @Test
    fun can_verify_default_subject_attributes() {
        assertThat(
            euDccForJeromeDupont,
            isEuDcc()
                .forJeromeDupont()
        )
    }

    @Test
    fun can_detect_default_subject_attributes_mismatch() {
        val euDccForJeanMartin = jacksonObjectMapper().readTree(
            """
            {
              "ver": "1.0.0",
              "dob": "2010-01-30",
              "nam": {
                "fnt": "MARTIN",
                "fn": "Martin",
                "gnt": "JEAN",
                "gn": "Jean"
              }
            }
            """
        )
        val error: AssertionError = assertThrows {
            assertThat(
                euDccForJeanMartin,
                isEuDcc()
                    .forJeromeDupont()
            )
        }
        assertThat(
            error.message?.replace("\r", ""),
            equalTo(
                """

                Expected:  json containing {
                  "ver": a text node with value that is "1.3.0"
                  "dob": a text node with value that is "1955-05-05"
                  "nam": {
                    "fnt": a text node with value that is "DUPONT"
                    "fn": a text node with value that is "Dupont"
                    "gnt": a text node with value that is "JEROME"
                    "gn": a text node with value that is "Jérôme"
                  }
                }
                     but: was json {
                  "ver" : "1.0.0",
                  "dob" : "2010-01-30",
                  "nam" : {
                    "fnt" : "MARTIN",
                    "fn" : "Martin",
                    "gnt" : "JEAN",
                    "gn" : "Jean"
                  }
                } with differences {
                  "ver": was a text node with value that was "1.0.0"
                  "dob": was a text node with value that was "2010-01-30"
                  "nam": {
                    "fnt": was a text node with value that was "MARTIN"
                    "fn": was a text node with value that was "Martin"
                    "gnt": was a text node with value that was "JEAN"
                    "gn": was a text node with value that was "Jean"
                  }
                }
                """.trimIndent()
            )
        )
    }

    @Test
    fun can_detect_invalid_custom_attribute_ver() {
        val error: AssertionError = assertThrows {
            assertThat(
                euDccForJeromeDupont,
                isEuDcc()
                    .forJeromeDupontWith(ver = "CUSTOM")
            )
        }
        assertThat(
            error.message?.replace("\r", ""),
            equalTo(
                """

                Expected:  json containing {
                  "ver": a text node with value that is "CUSTOM"
                  "dob": a text node with value that is "1955-05-05"
                  "nam": {
                    "fnt": a text node with value that is "DUPONT"
                    "fn": a text node with value that is "Dupont"
                    "gnt": a text node with value that is "JEROME"
                    "gn": a text node with value that is "Jérôme"
                  }
                }
                     but: was json {
                  "ver" : "1.3.0",
                  "dob" : "1955-05-05",
                  "nam" : {
                    "fnt" : "DUPONT",
                    "fn" : "Dupont",
                    "gnt" : "JEROME",
                    "gn" : "Jérôme"
                  }
                } with differences {
                  "ver": was a text node with value that was "1.3.0"
                  ...
                }
                """.trimIndent()
            )
        )
    }

    @Test
    fun can_detect_vaccination_attributes_mismatch() {
        val euDccVaccinationForJeromeDupont = jacksonObjectMapper().readTree(
            """
            {
              "ver": "1.3.0",
              "dob": "1955-05-05",
              "nam": {
                "fnt": "DUPONT",
                "fn": "Dupont",
                "gnt": "JEROME",
                "gn": "Jérôme"
              },
              "v": [{
                "is": "someone"
              }]              
            }
            """
        )
        val error: AssertionError = assertThrows {
            assertThat(
                euDccVaccinationForJeromeDupont,
                isEuDcc()
                    .forJeromeDupont()
                    .withProofType("v")
                    .where("sd", "2022-03-30")
            )
        }
        assertThat(
            error.message?.replace("\r", ""),
            equalTo(
                """

                Expected:  json containing {
                  "ver": a text node with value that is "1.3.0"
                  "dob": a text node with value that is "1955-05-05"
                  "nam": {
                    "fnt": a text node with value that is "DUPONT"
                    "fn": a text node with value that is "Dupont"
                    "gnt": a text node with value that is "JEROME"
                    "gn": a text node with value that is "Jérôme"
                  }
                  "v": an array node whose elements iterable containing [{
                    "sd": a text node with value that is "2022-03-30"
                  }]
                }
                     but: was json {
                  "ver" : "1.3.0",
                  "dob" : "1955-05-05",
                  "nam" : {
                    "fnt" : "DUPONT",
                    "fn" : "Dupont",
                    "gnt" : "JEROME",
                    "gn" : "Jérôme"
                  },
                  "v" : [ {
                    "is" : "someone"
                  } ]
                } with differences {
                  ...
                  "v": was an array node whose elements item 0: {
                    "sd": was not a string node, but a missing node
                  }
                }
                """.trimIndent()
            )
        )
    }
}
