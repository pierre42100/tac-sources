package fr.gouv.tac.dcclight.controller

import com.spotify.hamcrest.jackson.IsJsonObject.jsonObject
import fr.gouv.tac.dcclight.api.model.DccAggregationRequest
import fr.gouv.tac.dcclight.test.DccSigningKeysManager.Companion.DCC_SERVER_KEYS
import fr.gouv.tac.dcclight.test.IntegrationTest
import fr.gouv.tac.dcclight.test.RestAssuredManager.Companion.When
import fr.gouv.tac.dcclight.test.RestAssuredManager.Companion.bodyEncrypted
import fr.gouv.tac.dcclight.test.RestAssuredManager.Companion.givenBaseHeaders
import fr.gouv.tac.dcclight.test.TransportEncryptionKeysManager.Companion.CLIENT_PUBLIC_KEY_BASE64
import fr.gouv.tac.dcclight.test.eu.DccCryptographer.Companion.encodeToDccAndEncrypt
import fr.gouv.tac.dcclight.test.extension.withFamilyName
import fr.gouv.tac.dcclight.test.extension.withFamilyNameTransliterated
import fr.gouv.tac.dcclight.test.extension.withGivenName
import fr.gouv.tac.dcclight.test.extension.withGivenNameTransliterated
import fr.gouv.tac.dcclight.test.matchers.isEuDcc
import fr.gouv.tac.dcclight.test.matchers.isHcertEncoded
import fr.gouv.tac.dcclight.test.matchers.matchesUvciPattern
import fr.gouv.tac.dcclight.test.matchers.where
import fr.gouv.tac.dcclight.test.pfizer1_1VaccinationCertificate
import fr.gouv.tac.dcclight.test.pfizer2_2VaccinationCertificate
import fr.gouv.tac.dcclight.test.positiveAntigenicTestCertificate
import fr.gouv.tac.dcclight.test.recoveryStatementCertificate
import fr.gouv.tac.dcclight.test.today
import kotlinx.datetime.Clock.System.now
import org.junit.jupiter.api.Test
import org.springframework.http.HttpStatus.OK
import kotlin.time.Duration.Companion.days

@IntegrationTest
class AggregationTest {

    @Test
    fun double_vaccination_input_matches_vaccination_rule_and_does_not_override_issuer_because_absent_of_output_rule_with_check_on_expiration_time() {
        // target rule name: "Double vaccination"

        val vaccinationDcc = pfizer2_2VaccinationCertificate(today(minus = 100.days))

        val vaccination2Dcc = pfizer1_1VaccinationCertificate(today(minus = 39.days))

        givenBaseHeaders()
            .body(
                DccAggregationRequest(
                    publicKey = CLIENT_PUBLIC_KEY_BASE64,
                    certificates = listOf(
                        encodeToDccAndEncrypt(vaccinationDcc),
                        encodeToDccAndEncrypt(vaccination2Dcc)
                    ),
                )
            )

            .When()
            .post("/api/v1/aggregate")

            .then()
            .statusCode(OK.value())
                /*
                 * vaccination2 is the dcc with index equal to "referenceDccRanking" in output rule
                 * @see fr.gouv.tac.dcclight.rules.OutputRule.referentialDccRanking
                 */
            .bodyEncrypted(
                "response",
                jsonObject()
                    .where(
                        "certificate",
                        isHcertEncoded(
                            issuedBy = DCC_SERVER_KEYS,
                            euDcc = isEuDcc()
                                .forJeromeDupont()
                                .withProofType("v")
                                .where("dn", 3)
                                .where("ma", "ORG-100030215")
                                .where("vp", "1119349007")
                                .where("dt", today(minus = 39.days).toString())
                                .where("co", "FR")
                                .where("ci", matchesUvciPattern())
                                .where("mp", "EU/1/20/1528")
                                .where("is", "CNAM")
                                .where("sd", 3)
                                .where("tg", "840539006")
                        )
                    )
            )
    }

    @Test
    fun double_vaccination_input_matches_and_null_certificates_are_ignored() {
        // target rule name: "Double vaccination"

        val vaccinationDcc = pfizer2_2VaccinationCertificate(today(minus = 100.days))

        val vaccination2Dcc = pfizer1_1VaccinationCertificate(today(minus = 39.days))

        givenBaseHeaders()
            .body(
                """
                            {
                                "publicKey": "$CLIENT_PUBLIC_KEY_BASE64",
                                "certificates": [
                                    "${encodeToDccAndEncrypt(vaccinationDcc)}",
                                    "${encodeToDccAndEncrypt(vaccination2Dcc)}",
                                    null
                                ]
                            }
                """.trimIndent()
            )

            .When()
            .post("/api/v1/aggregate")

            .then()
            .statusCode(OK.value())
    }

    @Test
    fun vaccination_and_test_combination_input_match_configured_rule() {
        // target rule name: "Positive antigenic test + vaccination"

        val now = now()

        val vaccinationDcc = pfizer1_1VaccinationCertificate(today(minus = 7.days))
        val testDcc = positiveAntigenicTestCertificate(now.minus(60.days))

        givenBaseHeaders()
            .body(
                DccAggregationRequest(
                    publicKey = CLIENT_PUBLIC_KEY_BASE64,
                    certificates = listOf(
                        encodeToDccAndEncrypt(vaccinationDcc),
                        encodeToDccAndEncrypt(testDcc)
                    ),
                )
            )

            .When()
            .post("/api/v1/aggregate")

            .then()
            .statusCode(OK.value())
            .bodyEncrypted(
                "response",
                jsonObject()
                    .where(
                        "certificate",
                        isHcertEncoded(
                            issuedBy = DCC_SERVER_KEYS,
                            euDcc = isEuDcc()
                                .forJeromeDupont()
                                .withProofType("v")
                                .where("dn", 3)
                                .where("ma", "ORG-100030215")
                                .where("vp", "1119349007")
                                .where("dt", today(minus = 7.days).toString())
                                .where("co", "FR")
                                .where("ci", matchesUvciPattern())
                                .where("mp", "EU/1/20/1528")
                                .where("is", "CNAM")
                                .where("sd", 3)
                                .where("tg", "840539006")
                        )
                    )
            )
    }

    @Test
    fun vaccination_and_test_combination_input_match_configured_rule_when_vaccination_is_in_less_than_2_days() {
        // target rule name: "Positive antigenic test + vaccination"

        val now = now()

        val vaccinationDcc = pfizer1_1VaccinationCertificate(today(minus = 1.days))
        val testDcc = positiveAntigenicTestCertificate(now.minus(60.days))

        givenBaseHeaders()
            .body(
                DccAggregationRequest(
                    publicKey = CLIENT_PUBLIC_KEY_BASE64,
                    certificates = listOf(
                        encodeToDccAndEncrypt(vaccinationDcc),
                        encodeToDccAndEncrypt(testDcc)
                    ),
                )
            )

            .When()
            .post("/api/v1/aggregate")

            .then()
            .statusCode(OK.value())
            .bodyEncrypted(
                "response",
                jsonObject()
                    .where(
                        "certificate",
                        isHcertEncoded(
                            issuedBy = DCC_SERVER_KEYS,
                            euDcc = isEuDcc()
                                .forJeromeDupont()
                                .withProofType("v")
                                .where("dn", 3)
                                .where("ma", "ORG-100030215")
                                .where("vp", "1119349007")
                                .where("dt", today(minus = 1.days).toString())
                                .where("co", "FR")
                                .where("ci", matchesUvciPattern())
                                .where("mp", "EU/1/20/1528")
                                .where("is", "CNAM")
                                .where("sd", 3)
                                .where("tg", "840539006")
                        )
                    )
            )
    }

    @Test
    fun triple_vaccination_input_matches_triple_vaccination_rule_with_vaccinations_not_time_ordered() {
        // target rule name: "Triple vaccination"

        val vaccinationDcc = pfizer2_2VaccinationCertificate(today(minus = 200.days))
        val vaccination2Dcc = pfizer1_1VaccinationCertificate(today(minus = 140.days))
        val vaccination3Dcc = pfizer1_1VaccinationCertificate(today(minus = 45.days))

        givenBaseHeaders()
            .body(
                DccAggregationRequest(
                    publicKey = CLIENT_PUBLIC_KEY_BASE64,
                    certificates = listOf(
                        encodeToDccAndEncrypt(vaccination2Dcc),
                        encodeToDccAndEncrypt(vaccination3Dcc),
                        encodeToDccAndEncrypt(vaccinationDcc),
                    ),
                )
            )

            .When()
            .post("/api/v1/aggregate")

            .then()
            .statusCode(OK.value())
            .bodyEncrypted(
                "response",
                jsonObject()
                    .where(
                        "certificate",
                        isHcertEncoded(
                            issuedBy = DCC_SERVER_KEYS,
                            euDcc = isEuDcc()
                                .forJeromeDupont()
                                .withProofType("v")
                                .where("dn", 3)
                                .where("ma", "ORG-100030215")
                                .where("vp", "1119349007")
                                .where("dt", today(minus = 45.days).toString())
                                .where("co", "FR")
                                .where("ci", matchesUvciPattern())
                                .where("mp", "EU/1/20/1528")
                                .where("is", "CNAM")
                                .where("sd", 3)
                                .where("tg", "840539006")
                        )
                    )
            )
    }

    @Test
    fun recovery_vaccination_test_input_matches_triple_proofs_and_output_rules_overload_generated_dcc_output() {
        // target rule name: "Recovery, vaccination and test"

        val now = now()

        val recoveryDcc = recoveryStatementCertificate(now.minus(100.days))
        val vaccinationDcc = pfizer1_1VaccinationCertificate(today(minus = 30.days))
        val testDcc = positiveAntigenicTestCertificate(now.minus(5.days))

        givenBaseHeaders()
            .body(
                DccAggregationRequest(
                    publicKey = CLIENT_PUBLIC_KEY_BASE64,
                    certificates = listOf(
                        encodeToDccAndEncrypt(vaccinationDcc),
                        encodeToDccAndEncrypt(recoveryDcc),
                        encodeToDccAndEncrypt(testDcc),
                    ),
                )
            )

            .When()
            .post("/api/v1/aggregate")

            .then()
            .statusCode(OK.value())
            .bodyEncrypted(
                "response",
                jsonObject()
                    .where(
                        "certificate",
                        isHcertEncoded(
                            issuedBy = DCC_SERVER_KEYS,
                            euDcc = isEuDcc()
                                .forJeromeDupont()
                                .withProofType("t")
                                .where("sc", now.minus(5.days))
                                .where("ma", "1232")
                                .where("tt", "LP217198-3")
                                .where("tc", "Test centre west region 245")
                                .where("co", "FR")
                                .where("ci", matchesUvciPattern())
                                .where("is", "DGSAG")
                                .where("tg", "840539006")
                                .where("tr", "260373001")
                        )
                    )
            )
    }

    @Test
    fun vaccination_and_test_combination_input_match_even_if_transliterated_names_are_not_consistent() {
        // target rule name: "Positive test + vaccination"

        val now = now()

        val vaccinationDcc = pfizer1_1VaccinationCertificate(today(minus = 7.days))

        val testDcc = positiveAntigenicTestCertificate(now.minus(60.days))
            .withFamilyNameTransliterated("DUPONT<DIFFERENT")
            .withGivenNameTransliterated("JEROME<DIFFERENT")

        givenBaseHeaders()
            .body(
                DccAggregationRequest(
                    publicKey = CLIENT_PUBLIC_KEY_BASE64,
                    certificates = listOf(
                        encodeToDccAndEncrypt(vaccinationDcc),
                        encodeToDccAndEncrypt(testDcc)
                    ),
                )
            )
            .post("/api/v1/aggregate")
            .then()
            .statusCode(OK.value())
            .bodyEncrypted(
                "response",
                jsonObject()
                    .where(
                        "certificate",
                        isHcertEncoded(
                            issuedBy = DCC_SERVER_KEYS,
                            euDcc = isEuDcc()
                                .forJeromeDupont()
                                .withProofType("v")
                                .where("dn", 3)
                                .where("ma", "ORG-100030215")
                                .where("vp", "1119349007")
                                .where("dt", today(minus = 7.days))
                                .where("co", "FR")
                                .where("ci", matchesUvciPattern())
                                .where("mp", "EU/1/20/1528")
                                .where("is", "CNAM")
                                .where("sd", 3)
                                .where("tg", "840539006")
                        )
                    )
            )
    }

    @Test
    fun vaccination_and_test_combination_input_match_even_if_names_are_not_consistent() {
        // target rule name: "Positive test + vaccination"

        val now = now()

        val vaccinationDcc = pfizer1_1VaccinationCertificate(today(minus = 7.days))

        val testDcc = positiveAntigenicTestCertificate(now.minus(60.days))
            .withGivenName("Jack")
            .withFamilyName("Johnson")

        givenBaseHeaders()
            .body(
                DccAggregationRequest(
                    publicKey = CLIENT_PUBLIC_KEY_BASE64,
                    certificates = listOf(
                        encodeToDccAndEncrypt(vaccinationDcc),
                        encodeToDccAndEncrypt(testDcc)
                    ),
                )
            )
            .post("/api/v1/aggregate")
            .then()
            .statusCode(OK.value())
            .bodyEncrypted(
                "response",
                jsonObject()
                    .where(
                        "certificate",
                        isHcertEncoded(
                            issuedBy = DCC_SERVER_KEYS,
                            euDcc = isEuDcc()
                                .forJeromeDupont()
                                .withProofType("v")
                                .where("dn", 3)
                                .where("ma", "ORG-100030215")
                                .where("vp", "1119349007")
                                .where("dt", today(minus = 7.days).toString())
                                .where("co", "FR")
                                .where("ci", matchesUvciPattern())
                                .where("mp", "EU/1/20/1528")
                                .where("is", "CNAM")
                                .where("sd", 3)
                                .where("tg", "840539006")
                        )
                    )
            )
    }
}
