package fr.gouv.tac.dcclight.test.eu

import ehn.techiop.hcert.kotlin.chain.Error.SCHEMA_VALIDATION_FAILED
import ehn.techiop.hcert.kotlin.chain.SchemaValidationService
import ehn.techiop.hcert.kotlin.chain.VerificationException
import ehn.techiop.hcert.kotlin.chain.VerificationResult
import ehn.techiop.hcert.kotlin.data.CborObject
import ehn.techiop.hcert.kotlin.data.GreenCertificate
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import net.pwall.json.schema.JSONSchema.Companion.parse
import org.springframework.core.io.ClassPathResource

/**
 * Validates the HCERT data against the JSON schema.
 */
class DCCLightSchemaValidationService : SchemaValidationService {

    @OptIn(ExperimentalSerializationApi::class)
    override fun validate(cbor: CborObject, verificationResult: VerificationResult): GreenCertificate {

        val schema = parse(ClassPathResource("json/dcclight/schema/1.0.0/DCC-Activités.schema.json").file)
        val json = cbor.toJsonString()
        val result = schema.validateBasic(json)
        val errors = result.errors?.map { SchemaError("${it.error}, ${it.keywordLocation}, ${it.instanceLocation}") }
            ?: listOf()
        if (errors.isNotEmpty()) throw VerificationException(
            SCHEMA_VALIDATION_FAILED,
            "Data does not follow fallback schema: $errors}",
            details = mapOf("schemaErrors" to errors.joinToString())
        )
        return Json.decodeFromString(json)
    }
}

data class SchemaError(val error: String)
