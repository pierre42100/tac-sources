# language: fr
Fonctionnalité: Cas11 Infection (recovery) puis vaccination doubledose

  Plan du Scénario: Cas nominaux avec différentes combinaisons de vaccins 2/2 et preuve de test antigénique positive
    Etant donné une preuve de rétablissement valable depuis 100 jours en FR au nom de MARIE, CURIE - Marie, Curie né le 14/04/1979
    Et une preuve de vaccin <vaccin> 2/2 injecté il y a 5 jours en FR au nom de MARIE, CURIE - Marie, Curie né le 14/04/1979
    Quand une demande d'agrégation de preuves est réalisée
    Alors réception d'une preuve de type vaccination valide à partir de maintenant au nom de MARIE, CURIE - Marie, Curie né le 14/04/1979
    Et la preuve de vaccination est émise par DGSAG
    Et la preuve de vaccination a un nombre de doses total de 2/1

    Exemples:
      | vaccin               |
      | Pfizer               |
      | Moderna              |
      | Astra-zeneca         |
      | Covid-19-recombinant |

  Plan du Scénario: Cas nominaux avec différentes combinaisons de pays et schéma vaccinal valide
    Etant donné une preuve de rétablissement valable depuis 100 jours en <code pays 1> au nom de MARIE, CURIE - Marie, Curie né le 14/04/1979
    Et une preuve de vaccin Pfizer 2/2 injecté il y a 5 jours en <code pays 2> au nom de MARIE, CURIE - Marie, Curie né le 14/04/1979
    Quand une demande d'agrégation de preuves est réalisée
    Alors réception d'une preuve de type vaccination valide à partir de maintenant au nom de MARIE, CURIE - Marie, Curie né le 14/04/1979
    Et la preuve de vaccination est émise par DGSAG
    Et la preuve de vaccination a un nombre de doses total de 2/1

    Exemples:
      | code pays 1   | code pays 2 |
      | FR            | FR          |
      | ES            | DE          |
      | IT            | ES          |
      | FR            | IT          |
      | FR            | LU          |

  Scénario: Cas de rejet si preuve de rétablissement et test antigénique valides
    Etant donné une preuve de rétablissement valable depuis 100 jours en FR au nom de PIERRE, CURIE - Pierre, Curie né le 14/04/1979
    Et une preuve positive de test pcr 1341 effectué il y a 1 jour en FR par CNAM au nom de PIERRE, CURIE - Pierre, Curie né le 14/04/1979
    Quand une demande d'agrégation de preuves est réalisée
    Alors l'utilisateur ne reçoit pas de preuve

  Scénario: Cas de rejet si preuve de rétablissement et test PCR valides
    Etant donné une preuve de rétablissement valable depuis 100 jours en FR au nom de PIERRE, CURIE - Pierre, Curie né le 14/04/1979
    Et une preuve positive de test pcr 1341 effectué il y a 1 jour en FR par CNAM au nom de PIERRE, CURIE - Pierre, Curie né le 14/04/1979
    Quand une demande d'agrégation de preuves est réalisée
    Alors l'utilisateur ne reçoit pas de preuve

  Scénario: Cas de rejet si les preuves envoyées sont deux rétablissements valides
    Etant donné une preuve de rétablissement valable depuis 100 jours en FR au nom de PIERRE, CURIE - Pierre, Curie né le 14/04/1979
    Et une preuve de rétablissement valable depuis 2 jours en FR au nom de PIERRE, CURIE - Pierre, Curie né le 14/04/1979
    Quand une demande d'agrégation de preuves est réalisée
    Alors l'utilisateur ne reçoit pas de preuve

  Scénario: Cas de rejet si preuve de rétablissement avec date de validité passée et vaccin 2/2 valide
    Etant donné une preuve de rétablissement valable depuis 3800 jours en FR au nom de PIERRE, CURIE - Pierre, Curie né le 14/04/1979
    Et une preuve de vaccin Pfizer 2/2 injecté il y a 10 jours en FR au nom de PIERRE, CURIE - Pierre, Curie né le 14/04/1979
    Quand une demande d'agrégation de preuves est réalisée
    Alors l'utilisateur ne reçoit pas de preuve

  Plan du Scénario: Cas de rejets si le nombre de doses est incompatible
    Etant donné une preuve de rétablissement valable depuis 100 jours en FR au nom de PIERRE, CURIE - Pierre, Curie né le 14/04/1979
    Et une preuve de vaccin Pfizer <doses vaccin> injecté il y a 5 jours en FR au nom de PIERRE, CURIE - Pierre, Curie né le 14/04/1979
    Quand une demande d'agrégation de preuves est réalisée
    Alors l'utilisateur ne reçoit pas de preuve

    Exemples:
      | doses vaccin |
      | 1/2          |
      | 3/2          |
      | 1/1          |
      | 3/3          |
