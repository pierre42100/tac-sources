# language: fr
Fonctionnalité: Cas3 trois injections dans 3 SI differents

  Plan du Scénario: Cas nominaux avec différentes combinaisons de vaccins 1/1
    Etant donné une preuve de vaccin <vaccin 1> 1/1 injecté il y a 200 jours en FR au nom de MARIE, CURIE - Marie, Curie né le 14/04/1979
    Et une preuve de vaccin <vaccin 2> 1/1 injecté il y a 8 jours en FR au nom de MARIE, CURIE - Marie, Curie né le 14/04/1979
    Et une preuve de vaccin <vaccin 3> 1/1 injecté il y a 100 jours en FR au nom de MARIE, CURIE - Marie, Curie né le 14/04/1979
    Quand une demande d'agrégation de preuves est réalisée
    Alors réception d'une preuve de type vaccination valide à partir de maintenant au nom de MARIE, CURIE - Marie, Curie né le 14/04/1979
    Et la preuve de vaccination est émise par DGSAG
    Et la preuve de vaccination a un nombre de doses total de 3/1

    Exemples:
      | vaccin 1             | vaccin 2     | vaccin 3     |
      | Pfizer               | Moderna      | Moderna      |
      | Moderna              | Astra-zeneca | Astra-zeneca |
      | Astra-zeneca         | Moderna      | Moderna      |
      | Novavax              | Pfizer       | Pfizer       |
      | Pfizer               | Pfizer       | Pfizer       |
      | Moderna              | Moderna      | Moderna      |
      | Astra-zeneca         | Astra-zeneca | Astra-zeneca |
      | Covid-19-recombinant | Moderna      | Moderna      |

  Plan du Scénario: Cas nominaux avec différentes combinaisons de pays et schéma vaccinal valide
    Etant donné une preuve de vaccin Pfizer 1/1 injecté il y a 200 jours en <code pays 1> au nom de MARIE, CURIE - Marie, Curie né le 14/04/1979
    Et une preuve de vaccin Pfizer 1/1 injecté il y a 100 jours en <code pays 1> au nom de MARIE, CURIE - Marie, Curie né le 14/04/1979
    Et une preuve de vaccin Pfizer 1/1 injecté il y a 10 jours en <code pays 2> au nom de MARIE, CURIE - Marie, Curie né le 14/04/1979
    Quand une demande d'agrégation de preuves est réalisée
    Alors réception d'une preuve de type vaccination valide à partir de maintenant au nom de MARIE, CURIE - Marie, Curie né le 14/04/1979
    Et la preuve de vaccination est émise par DGSAG
    Et la preuve de vaccination a un nombre de doses total de 3/1

    Exemples:
      | code pays 1   | code pays 2 |
      | FR            | FR          |
      | DE            | DE          |
      | ES            | ES          |
      | IT            | IT          |
      | LU            | LU          |
      | BE            | BE          |
      | CH            | CH          |
      | DK            | DK          |

  Scénario: Cas nominal si preuve 1/1 avec date de validité passée et 2 preuves vaccinales 1/1 valides
    Etant donné une preuve de vaccin Pfizer 1/1 injecté il y a 100 jours en FR au nom de PIERRE, CURIE - Pierre, Curie né le 29/02/2000
    Et une preuve de vaccin Pfizer 1/1 injecté il y a 3800 jours en FR au nom de PIERRE, CURIE - Pierre, Curie né le 29/02/2000
    Et une preuve de vaccin Pfizer 1/1 injecté il y a 10 jours en FR au nom de PIERRE, CURIE - Pierre, Curie né le 29/02/2000
    Quand une demande d'agrégation de preuves est réalisée
    Alors réception d'une preuve de type vaccination valide à partir de maintenant au nom de PIERRE, CURIE - Pierre, Curie né le 29/02/2000
    Et la preuve de vaccination est émise par DGSAG
    Et la preuve de vaccination a un nombre de doses total de 3/1

  Scénario: Cas de rejet si preuve 1/1 avec type de vaccin erroné et 2 preuves vaccinales 1/1 valides
    Etant donné une preuve de vaccin mauvais medicinal product 1/1 injecté il y a 200 jours en FR au nom de PIERRE, CURIE - Pierre, Curie né le 29/02/2000
    Et une preuve de vaccin Pfizer 1/1 injecté il y a 10 jours en FR au nom de PIERRE, CURIE - Pierre, Curie né le 29/02/2000
    Et une preuve de vaccin Pfizer 1/1 injecté il y a 120 jours en FR au nom de PIERRE, CURIE - Pierre, Curie né le 29/02/2000
    Quand une demande d'agrégation de preuves est réalisée
    Alors l'utilisateur ne reçoit pas de preuve

  Scénario: Cas de rejet avec une liste de preuve trop grande comprenant 4 preuves valides ordonnée tel que "un vaccin 1/1, un vaccin 1/1, un vaccin 1/1, un vaccin 2/2"
    Etant donné une preuve de vaccin Pfizer 1/1 injecté il y a 250 jours en FR au nom de PIERRE, CURIE - Pierre, Curie né le 29/02/2000
    Et une preuve de vaccin Pfizer 1/1 injecté il y a 180 jours en FR au nom de PIERRE, CURIE - Pierre, Curie né le 29/02/2000
    Et une preuve de vaccin Pfizer 1/1 injecté il y a 100 jours en FR au nom de PIERRE, CURIE - Pierre, Curie né le 29/02/2000
    Et une preuve de vaccin Pfizer 2/2 injecté il y a 10 jours en FR au nom de PIERRE, CURIE - Pierre, Curie né le 29/02/2000
    Quand une demande d'agrégation de preuves est réalisée
    Alors l'utilisateur ne reçoit pas de preuve

  Plan du Scénario: Cas de rejets si le nombre de doses est incompatible
    Etant donné une preuve de vaccin Pfizer <doses vaccin 1> injecté il y a 200 jours en FR au nom de PIERRE, CURIE - Pierre, Curie né le 14/04/1979
    Et une preuve de vaccin Pfizer <doses vaccin 2> injecté il y a 100 jours en FR au nom de PIERRE, CURIE - Pierre, Curie né le 14/04/1979
    Et une preuve de vaccin Pfizer <doses vaccin 3> injecté il y a 10 jours en FR au nom de PIERRE, CURIE - Pierre, Curie né le 14/04/1979
    Quand une demande d'agrégation de preuves est réalisée
    Alors l'utilisateur ne reçoit pas de preuve

    Exemples:
      | doses vaccin 1 | doses vaccin 2 | doses vaccin 3 |
      | 1/2            | 1/1            | 1/1            |
      | 1/2            | 1/1            | 1/1            |
      | 3/3            | 1/1            | 1/1            |
      | 2/1            | 1/1            | 1/1            |
      | 3/1            | 1/1            | 1/1            |
