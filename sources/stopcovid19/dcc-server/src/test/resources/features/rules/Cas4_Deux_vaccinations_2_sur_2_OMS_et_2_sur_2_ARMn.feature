# language: fr
Fonctionnalité: Cas4 Deux vaccinations 2/2 OMS et 2/2 ARMn

  Plan du Scénario: Cas nominaux avec différentes combinaisons de vaccins 2/2 et 2/2
    Etant donné une preuve de vaccin <vaccin 1> 2/2 injecté il y a 130 jours en IN au nom de MARIE, CURIE - Marie, Curie né le 14/04/1979
    Et  une preuve de vaccin <vaccin 2> 2/2 injecté il y a 39 jours en FR au nom de MARIE, CURIE - Marie, Curie né le 14/04/1979
    Quand une demande d'agrégation de preuves est réalisée
    Alors réception d'une preuve de type vaccination valide à partir de maintenant au nom de MARIE, CURIE - Marie, Curie né le 14/04/1979
    Et la preuve de vaccination est émise par DGSAG
    Et la preuve de vaccination a un nombre de doses total de 3/3

    Exemples:
      | vaccin 1 | vaccin 2 |
      | Covovax  | Pfizer   |
      | Sinovac  | Moderna  |
      | Bharat   | Pfizer   |

  Plan du Scénario: Cas nominaux avec différentes combinaisons de pays et schéma vaccinal valide
    Etant donné une preuve de vaccin Sinovac 2/2 injecté il y a 125 jours en <code pays 1> au nom de MARIE, CURIE - Marie, Curie né le 14/04/1979
    Et une preuve de vaccin Pfizer 2/2 injecté il y a 2 jours en <code pays 2> au nom de MARIE, CURIE - Marie, Curie né le 14/04/1979
    Quand une demande d'agrégation de preuves est réalisée
    Alors réception d'une preuve de type vaccination valide à partir de maintenant au nom de MARIE, CURIE - Marie, Curie né le 14/04/1979
    Et la preuve de vaccination est émise par DGSAG
    Et la preuve de vaccination a un nombre de doses total de 3/3

    Exemples:
      | code pays 1 | code pays 2 |
      | IN          | FR          |
      | CN          | DE          |
      | GT          | ES          |
      | HK          | IT          |
      | IL          | LU          |
      | ID          | BE          |

  Plan du Scénario: Cas de rejets si le nombre de doses est incompatible
    Etant donné une preuve de vaccin Sinovac <doses vaccin 1> injecté il y a 125 jours en IN au nom de PIERRE, CURIE - Pierre, Curie né le 14/04/1979
    Et une preuve de vaccin Pfizer <doses vaccin 2> injecté il y a 2 jours en FR au nom de PIERRE, CURIE - Pierre, Curie né le 14/04/1979
    Quand une demande d'agrégation de preuves est réalisée
    Alors l'utilisateur ne reçoit pas de preuve

    Exemples:
      | doses vaccin 1 | doses vaccin 2 |
      | 3/1            | 2/2            |
      | 1/2            | 2/2            |
      | 2/1            | 2/2            |
      | 3/3            | 2/2            |
