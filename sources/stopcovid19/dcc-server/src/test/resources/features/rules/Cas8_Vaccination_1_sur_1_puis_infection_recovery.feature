# language: fr
Fonctionnalité: Cas8 Vaccination 1/1 puis infection (recovery)

  Plan du Scénario: Cas nominaux avec différentes combinaisons de vaccins 1/1 et recovery
    Etant donné une preuve de vaccin <vaccin> 1/1 injecté il y a 95 jours en FR au nom de MARIE, CURIE - Marie, Curie né le 14/04/1979
    Et une preuve de rétablissement valable depuis 2 jours en FR au nom de MARIE, CURIE - Marie, Curie né le 14/04/1979
    Quand une demande d'agrégation de preuves est réalisée
    Alors réception d'une preuve de type rétablissement valide à partir de maintenant au nom de MARIE, CURIE - Marie, Curie né le 14/04/1979
    Et la preuve de rétablissement est émise par TV-DGSAG

    Exemples:
      | vaccin               |
      | Pfizer               |
      | Moderna              |
      | Astra-zeneca         |
      | Novavax              |
      | Covid-19-recombinant |

  Plan du Scénario: Cas nominaux avec différentes combinaisons de pays et schéma vaccinal valide
    Etant donné une preuve de vaccin Pfizer 1/1 injecté il y a 95 jours en <code pays 2> au nom de MARIE, CURIE - Marie, Curie né le 14/04/1979
    Et une preuve de rétablissement valable depuis 2 jours en <code pays 1> au nom de MARIE, CURIE - Marie, Curie né le 14/04/1979
    Quand une demande d'agrégation de preuves est réalisée
    Alors réception d'une preuve de type rétablissement valide à partir de maintenant au nom de MARIE, CURIE - Marie, Curie né le 14/04/1979
    Et la preuve de rétablissement est émise par TV-DGSAG

    Exemples:
      | code pays 2   | code pays 1 |
      | FR            | FR          |
      | FR            | DE          |
      | ES            | ES          |
      | BE            | IT          |
      | FR            | LU          |
      | FR            | BE          |
      | LU            | CH          |
      | FR            | DK          |


  Scénario: Cas de rejet si preuve de rétablissement et test antigénique valides
    Etant donné une preuve de rétablissement valable depuis 100 jours en FR au nom de MARIE, CURIE - Marie, Curie né le 14/04/1979
    Et une preuve positive de test antigénique 1341 effectué il y a 2 jours en FR par CNAM au nom de MARIE, CURIE - Marie, Curie né le 14/04/1979
    Quand une demande d'agrégation de preuves est réalisée
    Alors l'utilisateur ne reçoit pas de preuve

  Scénario: Cas de rejet si preuve de rétablissement et test PCR valides
    Etant donné une preuve de rétablissement valable depuis 100 jours en FR au nom de MARIE, CURIE - Marie, Curie né le 14/04/1979
    Et une preuve positive de test pcr 1341 effectué il y a 2 jours en FR par CNAM au nom de MARIE, CURIE - Marie, Curie né le 14/04/1979
    Quand une demande d'agrégation de preuves est réalisée
    Alors l'utilisateur ne reçoit pas de preuve

  Scénario: Cas de rejet si les preuves envoyées sont deux rétablissements valides
    Etant donné une preuve de rétablissement valable depuis 100 jours en FR au nom de MARIE, CURIE - Marie, Curie né le 14/04/1979
    Et une preuve de rétablissement valable depuis 2 jours en FR au nom de MARIE, CURIE - Marie, Curie né le 14/04/1979
    Quand une demande d'agrégation de preuves est réalisée
    Alors l'utilisateur ne reçoit pas de preuve

  Scénario: Cas de rejet si preuve de rétablissement avec date de validité passée et vaccin 1/1 valide
    Etant donné une preuve de rétablissement valable depuis 400 jours en FR au nom de MARIE, CURIE - Marie, Curie né le 14/04/1979
    Et une preuve de vaccin Pfizer 1/1 injecté il y a 95 jours en FR au nom de MARIE, CURIE - Marie, Curie né le 14/04/1979
    Quand une demande d'agrégation de preuves est réalisée
    Alors l'utilisateur ne reçoit pas de preuve

  Plan du Scénario: Cas de rejets si le nombre de doses est incompatible
    Etant donné une preuve de rétablissement valable depuis 2 jours en FR au nom de MARIE, CURIE - Marie, Curie né le 14/04/1979
    Et une preuve de vaccin Pfizer <doses vaccin> injecté il y a 95 jours en FR au nom de MARIE, CURIE - Marie, Curie né le 14/04/1979
    Quand une demande d'agrégation de preuves est réalisée
    Alors l'utilisateur ne reçoit pas de preuve

    Exemples:
      | doses vaccin |
      | 2/1          |
      | 3/2          |
      | 3/3          |
      | 1/2          |
