# language: fr
Fonctionnalité: Conversion de TAG+ vers certificat de rétablissement

  Scénario: Cas nominal
    Etant donné une preuve positive de test antigénique 1833 effectué il y a 2 jours en FR par APHP au nom de MARIE, CURIE - Marie, Curie né le 14/04/1979
    Quand une demande de conversion de preuve est réalisée
    Alors réception d'une preuve de type rétablissement valide à partir de maintenant au nom de MARIE, CURIE - Marie, Curie né le 14/04/1979
    Et la preuve de rétablissement est émise par DGSAG
    Et le début de validité du certificat est la date de collection du test + 11 jours et la fin de validité est la date de collection du test + 180 jours

  Plan du Scénario: Cas nominal, les manufacturer "Autre/AUTRE" ne sont pas dans la allowlist
    Etant donné une preuve positive de test antigénique <fabricant> effectué il y a 2 jours en FR par APHP au nom de MARIE, CURIE - Marie, Curie né le 14/04/1979
    Quand une demande de conversion de preuve est réalisée
    Alors l'utilisateur ne reçoit pas de preuve

    Exemples:
      | fabricant |
      | Autre     |
      | AUTRE     |
