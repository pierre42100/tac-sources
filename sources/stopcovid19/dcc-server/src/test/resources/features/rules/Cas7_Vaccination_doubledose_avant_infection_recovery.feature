# language: fr
Fonctionnalité: Cas7 Vaccination doubledose avant infection (recovery)

  Plan du Scénario: Cas nominaux avec différentes combinaisons de vaccins 2/2 antérieures à une preuve de rétablissement
    Etant donné une preuve de vaccin <vaccin> 2/2 injecté il y a 100 jours en FR au nom de MARIE, CURIE - Marie, Curie né le 14/04/1979
    Et une preuve de rétablissement valable depuis 1 jours en FR au nom de MARIE, CURIE - Marie, Curie né le 14/04/1979
    Quand une demande d'agrégation de preuves est réalisée
    Alors réception d'une preuve de type rétablissement valide à partir de maintenant au nom de MARIE, CURIE - Marie, Curie né le 14/04/1979
    Et la preuve de rétablissement est émise par TV-DGSAG

    Exemples:
      | vaccin               |
      | Pfizer               |
      | Moderna              |
      | Astra-zeneca         |
      | Novavax              |
      | Covid-19-recombinant |

  Plan du Scénario: Cas nominaux avec différentes combinaisons de pays et schéma vaccinal valide
    Etant donné une preuve de vaccin Pfizer 2/2 injecté il y a 120 jours en <code pays 1> au nom de MARIE, CURIE - Marie, Curie né le 14/04/1979
    Et une preuve de rétablissement valable depuis 10 jours en <code pays 2> au nom de MARIE, CURIE - Marie, Curie né le 14/04/1979
    Quand une demande d'agrégation de preuves est réalisée
    Alors réception d'une preuve de type rétablissement valide à partir de maintenant au nom de MARIE, CURIE - Marie, Curie né le 14/04/1979
    Et la preuve de rétablissement est émise par TV-DGSAG

    Exemples:
      | code pays 1   | code pays 2 |
      | FR            | FR          |
      | ES            | DE          |
      | IT            | ES          |
      | FR            | IT          |
      | FR            | LU          |

  Plan du Scénario: Cas de rejets si le nombre de doses est incompatible
    Etant donné une preuve de vaccin Pfizer <doses vaccin> injecté il y a 120 jours en FR au nom de PIERRE, CURIE - Pierre, Curie né le 14/04/1979
    Et une preuve de rétablissement valable depuis 20 jours en FR au nom de PIERRE, CURIE - Pierre, Curie né le 14/04/1979
    Quand une demande d'agrégation de preuves est réalisée
    Alors l'utilisateur ne reçoit pas de preuve

    Exemples:
      | doses vaccin |
      | 3/2          |
      | 3/3          |
