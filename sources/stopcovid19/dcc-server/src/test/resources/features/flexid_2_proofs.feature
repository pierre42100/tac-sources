# language: fr
Fonctionnalité: Démonstration FlexID avec 2 preuves

  Contexte:
    Etant donné une preuve de vaccin Pfizer 1/1 injecté il y a 150 jours en FR au nom de MARIE, DUPONT - Marie, Dupont né le 29/02/2000
    Et une preuve de vaccin Pfizer 1/1 injecté il y a 80 jours en FR au nom de MARIE, DUPONT - Marie, Dupont né le 29/02/2000

  Scénario: Temoin : L'ensemble de preuves sans modifications est accepté
    Quand une demande d'agrégation de preuves est réalisée
    Alors l'utilisateur reçoit une preuve

  Scénario: 1 - Cas valide car l'ensemble [MARIE<JEANNE] est présent dans tous les ensembles de prénoms translitérés
    Lorsque le prénom translitéré de la preuve 1 est MARIE<JEANNE<ELISE
    Et le prénom translitéré de la preuve 2 est MARIE<JEANNE
    Quand une demande d'agrégation de preuves est réalisée
    Alors l'utilisateur reçoit une preuve

  Scénario: 2 - Cas valide car l'ensemble [MARIE<ADELINE] est présent dans tous les ensembles de prénoms translitérés
  même si les prénoms diffèrent
    Lorsque le prénom translitéré de la preuve 1 est MARIE<ADELINE
    Et le prénom translitéré de la preuve 2 est MARIE<ADELINE
    Et le prénom de la preuve 2 est Marie Adeline
    Quand une demande d'agrégation de preuves est réalisée
    Alors l'utilisateur reçoit une preuve

  Scénario: 3 - Cas valide car l'ensemble [MARIE<ADELINE] est présent dans tous les ensembles de prénoms translitérés
  même si les prénoms sont égaux entre eux mais différents des prénoms translitérés
    Lorsque le prénom translitéré de la preuve 1 est MARIE<ADELINE<ELISE
    Et le prénom translitéré de la preuve 2 est MARIE<ADELINE
    Quand une demande d'agrégation de preuves est réalisée
    Alors l'utilisateur reçoit une preuve

  Scénario: 4 - Cas invalide car aucun ensemble de prénoms translitérés n'est contenu dans l'autre et les prénoms
  diffèrent
    Lorsque le prénom translitéré de la preuve 1 est SONIA
    Et le prénom translitéré de la preuve 2 est MARIE
    Et le prénom de la preuve 2 est Marie Jeanne
    Quand une demande d'agrégation de preuves est réalisée
    Alors l'utilisateur ne reçoit pas de preuve

  Scénario: 5 - Cas invalide car aucun ensemble de noms translitérés n'est contenu dans l'autre et les noms
  diffèrent
    Lorsque le nom de famille de la preuve 1 est Durand
    Et le nom de famille translitéré de la preuve 1 est DURAND
    Et le nom de famille de la preuve 2 est Dupont Aignan
    Et le nom de famille translitéré de la preuve 2 est DUPONT<AIGNAN
    Quand une demande d'agrégation de preuves est réalisée
    Alors l'utilisateur ne reçoit pas de preuve

  Scénario: 5 bis - Cas invalide car aucun ensemble de noms translitérés n'est contenu dans l'autre et les noms
  diffèrent
    Lorsque le nom de famille de la preuve 1 est Dupont
    Et le nom de famille translitéré de la preuve 1 est DUPONT<MORICEAU
    Et le nom de famille de la preuve 2 est Dupont Aignan
    Et le nom de famille translitéré de la preuve 2 est DUPONT<AIGNAN
    Quand une demande d'agrégation de preuves est réalisée
    Alors l'utilisateur ne reçoit pas de preuve

  Scénario: 6 - Cas valide car le nom translitéré DUPONT est contenu dans l'autre nom translitéré
    Lorsque le nom de famille de la preuve 1 est Dupont
    Et le nom de famille translitéré de la preuve 1 est DUPONT
    Et le nom de famille de la preuve 2 est Moriceau
    Et le nom de famille translitéré de la preuve 2 est DUPONT<MORICEAU
    Quand une demande d'agrégation de preuves est réalisée
    Alors l'utilisateur reçoit une preuve
    