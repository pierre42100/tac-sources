# language: fr
Fonctionnalité: Transverse pour les différents cas d'usages de l'agrégation

  Scénario: Cas nominal avec suppression chevrons de fin
    Etant donné une preuve de vaccin Pfizer 2/2 injecté il y a 89 jours en FR au nom de MARIE, GLADYS<DADOU<< - Marie, Gladys-Dadou né le 29/02/2000
    Et une preuve de vaccin Pfizer 1/1 injecté il y a 20 jours en FR au nom de MARIE, GLADYS<DADOU - Marie, Gladys-Dadou né le 29/02/2000
    Quand une demande d'agrégation de preuves est réalisée
    Alors réception d'une preuve de type vaccination valide à partir de maintenant au nom de MARIE, GLADYS<DADOU - Marie, Gladys-Dadou né le 29/02/2000

  Scénario: Cas nominal avec noms et prénoms différents
    Etant donné une preuve de vaccin Pfizer 2/2 injecté il y a 89 jours en FR au nom de MARIE, GLADYS<DADOU<< - Marie, Gladys-Dadou né le 29/02/2000
    Et une preuve de vaccin Pfizer 1/1 injecté il y a 20 jours en FR au nom de MARIE, GLADYS<DADOU - Mari, Gladys-Dad né le 29/02/2000
    Quand une demande d'agrégation de preuves est réalisée
    Alors réception d'une preuve de type vaccination valide à partir de maintenant au nom de MARIE, GLADYS<DADOU - Mari, Gladys-Dad né le 29/02/2000

  Scénario: Cas nominal avec noms et prénoms translittérés différents
    Etant donné une preuve de vaccin Pfizer 2/2 injecté il y a 89 jours en FR au nom de MARIE, GLADYS<DADOU<< - Marie, Gladys-Dadou né le 29/02/2000
    Et une preuve de vaccin Pfizer 1/1 injecté il y a 20 jours en FR au nom de MARI, GLADYS<DAD - Marie, Gladys-Dadou né le 29/02/2000
    Quand une demande d'agrégation de preuves est réalisée
    Alors réception d'une preuve de type vaccination valide à partir de maintenant au nom de MARI, GLADYS<DAD - Marie, Gladys-Dadou né le 29/02/2000

  Scénario: Cas de rejet si les noms sont différents
    Etant donné une preuve de vaccin Pfizer 2/2 injecté il y a 89 jours en FR au nom de MARIE, GLADYS<DAD - Marie, Gladys-Dad né le 29/02/2000
    Et une preuve de vaccin Pfizer 1/1 injecté il y a 20 jours en FR au nom de MARIE, GLADYS<DADOU - Marie, Gladys-Dadou né le 29/02/2000
    Quand une demande d'agrégation de preuves est réalisée
    Alors l'utilisateur ne reçoit pas de preuve

  Scénario: Cas de rejet si les prénoms sont différents
    Etant donné une preuve de vaccin Pfizer 2/2 injecté il y a 89 jours en FR au nom de ADELINE, FANNY - Adeline, Fanny né le 29/02/1996
    Et une preuve de vaccin Pfizer 1/1 injecté il y a 20 jours en FR au nom de MAXIME, FANNY - Maxime, Fanny né le 29/02/1996
    Quand une demande d'agrégation de preuves est réalisée
    Alors l'utilisateur ne reçoit pas de preuve

  Scénario: Cas de rejet avec format de date erroné
    Etant donné une preuve de vaccin Pfizer 2/2 injecté il y a 89 jours en FR au nom de PIERRE, CURIE - Pierre, Curie né une mauvaise date le 1956-02
    Et une preuve de vaccin Pfizer 1/1 injecté il y a 20 jours en FR au nom de PIERRE, CURIE - Pierre, Curie né une mauvaise date le 1956-02
    Quand une demande d'agrégation de preuves est réalisée
    Alors l'utilisateur ne reçoit pas de preuve

  Scénario: Cas de rejet si dates de naissance différentes
    Etant donné une preuve de vaccin Moderna 2/2 injecté il y a 89 jours en FR au nom de PIERRE, CURIE - Pierre, Curie né le 29/02/1996
    Et une preuve de vaccin Pfizer 1/1 injecté il y a 20 jours en FR au nom de PIERRE, CURIE - Pierre, Curie né le 01/01/2000
    Quand une demande d'agrégation de preuves est réalisée
    Alors l'utilisateur ne reçoit pas de preuve

  Scénario: Cas de rejet avec schéma vaccinal Janssen et test négatif (antigénique) valides
    Etant donné une preuve de vaccin Moderna 1/1 injecté il y a 100 jours en FR au nom de JEAN<RICHARD, PAUL - Jean-Richard, Paul né le 29/02/1956
    Et une preuve négative de test antigénique 1304 effectué il y a 6 heures en FR par CNAM au nom de JEAN<RICHARD, PAUL - Jean-Richard, Paul né le 29/02/1956
    Quand une demande d'agrégation de preuves est réalisée
    Alors l'utilisateur ne reçoit pas de preuve

  Scénario: Cas de rejet avec schéma vaccinal Janssen et preuve ARNm valides
    Etant donné une preuve de vaccin Jensen 1/1 injecté il y a 20 jours en FR au nom de JEAN<RICHARD, PAUL - Jean-Richard, Paul né le 29/02/1956
    Et une preuve de vaccin Moderna 2/2 injecté il y a 100 jours en FR au nom de JEAN<RICHARD, PAUL - Jean-Richard, Paul né le 29/02/1956
    Quand une demande d'agrégation de preuves est réalisée
    Alors l'utilisateur ne reçoit pas de preuve

  Scénario: Cas de rejet avec schéma vaccinal Janssen 1/1 et rétablissement
    Etant donné une preuve de vaccin Jensen 1/1 injecté il y a 20 jours en FR au nom de PIERRE, CURIE - Pierre, Curie né le 29/02/1956
    Et une preuve de rétablissement valable depuis 60 jours en FR au nom de PIERRE, CURIE - Pierre, Curie né le 14/04/1979
    Quand une demande d'agrégation de preuves est réalisée
    Alors l'utilisateur ne reçoit pas de preuve

  Scénario: Cas de rejet si les preuves envoyées sont deux tests négatifs (antigienique et PCR) valides
    Etant donné une preuve négative de test pcr sans fabricant effectué il y a 3 heures en FR par CNAM au nom de PIERRE, CURIE - Pierre, Curie né le 15/02/2003
    Et une preuve négative de test antigénique 1253 effectué il y a 4 heures en FR par CNAM au nom de PIERRE, CURIE - Pierre, Curie né le 15/02/2003
    Quand une demande d'agrégation de preuves est réalisée
    Alors l'utilisateur ne reçoit pas de preuve

  Scénario: Cas de rejet si les preuves envoyées sont deux tests positifs (antigienique et PCR) valides
    Etant donné une preuve positive de test pcr sans fabricant effectué il y a 10 jours en FR par CNAM au nom de PIERRE, CURIE - Pierre, Curie né le 15/02/2003
    Et une preuve positive de test antigénique 1253 effectué il y a 15 jours en FR par CNAM au nom de PIERRE, CURIE - Pierre, Curie né le 15/02/2003
    Quand une demande d'agrégation de preuves est réalisée
    Alors l'utilisateur ne reçoit pas de preuve

  Scénario: Cas de rejet si les preuves envoyées sont deux rétablissement valides
    Etant donné une preuve de rétablissement valable depuis 40 jours en FR au nom de PIERRE, CURIE - Pierre, Curie né le 29/02/1956
    Et une preuve de rétablissement valable depuis 5 jours en FR au nom de PIERRE, CURIE - Pierre, Curie né le 29/02/1956
    Quand une demande d'agrégation de preuves est réalisée
    Alors l'utilisateur ne reçoit pas de preuve

  Scénario: Cas de rejet avec fabricant erroné de schéma vaccinal Pfizer
    Etant donné une preuve de vaccin mauvais authorization holder 2/2 injecté il y a 89 jours en FR au nom de PIERRE, CURIE - Pierre, Curie né le 29/02/1956
    Et une preuve de vaccin Pfizer 1/1 injecté il y a 20 jours en FR au nom de PIERRE, CURIE - Pierre, Curie né le 29/02/1956
    Quand une demande d'agrégation de preuves est réalisée
    Alors l'utilisateur ne reçoit pas de preuve

  Scénario: Cas de rejet avec un type de vaccin erroné
    Etant donné une preuve de vaccin mauvais medicinal product 2/2 injecté il y a 89 jours en FR au nom de PIERRE, CURIE - Pierre, Curie né le 29/02/1956
    Et une preuve de vaccin Pfizer 1/1 injecté il y a 20 jours en FR au nom de PIERRE, CURIE - Pierre, Curie né le 29/02/1956
    Quand une demande d'agrégation de preuves est réalisée
    Alors l'utilisateur ne reçoit pas de preuve

  Scénario: Cas de rejet avec schéma vaccinal Pfizer avec une unique preuve
    Etant donné une preuve de vaccin Pfizer 2/2 injecté il y a 89 jours en FR au nom de PIERRE, CURIE - Pierre, Curie né le 29/02/2000
    Quand une demande d'agrégation de preuves est réalisée
    Alors l'utilisateur ne reçoit pas de preuve

  Scénario: Cas de rejet avec schéma vaccinal block listé
    Etant donné une preuve de vaccin Pfizer 2/2 injecté il y a 89 jours en FR au nom de PIERRE, CURIE - Pierre, Curie né le 29/02/2000
    Et que la preuve est sur liste d'exclusion
    Et une preuve de vaccin Pfizer 1/1 injecté il y a 20 jours en FR au nom de PIERRE, CURIE - Pierre, Curie né le 29/02/2000
    Quand une demande d'agrégation de preuves est réalisée
    Alors l'utilisateur ne reçoit pas de preuve

  Scénario: Cas de rejet avec utilisation de mauvaises clés de signatures
    Etant donné que la clé publique utilisée pour le chiffrement est "APFC_BAD_KEY"
    Etant donné une preuve de vaccin Pfizer 2/2 injecté il y a 89 jours en FR au nom de PIERRE, CURIE - Pierre, Curie né le 29/02/2000
    Et une preuve de vaccin Pfizer 1/1 injecté il y a 20 jours en FR au nom de PIERRE, CURIE - Pierre, Curie né le 29/02/2000
    Quand une demande d'agrégation de preuves est réalisée
    Alors l'utilisateur ne reçoit pas de preuve

  Scénario: Cas de rejet si le le certificate issuer est vide
    Etant donné une preuve de vaccin Pfizer 2/2 injecté il y a 89 jours en FR sans émetteur au nom de MARIE, GLADYS<DADOU<< - Marie, Gladys-Dadou né le 29/02/2000
    Et une preuve de vaccin Pfizer 1/1 injecté il y a 20 jours en FR au nom de MARIE, GLADYS<DADOU - Marie, Gladys-Dadou né le 29/02/2000
    Quand une demande d'agrégation de preuves est réalisée
    Alors l'utilisateur ne reçoit pas de preuve

  Scénario: Cas de rejet avec preuves avec un délai de moins de 2 mois avec vaccin 2/2 et vaccin 1/1 (ARNm,Az,Nova)
    Etant donné une preuve de vaccin Pfizer 2/2 injecté il y a 89 jours en FR au nom de MARIE, GLADYS<DADOU - Marie, Gladys-Dadou<<, Marie Gladys-Dadou né le 29/02/2000
    Et une preuve de vaccin Pfizer 1/1 injecté il y a 20 jours en FR au nom de MARIE, GLADYS<DADOU - Marie, Gladys-Dadou né le 29/02/2000
    Quand une demande d'agrégation de preuves est réalisée
    Alors réception d'une preuve de type vaccination valide à partir de maintenant au nom de MARIE, GLADYS<DADOU - Marie, Gladys-Dadou né le 29/02/2000
    Etant donné que l'utilisateur commence une nouvelle procédure d'agrégation
    Et que l'utilisateur renvoie la dernière preuve réceptionnée
    Et une preuve de vaccin Pfizer 2/2 injecté il y a 89 jours en FR au nom de MARIE, GLADYS<DADOU - Marie, Gladys-Dadou<<, Marie Gladys-Dadou né le 29/02/2000
    Quand une demande d'agrégation de preuves est réalisée
    Alors l'utilisateur ne reçoit pas de preuve

  Scénario: Cas de rejet avec date d'émission du DCC dans le futur
    Etant donné une preuve de vaccin Pfizer 2/2 injecté il y a 89 jours émis il y a 89 jours en FR au nom de PIERRE, CURIE - Pierre, Curie né le 29/02/1956
    Et une preuve de vaccin Pfizer 1/1 injecté il y a 20 jours émis dans 30 jours en FR au nom de PIERRE, CURIE - Pierre, Curie né le 29/02/1956
    Quand une demande d'agrégation de preuves est réalisée
    Alors l'utilisateur ne reçoit pas de preuve

  Scénario: Cas nominal avec date d'injection dans le futur de la preuve vaccinale (+24h max)
    Etant donné une preuve de vaccin Pfizer 2/2 injecté il y a 89 jours émis il y a 89 jours en FR au nom de PIERRE, CURIE - Pierre, Curie né le 29/02/1956
    Et une preuve de vaccin Pfizer 1/1 injecté dans 23 heures en FR au nom de PIERRE, CURIE - Pierre, Curie né le 29/02/1956
    Quand une demande d'agrégation de preuves est réalisée
    Alors réception d'une preuve de type vaccination valide à partir de maintenant au nom de PIERRE, CURIE - Pierre, Curie né le 29/02/1956

  Scénario: Cas de rejet avec date d'injection de la preuve vaccinale dans plus de deux jours
    Etant donné une preuve de vaccin Pfizer 2/2 injecté il y a 89 jours émis il y a 89 jours en FR au nom de PIERRE, CURIE - Pierre, Curie né le 29/02/1956
    Et une preuve de vaccin Pfizer 1/1 injecté dans 2 jours en FR au nom de PIERRE, CURIE - Pierre, Curie né le 29/02/1956
    Quand une demande d'agrégation de preuves est réalisée
    Alors l'utilisateur ne reçoit pas de preuve
