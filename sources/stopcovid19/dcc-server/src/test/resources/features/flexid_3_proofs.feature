# language: fr
Fonctionnalité: Démonstration FlexID avec 3 preuves

  Contexte:
    Etant donné une preuve de vaccin Pfizer 1/1 injecté il y a 150 jours en FR au nom de MARIE, DUPONT - Marie, Dupont né le 29/02/2000
    Et une preuve de vaccin Pfizer 1/1 injecté il y a 80 jours en FR au nom de MARIE, DUPONT - Marie, Dupont né le 29/02/2000
    Et une preuve de vaccin Pfizer 1/1 injecté il y a 10 jours en FR au nom de MARIE, DUPONT - Marie, Dupont né le 29/02/2000

  Scénario: Temoin : L'ensemble de preuves sans modifications est accepté
    Quand une demande d'agrégation de preuves est réalisée
    Alors l'utilisateur reçoit une preuve

  Scénario: 7 - Cas valide car le nom de famille est le même partout
    Lorsque le nom de famille de la preuve 1 est Dupont
    Et le nom de famille translitéré de la preuve 1 est DUPONT<AIGNAN
    Et le nom de famille de la preuve 2 est Dupont
    Et le nom de famille translitéré de la preuve 2 est DUPONT<DELORME
    Et le nom de famille de la preuve 3 est Dupont
    Et le nom de famille translitéré de la preuve 3 est DUPONT<AIGNAN<DELORME
    Quand une demande d'agrégation de preuves est réalisée
    Alors l'utilisateur reçoit une preuve

  Scénario: 8 - Cas invalide car les noms de famille ne sont pas égaux et aucun ensemble de nom translitéré n'est contenu
    dans les autres
    Lorsque le nom de famille de la preuve 1 est Dupont
    Et le nom de famille translitéré de la preuve 1 est DUPONT<AIGNAN
    Et le nom de famille de la preuve 2 est Dupont Aignan
    Et le nom de famille translitéré de la preuve 2 est DUPONT<DELORME
    Et le nom de famille de la preuve 3 est Dupont
    Et le nom de famille translitéré de la preuve 3 est DURAND<GRAVIER
    Quand une demande d'agrégation de preuves est réalisée
    Alors l'utilisateur ne reçoit pas de preuve

  Scénario: 9 - Cas valide car le nom translitéré DUPONT est compris dans chacun des ensembles de noms translitérés
    Lorsque le nom de famille de la preuve 1 est Dupont
    Et le nom de famille translitéré de la preuve 1 est DUPONT<AIGNAN
    Et le nom de famille de la preuve 2 est Dupont
    Et le nom de famille translitéré de la preuve 2 est DUPONT<CHARLES
    Et le nom de famille de la preuve 3 est Dupont
    Et le nom de famille translitéré de la preuve 3 est DUPONT
    Quand une demande d'agrégation de preuves est réalisée
    Alors l'utilisateur reçoit une preuve

  Scénario: 10 - Cas valide car l'ensemble [DUPONT<AIGNAN] est compris dans chacun des ensembles de noms translitérés
    Lorsque le nom de famille de la preuve 1 est Dupont Aignan
    Et le nom de famille translitéré de la preuve 1 est DUPONT<AIGNAN<DELORME
    Et le nom de famille de la preuve 2 est Dupont Aignan
    Et le nom de famille translitéré de la preuve 2 est DUPONT<AIGNAN
    Et le nom de famille de la preuve 3 est Dupont Aignan
    Et le nom de famille translitéré de la preuve 3 est DUPONT<AIGNAN<GRAVIER
    Quand une demande d'agrégation de preuves est réalisée
    Alors l'utilisateur reçoit une preuve

  Scénario: 11 - Cas valide car l'ensemble [DUPONT<AIGNAN] est compris dans chacun des ensembles de noms translitérés
    Lorsque le nom de famille de la preuve 1 est Dupont Aignan
    Et le nom de famille translitéré de la preuve 1 est DUPONT<AIGNAN<DELORME
    Et le nom de famille de la preuve 2 est Dupont Aignan
    Et le nom de famille translitéré de la preuve 2 est DUPONT<AIGNAN
    Et le nom de famille de la preuve 3 est Dupont Gravier
    Et le nom de famille translitéré de la preuve 3 est DUPONT<AIGNAN<GRAVIER
    Quand une demande d'agrégation de preuves est réalisée
    Alors l'utilisateur reçoit une preuve

  Scénario: 12 - Cas invalide car aucun ensemble de noms translitérés n'est contenu dans tous les autres et les
  noms de  famille ne sont pas tous égaux
    Lorsque le nom de famille de la preuve 1 est Dupont Aignan
    Et le nom de famille translitéré de la preuve 1 est DUPONT<AIGNAN<DELORME
    Et le nom de famille de la preuve 2 est Dupont Aignan
    Et le nom de famille translitéré de la preuve 2 est DUPONT<AIGNAN
    Et le nom de famille de la preuve 3 est Durand Aignan
    Et le nom de famille translitéré de la preuve 3 est DURAND<AIGNAN<GRAVIER
    Quand une demande d'agrégation de preuves est réalisée
    Alors l'utilisateur ne reçoit pas de preuve