package fr.gouv.tac.dcclight.validation

import ehn.techiop.hcert.kotlin.data.GreenCertificate
import fr.gouv.tac.dcclight.service.MetricsService
import fr.gouv.tac.dcclight.service.error.DccValidationError.FLEX_ID_MISMATCH
import fr.gouv.tac.dcclight.service.error.DccValidationError.INCONSISTENT_FAMILY_NAME
import fr.gouv.tac.dcclight.service.error.DccValidationError.MISSING_FAMILY_NAME
import fr.gouv.tac.dcclight.service.error.rejectCertificate
import fr.gouv.tac.dcclight.service.error.rejectCertificates
import org.springframework.stereotype.Component
import org.springframework.validation.BindingResult

@Component
class FamilyNameValidator : GreenCertificatesValidator, FlexIdValidator() {

    /**
     * @param greenCertificatesToValidate: contains a Pair with familyName (first) and familyNameTransliterated (second)
     */
    override fun validate(
        greenCertificatesToValidate: List<GreenCertificate>,
        metricsService: MetricsService,
        validation: BindingResult
    ) {

        // Prerequisites to referenceCertificate selection
        if (greenCertificatesToValidate.all { it.subject.familyName == null }) {
            greenCertificatesToValidate.forEachIndexed { index, _ ->
                validation.rejectCertificate(index, MISSING_FAMILY_NAME)
                metricsService.incrementCounter(MISSING_FAMILY_NAME)
            }
            return
        }

        val referenceFamilyName = greenCertificatesToValidate.first { it.subject.familyName != null }.subject.familyName

        val namesTransliterated = greenCertificatesToValidate.map { it.subject.familyNameTransliterated }
        val namesTransliteratedMatchFlexId = matchFlexId(namesTransliterated)

        // Metrics incrementation

        greenCertificatesToValidate.forEach {
            if (it.subject.familyName == null) {
                metricsService.incrementCounter(MISSING_FAMILY_NAME)
            }
            if (it.subject.familyName != referenceFamilyName) {
                metricsService.incrementCounter(INCONSISTENT_FAMILY_NAME)
            }
        }

        if (!namesTransliteratedMatchFlexId) {
            metricsService.incrementCounter(FLEX_ID_MISMATCH)
        }

        // Validation rejection
        if (greenCertificatesToValidate.any { it.subject.familyName != referenceFamilyName } && !namesTransliteratedMatchFlexId) {
            greenCertificatesToValidate.forEachIndexed { index, dcc ->
                if (dcc.subject.familyName == null) {
                    validation.rejectCertificate(index, MISSING_FAMILY_NAME)
                }
                if (dcc.subject.familyName != referenceFamilyName) {
                    validation.rejectCertificate(index, INCONSISTENT_FAMILY_NAME)
                }
            }
            validation.rejectCertificates(FLEX_ID_MISMATCH)
            if (matchesFlexIdAfterRemovingAccents(namesTransliterated)) {
                metricsService.incrementFlexIdAccentsMismatchs()
            }
        }
    }
}
