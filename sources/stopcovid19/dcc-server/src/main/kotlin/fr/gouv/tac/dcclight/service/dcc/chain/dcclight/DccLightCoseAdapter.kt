package fr.gouv.tac.dcclight.service.dcc.chain.dcclight

import COSE.MessageTag
import COSE.Sign1Message
import ehn.techiop.hcert.kotlin.chain.CertificateRepository
import ehn.techiop.hcert.kotlin.chain.Error
import ehn.techiop.hcert.kotlin.chain.VerificationException
import ehn.techiop.hcert.kotlin.chain.VerificationResult
import ehn.techiop.hcert.kotlin.crypto.JvmPubKey

class DccLightCoseAdapter constructor(input: ByteArray) {

    private val sign1Message = try {
        Sign1Message.DecodeFromBytes(augmentedInput(input), MessageTag.Sign1) as Sign1Message
    } catch (t: Throwable) {
        throw VerificationException(Error.SIGNATURE_INVALID, cause = t)
    }

    fun getProtectedAttributeByteArray(key: Int) =
        sign1Message.protectedAttributes[key]?.GetByteString()

    fun getUnprotectedAttributeByteArray(key: Int) =
        sign1Message.unprotectedAttributes[key]?.GetByteString()

    fun validate(
        kid: ByteArray,
        repository: CertificateRepository,
        verificationResult: VerificationResult
    ): Boolean {
        repository.loadTrustedCertificates(kid, verificationResult).forEach {
            if (sign1Message.validate((it.publicKey as JvmPubKey).toCoseRepresentation())) {
                verificationResult.setCertificateData(it)
                return true
            } // else try next
        }
        return false
    }

    fun validate(
        kid: ByteArray,
        cryptoService: ehn.techiop.hcert.kotlin.chain.CryptoService,
        verificationResult: VerificationResult
    ): Boolean {
        val verificationKey = cryptoService.getCborVerificationKey(kid, verificationResult)
        return sign1Message.validate((verificationKey as JvmPubKey).toCoseRepresentation()).also {
            if (it)
                verificationResult.setCertificateData(cryptoService.getCertificate())
        }
    }

    fun getContent(): ByteArray = sign1Message.GetContent()

    /**
     * Input may be missing COSE Tag 0xD2 = 18 = cose-sign1.
     * But we need this, to cast the parsed object to [Cbor.Tagged].
     * So we'll add the Tag to the input.
     *
     * It may also be tagged as a CWT (0xD8, 0x3D) and a Sign1 (0xD2).
     * But the library expects only one tag.
     * So we'll strip the CWT tag from the input.
     */
    private fun augmentedInput(input: ByteArray): ByteArray {
        if (input.isNotEmpty() && isArray(input[0]))
            return byteArrayOf(0xD2.toByte()) + input
        if (input.size >= 3 && isCwt(input[0], input[1]) && isSign1(input[2])) {
            return input.drop(2).toByteArray()
        }
        return input
    }

    private fun isSign1(byte: Byte) = byte == 0xD2.toByte()

    private fun isCwt(firstByte: Byte, secondByte: Byte) = firstByte == 0xD8.toByte() && secondByte == 0x3D.toByte()

    private fun isArray(byte: Byte) = byte == 0x84.toByte()
}
