package fr.gouv.tac.dcclight.configuration

import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import ehn.techiop.hcert.kotlin.crypto.CertificateAdapter
import fr.gouv.tac.dcclight.model.DccBlocklist
import fr.gouv.tac.dcclight.rules.model.Rules
import fr.gouv.tac.dcclight.rules.model.YamlRules
import io.micrometer.core.instrument.MeterRegistry
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.core.io.ClassPathResource
import org.springframework.scheduling.TaskScheduler
import org.springframework.web.reactive.function.client.WebClient
import org.yaml.snakeyaml.Yaml

@Configuration
class ExternalResourcesConfiguration(
    private val properties: DccServerProperties,
    private val meterRegistry: MeterRegistry,
    private val taskScheduler: TaskScheduler,
    private val webClient: WebClient,
    private val jsonObjectMapper: ObjectMapper,
    private val yamlObjectMapper: Yaml,
) {

    @Bean
    fun blockList() = ExternalResource<DccBlocklist?>(
        endpointConfiguration = properties.blocklistEndpoint,
        metricName = "blocklist",
        initialValue = null,
        meterRegistry = meterRegistry,
        taskScheduler = taskScheduler,
        webClient = webClient,
        dataExtractor = { jsonObjectMapper.readValue(it, object : TypeReference<DccBlocklist>() {}) }
    )

    @Bean
    fun aggregationRules(): ExternalResource<Rules> {
        val defaultRulesContent = ClassPathResource("aggregation/rules.yml")
        val defaultRules: Rules = defaultRulesContent.inputStream.use {
            yamlObjectMapper.loadAs(it, YamlRules::class.java).toRules()
        }
        return ExternalResource(
            endpointConfiguration = properties.aggregationRulesEndpoint,
            metricName = "aggregation-rules",
            initialValue = defaultRules,
            meterRegistry = meterRegistry,
            taskScheduler = taskScheduler,
            webClient = webClient,
            dataExtractor = { yamlObjectMapper.loadAs(it, YamlRules::class.java).toRules() }
        )
    }

    @Bean
    fun dscRepository() = ExternalResource<Map<String, List<CertificateAdapter>>>(
        endpointConfiguration = properties.signingCertificatesEndpoint,
        metricName = "signing-certificates",
        initialValue = emptyMap(),
        meterRegistry = meterRegistry,
        taskScheduler = taskScheduler,
        webClient = webClient,
        dataExtractor = { data ->
            val trustedCertificates: Map<String, List<String>> = jsonObjectMapper.readValue(data)
            trustedCertificates.mapValues { pemCertificate -> pemCertificate.value.map(::CertificateAdapter) }
        }
    )
}
