package fr.gouv.tac.dcclight.validation

import ehn.techiop.hcert.kotlin.data.GreenCertificate
import fr.gouv.tac.dcclight.extension.getIssuer
import fr.gouv.tac.dcclight.service.MetricsService
import fr.gouv.tac.dcclight.service.error.DccValidationError.MISSING_CERTIFICATE_ISSUER
import fr.gouv.tac.dcclight.service.error.rejectCertificate
import org.springframework.stereotype.Component
import org.springframework.validation.BindingResult

@Component
class IssuerValidator : GreenCertificatesValidator {
    override fun validate(
        greenCertificatesToValidate: List<GreenCertificate>,
        metricsService: MetricsService,
        validation: BindingResult
    ) {
        greenCertificatesToValidate.forEachIndexed { index, dcc ->
            if (dcc.getIssuer().isEmpty()) {
                validation.rejectCertificate(index, MISSING_CERTIFICATE_ISSUER)
                metricsService.incrementCounter(MISSING_CERTIFICATE_ISSUER)
            }
        }
    }
}
