package fr.gouv.tac.dcclight.service.dcc.chain.dcclight

import java.security.KeyFactory
import java.security.Signature
import java.security.spec.PKCS8EncodedKeySpec
import java.util.Base64

class DccLightSigningService(signingKey: String) {
    private val decoded: ByteArray = Base64.getDecoder().decode(signingKey)
    private val spec = PKCS8EncodedKeySpec(decoded)
    private val kf = KeyFactory.getInstance("EC")
    private val privKey = kf.generatePrivate(spec)

    fun sign(payload: ByteArray): ByteArray {
        val sig = Signature.getInstance("SHA256withECDSAinP1363Format")
        sig.initSign(privKey)
        sig.update(payload)
        return sig.sign()
    }
}
