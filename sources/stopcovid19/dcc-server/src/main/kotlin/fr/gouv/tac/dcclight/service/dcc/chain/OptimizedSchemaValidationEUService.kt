package fr.gouv.tac.dcclight.service.dcc.chain

import ehn.techiop.hcert.kotlin.chain.Error
import ehn.techiop.hcert.kotlin.chain.SchemaValidationService
import ehn.techiop.hcert.kotlin.chain.VerificationException
import ehn.techiop.hcert.kotlin.chain.VerificationResult
import ehn.techiop.hcert.kotlin.chain.impl.SchemaError
import ehn.techiop.hcert.kotlin.data.CborObject
import ehn.techiop.hcert.kotlin.data.GreenCertificate
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import net.pwall.json.schema.JSONSchema
import net.pwall.json.schema.parser.Parser
import java.io.InputStream
import java.net.URI

/**
 * This is a copy of hcert library. The initialization of JvmSchemaLoader is made out of the validation() function to avoid loading Json schemas each time the validation() function is invoked.
 *
 * @see ehn.techiop.hcert.kotlin.chain.impl.DefaultSchemaValidationService
 */
class OptimizedSchemaValidationEUService(private val useFallback: Boolean = true) : SchemaValidationService {

    private val schemaLoader = JvmSchemaLoader()

    override fun validate(cbor: CborObject, verificationResult: VerificationResult): GreenCertificate {
        val adapter = SchemaValidationAdapter(schemaLoader, cbor)

        val versionString = cbor.getVersionString() ?: throw VerificationException(
            Error.CBOR_DESERIALIZATION_FAILED,
            "No schema version specified",
            details = mapOf("schemaVersion" to "null")
        )
        if (!adapter.hasValidator(versionString)) throw VerificationException(
            Error.SCHEMA_VALIDATION_FAILED,
            "Schema version $versionString is not supported",
            details = mapOf("schemaVersion" to versionString)
        )

        if (useFallback) {
            val fallbackErrors = adapter.validateWithFallback()
            if (fallbackErrors.isNotEmpty()) throw VerificationException(
                Error.SCHEMA_VALIDATION_FAILED,
                "Data does not follow fallback schema: $fallbackErrors}",
                details = mapOf("schemaErrors" to fallbackErrors.joinToString())
            )
        } else {
            val errors = adapter.validateBasic(versionString)
            if (errors.isNotEmpty()) throw VerificationException(
                Error.SCHEMA_VALIDATION_FAILED,
                "Data does not follow fallback schema: $errors}",
                details = mapOf("schemaErrors" to errors.joinToString())
            )
        }

        return adapter.toJson()
    }
}

/**
 * This is a strict copy of hcert library.
 *
 * @see ehn.techiop.hcert.kotlin.chain.impl.SchemaLoader
 */
abstract class SchemaLoader<T>(vararg validVersions: String = KNOWN_VERSIONS) {

    companion object {
        internal val KNOWN_VERSIONS = arrayOf(
            "1.0.0",
            "1.0.1",
            "1.1.0",
            "1.2.0",
            "1.2.1",
            "1.3.0"
        )
    }

    internal val validators = validVersions.mapIndexed { i, version ->
        validVersions[i] to loadSchema(version)
    }.toMap()

    internal abstract fun loadSchema(version: String): T

    internal abstract fun loadFallbackSchema(): T
}

/**
 * This is a strict copy of hcert library.
 *
 * @see ehn.techiop.hcert.kotlin.chain.impl.JvmSchemaLoader
 */
class JvmSchemaLoader : SchemaLoader<JSONSchema>() {

    override fun loadSchema(version: String) = getSchemaResource(version).use(this@JvmSchemaLoader::parse)

    override fun loadFallbackSchema() = getFallbackSchema().use(this@JvmSchemaLoader::parse)

    private fun parse(resource: InputStream) = Parser(uriResolver = { resource }).parse(URI.create("dummy:///"))

    private fun getSchemaResource(version: String) =
        classLoader().getResourceAsStream("json/schema/$version/DCC.combined-schema.json")
            ?: throw IllegalArgumentException("Schema not found: $version")

    private fun getFallbackSchema() =
        classLoader().getResourceAsStream("json/schema/fallback/DCC.combined-schema.json")
            ?: throw IllegalArgumentException("Fallback schema not found")

    private fun classLoader() = ehn.techiop.hcert.kotlin.chain.impl.SchemaValidationAdapter::class.java.classLoader
}

/**
 * This is a strict copy of hcert library.
 *
 * @see ehn.techiop.hcert.kotlin.chain.impl.SchemaValidationAdapter
 */
class SchemaValidationAdapter constructor(private val schemaLoader: JvmSchemaLoader, cbor: CborObject) {

    private val json = cbor.toJsonString()

    fun hasValidator(versionString: String): Boolean {
        return schemaLoader.validators[versionString] != null
    }

    fun validateBasic(versionString: String): Collection<SchemaError> {
        val validator = schemaLoader.validators[versionString] ?: throw IllegalArgumentException("versionString")
        return validate(validator)
    }

    fun validateWithFallback(): Collection<SchemaError> {
        val validator = schemaLoader.loadFallbackSchema()
        return validate(validator)
    }

    private fun validate(validator: JSONSchema): Collection<SchemaError> {
        val result = validator.validateBasic(json)
        return result.errors?.map { SchemaError("${it.error}, ${it.keywordLocation}, ${it.instanceLocation}") }
            ?: listOf()
    }

    @OptIn(ExperimentalSerializationApi::class)
    fun toJson(): GreenCertificate {
        return Json.decodeFromString(json)
    }
}
