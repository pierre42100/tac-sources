package fr.gouv.tac.dcclight.validation

import ehn.techiop.hcert.kotlin.data.GreenCertificate
import fr.gouv.tac.dcclight.service.MetricsService
import org.springframework.validation.BindingResult

interface GreenCertificatesValidator {

    fun validate(
        greenCertificatesToValidate: List<GreenCertificate>,
        metricsService: MetricsService,
        validation: BindingResult
    )
}
