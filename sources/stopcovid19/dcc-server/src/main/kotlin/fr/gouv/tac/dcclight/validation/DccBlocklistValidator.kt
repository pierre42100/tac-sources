package fr.gouv.tac.dcclight.validation

import ehn.techiop.hcert.kotlin.chain.ChainDecodeResult
import fr.gouv.tac.dcclight.configuration.ExternalResource
import fr.gouv.tac.dcclight.extension.countryUvciHash
import fr.gouv.tac.dcclight.extension.uvciHash
import fr.gouv.tac.dcclight.model.DccBlocklist
import fr.gouv.tac.dcclight.service.MetricsService
import fr.gouv.tac.dcclight.service.dcc.DccSignatureHash
import fr.gouv.tac.dcclight.service.error.DccValidationError.BLOCKLISTED_CERTIFICATE
import fr.gouv.tac.dcclight.service.error.rejectCertificate
import org.springframework.stereotype.Component
import org.springframework.validation.BindingResult

@Component
class DccBlocklistValidator(private val blockList: ExternalResource<DccBlocklist>) {

    /**
     * evaluates DCCs identifiers built from DCC country and identifier
     * @param greenCertificatesToValidate: Pair composed of DCC country (first) and DCC identifier (second)
     */
    fun validate(
        chainDecodeResults: List<ChainDecodeResult>,
        metricsService: MetricsService,
        validation: BindingResult
    ) {
        chainDecodeResults.forEachIndexed { index, decodeResult ->
            if (
                blockList.content.countryUvciHashes.contains(decodeResult.eudgc!!.countryUvciHash()) ||
                blockList.content.uvciHashes.contains(decodeResult.eudgc!!.uvciHash()) ||
                blockList.content.signatureHashes.contains(DccSignatureHash(decodeResult.step2Cose!!).buildSignatureHash())
            ) {
                validation.rejectCertificate(index, BLOCKLISTED_CERTIFICATE)
                metricsService.incrementCounter(BLOCKLISTED_CERTIFICATE)
            }
        }
    }
}
