package fr.gouv.tac.dcclight.service.dcc.chain.dcclight

import ehn.techiop.hcert.kotlin.chain.Base45Service
import ehn.techiop.hcert.kotlin.chain.CborService
import ehn.techiop.hcert.kotlin.chain.CompressorService
import ehn.techiop.hcert.kotlin.chain.ContextIdentifierService
import ehn.techiop.hcert.kotlin.chain.CoseService
import ehn.techiop.hcert.kotlin.data.GreenCertificate
import kotlinx.datetime.Instant
import kotlin.time.Duration

class DccLightChain(
    private val contextIdentifierService: ContextIdentifierService,
    private val base45Service: Base45Service,
    private val compressorService: CompressorService,
    private val coseService: CoseService,
    private val cwtService: DccLightCwtEUService,
    private val cborService: CborService
) {
    fun encode(newCertificate: GreenCertificate, instant: Instant, validity: Duration) =
        contextIdentifierService.encode(
            base45Service.encode(
                compressorService.encode(
                    coseService.encode(
                        cwtService.encode(
                            cborService.encode(
                                newCertificate
                            ),
                            instant, validity
                        )
                    )
                )
            )
        )
}
