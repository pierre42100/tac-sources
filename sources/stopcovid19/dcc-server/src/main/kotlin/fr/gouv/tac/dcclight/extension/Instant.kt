package fr.gouv.tac.dcclight.extension

import kotlinx.datetime.Instant
import kotlinx.datetime.TimeZone.Companion.currentSystemDefault
import kotlinx.datetime.toLocalDateTime

/**
 * Returns a local date for the given instant.
 */
fun Instant.toLocalDate() = this.toLocalDateTime(currentSystemDefault()).date
