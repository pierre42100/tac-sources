package fr.gouv.tac.dcclight.service.dcc

import ehn.techiop.hcert.kotlin.chain.CertificateRepository
import ehn.techiop.hcert.kotlin.chain.Error.KEY_NOT_IN_TRUST_LIST
import ehn.techiop.hcert.kotlin.chain.VerificationException
import ehn.techiop.hcert.kotlin.chain.VerificationResult
import ehn.techiop.hcert.kotlin.chain.asBase64
import ehn.techiop.hcert.kotlin.crypto.CertificateAdapter
import fr.gouv.tac.dcclight.configuration.ExternalResource
import org.springframework.stereotype.Component

@Component
class DccSigningCertificatesRepository(
    private val dscRepository: ExternalResource<Map<String, List<CertificateAdapter>>>
) : CertificateRepository {

    override fun loadTrustedCertificates(
        kid: ByteArray,
        verificationResult: VerificationResult
    ): List<CertificateAdapter> {
        return dscRepository.content[kid.asBase64()]
            ?: throw VerificationException(
                KEY_NOT_IN_TRUST_LIST,
                "kid not found",
                details = mapOf("base64EncodedKid" to kid.asBase64())
            )
    }
}
