package fr.gouv.tac.dcclight.service

import ehn.techiop.hcert.kotlin.chain.CryptoService
import ehn.techiop.hcert.kotlin.chain.impl.DefaultCborService
import ehn.techiop.hcert.kotlin.chain.impl.DefaultCompressorService
import ehn.techiop.hcert.kotlin.chain.impl.DefaultContextIdentifierService
import ehn.techiop.hcert.kotlin.chain.impl.FileBasedCryptoService
import ehn.techiop.hcert.kotlin.data.GreenCertificate
import fr.gouv.tac.dcclight.configuration.DccServerProperties
import fr.gouv.tac.dcclight.service.dcc.chain.OptimizedBase45EUService
import fr.gouv.tac.dcclight.service.dcc.chain.dcclight.DCCLightCoseEUService
import fr.gouv.tac.dcclight.service.dcc.chain.dcclight.DccLightChain
import fr.gouv.tac.dcclight.service.dcc.chain.dcclight.DccLightCwtEUService
import fr.gouv.tac.dcclight.service.dcc.chain.dcclight.DccLightSigningService
import kotlinx.datetime.Instant
import org.springframework.stereotype.Service
import kotlin.time.Duration
import kotlin.time.Duration.Companion.hours

@Service
class DccLightEncodingService(dccServerProperties: DccServerProperties) {

    // DCCLight encoding classes extracted from Chain class
    private val dcclightCryptoService: CryptoService = FileBasedCryptoService(
        dccServerProperties.dcclight.getSigningPrivateKeyAsPem(),
        dccServerProperties.dcclight.signingCertificate
    )
    private val dcclightCborService = DefaultCborService()

    private val dcclightCwtService = DccLightCwtEUService("FR")
    private val dcclightSigningService = DccLightSigningService(dccServerProperties.dcclight.signingPrivateKey)
    private val dcclightCoseService = DCCLightCoseEUService(dcclightCryptoService, dcclightSigningService)
    private val dcclightCompressorService = DefaultCompressorService()
    private val dcclightBase45Service = OptimizedBase45EUService()
    private val dcclightContextIdentifierService = DefaultContextIdentifierService("HCFR1:")

    private fun encoder() =
        DccLightChain(
            dcclightContextIdentifierService,
            dcclightBase45Service,
            dcclightCompressorService,
            dcclightCoseService,
            dcclightCwtService,
            dcclightCborService
        )

    private val validity: Duration = 24.hours

    fun encode(newCertificate: GreenCertificate, issueTime: Instant) =
        encoder().encode(
            newCertificate,
            issueTime,
            validity
        )
}
