package fr.gouv.tac.dcclight.extension

import ehn.techiop.hcert.kotlin.data.Person

fun Person.trimTransliteratedNames() = this.copy(
    familyNameTransliterated = this.familyNameTransliterated.trimEnd('<'),
    givenNameTransliterated = this.givenNameTransliterated?.trimEnd('<')
)
