package fr.gouv.tac.dcclight.rules

import com.fasterxml.jackson.annotation.JsonValue

/*
https://ec.europa.eu/health/sites/default/files/ehealth/docs/digital-green-value-sets_en.pdf
 */
enum class TestResult(@JsonValue val EUValue: String) {
    DETECTED("260373001"),
    NOT_DETECTED("260415000");

    companion object {
        fun fromEuValue(euValue: String) =
            values().find { euValue == it.EUValue } ?: throw IllegalArgumentException("No enum value for '$euValue'")
    }
}

enum class CertificateTarget(@JsonValue val EUValue: String) {
    COVID_19("840539006");
}
