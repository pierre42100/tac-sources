package fr.gouv.tac.dcclight.rules.model

enum class DccSchema {
    DCC,
    DCCLIGHT;
}
