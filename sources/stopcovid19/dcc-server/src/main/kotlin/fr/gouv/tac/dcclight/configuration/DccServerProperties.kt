package fr.gouv.tac.dcclight.configuration

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding
import java.net.URL

@ConstructorBinding
@ConfigurationProperties(prefix = "dcclight")
data class DccServerProperties(
    val encryptionPrivateKey: String,
    val dcclight: DccEncryptionInformation,
    val dcc: DccEncryptionInformation,

    val signingCertificatesEndpoint: Endpoint,
    val aggregationRulesEndpoint: Endpoint,
    val blocklistEndpoint: Endpoint
)

class DccEncryptionInformation(
    val signingCertificate: String,
    val signingPrivateKey: String
) {
    fun getSigningPrivateKeyAsPem() = "-----BEGIN PRIVATE KEY-----\n$signingPrivateKey\n-----END PRIVATE KEY-----"
}

open class Endpoint(
    val url: URL,
    val refreshScheduling: RefreshScheduling,
    val token: String?
)

class RefreshScheduling(val cron: String)
