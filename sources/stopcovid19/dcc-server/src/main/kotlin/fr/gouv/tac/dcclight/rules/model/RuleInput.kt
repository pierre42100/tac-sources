package fr.gouv.tac.dcclight.rules.model

import ehn.techiop.hcert.kotlin.data.GreenCertificate
import fr.gouv.tac.dcclight.extension.getProofAsJsonObject
import fr.gouv.tac.dcclight.extension.getProofInstant
import fr.gouv.tac.dcclight.extension.getProofType
import fr.gouv.tac.dcclight.rules.SanitaryProofType
import kotlinx.serialization.json.JsonPrimitive
import kotlinx.serialization.json.jsonPrimitive

class RuleInput(
    private val type: SanitaryProofType,
    private val dateConstraints: DateConstraints,
    private val allowLists: Map<String, ControlList>,
    private val blockLists: Map<String, ControlList>
) {

    fun matches(certificate: GreenCertificate) =
        type == certificate.getProofType() && validateProofAttributes(certificate)

    private fun validateProofAttributes(certificate: GreenCertificate): Boolean {
        val proof = certificate.getProofAsJsonObject()
        val certificateContainsAllowListKeys = proof.keys.containsAll(allowLists.keys)

        val certificateContentMatchesLists =
            proof
                .filterValues { it is JsonPrimitive }
                .mapValues { it.value.jsonPrimitive.content }
                .all { certificateElement ->
                    allowLists[certificateElement.key]?.accept(certificateElement.value) ?: true &&
                        blockLists[certificateElement.key]?.accept(certificateElement.value) ?: true
                }

        return dateConstraints.matches(certificate.getProofInstant()) &&
            certificateContainsAllowListKeys &&
            certificateContentMatchesLists
    }

    fun minDaysAfterPreviousDcc() = dateConstraints.minDaysAfterPreviousDcc
}
