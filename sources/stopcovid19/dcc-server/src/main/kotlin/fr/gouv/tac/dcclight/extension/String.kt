package fr.gouv.tac.dcclight.extension

import java.text.Normalizer

fun String.removeAccents() = Normalizer.normalize(this, Normalizer.Form.NFD)
    .replace("[\\p{InCombiningDiacriticalMarks}]".toRegex(), "")
