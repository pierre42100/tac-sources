package fr.gouv.tac.dcclight.configuration

import fr.gouv.tac.dcclight.model.DccBlocklist
import org.springframework.boot.actuate.health.AbstractHealthIndicator
import org.springframework.boot.actuate.health.Health
import org.springframework.boot.actuate.health.Status
import org.springframework.stereotype.Component

@Component
class BlocklistHealthIndicator(
    private val blockList: ExternalResource<DccBlocklist>
) : AbstractHealthIndicator("Blocklist not fetched yet, the application is not ready to receive requests") {

    override fun doHealthCheck(builder: Health.Builder) {
        if (blockList.content == null) {
            builder.status(Status.OUT_OF_SERVICE)
        } else {
            builder.status(Status.UP)
        }
    }
}
