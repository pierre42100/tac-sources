package fr.gouv.tac.dcclight.validation

import ehn.techiop.hcert.kotlin.data.GreenCertificate
import fr.gouv.tac.dcclight.service.MetricsService
import fr.gouv.tac.dcclight.service.error.DccValidationError.FLEX_ID_MISMATCH
import fr.gouv.tac.dcclight.service.error.DccValidationError.INCONSISTENT_GIVEN_NAME
import fr.gouv.tac.dcclight.service.error.DccValidationError.MISSING_GIVEN_NAME
import fr.gouv.tac.dcclight.service.error.DccValidationError.MISSING_GIVEN_NAME_TRANSLITERATED
import fr.gouv.tac.dcclight.service.error.rejectCertificate
import fr.gouv.tac.dcclight.service.error.rejectCertificates
import org.springframework.stereotype.Component
import org.springframework.validation.BindingResult

@Component
class GivenNameValidator : GreenCertificatesValidator, FlexIdValidator() {

    /**
     * @param greenCertificatesToValidate: contains a Pair with givenName (first) and givenNameTransliterated (second)
     */
    override fun validate(
        greenCertificatesToValidate: List<GreenCertificate>,
        metricsService: MetricsService,
        validation: BindingResult
    ) {

        // Prerequisites to referenceCertificate selection

        if (greenCertificatesToValidate.all { it.subject.givenName == null }) {
            greenCertificatesToValidate.forEachIndexed { index, _ ->
                validation.rejectCertificate(index, MISSING_GIVEN_NAME)
                metricsService.incrementCounter(MISSING_GIVEN_NAME)
            }
            return
        }

        if (greenCertificatesToValidate.all { it.subject.givenNameTransliterated == null }) {
            greenCertificatesToValidate.forEachIndexed { index, _ ->
                validation.rejectCertificate(index, MISSING_GIVEN_NAME_TRANSLITERATED)
                metricsService.incrementCounter(MISSING_GIVEN_NAME_TRANSLITERATED)
            }
            return
        }

        val referenceGivenName = greenCertificatesToValidate.first { it.subject.givenName != null }.subject.givenName

        val namesTransliterated = greenCertificatesToValidate.mapNotNull { it.subject.givenNameTransliterated }
        val namesTransliteratedMatchFlexId = matchFlexId(namesTransliterated)

        // Metrics incrementation

        greenCertificatesToValidate.forEachIndexed { index, dcc ->
            if (dcc.subject.givenName == null) {
                metricsService.incrementCounter(MISSING_GIVEN_NAME)
            }
            if (dcc.subject.givenName != referenceGivenName) {
                metricsService.incrementCounter(INCONSISTENT_GIVEN_NAME)
            }
            if (dcc.subject.givenNameTransliterated == null) {
                validation.rejectCertificate(index, MISSING_GIVEN_NAME_TRANSLITERATED)
                metricsService.incrementCounter(MISSING_GIVEN_NAME_TRANSLITERATED)
            }
        }

        if (!namesTransliteratedMatchFlexId) {
            metricsService.incrementCounter(FLEX_ID_MISMATCH)
        }

        // Validation rejection

        if (greenCertificatesToValidate.any { (it.subject.givenName != referenceGivenName) } && !namesTransliteratedMatchFlexId) {
            greenCertificatesToValidate.forEachIndexed { index, dcc ->
                if (dcc.subject.givenName == null) {
                    validation.rejectCertificate(index, MISSING_GIVEN_NAME)
                }
                if (dcc.subject.givenName != referenceGivenName) {
                    validation.rejectCertificate(index, INCONSISTENT_GIVEN_NAME)
                }
                if (dcc.subject.givenNameTransliterated == null) {
                    validation.rejectCertificate(index, MISSING_GIVEN_NAME_TRANSLITERATED)
                }
            }
            validation.rejectCertificates(FLEX_ID_MISMATCH)
            if (matchesFlexIdAfterRemovingAccents(namesTransliterated)) {
                metricsService.incrementFlexIdAccentsMismatchs()
            }
        }
    }
}
