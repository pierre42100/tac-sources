package fr.gouv.tac.dcclight.validation

import ehn.techiop.hcert.kotlin.data.GreenCertificate
import fr.gouv.tac.dcclight.service.MetricsService
import fr.gouv.tac.dcclight.service.error.DccValidationError.INVALID_DATE_OF_BIRTH
import fr.gouv.tac.dcclight.service.error.rejectCertificate
import org.springframework.stereotype.Component
import org.springframework.validation.BindingResult

@Component
class DateOfBirthValidator :
    GreenCertificatesValidator {
    override fun validate(
        greenCertificatesToValidate: List<GreenCertificate>,
        metricsService: MetricsService,
        validation: BindingResult
    ) {
        val datesOfBirth = greenCertificatesToValidate.map(GreenCertificate::dateOfBirth)

        datesOfBirth.forEachIndexed { index, date ->
            if (date == null || date != datesOfBirth.first()) {
                validation.rejectCertificate(index, INVALID_DATE_OF_BIRTH)
                metricsService.incrementCounter(INVALID_DATE_OF_BIRTH)
            }
        }
    }
}
