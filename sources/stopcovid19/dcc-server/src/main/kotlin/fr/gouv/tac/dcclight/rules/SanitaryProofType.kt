package fr.gouv.tac.dcclight.rules

enum class SanitaryProofType(val type: String) {
    VACCINATION("v"),
    TEST("t"),
    RECOVERY_STATEMENT("r");

    companion object {
        fun fromType(type: String) = values().firstOrNull { it.type == type }
            ?: throw RuntimeException("This sanitary proof type does not exist.")
    }
}
