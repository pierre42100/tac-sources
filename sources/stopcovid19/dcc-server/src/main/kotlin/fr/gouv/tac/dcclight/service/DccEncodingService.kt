package fr.gouv.tac.dcclight.service

import ehn.techiop.hcert.kotlin.chain.Chain
import ehn.techiop.hcert.kotlin.chain.CwtService
import ehn.techiop.hcert.kotlin.chain.impl.DefaultCborService
import ehn.techiop.hcert.kotlin.chain.impl.DefaultCompressorService
import ehn.techiop.hcert.kotlin.chain.impl.DefaultContextIdentifierService
import ehn.techiop.hcert.kotlin.chain.impl.DefaultCoseService
import ehn.techiop.hcert.kotlin.chain.impl.DefaultCwtService
import ehn.techiop.hcert.kotlin.chain.impl.DefaultHigherOrderValidationService
import ehn.techiop.hcert.kotlin.chain.impl.FileBasedCryptoService
import ehn.techiop.hcert.kotlin.crypto.CertificateAdapter
import ehn.techiop.hcert.kotlin.data.GreenCertificate
import fr.gouv.tac.dcclight.configuration.DccServerProperties
import fr.gouv.tac.dcclight.extension.getCountry
import fr.gouv.tac.dcclight.service.dcc.chain.OptimizedBase45EUService
import fr.gouv.tac.dcclight.service.dcc.chain.OptimizedSchemaValidationEUService
import kotlinx.datetime.Clock.System.now
import org.springframework.stereotype.Service

@Service
class DccEncodingService(dccServerProperties: DccServerProperties) {

    private val higherOrderValidationService = DefaultHigherOrderValidationService()
    private val schemaValidationEUService = OptimizedSchemaValidationEUService()
    private val cborService = DefaultCborService()
    private val coseService = DefaultCoseService(
        FileBasedCryptoService(
            dccServerProperties.dcc.getSigningPrivateKeyAsPem(),
            dccServerProperties.dcc.signingCertificate
        )
    )
    private val compressorService = DefaultCompressorService()
    private val base45EUService = OptimizedBase45EUService()
    private val contextIdentifierService = DefaultContextIdentifierService("HC1:")

    private fun encoder(cwtService: CwtService) = Chain(
        higherOrderValidationService,
        schemaValidationEUService,
        cborService,
        cwtService,
        coseService,
        compressorService,
        base45EUService,
        contextIdentifierService
    )

    private val dscExpirationDate = CertificateAdapter(dccServerProperties.dcc.signingCertificate).validUntil

    fun encode(certificate: GreenCertificate): String {
        val cwtService = DefaultCwtService(certificate.getCountry(), dscExpirationDate.minus(now()))
        return encoder(cwtService).encode(certificate).step5Prefixed
    }
}
