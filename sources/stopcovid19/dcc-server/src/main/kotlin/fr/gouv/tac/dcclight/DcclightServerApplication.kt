package fr.gouv.tac.dcclight

import com.amazon.corretto.crypto.provider.AmazonCorrettoCryptoProvider
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.ConfigurationPropertiesScan
import org.springframework.boot.runApplication
import org.springframework.scheduling.annotation.EnableScheduling

@EnableScheduling
@SpringBootApplication
@ConfigurationPropertiesScan
class DcclightServerApplication {

    companion object {
        init {
            AmazonCorrettoCryptoProvider.install()
        }
    }
}

fun main(args: Array<String>) {
    runApplication<DcclightServerApplication>(*args)
}
