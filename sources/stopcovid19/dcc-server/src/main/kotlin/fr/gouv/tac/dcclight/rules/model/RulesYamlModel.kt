package fr.gouv.tac.dcclight.rules.model

import fr.gouv.tac.dcclight.extension.toKLocalDate
import fr.gouv.tac.dcclight.rules.SanitaryProofType
import org.yaml.snakeyaml.DumperOptions
import org.yaml.snakeyaml.LoaderOptions
import org.yaml.snakeyaml.Yaml
import org.yaml.snakeyaml.constructor.Constructor
import org.yaml.snakeyaml.introspector.BeanAccess
import org.yaml.snakeyaml.nodes.Tag.MAP
import org.yaml.snakeyaml.representer.Representer
import java.util.Date
import kotlin.time.Duration

fun yamlRulesObjectMapper() =
    Yaml(
        Constructor(),
        Representer().apply {
            addClassTag(YamlRules::class.java, MAP)
            addClassTag(YamlRule::class.java, MAP)
            propertyUtils.isSkipMissingProperties = true
        },
        DumperOptions(),
        LoaderOptions().apply { maxAliasesForCollections = 250 }
    ).apply {
        setBeanAccess(BeanAccess.FIELD)
    }

data class YamlRules(
    val rules: List<YamlRule> = listOf()
) {
    fun toRules() = Rules(rules.map(YamlRule::toRule))
}

data class YamlRule(
    private val uniqueID: String = "",
    private val description: String = "",
    private val isRuleActive: Boolean = false,
    val input: List<YamlInputRule> = listOf(),
    private val output: YamlOutputRule = YamlOutputRule()
) {
    fun toRule(): Rule {
        val ruleInputList = input.map(YamlInputRule::toRuleInput)
        return Rule(uniqueID, description, isRuleActive, ruleInputList, output.toRuleOutput())
    }
}

data class YamlInputRule(
    private val type: String = "",
    private val proofDateConstraints: YamlDateConstraints = YamlDateConstraints(),
    val allowLists: Map<String, List<String>> = mapOf(),
    val blockLists: Map<String, List<String>> = mapOf()
) {
    fun toRuleInput() = RuleInput(
        SanitaryProofType.fromType(type),
        proofDateConstraints.toDateConstraints(),
        allowLists.mapValues { AllowList(it.key, it.value) },
        blockLists.mapValues { BlockList(it.key, it.value) }
    )
}

data class YamlDateConstraints(
    val validIfAfter: Date? = null,
    val validIfBefore: Date? = null,
    val validIfDatedWithin: String? = null,
    val minDaysAfterPreviousDcc: Int? = null
) {
    fun toDateConstraints() = DateConstraints(
        validIfAfter = validIfAfter?.toKLocalDate(),
        validIfBefore = validIfBefore?.toKLocalDate(),
        validIfDatedWithin = validIfDatedWithin?.let { Duration.parse(it) },
        minDaysAfterPreviousDcc = minDaysAfterPreviousDcc
    )
}

data class YamlOutputRule(
    private val schema: String = "",
    private val type: String = "",
    private val referentialDccRanking: Int = 0,
    private val overrideValues: Map<String, String> = mapOf(),
    private val copyValues: Map<String, String> = mapOf(),
    private val incDateByDays: Map<String, Int> = mapOf()
) {
    fun toRuleOutput() = RuleOutput(
        DccSchema.valueOf(schema),
        SanitaryProofType.fromType(type),
        referentialDccRanking,
        overrideValues.map { AttributeOverrideSpec(it.key, it.value) },
        copyValues.map { AttributeCopySpec(it.key, it.value) },
        incDateByDays.map { AttributeIncrementSpec(it.key, it.value) }
    )
}
