package fr.gouv.tac.dcclight.rules.model

import ehn.techiop.hcert.kotlin.data.GreenCertificate
import fr.gouv.tac.dcclight.extension.getProofInstant
import kotlinx.datetime.TimeZone.Companion.currentSystemDefault
import kotlinx.datetime.daysUntil

class Rule(
    val id: String,
    val description: String,
    private val active: Boolean,
    val inputRules: List<RuleInput>,
    val output: RuleOutput
) {
    fun matches(temporallySortedDcc: List<GreenCertificate>): Boolean {
        return active &&
            inputRules.size == temporallySortedDcc.size &&
            verifyCertificatesDelays(temporallySortedDcc) &&
            certificatesMatchRules(temporallySortedDcc)
    }

    private fun certificatesMatchRules(temporallySortedDcc: List<GreenCertificate>) =
        inputRules
            .zip(temporallySortedDcc) { rule, certificate -> rule.matches(certificate) }
            .all { it }

    private fun verifyCertificatesDelays(temporallySortedDcc: List<GreenCertificate>): Boolean {
        val dccDates = temporallySortedDcc.map { it.getProofInstant() }
        // [1, 2, 3, 4] -> [(1,2), (2,3), (3,4)]
        val adjacentCertificatesDates = dccDates.zip(dccDates.drop(1))
        return inputRules
            // start comparison with 2nd dcc compared to first
            .drop(1)
            .map(RuleInput::minDaysAfterPreviousDcc)
            .zip(adjacentCertificatesDates) { minDaysBetweenDccs, dates ->
                dates.first.daysUntil(dates.second, currentSystemDefault()) >= minDaysBetweenDccs
            }
            .all { it }
    }
}
