package fr.gouv.tac.dcclight.rules.model

import fr.gouv.tac.dcclight.extension.isEmptyOrContains

interface ControlList {
    /**
     * Returns whether the given value is accepted or not by this control list.
     */
    fun accept(value: String): Boolean
}

data class AllowList(
    val bigram: String,
    val values: List<String>
) : ControlList {
    override fun accept(value: String): Boolean = values.isEmptyOrContains(value)
}

data class BlockList(
    val bigram: String,
    val values: List<String>
) : ControlList {
    override fun accept(value: String): Boolean = !values.contains(value)
}
