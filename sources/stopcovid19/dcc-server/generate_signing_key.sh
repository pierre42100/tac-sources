#!/usr/bin/env bash

set -e

mkdir -p target/generatedKeys/

openssl ecparam -name prime256v1 -genkey -noout -out target/generatedKeys/sign.priv.ec.key
openssl pkcs8 -topk8 -nocrypt -in target/generatedKeys/sign.priv.ec.key -out target/generatedKeys/sign.priv.pem
openssl req -new -key target/generatedKeys/sign.priv.pem -x509 -nodes -days 7300 -batch -out target/generatedKeys/sign.cert.pem
