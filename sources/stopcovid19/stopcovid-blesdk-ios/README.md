# ProximityNotification SDK for iOS

`ProximityNotification` SDK uses Bluetooth Low Energy (BLE) in order to exchange data with proximity nearby.

Each time a device is nearby a `ProximityInfo` is notified. `ProximityInfo` contains following information:
- Proximity payload containing exchanged data.
- Proximity timestamp.
- Some metadata such as BLE calibrated received signal strength indicator (RSSI) and transmitting (TX) power level.

In order to use `ProximityNotification`, the application must declare `bluetooth-central` and  `bluetooth-peripheral` background modes.

Then, you must create a `ProximityNotificationService` object with the following parameters :
- A `ProximityNotificationSettings` containing a `BluetoothSettings` with these mandatory parameters :
    - The service unique identifier.
    - The characteristic unique identifier.
    - A `BluetoothDynamicSettings` that contains :
        - The calibrated TX power level of your device.
        - The RSSI compensation gain of your device.
- A closure `ProximityPayloadProvider` called when the SDK will send a payload to a remote.
- A closure `ProximityInfoUpdateHandler` called when a `ProximityInfo` is notified.
- A closure `IdentifierFromProximityPayload` called to extract an identifier from a payload.
- A closure `StateChangedHandler` called when the `ProximityNotification` state has changed.

```swift
import ProximityNotification

let bleSettings = BluetoothSettings(serviceUniqueIdentifier: "",
                                    serviceCharacteristicUniqueIdentifier: "",
                                    dynamicSettings: BluetoothDynamicSettings(txCompensationGain: 0, rxCompensationGain: 0))

let proximityPayloadProvider: ProximityPayloadProvider = {}

let proximityInfoUpdateHandler: ProximityInfoUpdateHandler = { proximity in }

let identifierFromProximityPayload: IdentifierFromProximityPayload = { payload in }

let stateChangedHandler: StateChangedHandler = { state in }

let service = ProximityNotificationService(settings: ProximityNotificationSettings(bluetoothSettings: bleSettings),
                                           proximityPayloadProvider: proximityPayloadProvider,
                                           proximityInfoUpdateHandler: proximityInfoUpdateHandler,
                                           identifierFromProximityPayload: identifierFromProximityPayload,
                                           stateChangedHandler: stateChangedHandler)
```

To start the service, you need to call `start`.

```swift
service.start()
```

To stop the service, you can call `stop`.

```swift
service.stop()
```

To update the `BluetoothDynamicSettings` without restarting the `ProximityNotificationService`, you can use the `dynamicSettings` property.

```swift
let bluetoothDynamicSettings = BluetoothDynamicSettings(txCompensationGain: 0, rxCompensationGain: 0)
service.dynamicSettings = ProximityNotificationDynamicSettings(bluetoothDynamicSettings: bluetoothDynamicSettings) 
```