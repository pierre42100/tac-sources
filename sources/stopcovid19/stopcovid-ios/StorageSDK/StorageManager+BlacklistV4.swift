// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  StorageManager+BlacklistV4.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 01/03/2022 - for the TousAntiCovid project.
//

import RealmSwift

public extension StorageManager {
    // Get content of blacklist
    func certificateBlacklistedIndexes() -> [String] {
        guard let realm = realm else { return [] }
        var indexes: [String] = []
        Realm.queue.sync {
            indexes = realm.objects(RealmBlacklistedIndex.self).map { $0.indexString }
        }
        return indexes
    }
    
    // Delete all the content of the blacklist
    func clearCertificateHashesBlacklist() {
        guard let realm = realm else { return }
        Realm.queue.sync {
            try! realm.write {
                realm.delete(realm.objects(RealmBlacklistedHash.self))
            }
        }
    }
    
    // Update blacklist with new or existing indexes
    func updateCertificateIndexesBlacklist(_ indexes: [String]) {
        guard let realm = realm else { return }
        Realm.queue.sync {
            try! realm.write {
                realm.delete(realm.objects(RealmBlacklistedIndex.self))
                realm.add(indexes.map { RealmBlacklistedIndex(index: $0) }, update: .all)
            }
        }
    }
    
    func save(blacklistedCertificatesHashes: [String]) {
        guard let realm = realm else { return }
        Realm.queue.sync {
            try! realm.write {
                realm.delete(realm.objects(RealmBlacklistedHash.self))
                realm.add(blacklistedCertificatesHashes.map { RealmBlacklistedHash(hash: $0) }, update: .all)
            }
        }
    }
    
    func add(blacklistedCertificatesHash: String) {
        guard let realm = realm else { return }
        Realm.queue.sync {
            try! realm.write {
                realm.add(RealmBlacklistedHash(hash: blacklistedCertificatesHash), update: .all)
            }
        }
    }
    
    func remove(certificatesHash: String) {
        guard let realm = realm else { return }
        Realm.queue.sync {
            guard let object = realm.object(ofType: RealmBlacklistedHash.self, forPrimaryKey: certificatesHash) else { return }
            try! realm.write {
                realm.delete(object)
            }
        }
    }
    
    func blacklistedCertificatesHashes() -> [String] {
        guard let realm = realm else { return [] }
        var hashes: [String] = []
        Realm.queue.sync {
            hashes = realm.objects(RealmBlacklistedHash.self).map { $0.hashString }
        }
        return hashes
    }
}
