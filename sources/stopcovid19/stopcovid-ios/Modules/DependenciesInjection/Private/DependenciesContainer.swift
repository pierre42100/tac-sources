//
//  DependenciesContainer.swift
//  BioDivSports
//
//  Created by Rémi Lanteri on 24/01/2023.
//

final class DependenciesContainer {
    static let shared: DependenciesContainer = .init()
    private var dependencies: LazyDictionary<Any> = .init()

    private init() {}

    deinit {
        dependencies.clear()
    }

    func register<DependencyType>(_ dependencyBuilder: @escaping () -> DependencyType) {
        register(key: String(describing: DependencyType.self), factoryClosure: dependencyBuilder)
    }

    func resolveRepository<DependencyType>() -> DependencyType {
        let key: String = .init(describing: DependencyType.self)
        return dependencies.value(for: key) as! DependencyType
    }
}

private extension DependenciesContainer {
    func register(key: String, factoryClosure: @escaping () -> Any) {
        dependencies.set(value: factoryClosure, for: key)
    }
}
