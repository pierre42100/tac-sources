// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  AppContextDataProvider.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 02/02/2023 - for the TousAntiCovid project.
//

import Foundation
import AppModel

public enum AppContextDataProvider {
    public static var defaultLanguage: Language = Language.english

    public static var currentAppLanguageCode: String? {
        get { AppContextDataProvider.currentAppLanguageCodeSaved }
        set { AppContextDataProvider.currentAppLanguageCodeSaved = newValue }
    }
}

private extension AppContextDataProvider {
    @UserDefault(key: .currentAppLanguageCode) static var currentAppLanguageCodeSaved: String?
}
