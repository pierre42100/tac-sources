// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  AppModel.h
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 12/01/2023 - for the TousAntiCovid project.
//

#import <Foundation/Foundation.h>

//! Project version number for AppModel.
FOUNDATION_EXPORT double AppModelVersionNumber;

//! Project version string for AppModel.
FOUNDATION_EXPORT const unsigned char AppModelVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <AppModel/PublicHeader.h>


