// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  ParagraphLink.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 27/02/2023 - for the TousAntiCovid project.
//

public struct ParagraphLink {
    public let label: String
    public let action: Action

    public enum Action {
        case web(_ urlToLocalized: String)
        case gesturesController
    }

    public init(label: String, action: ParagraphLink.Action) {
        self.label = label
        self.action = action
    }
}
