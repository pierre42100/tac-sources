// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  InfoRepositoryObserver.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 03/02/2023 - for the TousAntiCovid project.
//

import Foundation

public protocol InfoRepositoryObserver: AnyObject {
    func infoCenterDidUpdate()
}
