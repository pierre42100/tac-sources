// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  FetchMyHealthDataUseCase.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 27/02/2023 - for the TousAntiCovid project.
//

extension UseCases {
    public static func fetchMyHealthData(forceRefresh: Bool = false, _ completion: ((Error?) -> ())? = nil) {
        myHealthRepository.fetchObjects(forceRefresh: forceRefresh) { error in
            completion?(error)
        }
    }
}
