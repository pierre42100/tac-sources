// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  ObserveMyHealthDataUseCase.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 27/02/2023 - for the TousAntiCovid project.
//

import Protocols

extension UseCases {
    public static func addObserverInfo(_ observer: MyHealthRepositoryObserver) {
        myHealthRepository.addObserver(observer)
    }

    public static func removeObserverInfo(_ observer: MyHealthRepositoryObserver) {
        myHealthRepository.removeObserver(observer)
    }
}
