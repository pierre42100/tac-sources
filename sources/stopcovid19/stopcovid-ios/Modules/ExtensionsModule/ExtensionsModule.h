// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  ExtensionsModule.h
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 25/01/2023 - for the TousAntiCovid project.
//

#import <Foundation/Foundation.h>

//! Project version number for ExtensionsModule.
FOUNDATION_EXPORT double ExtensionsModuleVersionNumber;

//! Project version string for ExtensionsModule.
FOUNDATION_EXPORT const unsigned char ExtensionsModuleVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <ExtensionsModule/PublicHeader.h>


