// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  FileManager+Extension.swift
//  ExtensionsModule
//
//  Created by Lunabee Studio / Date - 12/01/2023 - for the TousAntiCovid project.
//

import Foundation

public extension FileManager {
    class func documentsDirectory() -> URL {
        try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
    }

    class func libraryDirectory() -> URL {
        try! FileManager.default.url(for: .libraryDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
    }
}
