// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  RESTInfo.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 12/01/2023 - for the TousAntiCovid project.
//

public struct RESTInfo: Codable {
    public let titleKey: String
    public let descriptionKey: String
    public let buttonLabelKey: String?
    public let urlKey: String?
    public let timestamp: Int
    public let tagIds: [String]?
}
