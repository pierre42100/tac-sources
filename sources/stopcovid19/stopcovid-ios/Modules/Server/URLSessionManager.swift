// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  URLSessionManager.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 02/02/2023 - for the TousAntiCovid project.
//

import Foundation

final class UrlSessionManager: NSObject {
    static let shared: UrlSessionManager = .init()
    private override init() { super.init() }

    private lazy var session: URLSession = {
        let session: URLSession = URLSession(configuration: .default, delegate: self, delegateQueue: .main)
        session.configuration.requestCachePolicy = .reloadIgnoringLocalAndRemoteCacheData
        session.configuration.httpShouldUsePipelining = true
        return session
    }()

    func dataTaskWithETag(with url: URL, resetEtag: Bool = false, completionHandler: ((Data?, URLResponse?, Error?, _ updateEtagBlock: @escaping () -> Void) -> Void)? = nil) -> URLSessionDataTask {
        dataTaskWithETag(with: URLRequest(url: url), completionHandler: completionHandler)
    }
    
    private func dataTaskWithETag(with request: URLRequest, resetEtag: Bool = false, completionHandler: ((Data?, URLResponse?, Error?, _ updateEtagBlock: @escaping () -> Void) -> Void)? = nil) -> URLSessionDataTask {
        var request: URLRequest = request
        if !resetEtag, let url = request.url, let eTag = ETagManager.shared.eTag(for: url.absoluteString) {
            request.addValue(eTag, forHTTPHeaderField: Constant.ETag.requestHeaderField)
        }
        return URLSessionDataTaskFactory.shared.dataTask(with: request, session: session) { data, response, error in
            if response?.isNotModified == true { // json not modified since last fetch
                completionHandler?(nil, response, nil) { }
            } else {
                completionHandler?(data, response, error) {
                    if let url = request.url, let eTag = response?.eTag {
                        ETagManager.shared.save(eTag: eTag, for: url.absoluteString)
                    }
                }
            }
        }
    }
}

extension UrlSessionManager: URLSessionDelegate {
    func urlSession(_ session: URLSession, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        let certificates: [Data] = Constant.Certificates.certificates
        CertificatePinning.validateChallenge(challenge, certificateFiles: certificates) { validated, credential in
            validated ? completionHandler(.useCredential, credential) : completionHandler(.cancelAuthenticationChallenge, nil)
        }
    }
}
