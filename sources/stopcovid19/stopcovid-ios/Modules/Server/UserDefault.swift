// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  UserDefault.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 25/01/2023 - for the TousAntiCovid project.
//

import Foundation
import ExtensionsModule

@propertyWrapper
internal struct UserDefault<T: Equatable> {
    var wrappedValue: T {
        get {
            defaults.object(forKey: key) as? T ?? defaultValue
        }
        set {
            if let optional = newValue as? AnyOptional, optional.isNil {
                defaults.removeObject(forKey: key)
            } else {
                defaults.set(newValue, forKey: key)
            }
            defaults.synchronize()
        }
    }

    var projectedValue: String { key }

    private var defaults: UserDefaults
    private let key: String
    private let defaultValue: T

    init(wrappedValue: T, key: Key) {
        self.init(wrappedValue: wrappedValue, key: key.rawValue)
    }

    private init(wrappedValue: T, key: String, userDefaults: UserDefaults = .standard) {
        self.defaultValue = wrappedValue
        self.key = key
        self.defaults = userDefaults
    }
}

extension UserDefault where T: ExpressibleByNilLiteral {
    init(key: Key) {
        self.init(wrappedValue: nil, key: key)
    }
}
