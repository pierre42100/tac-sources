// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  Constant.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 01/02/2023 - for the TousAntiCovid project.
//

import ExtensionsModule

enum Constant {
    // TODO: Update Application/Constant.swift jsonVersion as well (while the migration to the new architecture is not fullfill)
    static let jsonVersion: Int = 45

    enum Url {
        static let staticResourcesRootDomain: String = "app-static.tousanticovid.gouv.fr"
        
    }
    
    enum Certificates {
        static var certificates: [Data] { ["certigna-root", "certigna-services"].compactMap { Bundle.main.fileDataFor(fileName: $0, ofType: "pem") } }
    }

    enum MyHealth {
        static let baseUrl: URL = .init(string: "https://\(Url.staticResourcesRootDomain)/json/version-\(jsonVersion)/Risks/risks.json")!
        
    }
    
    enum InfoCenter {
        static let directoryName: String = "InfoCenter"
        
        static let baseUrl: String = "https://\(Url.staticResourcesRootDomain)/json/news/v1/\(directoryName)"
        static let tagsUrl: URL = .init(string: "\(baseUrl)/info-tags.json")!
        static let infoCenterUrl: URL = .init(string: "\(baseUrl)/info-center.json")!
    }
    
    enum ETag {
        static let requestHeaderField: String = "If-None-Match"
        static let responseHeaderField: String = "Etag"
    }
}
