// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  FeaturedInfoManager.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 05/01/2023 - for the TousAntiCovid project.
//

import UIKit
import UseCases

protocol FeaturedInfoChangesObserver: AnyObject {

    func featuredInfoDidUpdate()

}

final class FeaturedInfoObserverWrapper: NSObject {

    weak var observer: FeaturedInfoChangesObserver?

    init(observer: FeaturedInfoChangesObserver) {
        self.observer = observer
    }

}

final class FeaturedInfoManager {

    static let shared: FeaturedInfoManager = .init()
    private init() {}

    var featuredInfos: [FeaturedInfo] = []
    var thumbnails: [String: UIImage] = [:]
    var ressources: [String: URL] = [:]

    private var observers: [FeaturedInfoObserverWrapper] = []
    // TODO: Use Storage Data directly with the new architecture
    private var defaultAppLanguageCode: String = UseCases.defaultAppLanguage.rawValue

    func start() {
        addObserver()
        let succeed: Bool = loadLocalData()
        if !succeed {
            fetchAndSaveFeaturedInfosFile(forceReload: true)
        }
    }

    func reloadLanguage() {
        let succeed: Bool = loadLocalData()
        fetchAndSaveFeaturedInfosFile(forceReload: !succeed)
    }

    private func loadLocalData() -> Bool {
        let languageCode: String = Locale.currentAppLanguageCode
        guard loadLocalFeaturedInfos(languageCode: languageCode) else { return false }
        guard loadLocalThumbnails(languageCode: languageCode) else { return false}
        guard loadLocalRessources(languageCode: languageCode) else { return false }
        return true
    }

    private func addObserver() {
        NotificationCenter.default.addObserver(self, selector: #selector(appDidBecomeActive), name: UIApplication.didBecomeActiveNotification, object: nil)
    }

    @objc private func appDidBecomeActive() {
        fetchAndSaveFeaturedInfosFile(forceReload: false)
    }

}

// MARK: - All fetching methods -
extension FeaturedInfoManager {

    private func featuredInfosUrl(for languageCode: String) -> URL {
        URL(string: "\(FeaturedInfoConstant.baseUrl)/featured-\(languageCode).json")!
    }

    private func fetchAndSaveFeaturedInfosFile(forceReload: Bool) {
        let languageCode: String = Locale.currentAppLanguageCode
        let dataTask: URLSessionDataTask = UrlSessionManager.shared.session.dataTaskWithETag(with: featuredInfosUrl(for: languageCode), resetEtag: forceReload) { data, response, error, updateEtag in
            guard let data = data else {
                return
            }
            do {
                // Featured Infos
                let receivedFeaturedInfos = try JSONDecoder().decode([JSONFeaturedInfo].self, from: data).map { $0.toAppModel() }
                if !receivedFeaturedInfos.isEmpty {
                    self.featuredInfos = receivedFeaturedInfos
                    let localUrl: URL = self.localFeaturedInfosUrl(for: languageCode)
                    let appData: Data = try JSONEncoder().encode(self.featuredInfos)
                    try appData.write(to: localUrl)
                    updateEtag()
                }

                /// Thumbnails
                self.deleteAllThumbnails()
                let dispatchGroup: DispatchGroup = .init()
                self.featuredInfos.forEach { featureInfo in
                    dispatchGroup.enter()
                    self.fetchAndSaveThumbnail(featuredInfo: featureInfo, languageCode: languageCode) { _ in
                        dispatchGroup.leave()
                    }
                }
                dispatchGroup.notify(queue: .main) {
                    self.notifyObservers()
                }

                /// Ressources
                self.deleteAllRessources()
                let ressourcesDispatchGroup: DispatchGroup = .init()
                self.featuredInfos.forEach { featureInfo in
                    ressourcesDispatchGroup.enter()
                    self.fetchAndSaveRessources(featuredInfo: featureInfo, languageCode: languageCode) { _ in
                        ressourcesDispatchGroup.leave()
                    }
                }
                ressourcesDispatchGroup.notify(queue: .main) {
                    self.notifyObservers()
                }
            } catch {}
        }
        dataTask.resume()
    }

    private func fetchAndSaveThumbnail(featuredInfo: FeaturedInfo, languageCode: String, _ completion: @escaping (Error?) -> ()) {
        guard let url = URL(string: featuredInfo.thumbnail) else {
            completion(NSError.localizedError(message: NSLocalizedString("common.error.unknown", comment: ""), code: 0))
            return
        }
        let dataTask: URLSessionDataTask = UrlSessionManager.shared.session.dataTask(with: URLRequest(url: url)) { data, response, error in
            guard let data = data else {
                completion(error)
                return
            }
            do {
                let withExtension: String = url.pathExtension
                let localFileName: String = self.thumbnailFileName(featuredInfoId: featuredInfo.id, languageCode: languageCode, withExtension: withExtension)

                if let image = UIImage(data: data) {
                    self.thumbnails[featuredInfo.id] = image
                    let localUrl: URL = self.createRessourcesWorkingDirectoryIfNeeded().appendingPathComponent(localFileName)
                    try data.write(to: localUrl)
                } else {
                    guard self.loadLocalThumbnail(featuredInfoId: featuredInfo.id, languageCode: languageCode, withExtension: withExtension) else {
                        completion(NSError.localizedError(message: NSLocalizedString("common.error.unknown", comment: ""), code: 0))
                        return
                    }
                }
                completion(nil)
            } catch {
                completion(error)
            }
        }
        dataTask.resume()
    }

    private func fetchAndSaveRessources(featuredInfo: FeaturedInfo, languageCode: String, _ completion: @escaping (Error?) -> ()) {
        guard let url = URL(string: featuredInfo.hd) else {
            completion(NSError.localizedError(message: NSLocalizedString("common.error.unknown", comment: ""), code: 0))
            return
        }
        let dataTask: URLSessionDataTask = UrlSessionManager.shared.session.dataTask(with: URLRequest(url: url)) { data, response, error in
            guard let data = data else {
                completion(error)
                return
            }
            do {
                let withExtension: String = url.pathExtension
                let localFileName: String = self.ressourcesFileName(featuredInfoId: featuredInfo.id, languageCode: languageCode, withExtension: withExtension)
                let localUrl: URL = self.createRessourcesWorkingDirectoryIfNeeded().appendingPathComponent(localFileName)
                try data.write(to: localUrl)
                self.ressources[featuredInfo.id] = localUrl
                completion(nil)
            } catch {
                completion(error)
            }
        }
        dataTask.resume()
    }
}

// MARK: - Local files management -
extension FeaturedInfoManager {

    private func localFeaturedInfosUrl(for languageCode: String) -> URL {
        let directoryUrl: URL = createWorkingDirectoryIfNeeded()
        return directoryUrl.appendingPathComponent("featured-\(languageCode).json")
    }

    private func loadLocalFeaturedInfos(languageCode: String) -> Bool {
        var localUrl: URL = localFeaturedInfosUrl(for: languageCode)
        if !FileManager.default.fileExists(atPath: localUrl.path) {
            localUrl = localFeaturedInfosUrl(for: defaultAppLanguageCode)
        }
        do {
            let data: Data = try Data(contentsOf: localUrl)
            featuredInfos = (try JSONDecoder().decode([FeaturedInfo].self, from: data))
            return true
        } catch {
            return false
        }
    }

    private func createWorkingDirectoryIfNeeded() -> URL {
        let directoryUrl: URL = FileManager.libraryDirectory().appendingPathComponent("FeaturedInfo")
        if !FileManager.default.fileExists(atPath: directoryUrl.path, isDirectory: nil) {
            try? FileManager.default.createDirectory(at: directoryUrl, withIntermediateDirectories: false, attributes: nil)
        }
        return directoryUrl
    }

}

// MARK: - Local images management -
extension FeaturedInfoManager {

    private func loadLocalThumbnails(languageCode: String) -> Bool {
        var succeed: Bool = true
        featuredInfos.forEach { featuredInfo in
            let withExtension: String = URL(string: featuredInfo.thumbnail)?.pathExtension ?? "png"
            guard loadLocalThumbnail(featuredInfoId: featuredInfo.id, languageCode: languageCode, withExtension: withExtension) else {
                succeed = false
                return
            }
        }
        return succeed
    }

    private func thumbnailFileName(featuredInfoId: String, languageCode: String, withExtension: String) -> String {
        "thumbnail-\(featuredInfoId)-\(languageCode).\(withExtension)"
    }

    private func localThumbnailsUrl(featuredInfoId: String, languageCode: String, withExtension: String) -> URL {
        let directoryUrl: URL = self.createRessourcesWorkingDirectoryIfNeeded()
        return directoryUrl.appendingPathComponent(thumbnailFileName(featuredInfoId: featuredInfoId, languageCode: languageCode, withExtension: withExtension))
    }

    private func loadLocalThumbnail(featuredInfoId: String, languageCode: String, withExtension: String) -> Bool {
        var localUrl: URL = localThumbnailsUrl(featuredInfoId: featuredInfoId, languageCode: languageCode, withExtension: withExtension)
        if !FileManager.default.fileExists(atPath: localUrl.path) {
            localUrl = localThumbnailsUrl(featuredInfoId: featuredInfoId, languageCode: defaultAppLanguageCode, withExtension: withExtension)
        }
        guard let data = try? Data(contentsOf: localUrl) else {
            return false
        }
        guard let image = UIImage(data: data) else {
            return false
        }
        thumbnails[featuredInfoId] = image
        return true
    }

    private func createThumbnailsWorkingDirectoryIfNeeded() -> URL {
        let directoryUrl: URL = FileManager.libraryDirectory().appendingPathComponent("FeaturedInfo").appendingPathComponent("Thumbnails")
        if !FileManager.default.fileExists(atPath: directoryUrl.path, isDirectory: nil) {
            try? FileManager.default.createDirectory(at: directoryUrl, withIntermediateDirectories: false, attributes: nil)
        }
        return directoryUrl
    }

    private func deleteAllThumbnails() {
        try? FileManager.default.removeItem(at: createThumbnailsWorkingDirectoryIfNeeded())
    }

}

// MARK: - Local Ressources management -
extension FeaturedInfoManager {

    private func loadLocalRessources(languageCode: String) -> Bool {
        var succeed: Bool = true
        featuredInfos.forEach { featuredInfo in
            let withExtension: String = URL(string: featuredInfo.hd)?.pathExtension ?? "pdf"
            guard loadLocalRessource(featuredInfoId: featuredInfo.id, languageCode: languageCode, withExtension: withExtension) else {
                succeed = false
                return
            }
        }
        return succeed
    }

    private func ressourcesFileName(featuredInfoId: String, languageCode: String, withExtension: String) -> String {
        "hd-\(featuredInfoId)-\(languageCode).\(withExtension)"
    }

    private func localRessourceUrl(featuredInfoId: String, languageCode: String, withExtension: String) -> URL {
        let directoryUrl: URL = self.createRessourcesWorkingDirectoryIfNeeded()
        return directoryUrl.appendingPathComponent(ressourcesFileName(featuredInfoId: featuredInfoId, languageCode: languageCode, withExtension: withExtension))
    }

    private func loadLocalRessource(featuredInfoId: String, languageCode: String, withExtension: String) -> Bool {
        var localUrl: URL = localRessourceUrl(featuredInfoId: featuredInfoId, languageCode: languageCode, withExtension: withExtension)
        if !FileManager.default.fileExists(atPath: localUrl.path) {
            localUrl = localRessourceUrl(featuredInfoId: featuredInfoId, languageCode: defaultAppLanguageCode, withExtension: withExtension)
            if !FileManager.default.fileExists(atPath: localUrl.path) {
                return false
            }
        }
        ressources[featuredInfoId] = localUrl
        return true
    }

    private func createRessourcesWorkingDirectoryIfNeeded() -> URL {
        let directoryUrl: URL = FileManager.libraryDirectory().appendingPathComponent("FeaturedInfo").appendingPathComponent("Ressources")
        if !FileManager.default.fileExists(atPath: directoryUrl.path, isDirectory: nil) {
            try? FileManager.default.createDirectory(at: directoryUrl, withIntermediateDirectories: false, attributes: nil)
        }
        return directoryUrl
    }

    private func deleteAllRessources() {
        try? FileManager.default.removeItem(at: createRessourcesWorkingDirectoryIfNeeded())
    }

}

// MARK: - Observer -
extension FeaturedInfoManager {

    func addObserver(_ observer: FeaturedInfoChangesObserver) {
        guard observerWrapper(for: observer) == nil else { return }
        observers.append(FeaturedInfoObserverWrapper(observer: observer))
    }

    func removeObserver(_ observer: FeaturedInfoChangesObserver) {
        guard let wrapper = observerWrapper(for: observer), let index = observers.firstIndex(of: wrapper) else { return }
        observers.remove(at: index)
    }

    private func observerWrapper(for observer: FeaturedInfoChangesObserver) -> FeaturedInfoObserverWrapper? {
        observers.first { $0.observer === observer }
    }

    private func notifyObservers() {
        observers.forEach { $0.observer?.featuredInfoDidUpdate() }
    }

}
