// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  JSONFeaturedInfo.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 05/01/2023 - for the TousAntiCovid project.
//

import Foundation

struct JSONFeaturedInfo: Decodable {
    let title: String
    let thumbnail: String
    let hd: String
    let hint: String
    let url: String?
}

extension JSONFeaturedInfo {
    func toAppModel() -> FeaturedInfo {
        FeaturedInfo(id: UUID().uuidString, title: title, thumbnail: thumbnail, hd: hd, hint: hint, url: url)
    }
}
