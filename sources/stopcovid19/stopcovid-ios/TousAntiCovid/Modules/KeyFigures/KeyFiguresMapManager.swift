// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  KeyFiguresMapManager.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 19/01/2023 - for the TousAntiCovid project.
//

import UIKit
import ServerSDK

@available(iOS 13.0, *)
protocol KeyFiguresMapChangesObserver: AnyObject {
    func keyFiguresMapDidUpdate()
}

@available(iOS 13.0, *)
final class KeyFiguresMapManager {
    
    static let shared: KeyFiguresMapManager = .init()
    
    weak var observer: KeyFiguresMapChangesObserver?

    @UserDefault(key: .lastInitialMapKeyFiguresBuildNumber)
    private var lastInitialMapKeyFiguresBuildNumber: String? = nil
    
    var error: Error? = nil {
        didSet {
            guard error != nil else { return }
            observer?.keyFiguresMapDidUpdate()
        }
    }
    var isLoading: Bool = false {
        didSet {
            observer?.keyFiguresMapDidUpdate()
        }
    }
        
    var keyFiguresMap: [KeyFigureMap] = []
    var keyFiguresForMapSelection: [KeyFigure] {
        let keys: [String] = keyFiguresMap.map { $0.labelKey }
        return KeyFiguresManager.shared.keyFigures.filter { keys.contains($0.labelKey) }
    }
    
    var keyFiguresMapActivated: Bool {
        return ParametersManager.shared.displayKeyFiguresMap
        
    }
    
    @UserDefault(key: .selectedKeyFigureForMap)
    private var selectedKeyFigureKeyForMap: String?
    
    var selectedKeyFigureForMap: KeyFigure? {
        if let key = selectedKeyFigureKeyForMap {
            return keyFiguresForMapSelection.first { $0.labelKey == key }
        } else {
            return keyFiguresForMapSelection.first
        }
    }
    
    var selectedKeyFigureMap: KeyFigureMap? {
        if let key = selectedKeyFigureKeyForMap {
            return keyFiguresMap.first { $0.labelKey == key }
        } else {
            return keyFiguresMap.first
        }
    }
    
    @UserDefault(key: .keyFiguresMapAutoplay)
    var keyFiguresMapAutoplay: Bool = true
    
    @UserDefault(key: .keyFiguresMapStates)
    private var keyFiguresMapStates: [String : Double] = [:] // [labelKey of keyfigure : timestamp of the value to display on start]
    
    var currentSerieIndexForSelectedKeyFigure: Int? {
        guard let keyFigure = selectedKeyFigureMap else { return nil }
        guard let date = keyFiguresMapStates[keyFigure.labelKey] else { return nil }
        return keyFigure.valuesDepartments?.first?.serieItems?.firstIndex { item in
            item.date == date
        }
    }
    
    private init() {}
    
    func start() {
        writeInitialFileIfNeeded()
        loadLocalMapKeyFigures()
    }
    
    func fetchMapKeyFigures(_ completion: (() -> ())? = nil) {
        guard keyFiguresMapActivated else {
            completion?()
            return
        }
        fetchAllMapFiles(completion)
    }
    
    func loadLocalMapKeyFigures() {
        guard keyFiguresMapActivated else { return }
        loadLocalMapFiles()
    }
    
    func updateCurrentSerieIndexForSelectedKeyFigure(with value: Int) -> Int? {
        guard let keyFigure = selectedKeyFigureMap else { return nil }
        guard let items = keyFigure.valuesDepartments?.first?.serieItems, !items.isEmpty else { return nil }
        var newIndex: Int = value
        if newIndex >= items.count { newIndex = 0 }
        guard let date = keyFigure.valuesDepartments?.first?.serieItems?.item(at: newIndex)?.date else { return nil }
        keyFiguresMapStates[keyFigure.labelKey] = date
        return newIndex
    }
}

@available(iOS 13.0, *)
// MARK: - All map fetching methods -
extension KeyFiguresMapManager {
    
    private func fetchAllMapFiles(_ completion: (() -> ())? = nil) {
        isLoading = keyFiguresMap.isEmpty // Show loading only if we do not have local data
        error = nil
        fetchKeyFiguresMapFile { [weak self] error in
            self?.isLoading = false
            self?.error = error
            completion?()
        }
    }
    
    private func fetchKeyFiguresMapFile(_ completion: @escaping (_ error: Error?) -> ()) {
        let dataTask: URLSessionDataTask = UrlSessionManager.shared.session.dataTaskWithETag(with: remoteMapFileUrl(), resetEtag: keyFiguresMap.isEmpty) { data, response, error, updateEtag in
            guard let data = data else {
                DispatchQueue.main.async { completion(nil) }
                return
            }
            do {
                let uncompressedData: Data = try data.gunzipped()
                let keyNumbers: Keynumbers_KeyNumbersMessage = try .init(serializedData: uncompressedData)
                let receivedKeyFiguresMap = keyNumbers.toAppModelMapKeyFigure()
                if !receivedKeyFiguresMap.isEmpty {
                    self.keyFiguresMap = receivedKeyFiguresMap
                    self.updateSelectionIfNecessary()
                    try uncompressedData.write(to: self.localKeyFiguresMapUrl())
                    updateEtag()
                }
                // In case we receive an empty file, we complete like on 304
                DispatchQueue.main.async { completion(nil) }
            } catch {
                DispatchQueue.main.async { completion(error) }
            }
        }
        dataTask.resume()
    }
    
    private func remoteMapFileUrl() -> URL {
        KeyFiguresConstant.baseUrl.appendingPathComponent("key-figures-map.pb.gz")
    }
    
    private func updateSelectionIfNecessary() {
        let noMoreSelectable: Bool = !keyFiguresMap.map { $0.labelKey }.contains(selectedKeyFigureKeyForMap)
        if selectedKeyFigureKeyForMap.isNil || noMoreSelectable {
            selectedKeyFigureKeyForMap = keyFiguresMap.first?.labelKey
        }
    }
    
}

@available(iOS 13.0, *)
extension KeyFiguresMapManager {
    
    func setSelectionForMap(keyFigure: KeyFigure) {
        selectedKeyFigureKeyForMap = keyFigure.labelKey
    }
    
}

@available(iOS 13.0, *)
// MARK: - Local map files management -
extension KeyFiguresMapManager {
    private func writeInitialFileIfNeeded() {
        guard let fileUrl: URL = Bundle.main.url(forResource: "key-figures-map", withExtension: "pb") else { return }
        let destinationFileUrl: URL = createMapWorkingDirectoryIfNeeded().appendingPathComponent(fileUrl.lastPathComponent)
        let currentBuildNumber: String = UIApplication.shared.buildNumber
        let isNewAppVersion: Bool = lastInitialMapKeyFiguresBuildNumber != currentBuildNumber
        if !FileManager.default.fileExists(atPath: destinationFileUrl.path) || isNewAppVersion {
            try? FileManager.default.removeItem(at: destinationFileUrl)
            try? FileManager.default.copyItem(at: fileUrl, to: destinationFileUrl)
            lastInitialMapKeyFiguresBuildNumber = currentBuildNumber
        }
    }
    
    private func loadLocalMapFiles() {
        let localUrl: URL?
        if FileManager.default.fileExists(atPath: localKeyFiguresMapUrl().path) {
            localUrl = localKeyFiguresMapUrl()
        } else {
            localUrl = (try? FileManager.default.contentsOfDirectory(at: createMapWorkingDirectoryIfNeeded(), includingPropertiesForKeys: nil, options: []))?.sorted { $0.modificationDate ?? .distantPast > $1.modificationDate ?? .distantPast }.first
        }
        guard let url = localUrl else { return }
        guard let data = try? Data(contentsOf: url) else { return }
        guard let keyNumbers = try? Keynumbers_KeyNumbersMessage(serializedData: data) else { return }
        self.keyFiguresMap = keyNumbers.toAppModelMapKeyFigure()
        self.updateSelectionIfNecessary()
    }
    
    private func createMapWorkingDirectoryIfNeeded() -> URL {
        let directoryUrl: URL = FileManager.libraryDirectory().appendingPathComponent("KeyFiguresMap")
        if !FileManager.default.fileExists(atPath: directoryUrl.path, isDirectory: nil) {
            try? FileManager.default.createDirectory(at: directoryUrl, withIntermediateDirectories: false, attributes: nil)
        }
        return directoryUrl
    }
    
    private func localKeyFiguresMapUrl() -> URL {
        createMapWorkingDirectoryIfNeeded()
            .appendingPathComponent(remoteMapFileUrl()
            .lastPathComponent
            .replacingOccurrences(of: ".gz", with: ""))
    }
}
