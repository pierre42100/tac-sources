// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  BlacklistV5Manager.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 12/10/2022 - for the TousAntiCovid project.
//

final class BlacklistV5Manager: BlacklistManager {
    static let shared: BlacklistV5Manager = .init(baseUrl: BlacklistV5Constant.baseUrl, indexesFilename: BlacklistV5Constant.indexesFilename)
    
    override func isBlacklisted(certificateHash: String, in hashesList: [String]?) -> Bool {
        hashesList?.contains(certificateHash.v5Truncated) == true
    }
}

private extension String {
    var v5Truncated: String {
        String(self.prefix(BlacklistV5Constant.hashesLength))
    }
}
