// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  BlacklistManager.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 12/10/2022 - for the TousAntiCovid project.
//

import UIKit
import Dispatch
import StorageSDK
import CoreGraphics

class BlacklistManager {
    enum Version {
        case v4
        case v5
    }
    
    static var current: BlacklistManager {
        switch BlacklistConstant.version {
        case .v4:
            return BlacklistV4Manager.shared
        case .v5:
            return BlacklistV5Manager.shared
        }
    }
    
    enum FetchIndexesResult {
        case success(changesOccured: Bool)
        case failure
    }

    private weak var storageManager: StorageManager?
    private let baseUrl: String
    private let indexesFilename: String
    private lazy var indexesListUrl: URL = { URL(string: "\(baseUrl)/\(indexesFilename)")! }()
    
    private var savedBlacklistedCertificatesHashes: [String] {
        storageManager?.blacklistedCertificatesHashes() ?? []
    }
    
    @UserDefault(key: .lastBlacklistCheckTimestamp)
    private var lastBlacklistCheckTimestamp: Double = .zero
    
    init(baseUrl: String, indexesFilename: String) {
        self.baseUrl = baseUrl
        self.indexesFilename = indexesFilename
    }
    
    func start(storageManager: StorageManager) {
        self.storageManager = storageManager
    }
    
    func clearAllData() {
        storageManager?.clearCertificateHashesBlacklist()
    }
    
    func isBlacklisted(certificateHash: String, in hashesList: [String]?) -> Bool {
        hashesList?.contains(certificateHash) == true
    }
    
    func isCertificateBlacklisted(_ certificate: WalletCertificate, queue: DispatchQueue, completion: @escaping (_ isBlacklisted: Bool?) -> ()) {
        fetchIndexesList(queue: queue) { [weak self] result, error in
            guard let self = self else { return }
            switch result {
            case .failure:
                // If error, can't determine if blacklisted
                completion(nil)
            case .success(changesOccured: _):
                // If no index for this certificate -> not blacklisted
                if let index = self.index(for: certificate) {
                    self.fetchHashes(for: index, queue: queue) { hashes, error in
                        if error == nil {
                            let isBlacklisted: Bool = self.isBlacklisted(certificateHash: certificate.uniqueHash, in: hashes)
                            isBlacklisted ? self.storageManager?.add(blacklistedCertificatesHash: certificate.uniqueHash) : self.storageManager?.remove(certificatesHash: certificate.uniqueHash)
                            completion(isBlacklisted)
                        } else {
                            // If error, can't determine if blacklisted
                            completion(nil)
                        }
                    }
                } else {
                    completion(false)
                }
            }
        }
    }
    
    func blacklistedCertificates(in certificates: [WalletCertificate], forceReload: Bool, queue: DispatchQueue, completion: @escaping (_ success: Bool, _ blacklistedCertificates: [WalletCertificate]?) -> ()) {
        let now: Double = Date().timeIntervalSince1970
        
        // if last blacklist check is less than blacklistCheckThresholdInSec ago -> use local blacklist
        guard now - lastBlacklistCheckTimestamp >= BlacklistV4Constant.blacklistFreqInSec || forceReload else {
            completion(true, localBlacklist(on: certificates))
            return
        }
        
        // update indexes if needed
        fetchIndexesList(queue: queue) { [weak self] result, _ in
            guard let self = self else { return }
            
            switch result {
            case .failure:
                // If failed to update indexes -> use local blacklist
                completion(false, self.localBlacklist(on: certificates))
            case .success(changesOccured: let changesOccured):
                if changesOccured || forceReload {
                    // Check all certificates because index list changed
                    self.matchHashes(for: certificates, queue: queue) { blacklistedCertificates, error in
                        if error == nil {
                            // Update local blacklist
                            self.storageManager?.save(blacklistedCertificatesHashes: blacklistedCertificates.compactMap { $0.uniqueHash })
                            self.lastBlacklistCheckTimestamp = now
                        }
                        completion(true, self.localBlacklist(on: certificates))
                    }
                } else {
                    completion(true, self.localBlacklist(on: certificates))
                }
            }
        }
    }
}

// MARK: - Fetching indexes functions
private extension BlacklistManager {
    
    func fetchIndexesList(queue: DispatchQueue, completion: @escaping (_ result: FetchIndexesResult, _ error: Error?) -> ()) {
        let dataTask: URLSessionDataTask = UrlSessionManager.shared.session.dataTaskWithETag(with: URLRequest(url: indexesListUrl)) { [weak self] data, response, error, updateEtag in
            queue.sync {
                self?.handleIndexesResponse(response: response, data: data, error: error) { result, error in
                    if error == nil { updateEtag() }
                    completion(result, error)
                }
            }
        }
        dataTask.resume()
    }
    
    func handleIndexesResponse(response: URLResponse?, data: Data?, error: Error?, completion: @escaping (_ result: FetchIndexesResult, _ error: Error?) -> ()) {
        if let response = response, let statusCode = response.responseStatusCode {
            switch statusCode {
            case 200...299:
                guard let data = data else {
                    completion(.failure, BlacklistV4Error.noIndexesData)
                    return
                }
                parseIndexes(data: data) { success, error in
                    completion(success ? .success(changesOccured: true) : .failure, error)
                }
            case 304:
                // Nothing to do because data haven't changed
                completion(.success(changesOccured: false), nil)
                return
            default:
                completion(.failure, BlacklistV4Error.serverErrorWithCode(response.responseStatusCode))
                return
            }
        } else if let error = error {
            completion(.failure, BlacklistV4Error.serverError(error))
        } else {
            completion(.failure, BlacklistV4Error.noServerResponse)
        }
    }
}

// MARK: - Hashes fetch functions
private extension BlacklistManager {
    func matchHashes(for certificates: [WalletCertificate], queue: DispatchQueue, completion: @escaping (_ certificates: [WalletCertificate], _ error: Error?) -> ()) {
        // Are there some certificates concerned in index list ?
        let indexes: Set<String> = Set(certificates.compactMap { self.index(for: $0) })
        // if no indexes -> no certificates blacklisted
        guard !indexes.isEmpty else {
            completion([], nil)
            return
        }
        // get hashes for selected indexes
        self.fetchHashes(for: Array(indexes), queue: queue) { [weak self] hashes, error in
            guard let self = self else { return }
            // return only certificates that are contained in blacklist
            completion(certificates.filter { self.isBlacklisted(certificateHash: $0.uniqueHash, in: hashes) }, error)
        }
    }
    
    func fetchHashes(for indexes: [String], queue: DispatchQueue, completion: @escaping (_ hashes: [String], _ error: Error?) -> ()) {
        let group: DispatchGroup = .init()
        var loadedHashes: [String] = []
        var hashesError: Error?
        
        indexes.forEach { index in
            group.enter()
            fetchHashes(for: index, queue: queue) { newHashes, error in
                if let error = error { hashesError = error }
                if let newHashes = newHashes { loadedHashes.append(contentsOf: newHashes) }
                group.leave()
            }
        }
        
        group.notify(queue: queue) {
            completion(loadedHashes, hashesError)
        }
    }
    
    func fetchHashes(for index: String, queue: DispatchQueue, completion: @escaping (_ hashes: [String]?, _ error: Error?) -> ()) {
        let dataTask: URLSessionDataTask = URLSessionDataTaskFactory.shared.dataTask(with: URLRequest(url: hashesUrl(for: index)),
                                                                                     session: UrlSessionManager.shared.session) { [weak self] data, response, error in
            queue.sync {
                self?.handleHashesResponse(response: response, data: data, error: error, completion: completion)
            }
        }
        dataTask.resume()
    }
    
    func handleHashesResponse(response: URLResponse?, data: Data?, error: Error?, completion: @escaping (_ hashes: [String]?, _ error: Error?) -> ()) {
        if let response = response, let statusCode = response.responseStatusCode {
            switch statusCode {
            case 200...299:
                guard let data = data else {
                    completion(nil, BlacklistV4Error.noIndexesData)
                    return
                }
                parseHashes(data: data, completion: completion)
            default:
                completion(nil, BlacklistV4Error.serverErrorWithCode(response.responseStatusCode))
                return
            }
        } else if let error = error {
            completion(nil, BlacklistV4Error.serverError(error))
        } else {
            completion(nil, BlacklistV4Error.noServerResponse)
        }
    }
}

// MARK: - Database utils
private extension BlacklistManager {
    func index(for certificate: WalletCertificate) -> String? {
        guard let storedIndexes = storageManager?.certificateBlacklistedIndexes() else { return nil }
        return storedIndexes.first { certificate.uniqueHash.starts(with: $0) }
    }
}

// MARK: - Utils
private extension BlacklistManager {
    func localBlacklist(on certificates: [WalletCertificate]) -> [WalletCertificate] {
        certificates.filter { savedBlacklistedCertificatesHashes.contains($0.uniqueHash) }
    }
    
    func hashesUrl(for index: String) -> URL { URL(string: "\(baseUrl)/\(hashesFilename(for: index))")! }
    
    func hashesFilename(for index: String) -> String { "\(index).\(BlacklistV4Constant.fileType.rawValue)" }
    
    func parseIndexes(data: Data, completion: @escaping (_ success: Bool, _ error: Error?) -> ()) {
        do {
            switch BlacklistV4Constant.fileType {
            case .pbgz:
                let uncompressedData: Data = try data.gunzipped()
                let blacklistMessage: BlackListedHashPrefix_indexMessage = try .init(serializedData: uncompressedData)
                storageManager?.updateCertificateIndexesBlacklist(blacklistMessage.p)
                completion(true, nil)
            case .pb:
                let blacklistMessage: BlackListedHashPrefix_indexMessage = try .init(serializedData: data)
                storageManager?.updateCertificateIndexesBlacklist(blacklistMessage.p)
                completion(true, nil)
            case .json:
                let blacklistIndexes: [String] = try JSONDecoder().decode(BlacklistV4Indexes.self, from: data).indexes
                storageManager?.updateCertificateIndexesBlacklist(blacklistIndexes)
                completion(true, nil)
            }
        } catch {
            completion(false, BlacklistV4Error.parsingError(error))
        }
    }
    
    func parseHashes(data: Data, completion: @escaping (_ hashes: [String]?, _ error: Error?) -> ()) {
        do {
            switch BlacklistV4Constant.fileType {
            case .pbgz:
                let uncompressedData: Data = try data.gunzipped()
                let blacklistMessage: BlackListedHashPrefix_prefixMessage = try .init(serializedData: uncompressedData)
                completion(blacklistMessage.h, nil)
            case .pb:
                let blacklistMessage: BlackListedHashPrefix_prefixMessage = try .init(serializedData: data)
                completion(blacklistMessage.h, nil)
            case .json:
                let blacklistHashes: [String] = try JSONDecoder().decode(BlacklistV4Hashes.self, from: data).hashes
                completion(blacklistHashes, nil)
            }
        } catch {
            completion(nil, BlacklistV4Error.parsingError(error))
        }
    }
}
