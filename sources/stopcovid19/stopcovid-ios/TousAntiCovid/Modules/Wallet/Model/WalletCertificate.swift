// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  WalletCertificate.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 25/03/2021 - for the TousAntiCovid project.
//

import ServerSDK
import StorageSDK
import UIKit

protocol WalletCertificate: AnyObject {

    var id: String { get }
    var value: String { get }
    var type: WalletConstant.CertificateType { get }

    var authority: String? { get }
    var certificateId: String? { get }
    var message: Data? { get }
    var signature: Data? { get }

    var pillTitles: [(text: String, backgroundColor: UIColor)] { get }
    var title: String? { get }
    var shortDescription: String? { get }
    var shortDescriptionForList: String? { get }
    var fullDescription: String? { get }
    var fullDescriptionForFullscreen: String? { get }

    var timestamp: Double { get }

    var is2dDoc: Bool { get }
    var codeImageTitle: String? { get }
    var codeImage: UIImage? { get }
    var uniqueHash: String { get }
    var publicKey: String? { get }
    var additionalInfo: [AdditionalInfo] { get }
    
    var technicalExpirationDate: Date? { get }

    func toRawCertificate() -> RawWalletCertificate
    func validityString(forceEnglish: Bool) -> String
    
}

// Do not declare protocol extension that are not valid for ALL class respecting the protocol
extension WalletCertificate {
    var is2dDoc: Bool { type.format == .wallet2DDoc }

    var codeImageTitle: String? {
        switch type.format {
        case .wallet2DDoc:
            return "2D-DOC"
        case .walletDCC, .walletDCCACT:
            return nil
        }
    }

    var codeImage: UIImage? {
        switch type.format {
        case .wallet2DDoc:
            return value.dataMatrix()
        case .walletDCC, .walletDCCACT:
            return value.qrCode()
        }
    }

    var publicKey: String? {
        guard let authority = authority else { return nil }
        guard let certificateId = certificateId else { return nil }
        return ParametersManager.shared.walletPublicKey(authority: authority, certificateId: certificateId)
    }

    func validityString(forceEnglish: Bool) -> String {
        let walletTestCertificateValidityThresholds: [Int] = ParametersManager.shared.walletTestCertificateValidityThresholds
        let maxValidityInHours: Int = walletTestCertificateValidityThresholds.max() ?? 0
        let timeSinceCreation: Double = Date().timeIntervalSince1970 - timestamp
        let validityThresholdInHours: Int? = walletTestCertificateValidityThresholds.filter { Double($0 * 3600) > timeSinceCreation } .min()
        if forceEnglish {
            return String(format: ( validityThresholdInHours == nil ? "wallet.proof.englishDescription.moreThanSpecificHours" : "wallet.proof.englishDescription.lessThanSpecificHours").localized, validityThresholdInHours ?? maxValidityInHours)
        } else {
            return String(format: ( validityThresholdInHours == nil ? "wallet.proof.moreThanSpecificHours" : "wallet.proof.lessThanSpecificHours").localized, validityThresholdInHours ?? maxValidityInHours)
        }
    }
    
    func getAdditionalInfo() -> [AdditionalInfo] {
        if WalletManager.shared.isCertificateBlacklisted(self) {
            return [AdditionalInfo(category: .warning, fullDescription: "wallet.blacklist.warning".localized)]
        } else {
            return []
        }
    }
}

extension Array where Element == AdditionalInfo {
    var warnings: [Element] { filter { $0.category == .warning } }
    var info: [Element] { filter { $0.category == .info } }
    var errors: [Element] { filter { $0.category == .error } }
}
