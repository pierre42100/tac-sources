// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  WalletRule.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 09/02/2022 - for the TousAntiCovid project.
//

import Foundation

struct WalletRule: Decodable {
    
    private var pivotString: String
    var rulesForAge: [RuleForAge]
    var desc: String?
    
    enum CodingKeys: String, CodingKey {
        case pivotString = "pivot"
        case rulesForAge
        case desc
    }
    
    var pivotDate: Date {
        let dateTimeFormatter = DateFormatter()
        dateTimeFormatter.dateFormat = "yyyy-MM-dd"
        dateTimeFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        dateTimeFormatter.locale = Locale.current
        return dateTimeFormatter.date(from: pivotString) ?? Date.distantPast
    }
    
    func rule(for age: Int) -> RuleForAge? {
        rulesForAge.sorted(by: { $0.age > $1.age }).first { age >= $0.age }
    }
}

// MARK: - RuleForAge
struct RuleForAge: Decodable {
    var age: Int
    var ageMinExpDays: Int?
    var vaccineConditions: [RuleCondition]?
    var recoveryConditions: [RuleCondition]?
    var positiveTestConditions: [RuleCondition]?
    var desc: String?
    
    enum CodingKeys: String, CodingKey {
        case age = "ageMin"
        case ageMinExpDays
        case vaccineConditions = "v"
        case recoveryConditions = "r"
        case positiveTestConditions = "p"
        case desc
    }
    
    var ageMinExpSec: Double { Double((ageMinExpDays ?? 0) * 24 * 3600) }
    
    func conditions(certificateType: WalletConstant.CertificateType,
                    certificateIsTestNegative: Bool?) -> [RuleCondition]? {
        switch certificateType {
        case .vaccinationEurope:
            return vaccineConditions
        case .recoveryEurope:
            return recoveryConditions
        case .sanitaryEurope:
            guard certificateIsTestNegative == false else { return nil }
            return positiveTestConditions
        default:
            return nil
        }
    }
}
