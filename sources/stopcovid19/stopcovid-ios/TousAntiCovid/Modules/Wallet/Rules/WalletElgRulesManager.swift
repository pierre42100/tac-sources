// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  WalletElgRulesManager.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 03/02/2022 - for the TousAntiCovid project.
//

import Foundation
import ServerSDK

final class EligibilityRulesObserverWrapper: NSObject, WalletRulesObserverWrapper {
    weak var observer: WalletRulesObserver?
}

struct EligibilityInfo {
    var eligibilityTimestamp: Double?
    var eligibilityLabelId: String?
}

final class WalletElgRulesManager: WalletRulesSyncManager {
    
    typealias DecodableJSONObject = WalletRule
    typealias ObserverWrapperObject = EligibilityRulesObserverWrapper
    
    static let shared: WalletElgRulesManager = .init()
    private init() {}
    
    var observers: [EligibilityRulesObserverWrapper] = []
    var objects: [WalletRule] = []
    var ruleType: WalletRulesConstant.WalletRulesType = .eligibility
    var workingDirectoryUrl: URL { WalletRulesConstant.workingDirectoryUrl }
        
    @UserDefault(key: .lastInitialElgRulesBuildNumber)
    private var lastInitialElgRulesBuildNumber: String? = nil
    
    @UserDefault(key: .lastElgRulesUpdateDate)
    private var lastElgRulesUpdateDate: Date = .distantPast
    
    func canUpdateData() -> Bool { Date().timeIntervalSince1970 - lastElgRulesUpdateDate.timeIntervalSince1970 >= WalletRulesConstant.minDurationBetweenUpdatesInSeconds }
    
    func saveUpdatedAt() { lastElgRulesUpdateDate = Date() }
    
    func lastBuildNumber() -> String? { lastInitialElgRulesBuildNumber }
    
    func saveLastBuildNumber(_ buildNumber: String) { lastInitialElgRulesBuildNumber = buildNumber }
    
    func start() {
        start(with: ruleType.remoteUrl, bundle: Bundle.main, forceWrite: false)
    }
    
    func eligibility(now: Date,
                     certificateType: WalletConstant.CertificateType,
                     certificateAlignedTimestamp: Double,
                     certificateAlignedDateOfBirth: Date,
                     certificateMedicalProductCode: String?,
                     certificateCurrentDosesNumber: Int?,
                     certificateTargetDosesNumber: Int?,
                     certificatePrefix: String?,
                     certificateIsTestNegative: Bool?,
                     vaccinsConfig: Vaccins?) -> EligibilityInfo? {
        let age: Int = certificateAlignedDateOfBirth.age(at: now)
        let conditions: [RuleCondition] = objects.compactMap { walletRule in
            eligibilityCondition(rule: walletRule,
                                 now: now,
                                 certificateType: certificateType,
                                 certificateAlignedTimestamp: certificateAlignedTimestamp,
                                 certificateAlignedDateOfBirth: certificateAlignedDateOfBirth,
                                 certificateMedicalProductCode: certificateMedicalProductCode,
                                 certificateCurrentDosesNumber: certificateCurrentDosesNumber,
                                 certificateTargetDosesNumber: certificateTargetDosesNumber,
                                 certificatePrefix: certificatePrefix,
                                 certificateIsTestNegative: certificateIsTestNegative,
                                 vaccinsConfig: vaccinsConfig)
        }
        guard let condition = conditions.sorted(by: { ($0.eligibilityInSec ?? 0.0) < ($1.eligibilityInSec ?? 0.0) }).last else { return nil }
        guard let delay = condition.eligibilityInSec else { return nil }
        return EligibilityInfo(eligibilityTimestamp: [certificateAlignedTimestamp + delay, certificateAlignedDateOfBirth.birthdayTimestamp(for: age)].max(), eligibilityLabelId: condition.labelId)
    }
}

// MARK: - Utils
private extension WalletElgRulesManager {
    func eligibilityCondition(rule: WalletRule?,
                              now: Date,
                              certificateType: WalletConstant.CertificateType,
                              certificateAlignedTimestamp: Double,
                              certificateAlignedDateOfBirth: Date,
                              certificateMedicalProductCode: String?,
                              certificateCurrentDosesNumber: Int?,
                              certificateTargetDosesNumber: Int?,
                              certificatePrefix: String?,
                              certificateIsTestNegative: Bool?,
                              vaccinsConfig: Vaccins?) -> RuleCondition? {
        guard let walletRule = rule else { return nil }
        let age: Int = certificateAlignedDateOfBirth.age(at: now)
        let ruleForAge: RuleForAge? = walletRule.rule(for: age)
        let conditions: [RuleCondition]? = ruleForAge?.conditions(certificateType: certificateType,
                                                                  certificateIsTestNegative: certificateIsTestNegative)
        return conditions?.firstVerified(certificateType: certificateType,
                                         certificateMedicalProductCode: certificateMedicalProductCode,
                                         certificateCurrentDosesNumber: certificateCurrentDosesNumber,
                                         certificateTargetDosesNumber: certificateTargetDosesNumber,
                                         certificatePrefix: certificatePrefix,
                                         certificateIsTestNegative: certificateIsTestNegative,
                                         vaccinsConfig: vaccinsConfig)
    }
}
