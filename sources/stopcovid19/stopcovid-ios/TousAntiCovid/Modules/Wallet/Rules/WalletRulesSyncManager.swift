// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  WalletRulesSyncManager.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 03/02/2022 - for the TousAntiCovid project.
//

import UIKit

protocol WalletRulesSyncManager: Observable {
    associatedtype DecodableJSONObject: Decodable
    
    var objects: [DecodableJSONObject] { get set }
    
    var ruleType: WalletRulesConstant.WalletRulesType { get }
    var workingDirectoryUrl: URL { get }
    
    func canUpdateData() -> Bool
    func saveUpdatedAt()
    func lastBuildNumber() -> String?
    func saveLastBuildNumber(_ buildNumber: String)
}
 
extension WalletRulesSyncManager {
    func start(with remoteUrl: URL?, bundle: Bundle, forceWrite: Bool) {
        writeInitialFileIfNeeded(in: bundle, forceWrite: forceWrite)
        loadLocalRulesFile()
        addObserver()
    }
}

// MARK: - Utils
private extension WalletRulesSyncManager {
    
    func addObserver() {
        NotificationCenter.default.addObserver(forName: UIApplication.didBecomeActiveNotification, object: nil, queue: .main) { [weak self] _ in
            self?.appDidBecomeActive()
        }
    }
    
    func appDidBecomeActive() {
        guard canUpdateData() else { return }
        fetchRulesFile()
    }

}

// MARK: - All fetching methods
private extension WalletRulesSyncManager {
    
    func fetchRulesFile(timeout: Double? = nil, completion: ((_ error: Error?) -> ())? = nil) {
        guard let remoteFileUrl = ruleType.remoteUrl else {
            completion?(nil)
            return
        }
        
        let configuration: URLSessionConfiguration = .default
        if let timeout = timeout {
            configuration.timeoutIntervalForRequest = timeout
            configuration.timeoutIntervalForResource = timeout
        }
        let dataTask: URLSessionDataTask = UrlSessionManager.shared.session.dataTask(with: remoteFileUrl) { data, response, error in
            guard let data = data else {
                completion?(error)
                return
            }
            do {
                self.objects = try JSONDecoder().decode([DecodableJSONObject].self, from: data)
                try data.write(to: self.localUrl())
                DispatchQueue.main.async { [weak self] in
                    self?.notifyObservers()
                    completion?(error)
                }
            } catch(let decodeError) {
                DispatchQueue.main.async {
                    completion?(decodeError)
                }
            }
        }
        dataTask.resume()
    }
    
}

// MARK: - Local files management
private extension WalletRulesSyncManager {
    
    func localUrl() -> URL {
        let directoryUrl: URL = self.createWorkingDirectoryIfNeeded()
        return directoryUrl.appendingPathComponent(ruleType.rawValue + ".json")
    }
    
    func loadLocalRulesFile() {
        let localUrl: URL = localUrl()
        guard FileManager.default.fileExists(atPath: localUrl.path) else { return }
        guard let data = try? Data(contentsOf: localUrl) else { return }
        objects = (try? JSONDecoder().decode([DecodableJSONObject].self, from: data)) ?? []
    }
    
    func createWorkingDirectoryIfNeeded() -> URL {
        let directoryUrl: URL = workingDirectoryUrl
        if !FileManager.default.fileExists(atPath: directoryUrl.path, isDirectory: nil) {
            try? FileManager.default.createDirectory(at: directoryUrl, withIntermediateDirectories: false, attributes: nil)
        }
        return directoryUrl
    }
    
    func initialFileUrl(bundle: Bundle) -> URL {
        bundle.url(forResource: ruleType.rawValue, withExtension: "json")!
    }
    
    func writeInitialFileIfNeeded(in bundle: Bundle, forceWrite: Bool) {
        let fileUrl: URL = initialFileUrl(bundle: bundle)
        let destinationFileUrl: URL = createWorkingDirectoryIfNeeded().appendingPathComponent(fileUrl.lastPathComponent)
        let currentBuildNumber: String = UIApplication.shared.buildNumber
        let isNewAppVersion: Bool = lastBuildNumber() != currentBuildNumber
        if forceWrite {
            try? FileManager.default.removeItem(at: destinationFileUrl)
            try? FileManager.default.copyItem(at: fileUrl, to: destinationFileUrl)
            saveLastBuildNumber(currentBuildNumber)
        } else if !FileManager.default.fileExists(atPath: destinationFileUrl.path) || isNewAppVersion {
            try? FileManager.default.removeItem(at: destinationFileUrl)
            try? FileManager.default.copyItem(at: fileUrl, to: destinationFileUrl)
            saveLastBuildNumber(currentBuildNumber)
        }
    }
    
}

// MARK: - Observers management
protocol WalletRulesObserver: AnyObject {
    func walletRulesDidUpdate()
}

protocol WalletRulesObserverWrapper: NSObject {
    var observer: WalletRulesObserver? { get set } // must be weak
    init(observer: WalletRulesObserver)
}

extension WalletRulesObserverWrapper {
    init(observer: WalletRulesObserver) {
        self.init()
        self.observer = observer
    }
}

protocol Observable: AnyObject {
    associatedtype ObserverWrapperObject: WalletRulesObserverWrapper
    var observers: [ObserverWrapperObject] { get set }
}

extension Observable {
    func addObserver(_ observer: WalletRulesObserver) {
        guard observerWrapper(for: observer) == nil else { return }
        observers.append(ObserverWrapperObject(observer: observer))
    }
    
    func removeObserver(_ observer: WalletRulesObserver) {
        guard let wrapper = observerWrapper(for: observer), let index = observers.firstIndex(of: wrapper) else { return }
        observers.remove(at: index)
    }
    
    private func observerWrapper(for observer: WalletRulesObserver) -> ObserverWrapperObject? {
        observers.first { $0.observer === observer }
    }
    
    func notifyObservers() {
        observers.forEach { $0.observer?.walletRulesDidUpdate() }
    }
}
