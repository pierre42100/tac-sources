// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  Boolean+Extension.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 03/06/2022 - for the TousAntiCovid project.
//

extension Optional where Wrapped == Bool {
    mutating func toggle(defaultValue: Bool) {
        if self != nil {
            self?.toggle()
        } else {
            self = defaultValue
        }
    }
}
