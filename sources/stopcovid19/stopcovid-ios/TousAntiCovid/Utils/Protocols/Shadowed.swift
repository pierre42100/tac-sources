// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  Shadowed.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 16/05/2022 - for the TousAntiCovid project.
//

import UIKit

protocol Shadowed where Self: CVCardCell { }
extension Shadowed {
    func addShadow() {
        layer.shadowOpacity = 0.25
        layer.shadowOffset = .init(width: 0, height: 4)
        layer.shadowRadius = 3
        layer.shadowColor = UIColor.black.cgColor
        layer.masksToBounds = false
    }
}

