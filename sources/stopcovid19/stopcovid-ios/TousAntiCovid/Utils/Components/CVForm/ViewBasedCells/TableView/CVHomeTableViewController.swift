// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  CVHomeTableViewController.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 11/03/2022 - for the TousAntiCovid project.
//

import UIKit

class CVHomeTableViewController: CVTableViewController {
    override func registerXibs() {
        let xibNames: Set<String> = Set<String>(sections.map { $0.rows.compactMap { $0.xibName.rawValue } }.reduce([], +))
        let sectionsXibNames: Set<String> = Set<String>([sections.map { $0.header }, sections.map { $0.footer }].flatMap { $0 }.compactMap { $0?.xibName.rawValue })
        xibNames.forEach {
            if $0 == XibName.Row.homeKeyFigureChartTableCell.rawValue {
                tableView.register(UINib(nibName: $0, bundle: nil), forCellReuseIdentifier: $0)
            } else {
                tableView.register(CVTableCell.self, forCellReuseIdentifier: $0)
            }
        }
        sectionsXibNames.forEach { tableView.register(UINib(nibName: $0, bundle: nil), forHeaderFooterViewReuseIdentifier: $0) }
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let row: CVRow = rowObject(at: indexPath)
        if let cell = (cell as? CVTableCell)?.contentCell {
            row.willDisplay?(cell)
        } else if let cell = cell as? CVTableViewCell { // Special case for lottie animation cell
            row.willDisplay?(cell)
        }
    }
}
