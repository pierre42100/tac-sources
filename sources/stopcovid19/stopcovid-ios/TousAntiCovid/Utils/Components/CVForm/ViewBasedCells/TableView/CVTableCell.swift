// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  CVTableCell.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 17/03/2022 - for the TousAntiCovid project.
//

import UIKit

class CVTableCell: CVTableViewCell {
    var contentCell: CVCell? { contentView.subviews.first as? CVCell }
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        contentCell?.setHighlighted(highlighted)
    }
    
    override func setup(with row: CVRow) {
        if let view = Bundle.main.loadNibNamed(row.xibName.rawValue, owner: nil, options: nil)?.first as? CVCell {
            contentView.subviews.first?.removeFromSuperview()
            contentView.addConstrainedSubview(view)
            contentView.backgroundColor = .clear
            backgroundColor = .clear
            view.setup(with: row)
            selectionStyle = (row.selectionAction == nil || contentCell is CVCardCell) ? .none : .default
            setupSeparator(with: row.theme)
        }
        isUserInteractionEnabled = row.enabled
    }
}

private extension CVTableCell {
    func setupSeparator(with theme: CVRow.Theme) {
        let leftInset: CGFloat? = theme.separatorLeftInset
        let rightInset: CGFloat? = theme.separatorRightInset
        if leftInset == nil && rightInset == nil {
            hideSeparator()
        } else {
            separatorInset = UIEdgeInsets(top: 0.0, left: leftInset ?? 0.0, bottom: 0.0, right: rightInset ?? 0.0)
        }
    }
}
