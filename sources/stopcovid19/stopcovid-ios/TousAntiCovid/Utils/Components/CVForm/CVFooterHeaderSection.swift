// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  CVFooterHeaderSection.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 05/10/2021 - for the TousAntiCovid project.
//

import UIKit

struct CVFooterHeaderSection {

    static var groupedHeader: CVFooterHeaderSection {
        var header: CVFooterHeaderSection = CVFooterHeaderSection(isHeader: true)
        header.theme.topInset = Appearance.Header.topMargin
        header.theme.bottomInset = .zero
        return header
    }
    
    static func footer(title: String?) -> CVFooterHeaderSection {
        var footer: CVFooterHeaderSection = CVFooterHeaderSection(isHeader: false, title: title)
        footer.theme.topInset = Appearance.Footer.topMargin
        footer.theme.bottomInset = .zero
        footer.theme.rightInset = Appearance.Cell.Inset.medium
        footer.theme.leftInset = Appearance.Cell.Inset.medium
        footer.theme.titleFont = { Appearance.Cell.Text.footerFont }
        footer.theme.titleColor = Appearance.Cell.Text.accessoryColor
        return footer
    }
    
    static func collectionHeader(title: String?, subtitle: String?, font: UIFont = Appearance.Cell.Text.headTitleFont, fontColor: UIColor = Appearance.Cell.Text.titleColor) -> CVFooterHeaderSection {
        var header: CVFooterHeaderSection = CVFooterHeaderSection(isHeader: true, title: title, subtitle: subtitle, xibName: .standardCollectionSectionHeader)
        header.theme.titleFont = { font }
        header.theme.titleColor = fontColor
        return header
    }
    
    static func header(title: String?, font: UIFont = Appearance.Cell.Text.footerFont, fontColor: UIColor = Appearance.Cell.Text.titleColor) -> CVFooterHeaderSection {
        var header: CVFooterHeaderSection = CVFooterHeaderSection(isHeader: true, title: title)
        header.theme.topInset = Appearance.Header.topMargin
        header.theme.bottomInset = Appearance.Header.bottomMargin
        header.theme.rightInset = Appearance.Cell.Inset.medium
        header.theme.leftInset = Appearance.Cell.Inset.medium
        header.theme.titleFont = { font }
        header.theme.titleColor = fontColor
        return header
    }

    struct Theme: Hashable {
        var backgroundColor: UIColor = .clear
        var topInset: CGFloat = Appearance.Header.topMargin
        var bottomInset: CGFloat = Appearance.Header.bottomMargin
        var leftInset: CGFloat = Appearance.Header.leftMargin
        var rightInset: CGFloat = Appearance.Header.rightMargin
        var textAlignment: NSTextAlignment = .natural
        var titleFont: (() -> UIFont) = { Appearance.Cell.Text.headTitleFont }
        var titleColor: UIColor = Appearance.Cell.Text.titleColor
        var subtitleFont: (() -> UIFont) = { Appearance.Cell.Text.subtitleFont }
        var subtitleColor: UIColor = Appearance.Cell.Text.subtitleColor
        var axis: NSLayoutConstraint.Axis = .horizontal
        
        func hash(into hasher: inout Hasher) {
            hasher.combine(backgroundColor)
            hasher.combine(topInset)
            hasher.combine(bottomInset)
            hasher.combine(leftInset)
            hasher.combine(rightInset)
            hasher.combine(textAlignment)
            hasher.combine(titleColor)
            hasher.combine(subtitleColor)
            hasher.combine(axis)
        }
        
        static func == (lhs: Theme, rhs: Theme) -> Bool { lhs.hashValue == rhs.hashValue }
    }

    var isHeader: Bool
    var title: String?
    var subtitle: String?
    var xibName: XibName.Section = .standardSectionHeader
    var theme: Theme = Theme()
    var selectionAction: (() -> ())?
    var willDisplay: ((_ view: UIView) -> ())?

}

extension CVFooterHeaderSection: Hashable {
    func hash(into hasher: inout Hasher) {
        hasher.combine(title)
        hasher.combine(isHeader)
        hasher.combine(subtitle)
        hasher.combine(xibName)
        hasher.combine(theme)
    }
    
    static func == (lhs: CVFooterHeaderSection, rhs: CVFooterHeaderSection) -> Bool { lhs.hashValue == rhs.hashValue }
}
