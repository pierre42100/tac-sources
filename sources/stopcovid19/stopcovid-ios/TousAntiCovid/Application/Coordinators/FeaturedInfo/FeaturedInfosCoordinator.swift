// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  FeaturedInfosCoordinator.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 09/01/2023 - for the TousAntiCovid project.
//

import UIKit

final class FeaturedInfoCoordinator: Coordinator {

    weak var parent: Coordinator?
    var childCoordinators: [Coordinator] = []

    private weak var navigationController: UINavigationController?
    private weak var presentingController: UIViewController?

    init(presentingController: UIViewController?, parent: Coordinator) {
        self.presentingController = presentingController
        self.parent = parent
        self.start()
    }

    private func start() {
        let controller: UIViewController = FeaturedInfosViewController { [weak self] featuredInfo, localUrl in
            self?.showFeaturedInfo(featuredInfo: featuredInfo, localUrl: localUrl)
        } deinitBlock: { [weak self] in
            self?.didDeinit()
        }
        let navigationController: UINavigationController = CVNavigationController(rootViewController: controller)

        self.navigationController = navigationController
        presentingController?.present(navigationController, animated: true)
    }

    private func showFeaturedInfo(featuredInfo: FeaturedInfo, localUrl: URL?) {
        let controller: UIViewController = FileViewerViewController(featuredInfo: featuredInfo, localUrl: localUrl)
        let navigationController: UINavigationController = CVNavigationController(rootViewController: controller)
        if #available(iOS 13.0, *) {
            navigationController.overrideUserInterfaceStyle = .light
        }
        navigationController.modalPresentationStyle = .fullScreen
        self.navigationController?.present(navigationController, animated: true)
    }

}
