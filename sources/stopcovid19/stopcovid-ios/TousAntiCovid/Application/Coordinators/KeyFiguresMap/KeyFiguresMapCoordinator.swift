// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  KeyFiguresMapCoordinator.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 17/01/2023 - for the TousAntiCovid project.
//

import UIKit

@available(iOS 13.0, *)
final class KeyFiguresMapCoordinator: Coordinator {

    weak var parent: Coordinator?
    var childCoordinators: [Coordinator] = []

    private weak var navigationController: UINavigationController?
    private weak var presentingController: UIViewController?

    init(presentingController: UIViewController?, parent: Coordinator) {
        self.presentingController = presentingController
        self.parent = parent
        start()
    }
}

// MARK: - Utils
@available(iOS 13.0, *)
private extension KeyFiguresMapCoordinator {
    func start() {
        let mapController: KeyFiguresMapController = .init(didTouchKeyFigureSelection: { [weak self] currentSelection, selectionDidChange in
            self?.showListScreen(mode: .map, with: [currentSelection], didSelect: selectionDidChange)
        }, didTouchKeyFigureDescription: { [weak self] title, description in
            self?.showBottomSheet(title: title, description: description)
        }, didTouchMoreInfo: { [weak self] in
            self?.showBottomSheet(title: "", description: "keyfigures.map.screen.displayMethod.description".localized)
        }, didTouchClose: { [weak self] in
            self?.presentingController?.dismiss(animated: true)
        }, deinitBlock: { [weak self] in
            self?.didDeinit()
        })

        let navigationController: CVNavigationController = .init(rootViewController: mapController)
        self.navigationController = navigationController
        presentingController?.present(navigationController, animated: true)
    }
}

// MARK: - List Screen related functions
@available(iOS 13.0, *)
private extension KeyFiguresMapCoordinator {
    func showListScreen(mode: KeyFiguresChoiceController.SelectionMode, with selection: [KeyFigure], didSelect: @escaping (_ keyfigure: KeyFigure) -> ()) {
        let listController: KeyFiguresChoiceController = .init(
            mode: mode,
            keyFigures: KeyFiguresMapManager.shared.keyFiguresForMapSelection,
            selected: selection) { [weak self] keyFigure in
                didSelect(keyFigure)
                self?.closeListScreen()
            } deinitBlock: {}
        navigationController?.pushViewController(listController, animated: true)
    }

    func closeListScreen() {
        navigationController?.popViewController(animated: true)
    }
}

// MARK: - BottomSheet related functions
@available(iOS 13.0, *)
private extension KeyFiguresMapCoordinator {
    func showBottomSheet(title: String, description: String) {
        let bottomSheet: BottomSheetAlertController = .init(title: title,
                                                            message: description,
                                                            okTitle: "common.ok".localized,
                                                            okButtonStyle: .secondary)
        bottomSheet.show()
    }
}
