// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  Info+Extension.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 03/02/2023 - for the TousAntiCovid project.
//

import Foundation
import AppModel

extension Info {
    var formattedDate: String {
        if Calendar.current.isDateInToday(date) {
            return String(format: "\("common.today".localized), %@", date.timeFormatted())
        } else if Calendar.current.isDateInYesterday(date) {
            return String(format: "\("common.yesterday".localized), %@", date.timeFormatted())
        } else {
            return String(format: "%@, %@", date.dayMonthFormatted(), date.timeFormatted())
        }
    }
}
