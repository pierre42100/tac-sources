// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  HomeCollectionViewController.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 02/02/2022 - for the TousAntiCovid project.
//

import UIKit

@available(iOS 13.0, *)
final class HomeCollectionViewController: CVCollectionViewController, HomeBased {

    var viewModel: HomeBasedViewModel
    var outputs: HomeOutputs
    var popRecognizer: InteractivePopGestureRecognizer?
    var wasActivated: Bool = false
    var isChangingState: Bool = false
    var didShowRobertErrorAlertOnce: Bool = false
    var areNotificationsAuthorized: Bool?
    var isWaitingForNeededInfo: Bool = true

    @UserDefault(key: .latestAvailableBuild)
    var latestAvailableBuild: Int?
        
    @UserDefault(key: .didAlreadyShowUserLanguage)
    var didAlreadyShowUserLanguage: Bool = false
    var reloadingTimer: Timer?

    private let margin: CGFloat = Appearance.Cell.Inset.normal
    private var shouldNextReloadBeAnimated: Bool = false
    
    init(viewModel: HomeBasedViewModel, outputs: HomeOutputs) {
        self.viewModel = viewModel
        self.outputs = outputs
        super.init(collectionViewLayout: UICollectionViewLayout())
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initUI(scrollView: collectionView)
        initCollectionView()
        onViewDidLoad()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        onViewDidAppear()
    }
    
    deinit {
        removeObservers()
        outputs.deinitBlock()
    }
    
    override func createSections() -> [CVCollectionSection] {
        buildModel().compactMap { $0.mapToCollectionSection(layoutMargin: margin) }
    }
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        navigationChildController?.scrollViewDidScroll(scrollView)
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let row: CVRow = rowObject(at: indexPath)
        switch row.xibName {
        case .homeKeyFigureChartCollectionCell:
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: row.xibName.rawValue, for: indexPath) as? HomeKeyFigureChartCollectionCell else { return UICollectionViewCell() }
            cell.setup(with: row)
            return cell
        default:
            return super.collectionView(collectionView, cellForItemAt: indexPath)
        }
    }
}

// MARK: - Reloading timer -
@available(iOS 13.0, *)
extension HomeCollectionViewController {
    func createReloadingTimer(shouldNextReloadBeAnimated: Bool) -> Timer {
        .init(timeInterval: HomeConstant.reloadingTimeInterval, repeats: false) { [weak self] _ in
            self?.reloadUI(animated: shouldNextReloadBeAnimated)
        }
    }
}

// MARK: - Utils
@available(iOS 13.0, *)
private extension HomeCollectionViewController {
    func initCollectionView() {
        let compositionalLayout: UICollectionViewCompositionalLayout = .init { [weak self] sectionIndex, _ -> NSCollectionLayoutSection? in
            guard let section = self?.sectionObject(at: sectionIndex) else { return nil }
            return section.layout
        }
        let config: UICollectionViewCompositionalLayoutConfiguration = .init()
        config.interSectionSpacing = .zero
        compositionalLayout.configuration = config
        collectionView.collectionViewLayout = compositionalLayout
        collectionView.backgroundColor = Appearance.Controller.cardTableViewBackgroundColor
        collectionView.bounces = true
        collectionView.backgroundView = UIView()
    }
}

// MARK: - Layouts
@available(iOS 13.0, *)
private extension CVGroup.HorizontalLayoutMode {
    var scrollingBehavior: UICollectionLayoutSectionOrthogonalScrollingBehavior {
        switch self {
        case .adaptive:
            return .continuous
        case .fixed(_):
            return .continuous
        }
    }
}

@available(iOS 13.0, *)
extension NSCollectionLayoutSection {
    static func fullWidthLayout(insets: NSDirectionalEdgeInsets = .zero) -> NSCollectionLayoutSection {
        let size: NSCollectionLayoutSize = .init(widthDimension: .fractionalWidth(1.0), heightDimension: .estimated(44.0))
        let item: NSCollectionLayoutItem = .init(layoutSize: size)
        let itemGroup: NSCollectionLayoutGroup = .horizontal(layoutSize: size, subitems: [item])
        let section: NSCollectionLayoutSection = .init(group: itemGroup)
        section.interGroupSpacing = .zero
        section.contentInsets = insets
        return section
    }
    
    static func horizontalScrollableLayout(margin: CGFloat, mode: CVGroup.HorizontalLayoutMode) -> NSCollectionLayoutSection {
        let itemSize: NSCollectionLayoutSize
        let groupSize: NSCollectionLayoutSize
        switch mode {
        case .adaptive:
            itemSize = .init(
                widthDimension: .estimated(120.0),
                heightDimension: .estimated(44.0))
            groupSize = itemSize
        case .fixed(let fraction):
            itemSize = .init(
                widthDimension: .fractionalWidth(1.0),
                heightDimension: .estimated(44.0))
            groupSize = .init(
                widthDimension: .fractionalWidth(fraction),
                heightDimension: .estimated(44.0))
        }
        let item = NSCollectionLayoutItem(layoutSize: itemSize)
        let group = NSCollectionLayoutGroup.horizontal(
            layoutSize: groupSize,
            subitems: [item])

        let section: NSCollectionLayoutSection = .init(group: group)
        section.contentInsets = .init(top: .zero, leading: margin, bottom: .zero, trailing: margin)
        section.interGroupSpacing = margin
        section.orthogonalScrollingBehavior = mode.scrollingBehavior

        return section
    }
}
