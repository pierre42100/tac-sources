// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  HeaderCell.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 10/03/2022 - for the TousAntiCovid project.
//

import Foundation

final class HeaderCell: CVCell {
    override func setupAccessibility() {
        super.setupAccessibility()
        cvTitleLabel?.accessibilityTraits = [.staticText, .header]
    }
}
