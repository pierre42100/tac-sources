// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  HomeKeyFigureChartCell.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 16/03/2022 - for the TousAntiCovid project.
//

import UIKit
import Charts

struct ComparisonChartAssociatedValue {
    let datas: [KeyFigureChartData]
    let chartViewBase: ChartViewBase?
}

extension ComparisonChartAssociatedValue: Hashable {
    static func == (lhs: ComparisonChartAssociatedValue, rhs: ComparisonChartAssociatedValue) -> Bool {
        lhs.datas == rhs.datas
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(datas)
    }
}

final class HomeKeyFigureChartTableCell: CVTableViewCell, HomeKeyFigureChartCellable {
    
    @IBOutlet var containerView: UIView!
    @IBOutlet var chartContainerView: UIView!
    @IBOutlet var legendStackView: UIStackView!
    @IBOutlet var footerLabel: UILabel!
    @IBOutlet var sharingImageView: UIImageView!
    @IBOutlet weak var sharingButton: UIButton!
    
    override func setup(with row: CVRow) {
        super.setup(with: row)
        accessoryType = .none
        setupUI(with: row)
        setupContent(with: row)
        setupAccessibility()
    }
    
    @IBAction func didTouchSharingButton(_ sender: Any) {
        currentAssociatedRow?.selectionActionWithCell?(self)
    }
}

final class HomeKeyFigureChartCollectionCell: UICollectionViewCell, HomeKeyFigureChartCellable {
    
    @IBOutlet var containerView: UIView!
    @IBOutlet var chartContainerView: UIView!
    @IBOutlet var legendStackView: UIStackView!
    @IBOutlet var footerLabel: UILabel!
    @IBOutlet var sharingImageView: UIImageView!
    @IBOutlet weak var sharingButton: UIButton!
    @IBOutlet private var leadingConstraint: NSLayoutConstraint?
    @IBOutlet private var trailingConstraint: NSLayoutConstraint?
    @IBOutlet private var topConstraint: NSLayoutConstraint?
    @IBOutlet private var bottomConstraint: NSLayoutConstraint?
    
    private var currentAssociatedRow: CVRow?
    
    func setup(with row: CVRow) {
        leadingConstraint?.constant = row.theme.leftInset ?? Appearance.Cell.leftMargin
        trailingConstraint?.constant = row.theme.rightInset ?? Appearance.Cell.rightMargin
        if let topInset = row.theme.topInset {
            topConstraint?.constant = topInset
        }
        if let bottomInset = row.theme.bottomInset {
            bottomConstraint?.constant = bottomInset
        }
        setupUI(with: row)
        setupContent(with: row)
        setupAccessibility()
        currentAssociatedRow = row
    }
    
    @IBAction func didTouchSharingButton(_ sender: Any) {
        currentAssociatedRow?.selectionActionWithCell?(self)
    }
}

protocol HomeKeyFigureChartCellable: UIView {
    var containerView: UIView! { get set }
    var chartContainerView: UIView! { get set }
    var legendStackView: UIStackView! { get set }
    var footerLabel: UILabel! { get set }
    var sharingImageView: UIImageView! { get set }
    var sharingButton: UIButton! { get set }
    
    func capture() -> UIImage?
    func captureWithoutFooter() -> UIImage?
}

extension HomeKeyFigureChartCellable {

    func setupUI(with row: CVRow) {
        containerView.layer.cornerRadius = 10.0
        containerView.layer.masksToBounds = true
        containerView.backgroundColor = row.theme.backgroundColor
        containerView.layer.maskedCorners = row.theme.maskedCorners
        backgroundColor = .clear
        footerLabel.font = Appearance.Cell.Text.captionTitleFont
        footerLabel.textColor = Appearance.Cell.Text.accessoryColor
        sharingImageView.image = Asset.Images.shareIcon.image
        sharingImageView.tintColor = Appearance.tintColor
        legendStackView?.isUserInteractionEnabled = false
    }

    func setupContent(with row: CVRow) {
        legendStackView.arrangedSubviews.forEach { $0.removeFromSuperview() }
        guard let chartDatas = row.associatedValue as? ComparisonChartAssociatedValue else { return }
        let legendViews: [UIView] = chartDatas.datas.map { LegendView.view(legend: $0.legend) }
        legendViews.forEach { legendStackView.addArrangedSubview($0) }
        footerLabel.text = chartDatas.datas.first?.footer
        guard let view = chartDatas.chartViewBase else { return }
        setupChartView(view)
    }
    
    func capture() -> UIImage? {
        sharingImageView.isHidden = true
        let image: UIImage = containerView.cvScreenshot()!
        sharingImageView.isHidden = false
        let captureImageView: UIImageView = UIImageView(image: image)
        captureImageView.frame.size = CGSize(width: image.size.width / UIScreen.main.scale, height: image.size.height / UIScreen.main.scale)
        captureImageView.backgroundColor = Appearance.Cell.cardBackgroundColor
        return captureImageView.cvScreenshot()
    }

    func captureWithoutFooter() -> UIImage? {
        footerLabel.isHidden = true
        let capture: UIImage? = capture()
        footerLabel.isHidden = false
        return capture
    }

    func setupAccessibility() {
        sharingButton?.isAccessibilityElement = true
        sharingButton?.accessibilityLabel = "accessibility.hint.keyFigureChart.share".localized
        containerView?.accessibilityLabel = "accessibility.hint.keyFigureChart.label".localized
        chartContainerView?.isAccessibilityElement = false
        legendStackView.isAccessibilityElement = false
        accessibilityElements = [footerLabel, sharingButton].compactMap { $0 }
    }
    
    private func setupChartView(_ chartView: ChartViewBase) {
        chartContainerView.subviews.forEach { $0.removeFromSuperview() }
        chartContainerView.addSubview(chartView)
        chartView.translatesAutoresizingMaskIntoConstraints = false
        chartView.leadingAnchor.constraint(equalTo: chartContainerView.leadingAnchor, constant: 0.0).isActive = true
        chartView.trailingAnchor.constraint(equalTo: chartContainerView.trailingAnchor, constant: 0.0).isActive = true
        chartView.topAnchor.constraint(equalTo: chartContainerView.topAnchor, constant: 0.0).isActive = true
        chartView.bottomAnchor.constraint(equalTo: chartContainerView.bottomAnchor, constant: 0.0).isActive = true
    }
}
