// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  HomeNotificationCell.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 08/10/2021 - for the TousAntiCovid project.
//

import UIKit

final class HomeNotificationCell: CVCardCell {
    @IBOutlet private weak var closeButton: UIButton!
    @IBOutlet private weak var closeImageView: UIImageView!

    override func setup(with row: CVRow) {
        super.setup(with: row)
        setupButton()
        setupShadow()
    }
    
    @IBAction private func closeButtonPressed(_ sender: Any) {
        currentAssociatedRow?.secondarySelectionAction?()
    }
}

private extension HomeNotificationCell {
    func setupButton() {
        closeImageView.tintColor = Appearance.tintColor
    }
    
    func setupShadow() {
        layer.shadowColor = Appearance.defaultShadowColor.cgColor
        if #available(iOS 13.0, *) {
            guard traitCollection.userInterfaceStyle == .light else { return }
            layer.shadowOpacity = 0.1
        } else {
            layer.shadowOpacity = 0.2
        }
        layer.shadowOffset = .zero
        layer.shadowRadius = 12.0
    }
}
