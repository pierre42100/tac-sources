// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  FullscreenCertificateViewController.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 29/10/2020 - for the TousAntiCovid project.
//

import UIKit
import PKHUD
import ServerSDK

final class FullscreenCertificateViewController: CVTableViewController {
    
    private enum Mode {
        case standard
        case border
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        if #available(iOS 13.0, *) {
            return .darkContent
        } else {
            return .default
        }
    }
    
    var deinitBlock: (() -> ())?
    var lastBrightness: CGFloat?
    
    @UserDefault(key: .autoBrightnessActivated)
    var autoBrightnessActivated: Bool = true {
        didSet {
            autoBrightnessActivated ? updateBrightnessForQRCodeReadability() : putBrightnessBackToOriginalValue()
        }
    }
    
    private var certificate: WalletCertificate
    private let didTouchShareCertificate: (_ didConfirmSharing: @escaping () -> ()) -> ()
    private let didTouchShowOptions: (_ brightnessParamDidChange: @escaping (_ activated: Bool) -> (), _ shareConfirmationHandler: @escaping () -> ()) -> ()
    private let dismissBlock: () -> ()
    private var isFirstLoad: Bool = true
    private var mode: Mode = .standard

    init(certificate: WalletCertificate,
         didTouchShareCertificate: @escaping (_ didConfirmSharing: @escaping () -> ()) -> (),
         didTouchShowOptions: @escaping (_ brightnessParamDidChange: @escaping (_ activated: Bool) -> (), _ shareConfirmationHandler: @escaping () -> ()) -> (),
         dismissBlock: @escaping () -> (),
         deinitBlock: @escaping () -> ()) {
        self.certificate = certificate
        self.didTouchShareCertificate = didTouchShareCertificate
        self.didTouchShowOptions = didTouchShowOptions
        self.dismissBlock = dismissBlock
        self.deinitBlock = deinitBlock
        super.init(style: .plain)
    }
    
    required init?(coder: NSCoder) {
        fatalError("Must use default init() method.")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        UIAccessibility.post(notification: .screenChanged, argument: certificate.type.format == .wallet2DDoc ? "common.2ddoc".localized : "common.qrCode".localized)
        initUI()
        initMode()
        reloadUI()
        addObservers()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if isFirstLoad { updateBrightnessForQRCodeReadability() }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if isFirstLoad {
            isFirstLoad = false
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        putBrightnessBackToOriginalValue()
    }
    
    deinit {
        removeObservers()
        deinitBlock?()
    }
    
    func updateCertificate(_ certificate: WalletCertificate) {
        self.certificate = certificate
        initMode()
        reloadUI()
    }
    
    private func initUI() {
        tableView.backgroundColor = .white
        tableView.showsVerticalScrollIndicator = false
        tableView.separatorStyle = .singleLine
        let barButtonItem: UIBarButtonItem = UIBarButtonItem(title: "common.close".localized, style: .plain, target: self, action: #selector(didTouchCloseButton))
        barButtonItem.accessibilityHint = "accessibility.closeModal.zGesture".localized
        self.navigationItem.leftBarButtonItem = barButtonItem
        updateRightBarButtonItems()
        if #available(iOS 13.0, *) { navigationController?.overrideUserInterfaceStyle = .light }
    }
    
    private func updateRightBarButtonItems() {
        let shareItem: UIBarButtonItem = .init(image: Asset.Images.shareItem.image, style: .done, target: self, action: #selector(didTouchShareCertificateButton))
        shareItem.accessibilityLabel = "walletController.menu.share".localized
        let optionsItem: UIBarButtonItem = .init(image: Asset.Images.moreColored.image, style: .plain, target: self, action: #selector(showOptions))
        optionsItem.accessibilityLabel = "accessibility.hint.otherActions".localized
        let items: [UIBarButtonItem]? = [optionsItem, shareItem]
        self.navigationItem.setRightBarButtonItems(items, animated: false)
    }
    
    private func initMode() {
        if certificate.type == .multiPass {
            mode = .border
        } else {
            mode = .standard
        }
    }
    
    private func addObservers() {
        WalletManager.shared.addObserver(self)
        NotificationCenter.default.addObserver(self, selector: #selector(appDidBecomeActive), name: UIApplication.didBecomeActiveNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(appWillResignActive), name: UIApplication.willResignActiveNotification, object: nil)
    }
    
    private func removeObservers() {
        WalletManager.shared.removeObserver(self)
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc private func didTouchCloseButton() {
        dismissBlock()
    }

    override func reloadUI(animated: Bool = false, animatedView: UIView? = nil, completion: (() -> ())? = nil) {
        super.reloadUI(animated: animated, animatedView: navigationController?.view, completion: completion)
    }
    
    override func createSections() -> [CVSection] {
        makeSections {
            switch mode {
            case .standard:
                standardSection()
            case .border:
                borderSection()
            }
        }
    }
    
    private func standardSection() -> CVSection {
        CVSection {
            if !certificate.is2dDoc && certificate.type != .multiPass {
                modeSelectionRow()
                logosRow()
            }
            codeImageRow(certificate)
            if certificate.is2dDoc { codeLegendRow() }
            codeShortDescriptionRow(certificate)
            certificate.is2dDoc ? codeHashRow() : explanationsRow()
        }
    }
    
    private func borderSection() -> CVSection {
        CVSection {
            if certificate.type != .multiPass { modeSelectionRow() }
            if (certificate as? EuropeanCertificate)?.type == .exemptionEurope { exemptionWarningRow() }
            if let euCertificate = certificate as? EuropeanCertificate, !WalletManager.shared.isCertificateEuValidated(euCertificate) { notEUTagWarningRow() }
            if (certificate as? EuropeanCertificate)?.isForeignCertificate == false { logosRow() }
            codeImageRow(certificate)
            codeFullDescriptionRow()
            if certificate.type != .multiPass { codeHashRow() }
            if let expirationDate = certificate.technicalExpirationDate { technicalExpirationRow(expirationDate: expirationDate) }
        }
    }
    
    override func accessibilityPerformEscape() -> Bool {
        dismissBlock()
        return true
    }

    @objc private func didTouchShareCertificateButton() {
        didTouchShareCertificate { [weak self] in
            self?.shareCertificate()
        }
    }
    
    @objc private func showOptions() {
        didTouchShowOptions({ [weak self] activated in
            self?.autoBrightnessActivated = activated
        }, { [weak self] in
            self?.shareCertificate()
        })
    }
    
    private func shareCertificate() {
        guard let screenshot = navigationController?.view.cvScreenshot() else { return }
        ScreenshotAnimator.shared.generateScreenshotAnimation(for: screenshot) { [weak self] in
            guard let self = self else { return }
            screenshot.share(from: self)
        }
    }
    
    private func selectBorderMode() {
        mode = .border
        reloadUI(animated: true)
    }
    
    private func selectStandardMode() {
        mode = .standard
        reloadUI(animated: true)
    }

}

extension FullscreenCertificateViewController {
    private func modeSelectionRow() -> CVRow {
        let standardModeSelectionAction: () -> () = { [weak self] in
            self?.selectStandardMode()
        }
        let borderModeSelectionAction: () -> () = { [weak self] in
            self?.selectBorderMode()
        }
        let borderSegmentTitle: String
        let minimumSegmentTitle: String
        if #available(iOS 13.0, *) {
            minimumSegmentTitle = "europeanCertificate.fullscreen.type13.minimum".localized
            borderSegmentTitle = "europeanCertificate.fullscreen.type13.border".localized
        } else {
            minimumSegmentTitle = "europeanCertificate.fullscreen.type.minimum".localized
            borderSegmentTitle = "europeanCertificate.fullscreen.type.border".localized
        }
        let modeSelectionRow: CVRow = CVRow(segmentsTitles: [minimumSegmentTitle, borderSegmentTitle],
                                            selectedSegmentIndex: mode == .border ? 1 : 0,
                                            xibName: .segmentedCell,
                                            theme:  CVRow.Theme(backgroundColor: .clear,
                                                                topInset: Appearance.Cell.Inset.normal,
                                                                bottomInset: 1.0,
                                                                textAlignment: .natural,
                                                                titleFont: { Appearance.SegmentedControl.selectedFont },
                                                                subtitleFont: { Appearance.SegmentedControl.normalFont }),
                                            segmentsActions: [standardModeSelectionAction, borderModeSelectionAction])
        return modeSelectionRow
    }
    
    private func logosRow() -> CVRow {
        let margin: CGFloat = .zero
        return CVRow(image: Asset.Images.logosPasseport.image,
                     xibName: .imageCell,
                     theme: CVRow.Theme(topInset: Appearance.Cell.Inset.medium,
                                        bottomInset: .zero,
                                        leftInset: margin,
                                        rightInset: margin,
                                        imageRatio: 375.0 / 79.0))
    }
    
    private func codeImageRow(_ certificate: WalletCertificate) -> CVRow {
        let margin: CGFloat = 80.0
        return CVRow(image: Asset.Images.screenshotLogo.image,
                     xibName: .protectedQrCodeCell,
                     theme: CVRow.Theme(topInset: Appearance.Cell.Inset.medium,
                                        bottomInset: .zero,
                                        leftInset: margin,
                                        rightInset: margin,
                                        imageRatio: 1.0),
                     associatedValue: certificate.codeImage)
    }
    
    private func codeLegendRow() -> CVRow {
        CVRow(title: certificate.codeImageTitle,
              xibName: .textCell,
              theme: CVRow.Theme(topInset: .zero,
                                 bottomInset: .zero,
                                 titleFont: { Appearance.Cell.Text.headTitleFont4 }))
    }
    
    private func codeShortDescriptionRow(_ certificate: WalletCertificate) -> CVRow {
        CVRow(title: certificate.shortDescription,
              xibName: .textCell,
              theme: CVRow.Theme(topInset: Appearance.Cell.Inset.small,
                                 bottomInset: .zero,
                                 titleFont: { .regular(size: Appearance.Cell.Inset.medium) }))
    }
    
    private func codeFullDescriptionRow() -> CVRow {
        CVRow(title: certificate.fullDescriptionForFullscreen,
              xibName: .textCell,
              theme: CVRow.Theme(topInset: Appearance.Cell.Inset.small,
                                 bottomInset: .zero,
                                 titleFont: { .regular(size: 20.0) }))
    }
    
    private func explanationsRow() -> CVRow {
        CVRow(title: "europeanCertificate.fullscreen.type.minimum.footer".localized,
              xibName: .textCell,
              theme: CVRow.Theme(topInset: Appearance.Cell.Inset.large,
                                 bottomInset: .zero,
                                 textAlignment: .natural,
                                 titleFont: { .regular(size: 17.0) }))
    }
    
    private func exemptionWarningRow() -> CVRow {
        CVRow(title: "europeanCertificate.fullscreen.exemption.border.warning".localized,
              xibName: .textCell,
              theme: CVRow.Theme(topInset: Appearance.Cell.Inset.normal,
                                 bottomInset: .zero,
                                 textAlignment: .natural,
                                 titleFont: { .regular(size: 17.0) }))
    }
    
    private func notEUTagWarningRow() -> CVRow {
        CVRow(title: "europeanCertificate.fullscreen.tagNotUe.border.warning".localized,
              xibName: .textCell,
              theme: CVRow.Theme(topInset: Appearance.Cell.Inset.normal,
                                 bottomInset: .zero,
                                 textAlignment: .natural,
                                 titleFont: { .regular(size: 17.0) }))
    }
    
    private func codeHashRow() -> CVRow {
        CVRow(title: certificate.uniqueHash,
              xibName: .protectedHashCell,
              theme: CVRow.Theme(topInset: Appearance.Cell.Inset.large,
                                 bottomInset: .zero,
                                 titleFont: { .regular(size: 11.0) },
                                 titleColor: .darkGray,
                                 accessoryType: UITableViewCell.AccessoryType.none),
              selectionAction: { [weak self] _ in
            guard let hash = self?.certificate.uniqueHash else { return }
            UIPasteboard.general.string = hash
            HUD.flash(.labeledSuccess(title: "common.copied".localized, subtitle: nil), delay: 0.5)
        }, willDisplay: { view in
            guard let cell = view as? CVTableViewCell else { return }
            cell.selectionStyle = .none
        })
    }
    
    private func technicalExpirationRow(expirationDate: Date) -> CVRow {
        CVRow(title: String(format: "europeanCertificate.fullscreen.expirationDate".localized, expirationDate.dayMonthYearFormatted()),
              xibName: .protectedHashCell,
              theme: CVRow.Theme(topInset: Appearance.Cell.Inset.small,
                                 bottomInset: Appearance.Cell.Inset.small,
                                 titleFont: { .regular(size: 11.0) },
                                 titleColor: .darkGray,
                                 accessoryType: UITableViewCell.AccessoryType.none),
              selectionAction: { _ in
            UIPasteboard.general.string = expirationDate.dayMonthYearFormatted()
            HUD.flash(.labeledSuccess(title: "common.copied".localized, subtitle: nil), delay: 0.5)
        }, willDisplay: { view in
            guard let cell = view as? CVTableViewCell else { return }
            cell.selectionStyle = .none
        })
    }
    
    private func kidRow() -> CVRow {
        CVRow(title: (certificate as? EuropeanCertificate)?.kid,
              xibName: .protectedHashCell,
              theme: CVRow.Theme(topInset: 0.0,
                                 bottomInset: 0.0,
                                 titleFont: { .regular(size: 11.0) },
                                 titleColor: .darkGray,
                                 accessoryType: UITableViewCell.AccessoryType.none),
              selectionAction: { [weak self] _ in
            guard let kid = (self?.certificate as? EuropeanCertificate)?.kid else { return }
            UIPasteboard.general.string = kid
            HUD.flash(.labeledSuccess(title: "common.copied".localized, subtitle: nil), delay: 0.5)
        }, willDisplay: { view in
            guard let cell = view as? CVTableViewCell else { return }
            cell.selectionStyle = .none
        })
    }

}

extension FullscreenCertificateViewController {
    
    private func updateBrightnessForQRCodeReadability() {
        guard autoBrightnessActivated else { return }
        if lastBrightness == nil { lastBrightness = round(UIScreen.main.brightness * 100.0) / 100.0 }
        UIScreen.main.brightness = 1.0
    }
    
    private func putBrightnessBackToOriginalValue() {
        lastBrightness.map { UIScreen.main.brightness = $0 }
    }
    
    @objc private func appDidBecomeActive() {
        updateBrightnessForQRCodeReadability()
    }
    
    @objc private func appWillResignActive() {
        putBrightnessBackToOriginalValue()
    }
    
}

extension FullscreenCertificateViewController: WalletChangesObserver {
    
    func walletCertificatesDidUpdate() { }
    func walletFavoriteCertificateDidUpdate() {}
    func walletSmartStateDidUpdate() {}
    func walletBlacklistDidUpdate() {}
    
}
