// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  UrgentDgsDetailController.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 04/10/2021 - for the TousAntiCovid project.
//

import UIKit

final class UrgentDgsDetailController: CVTableViewController {
    /// Action on the button in the reminders info row
    private let didTouchBosterInfo: (_ url: URL) -> ()
    private let didTouchEligibilityInfo: (_ url: URL) -> ()
    private let didTouchCloseButton: () -> ()
    private let deinitBlock: () -> ()
    private var isFirstLoad: Bool = true
    
    init(
        didTouchBosterInfo: @escaping (_ url: URL) -> (),
        didTouchEligibilityInfo: @escaping (_ url: URL) -> (),
        didTouchCloseButton: @escaping () -> (),
        deinitBlock: @escaping () -> ()) {
            self.didTouchBosterInfo = didTouchBosterInfo
            self.didTouchEligibilityInfo = didTouchEligibilityInfo
            self.didTouchCloseButton = didTouchCloseButton
            self.deinitBlock = deinitBlock
            super.init(style: .plain)
        }
    
    required init?(coder: NSCoder) {
        fatalError("Must use the other init method")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateTitle()
        initUI()
        reloadUI()
        LocalizationsManager.shared.addObserver(self)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if isFirstLoad {
            isFirstLoad = false
            updateTableViewLayout()
        }
    }
    
    deinit {
        LocalizationsManager.shared.removeObserver(self)
        deinitBlock()
    }
    
    override func createSections() -> [CVSection] {
        makeSections {
            CVSection {
                // Video row if there is an url defined in strings
                if let videoPath = "dgsUrgentController.videoUrl".localizedOrNil, !videoPath.isEmpty {
                    videoRow(videoPath: videoPath)
                }
                // Information row with details on the reminders
                if let boosterRowTitle = "dgsUrgentController.section1.title".localizedOrNil, !boosterRowTitle.isEmpty {
                    boosterRow(title: boosterRowTitle)
                }
                if let eligibilityRowTitle = "dgsUrgentController.section2.title".localizedOrNil, !eligibilityRowTitle.isEmpty {
                    eligibilityRow(title: eligibilityRowTitle)
                }
                // Need help row with possibility to call the help center
                let phoneNumber: String = "dgsUrgentController.phone.number".localized
                if !phoneNumber.isEmpty {
                    phoneRow(phoneNumber: phoneNumber)
                }
            }
        }
    }

    private func updateTableViewLayout() {
        tableView.beginUpdates()
        tableView.endUpdates()
    }
}

// MARK: - Privates functions -
private extension UrgentDgsDetailController {
    private func updateTitle() {
        title = "dgsUrgentController.title".localized
    }
    
    func initUI() {
        tableView.backgroundColor = Appearance.Controller.cardTableViewBackgroundColor
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "common.close".localized, style: .plain, target: self, action: #selector(didTouchClose))
        navigationItem.leftBarButtonItem?.accessibilityHint = "accessibility.closeModal.zGesture".localized
    }
    
    @objc func didTouchClose() {
        didTouchCloseButton()
    }
}

// MARK: - Rows -
private extension UrgentDgsDetailController {
    func videoRow(videoPath: String) -> CVRow {
        CVRow(xibName: .videoPlayerCell,
              theme: CVRow.Theme(backgroundColor: Appearance.Cell.cardBackgroundColor,
                                 topInset: .zero,
                                 bottomInset: .zero,
                                 leftInset: .zero,
                                 rightInset: .zero),
              associatedValue: URL(string: videoPath),
              valueChanged: { [weak self] _ in
            guard self?.isFirstLoad == false else { return }
            self?.updateTableViewLayout()
        })
    }

    func boosterRow(title: String) -> CVRow {
        CVRow(title: title,
              subtitle: "dgsUrgentController.section1.desc".localized,
              buttonTitle: "dgsUrgentController.section1.labelUrl".localized,
              xibName: .paragraphCell,
              theme: CVRow.Theme(backgroundColor: Appearance.Cell.cardBackgroundColor,
                                 topInset: Appearance.Cell.Inset.normal,
                                 bottomInset: .zero,
                                 textAlignment: .natural,
                                 titleFont: { Appearance.Cell.Text.headTitleFont }),
              secondarySelectionAction: "dgsUrgentController.section1.url".localized.isEmpty ? nil : { [weak self] in
            guard let boosterInfoUrl: URL = URL(string: "dgsUrgentController.section1.url".localized) else { return }
            self?.didTouchBosterInfo(boosterInfoUrl)
        })
    }
    
    func eligibilityRow(title: String) -> CVRow {
        CVRow(title: title,
              subtitle: "dgsUrgentController.section2.desc".localized,
              buttonTitle: "dgsUrgentController.section2.labelUrl".localized,
              xibName: .paragraphCell,
              theme: CVRow.Theme(backgroundColor: Appearance.Cell.cardBackgroundColor,
                                 topInset: Appearance.Cell.Inset.normal,
                                 bottomInset: .zero,
                                 textAlignment: .natural,
                                 titleFont: { Appearance.Cell.Text.headTitleFont }),
              secondarySelectionAction: "dgsUrgentController.section2.url".localized.isEmpty ? nil : { [weak self] in
            guard let eligibilityInfoUrl: URL = URL(string: "dgsUrgentController.section2.url".localized) else { return }
            self?.didTouchEligibilityInfo(eligibilityInfoUrl)
        })
    }

    func phoneRow(phoneNumber: String) -> CVRow {
        CVRow(title: "dgsUrgentController.phone.title".localized,
              subtitle: "dgsUrgentController.phone.subtitle".localized,
              image: Asset.Images.walletPhone.image,
              xibName: .phoneCell,
              theme: CVRow.Theme(backgroundColor: Asset.Colors.secondaryButtonBackground.color,
                                 topInset: Appearance.Cell.Inset.normal,
                                 bottomInset: .zero,
                                 textAlignment: .natural,
                                 titleFont: { Appearance.Cell.Text.smallHeadTitleFont },
                                 subtitleFont: { Appearance.Cell.Text.accessoryFont }),
              selectionAction: { [weak self] _ in
            guard let self = self else { return }
            phoneNumber.callPhoneNumber(from: self)
        })
    }
}

// MARK: - LocalizationManager observer -
extension UrgentDgsDetailController: LocalizationsChangesObserver {
    func localizationsChanged() {
        updateTitle()
        reloadUI()
    }
}
