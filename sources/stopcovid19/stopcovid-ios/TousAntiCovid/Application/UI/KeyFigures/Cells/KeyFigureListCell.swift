// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  KeyFigureListCell.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 07/12/2022 - for the TousAntiCovid project.
//

import UIKit

final class KeyFigureListCell: CVTableViewCell {
    override func setup(with row: CVRow) {
        super.setup(with: row)
        accessoryType = .none
        setupAccessibility()
    }
    
    override func setupAccessibility() {
        super.setupAccessibility()
        contentView.isAccessibilityElement = true
        cvTitleLabel?.isAccessibilityElement = false
        cvSubtitleLabel?.isAccessibilityElement = false
        accessibilityTraits = currentAssociatedRow?.selectionAction != nil ? .button : .staticText
        contentView.accessibilityTraits = currentAssociatedRow?.selectionAction != nil ? .button : .staticText
        accessibilityElements = [contentView]
        contentView.accessibilityLabel = "\(cvTitleLabel?.text ?? ""). \(cvSubtitleLabel?.text ?? "")"
    }
}
