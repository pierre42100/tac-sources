// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  KeyFigureComparisonDetailController.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 20/01/2023 - for the TousAntiCovid project.
//

import UIKit
final class KeyFigureComparisonDetailController: BottomSheetedTableViewController {

    override var mode: BottomSheetedTableViewController.Mode { .fitContent }

    private var keyFigure: KeyFigure
    private let deinitBlock: (() -> ())?
    private let showMin: Bool
    private let showMax: Bool

    init(keyFigure: KeyFigure, showMin: Bool, showMax: Bool, deinitBlock: (() -> ())?) {
        self.keyFigure = keyFigure
        self.deinitBlock = deinitBlock
        self.showMin = showMin
        self.showMax = showMax
        super.init(style: .plain)
    }

    required init?(coder: NSCoder) {
        fatalError("Must use default init() method.")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        initUI()
        reloadUI()
    }

    deinit {
        deinitBlock?()
    }

    private func initUI() {
        addHeaderView(height: 10.0)
        tableView.backgroundColor = Appearance.Controller.cardTableViewBackgroundColor
        tableView.showsVerticalScrollIndicator = false
        tableView.separatorStyle = .singleLine
    }
    
    override func createSections() -> [CVSection] {
        makeSections {
            CVSection {
                if showMin, let minValue = keyFigure.minSeriesValue?.toString(shortUnit: keyFigure.shortUnit)?.formattingValueWithThousandsSeparatorIfPossible() {
                    minRow(value: minValue, dates: keyFigure.minSeries.sorted(by: { $0.date > $1.date }).map { Date(timeIntervalSince1970: $0.date).dayShortMonthYearFormatted(timeZoneIndependant: true) })
                }

                if showMax, let maxValue = keyFigure.maxSeriesValue?.toString(shortUnit: keyFigure.shortUnit)?.formattingValueWithThousandsSeparatorIfPossible() {
                    maxRow(value: maxValue, dates: keyFigure.maxSeries.sorted(by: { $0.date > $1.date }).map { Date(timeIntervalSince1970: $0.date).dayShortMonthYearFormatted(timeZoneIndependant: true) })
                }
            }
        }
    }

}

// MARK: - Rows -
private extension KeyFigureComparisonDetailController {

    func minRow(value: String, dates: [String]) -> CVRow {
        row(title: String(format: "keyFigureDetailController.section.comparison.min.longLabel".localized, keyFigure.shortLabel), subtitle: String(format: "keyFigureDetailController.section.comparison.dateOccurrences.description".localized, value, String(dates.count)), dates: dates)
    }

    func maxRow(value: String, dates: [String]) -> CVRow {
        row(title: String(format: "keyFigureDetailController.section.comparison.max.longLabel".localized, keyFigure.shortLabel),
            subtitle: String(format: "keyFigureDetailController.section.comparison.dateOccurrences.description".localized, value, String(dates.count)),
            dates: dates)
    }

    func row(title: String, subtitle: String, dates: [String]) -> CVRow {
        CVRow(title: title,
              subtitle: subtitle,
              accessoryText: dates.joined(separator: "\n"),
              isOn: KeyFiguresManager.shared.isFavorite(keyFigure),
              xibName: .comparisonDetailCell,
              theme: CVRow.Theme(backgroundColor: Appearance.Cell.cardBackgroundColor,
                                 topInset: Appearance.Cell.Inset.small,
                                 bottomInset: .zero,
                                 textAlignment: .natural,
                                 titleFont: { Appearance.Cell.Text.headTitleFont },
                                 accessoryTextFont: { Appearance.Cell.Text.subtitleFont })

        )
    }

}
