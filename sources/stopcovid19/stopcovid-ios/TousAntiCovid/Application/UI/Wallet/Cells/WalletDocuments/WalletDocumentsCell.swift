// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  WalletDocumentsCell.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 11/05/2021 - for the TousAntiCovid project.
//

import UIKit

final class WalletDocumentsCell: CardCell {
    
    @IBOutlet private var leftImageView: UIImageView!
    @IBOutlet private var leftLabel: UILabel!

    override func setup(with row: CVRow) {
        super.setup(with: row)
        setupUI(with: row)
        setupContent(with: row)
        setupAccessibility()
    }

    private func setupUI(with row: CVRow) {
        cvSubtitleLabel?.font = row.theme.subtitleFont()
        cvSubtitleLabel?.textColor = row.theme.subtitleColor
        cvSubtitleLabel?.textAlignment = row.theme.textAlignment
        cvSubtitleLabel?.adjustsFontForContentSizeCategory = true
        leftLabel.font = row.theme.subtitleFont()

        guard !UIColor.isDarkMode else { return }
        leftImageView.layer.borderWidth = 1.0
        leftImageView.layer.borderColor = UIColor.black.withAlphaComponent(0.1).cgColor
    }
    
    private func setupContent(with row: CVRow) {
        leftImageView.image = row.image
        leftLabel.text = row.accessoryText
    }

    override func setupAccessibility() {
        super.setupAccessibility()
        leftImageView.accessibilityLabel = leftLabel?.text?.removingEmojis()
        leftImageView.accessibilityTraits = .image
        leftImageView.isAccessibilityElement = true
        leftLabel.isAccessibilityElement = false
        accessibilityElements = [containerView, leftImageView, leftLabel].compactMap { $0 }
    }
    
    @IBAction private func leftDocumentButtonDidPress() {
        currentAssociatedRow?.secondarySelectionAction?()
    }
    
}
