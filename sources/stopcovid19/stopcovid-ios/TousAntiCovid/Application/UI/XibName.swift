// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  XibName.swift
//  TousAntiCovid
//
//  Created by Lunabee Studio / Date - 08/04/2020 - for the TousAntiCovid project.
//

import Foundation

enum XibName {

    enum Section: String {
        case standardSectionHeader = "StandardSectionHeader"
        case standardCollectionSectionHeader = "StandardCollectionSectionHeader"
        case actionSectionHeader = "HeaderWithActionView"
        case actionCollectionSectionHeader = "HeaderCollectionWithActionView"
    }
    
    enum Row: String {
        // MARK:: - Onboarding cells -
        case onboardingImageCell = "OnboardingImageCell"
        case onboardingGovernmentCell = "OnboardingGovernmentCell"
        case onboardingWorkingStepCell = "OnboardingWorkingStepCell"
        case onboardingGestureCell = "OnboardingGestureCell"

        // MARK: - Other cells -
        case standardCell = "StandardCell"
        case textCell = "TextCell"
        case selectableCell = "SelectableCell"
        case textFieldCell = "TextFieldCell"
        case titleCell = "TitleCell"
        case buttonCell = "ButtonCell"
        case switchCell = "SwitchCell"
        case appVersionCell = "AppVersionCell"
        case centeredImageTextCell = "CenteredImageTextCell"
        case callCell = "CallCell"
        case emptyCell = "EmptyCell"
        case standardTextFieldCell = "StandardTextFieldCell"
        case infoCell = "InfoCell"
        case linkButtonCell = "LinkButtonCell"
        case declareCell = "DeclareCell"
        case cardCell = "CardCell"
        case paragraphCell = "ParagraphCell"
        case imageCell = "ImageCell"
        case keyFigureCell = "KeyFigureCell"
        case standardCardCell = "StandardCardCell"
        case standardCardHorizontalCell = "StandardCardHorizontalCell"
        case standardSwitchCell = "StandardSwitchCell"
        case qrCodeCell = "QRCodeCell"
        case dateCell = "DateCell"
        case vaccinationCell = "VaccinationCell"
        case keyFigureChartCell = "KeyFigureChartCell"
        case loadingCardCell = "LoadingCardCell"
        case centeredImageCardCell = "CenteredImageCardCell"
        case sanitaryCertificateCell = "SanitaryCertificateCell"
        case sanitaryCertificatesWalletCell = "SanitaryCertificatesWalletCell"
        case updateAppCell = "UpdateAppCell"
        case statusVerificationCell = "StatusVerificationCell"
        case walletDocumentsCell = "WalletDocumentsCell"
        case cardWithButtonCell = "CardWithButtonCell"
        case phoneCell = "PhoneCell"
        case segmentedCell = "SegmentedCell"
        case walletAddCertificateCell = "WalletAddCertificateCell"
        case checkDocumentCell = "CheckDocumentCell"
        case zoomableImageCell = "ZoomableImageCell"
        case favoriteCertificateCell = "FavoriteCertificateCell"
        case videoPlayerCell = "VideoPlayerCell"
        case homeNotificationCell = "HomeNotificationCell"
        case protectedQrCodeCell = "ProtectedQrCodeCell"
        case protectedHashCell = "ProtectedHashCell"
        case animatedHeaderCell = "AnimatedHeaderCell"
        case standardCardSwitchCell = "StandardCardSwitchCell"
        case keyFigureSelectionCell = "KeyFigureSelectionCell"
        case urgentDgsCell = "UrgentDgsCell"
        case collectionTableViewCell = "CVCollectionTableViewCell"
        case additionalInfoCell = "AdditionalInfoCell"
        case certificateSelectionCell = "CertificateSelectionCell"
        case headerCell = "HeaderCell"
        case classicButtonForHomeCell = "ClassicButtonForHomeCell"
        case homeStandardCardCell = "HomeStandardCardCell"
        case homeStandardCardHorizontalCell = "HomeStandardCardHorizontalCell"
        case homeKeyFigureChartTableCell = "HomeKeyFigureChartTableCell"
        case homeKeyFigureChartCollectionCell = "HomeKeyFigureChartCollectionCell"
        case standardCardWithImageCell = "StandardCardWithImageCell"
        case stateImageCell = "StateImageCell"
        case keyFigureListCell = "KeyFigureListCell"
        case seeAllCardCell = "SeeAllCardCell"
        case keyFiguresMapCell = "KeyFiguresMapCell"
        case keyFigureMapCell = "KeyFigureMapCell"
        case featuredInfoViewCell = "FeaturedInfoViewCell"
        case usefulLinksCell = "UsefulLinksCell"
        case keyFigureComparisonCell = "KeyFigureComparisonCell"
        case comparisonDetailCell = "ComparisonDetailCell"
        case vaccinationCenterCell = "VaccinationCenterCell"
        
        // MARK: - CollectionViewCell
        case keyFigureCollectionViewCell = "KeyFigureCollectionViewCell"
        case newsCollectionViewCell = "NewsCollectionViewCell"
        case featuredInfoCollectionViewCell = "FeaturedInfoCollectionViewCell"

    }
}
