#!/bin/sh
DEST="/actions"
REPO="https://gitlab.com/pierre42100/tac-sources"
REPO_SSH="git@gitlab.com:pierre42100/tac-sources.git"
KEY_PATH="/root/priv.pem"

# Remove any previous file
rm -rf "$DEST"

echo "Clone sources"
git clone "$REPO" "$DEST"

echo "Update sources"
cd "$DEST"
/bin/sh update_sources.sh

echo "Auto-commit repository"
git config user.name "TAC clone"
git config user.email "tac-clone@communiquons.org"
git add .
git commit -m "Update of $(date)"

echo "Move private key"
echo "$SSH_PUSH_KEY" >  "$KEY_PATH"
chmod 600 "$KEY_PATH"

echo "Auto-push repository"
git remote set-url origin "$REPO_SSH"
GIT_SSH_COMMAND="ssh -i $KEY_PATH -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no" git push -u origin master

