# Remove what was previously here
rm -rf sources
rm repos_list




# Get the list of repositories to get
get_repos_list() {
	echo "Processing repos in $1"

	wget -O - "$1" | sed 's/{"id":/\n/g' | sed 's/"relative_path":"/BAAAAD\n/g' | grep -v BAAAAD | cut -d'"' -f 1 | grep -v "\[" >> repos_list
}

process_slug() {
	SLUG="$1"
	echo Processing "$SLUG"

	get_repos_list "https://gitlab.inria.fr/groups/$SLUG/-/children.json"
	get_repos_list "https://gitlab.inria.fr/groups/$SLUG/-/children.json?archived=only"

}

process_slug "stopcovid19"
process_slug "tousanticovid-verif"


echo Download repositories
for repo in $(cat repos_list);
do
	URL="https://gitlab.inria.fr$repo"
	DEST="sources$repo"
	echo "Download $URL into $DEST"
	git clone "$URL" "$DEST"
	
	echo "Remove GIT repository of project"
	rm -rf "$DEST/.git"
done
